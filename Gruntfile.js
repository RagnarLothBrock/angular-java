module.exports = function (grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),


        clean: ["dist", '.tmp'],

        copy: {
            main: {
                expand: true,
                cwd: '../rl_angular/',
                src: ['**', 'js/**', '!lib/**', '!**/*.css', 'css/**', 'fonts/**', 'img/**', 'partials/**','!node_modules/**'],
                dest: 'dist/'
            },
            shims: {
                expand: true,
                cwd: '../rl_angular/lib/webshim/shims',
                src: ['**'],
                dest: 'dist/js/shims'
            }
        },

        rev: {
            files: {
                src: ['dist/**/*.{js,css}', 'dist/**/**/*.{js,css}', '!dist/js/shims/**', '.tmp/**/*.{js,css}', '.tmp/**/**/*.{js,css}']
            }
        },


        filename_hash: {
            options: {
                files: ['dist/**/*.{js,css}', 'dist/**/**/*.{js,css}', '!dist/js/shims/**'],
                index: ['rl_angular/index.html'],
                dest: {
                    files: 'dist',
                    index: 'dist'
                },
                filename: function (name, hash, extension) {
                    return `${hash}${extension}`;
                }
            },
        },
        useminPrepare: {
            html: 'dist/index.html'
        },

        usemin: {
            html: ['dist/index.html']
        },
        // concat: {
        //     options: {
        //         stripBanners: true,
        //         banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
        //         '<%= grunt.template.today("yyyy-mm-dd") %> */',
        //         separator: ';'
        //     },
        //     dist: {
        //         src: ['../rl_angular/js/modules/inventory/inventory.js','../rl_angular/js/modules/inventory/packaging_material.js'],
        //         dest: 'dist/js/modules/aa.js',
        //     },
        // },


        uglify: {
            options: {
                compress: false,
                mangle: false,
                beautify: true
            }
        },
        removelogging: {
            dist: {
                src: ['dist/js/*.{js,css}']            }
        },
        ngAnnotate: {
            dist: {
                files: [{
                    expand: true,
                    src: ['**/*.js', '!**/*.annotated.js', '!bower_components/**', '!dist/**', '!node_modules/**'],
                    ext: '.annotated.js',
                    extDot: 'last'
                }],
            }
        },

        cdn_switch: {
            'myDevTarget': {
                files: {
                    'templates/rev_index.html': ['templates/index.html']
                },
                options: {
                    use_local: false,
                    blocks: {
                        javascript: {
                            local_path: './js/libs/',
                            html: '<script src="{{resource}}"></script>',
                            resources: [
                                'http://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.15/angular.min.js',
                                'http://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-animate.js',
                                'http://cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.13/angular-ui-router.min.js',
                                'http://code.jquery.comcjquery-latest.js'
                            ],
                        },
                    }
                }
            },
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-filename-hash');
    grunt.loadNpmTasks('grunt-rev');
    grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-cdn-switch');
    grunt.loadNpmTasks('grunt-ng-annotate');
    grunt.loadNpmTasks("grunt-remove-logging");

    // Tell Grunt what to do when we type "grunt" into the terminal
    grunt.registerTask('default', [//'cdn_switch'
        'clean', 'copy',
        'useminPrepare',
        // 'ngAnnotate',
        'concat', 'cssmin', 'rev', 'usemin','removelogging'
    ]);
};