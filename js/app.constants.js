'use strict'

/** Define all constants used in the applications **/
//rlAMPMDetailsMaps

// app.constant('REST_API_URL',host_base_url+"server/");
 app.constant('REST_API_URL', "http://54.69.180.84/rlogistics/server/");
// app.constant('REST_API_URL', "http://52.88.108.9:9080/rlogistics-execution/rlservice/")
//app.constant('REST_API_URL',"http://test.bizlog.in/server/");
//app.constant('REST_API_URL',"http://localhost:8080/rlogistics-execution/rlservice/");
//app.constant('REST_API_URL',"http://rl.bizlog.in/server/");
app.constant('LIST_PAGE_SIZE', 10);
app.constant('MAX_DROPDOWN_SIZE', 1000);

app.constant('API', {
    "LOGIN": "auth/login",
    "LOGOUT": "userservice/logout",
    "HAS_ACCESS": "userservice/has_access",
    "POPULATE_MENU": "userservice/menu",
    "EXCEL_URL": "/server/attachment/download/search/abc/bca",

    "PROCESS": {
        "LIST": "process/list-deployed-processes",
        "FIRSTGROUP": "process/<deploymentId>/command/firstgroup",
        "NEXTGROUP": "process/<deploymentId>/<fillSessionId>/command/nextgroup",
        "START_PROCESS": "process/<deploymentId>/<fillSessionId>/command/start-process",
        "NEXT_TASK": "process/<processId>/get-my-task",
        "UPLOAD": "process/upload",
        "UPLOAD_EXCEL": "process/upload/excel"
    },
    "REPORT": {
        "GET_REPORT": "get/report",
        "GET_CALL_LOG": "get/exotel/details/list",
        "GET_DEVICE_INFO":"/get/device-info"
    },

    "AMPM": {
        "TRACK_PRODUCT": "ampm/track-product/<conNo>"
    },

    "FE": {
        "TRACK_EACH_FE": "sort/fe/list",
        "TRACK_FE": "/list/all/fe",
        "START_FE_TRACK": "fe/start-tracking",
        "GET_ALL_FE": "fe/tickets",
        "CALL_FE": "call/fe",
        "FE_DETAILS": "/get/fe/distance/1/100",
        "MAP_DETAILS": "get/fe/location"
    },
    "TASKS": {
        "INVOLVED": "tasks/involved/<first>/<limit>",
        "ASSIGNED": "tasks/assigned/<first>/<limit>",
        "MY_COMPLETED": "tasks/my-completed-instances/<first>/<limit>",
        "FIND_BY_VARIABLE_VALUES": "tasks/find-by-variable-values/<first>/<limit>",
        "FIND_COMPLETED_BY_VARIABLE_VALUES": "tasks/completed-tickets/<first>/<limit>",
        "GENERATE_EXCEL_FOR_COMPLETED_TICKETS": "/tasks/completed-tickets/generate-excel",
        "UPDATING_FE_TICKET_STATUS": "/task/update/fe-ticket-status",
        "MANAGER_UNCLAIMED": "tasks/manager/unclaimed/<first>/<limit>",
        "MANAGER_ASSIGNED": "tasks/manager/assigned/<first>/<limit>",
        "MANAGER_COMPLETED": "tasks/manager/completed/<first>/<limit>",
        "GENERATE_SEARCH_EXCEL": "/tasks/generate-excel/find-by-variable-values",
        "GET_PROCESS_STATUS": "rlprocessstatus/query",
        "GET_FINANCE_DETAILS": "/search/finance/details",
        "GET_SINGLE_FINANCE_DETAILS": "/search/single/finance/details",
        "PAY_TO_CUSTOMER": "/pay-to/customer",
        "PAYMENT_STATUS": "/fund/enquiry",
        "CALL_TO_CUSTOMER": "/call/customer",
        "CALL_CUSTOMER": "/call/customer/<processId>",
        "GET_ASSIGNEE_NUMBER": "/get/assignee/number"
    },

    "TASK": {
        "CLAIM": "task/<taskid>/command/claim",
        "FIRSTGROUP": "task/<taskid>/command/firstgroup",
        "NEXTGROUP": "task/<taskid>/<fillSessionId>/command/nextgroup",
        "COMPLETE": "task/<taskid>/<fillSessionId>/command/complete",
        "FIND_BY_PROCESS_ID": "tasks/find-by-process-id/<processId>",
        "TASK_HISTORY":"ticket/get/history",
        "DELETE_TASK": "tasks/delete/<processId>",
        "DOWNLOAD_RETAILER_TICKETS": "/tasks/write-to-excel",
        "BULK_PICKUP_TICKETS": "/bulk-pickup/tickets",
        "GENERATE_DEC": "/declaration-form/generate",
        "UPDATE_COST": "/update-product/cost",
        "UPDATE_DAMAGE_COST": "/set-damage/cost",
        "GETIMAGE": "/get/photos",
        "GET_LOCATION": "tasks/find-location/<processId>/<key3>/<key4>",
        "PICKDROPLIST": "retailerreturnorpickuplocations/query",
    },

    "USER": {
        "LIST": "processuser/query",
        "CREATE": "processuser/create",
        "GET": "processuser/get/<userId>",
        "GET_LOCATION_PINCODE": "processlocation/query",
        "GET_PINCODES": "coordinatorservicablepincodes/query",
        "GET_COORDINATORS": "coordinatorservicablepincodes/query",
        "SAVE_PINCODES": "coordinatorservicablepincodes/bulkcreate",
        "SAVE_SINGLE_PINCODE": "coordinatorservicablepincodes/create",
        "DELETE_PINCODE": "coordinatorservicablepincodes/delete/<id>",
        "UPDATE": "processuser/update",
        "DELETE": "processuser/delete/<userId>",
        "SAVE": "processuser/save",
        "GENERATE_RESET_PASSWORD_TOKEN": "processuser/generate-reset-token/<email>",
        "RESET_PASSWORD": "processuser/reset-password",
        "CHANGE_MY_PASSWORD": "processuser/change-password"
    },

    "ROLE_MANAGEMENT": {
        "ROLE": {
            "LIST": "rolemaster/query",
            "CREATE": "rolemaster/create",
            "GET": "rolemaster/get/<roleId>",
            "UPDATE": "rolemaster/update",
            "DELETE": "rolemaster/delete/<roleId>"
        },
        "MENU": {
            "LIST": "menu/query",
            "CREATE": "menu/create",
            "GET": "menu/get/<menuId>",
            "UPDATE": "menu/update",
            "DELETE": "menu/delete/<menuId>"
        },
        "FEATURE": {
            "LIST": "feature/listing/<first>/<limit>",
            "CREATE": "feature/create",
            "GET": "feature/get/<featureId>",
            "UPDATE": "feature/update",
            "DELETE": "feature/delete/<featureId>"
        },
        "ROLE_ACCESS": {
            "BULK_CREATE": "roleaccess/bulkcreate",
            "BULK_DELETE": "user/delete/role-based-access/<roleCode>",
            "LIST": "super/list/role-based-access/<roleCode>"
        }
    },

    "MASTERS": {
        "BRAND": {
            "LIST": "brand/query",
            "CREATE": "brand/create",
            "GET": "brand/get/<brandid>",
            "UPDATE": "brand/update",
            "DELETE": "brand/delete/<brandid>",
            "BY_SERVICE_PROVIDER_LOCATION": "services/filter-by-brand/<serviceProviderLocationId>",
            "UPLOAD": "brand/upload/excel"
        },
        "PACKING_SPECIFICATION": {
            "LIST": "get/packaging-specification/lists/<firstResult>/<maxResult>",
            "CREATE": "packingspecification/create",
            "GET": "packingspecification/get/<packingspecificationid>",
            "UPDATE": "packingspecification/update",
            "DELETE": "packingspecification/delete/<packingspecificationid>",
        },
        "LSP_DETAIL": {
            "LIST": "get/lsp-detail/lists/<firstResult>/<maxResult>",
            "CREATE": "lspdetail/create",
            "GET": "lspdetail/get/<lspdetailid>",
            "UPDATE": "lspdetail/update",
            "DELETE": "lspdetail/delete/<lspdetailid>",
        },
        "SERVICES": {
            "LIST": "servicemaster/query",
            "CREATE": "servicemaster/create",
            "GET": "servicemaster/get/<servicemasterid>",
            "UPDATE": "servicemaster/update",
            "DELETE": "servicemaster/delete/<servicemasterid>",
            "BY_RETAILER": "services/byretailer/<retailerid>",
            "FOR_RETAILER": "services/forRetailer/<retailerid>"
        },
        "CATEGORY": {
            "LIST": "mastercategory/query",
            "CREATE": "mastercategory/create",
            "GET": "mastercategory/get/<mastercategoryid>",
            "UPDATE": "mastercategory/update",
            "DELETE": "mastercategory/delete/<mastercategoryid>"
        },
        "PRODUCT_CATEGORY": {
            "LIST": "productcategory/query",
            "CREATE": "productcategory/create",
            "GET": "productcategory/get/<productcategoryid>",
            "UPDATE": "productcategory/update",
            "DELETE": "productcategory/delete/<productcategoryid>",
            "BY_RETAILER": "productcategories/byretailer/<retailerid>",
            "BY_SERVICE_PROVIDER_LOCATION": "services/filter-by-category/<serviceProviderLocationId>",
            "GET_ALL": "product/category/list/<firstResult>/<maxResults>",
            "UPLOAD": "product-category/upload/excel"
        },
        "EWASTE_PRODUCT_CATEGORY": {
            "LIST": "ewasteproductcategory/query",
            "CREATE": "ewasteproductcategory/create",
            "GET": "ewasteproductcategory/get/<productCategoryid>",
            "UPDATE": "ewasteproductcategory/update",
            "DELETE": "ewasteproductcategory/delete/<productCategoryid>",
            "GET_ALL": "product/category/list/<firstResult>/<maxResults>",
        },
        "PRODUCT_SUB_CATEGORY": {
            "LIST": "productsubcategory/query",
            "CREATE": "productsubcategory/create",
            "GET": "productsubcategory/get/<productsubcategoryid>",
            "UPDATE": "productsubcategory/update",
            "DELETE": "productsubcategory/delete/<productsubcategoryid>",
            "BY_SERVICE_PROVIDER_LOCATION": "services/filter-by-sub-category/<serviceProviderLocationId>/<categoryId>",
            "GET_ALL": "product/sub-category/list/<firstResult>/<maxResults>"
        },
        "PACKAGING_TYPE": {
            "LIST": "packagingtype/query",
            "CREATE": "packagingtype/create",
            "GET": "packagingtype/get/<packagingtypeid>",
            "UPDATE": "packagingtype/update",
            "DELETE": "packagingtype/delete/<packagingtypeid>"
        },
        "CITY": {
            "LIST": "city/query",
            "CREATE": "city/create",
            "GET": "city/get/<cityId>",
            "UPDATE": "city/update",
            "DELETE": "city/delete/<cityId>"
        },
        "LOCAL_PACKING": {
            "LIST": "localpacking/list",
            "CREATE": "localpacking/create",
            "GET": "localpacking/get/<cityId>",
            "UPDATE": "localpacking/update",
            "DELETE": "localpacking/delete/<cityId>"
        },
        "OUTSTATION_PACKING": {
            "LIST": "outstationpacking/list",
            "CREATE": "outstationpacking/create",
            "GET": "outstationpacking/get/<cityId>",
            "UPDATE": "outstationpacking/update",
            "DELETE": "outstationpacking/delete/<cityId>"
        },
        "LOCATION": {
            "LIST": "processlocation/query",
            "CREATE": "processlocation/create",
            "GET": "processlocation/get/<locationId>",
            "UPDATE": "processlocation/update",
            "DELETE": "processlocation/delete/<locationId>",

            "PINCODES": {
                "LIST": "locationpincodes/query",
                "CREATE": "locationpincodes/create",
                "GET": "locationpincodes/get/<locationPincodeId>",
                "UPDATE": "locationpincodes/update",
                "DELETE": "locationpincodes/delete/<locationPincodeId>",
                "BULK_CREATE": "locationpincodes/bulkcreate",
                "BULK_UPDATE": "locationpincodes/bulkupdate",
                "BULK_DELETE": "locationpincodes/bulkdelete",
            }
        },

        "FAQ": {
            "LIST": "faq/query",
            "CREATE": "faq/create",
            "GET": "faq/get/<faqId>",
            "UPDATE": "faq/update",
            "DELETE": "faq/delete/<faqId>",
            "LISTING": "faq/listing/<firstResult>/<maxResults>"
        },

        "SMS_EMAIL_TRIGGER": {
            "LIST": "smsemailtrigger/query",
            "CREATE": "smsemailtrigger/create",
            "GET": "smsemailtrigger/get/<smsEmailTriggerId>",
            "UPDATE": "smsemailtrigger/update",
            "DELETE": "smsemailtrigger/delete/<smsEmailTriggerId>"
        },

        "SMS_EMAIL_TEMPLATE": {
            "LIST": "smsemailtemplate/query",
            "CREATE": "smsemailtemplate/create",
            "GET": "smsemailtemplate/get/<smsEmailTemplateId>",
            "UPDATE": "smsemailtemplate/update",
            "DELETE": "smsemailtemplate/delete/<smsEmailTemplateId>",
            "COPY_MASTER_TEMPLATES": "copy/master-templates/<retailerId>"
        },

        "SMS_EMAIL_TRIGGER_CONFIG": {
            "LIST": "smsemailtriggerconfig/query",
            "CREATE": "smsemailtriggerconfig/create",
            "GET": "smsemailtriggerconfig/get/<smsEmailTriggerConfigId>",
            "UPDATE": "smsemailtriggerconfig/update",
            "DELETE": "smsemailtriggerconfig/delete/<smsEmailTriggerConfigId>",
            "CONFIG_LIST_BY_SERVICE": "get/sms-email-configuration",
            "BULK_CREATE": "smsemailtriggerconfig/bulkcreate",
            "BULK_UPDATE": "smsemailtriggerconfig/bulkupdate",
        },
        "COSTING_ACTIVITY": {
            "LIST": "costingactivity/query",
            "CREATE": "costingactivity/create",
            "GET": "costingactivity/get/<id>",
            "UPDATE": "costingactivity/update",
            "DELETE": "costingactivity/delete/<id>"
        },
        "COSTING_ACTIVITY_MAPPING": {
            "LIST": "get/costing-activity/mapping/<first>/<limit>",
            "CREATE": "costingactivitymapping/create",
            "GET": "costingactivitymapping/get/<id>",
            "UPDATE": "costingactivitymapping/update",
            "DELETE": "costingactivitymapping/delete/<id>",
            "UPLOAD": "costing-mapping/upload/excel"
        },
        "ADDITIONAL_COSTING": {
            "LIST": "additionalcosting/query",
            "CREATE": "additionalcosting/create",
            "GET": "additionalcosting/get/<id>",
            "UPDATE": "additionalcosting/update",
            "DELETE": "additionalcosting/delete/<id>"
        },
    },
    "TICKET_IMAGES":{
        "LIST":"ticketimages/query"
    },

    "RETAILER": {
        "LIST": "retailer/query",
        "CREATE": "retailer/create",
        "GET": "retailer/get/<retailerid>",
        "UPDATE": "retailer/update",
        "DELETE": "retailer/delete/<retailerid>",
        "ADD": "retailer/add",

        "PRODUCT": {
            //"LIST": "product/list/<first>/<limit>",
            "LIST": "product/list/<retailerId>/<first>/<limit>",
            "CREATE": "product/create",
            "GET": "product/get/<productid>",
            "UPDATE": "product/update",
            "DELETE": "product/delete/<productid>",
            "UPLOAD": "product/upload/excel"
        },

        "RETAILER_SERVICES": {
            "LIST": "retailerservices/query",
            "CREATE": "retailerservices/create",
            "GET": "retailerservices/get/<retailerserviceid>",
            "UPDATE": "retailerservices/update",
            "DELETE": "retailerservices/delete/<retailerserviceid>",
            "BULK_CREATE": "retailerservices/bulkcreate",
            "BULK_UPDATE": "retailerservices/bulkupdate",
            "BULK_DELETE": "retailerservices/bulkdelete",
            "UPLOADAUTH": "/attachment/upload",
            "CUSTOM_QUERY": {
                "SERVICE": "retailerservices/customqueries/servicequery/<retailerid>"
            }
        },


        "RETAILER_CATEGORY": {
            "LIST": "retailercategories/query",
            "CREATE": "retailercategories/create",
            "GET": "retailercategories/get/<retailercategoryid>",
            "UPDATE": "retailercategories/update",
            "DELETE": "retailercategories/delete/<retailercategoryid>",
            "BULK_CREATE": "retailercategories/bulkcreate",
            "BULK_UPDATE": "retailercategories/bulkupdate",
            "BULK_DELETE": "retailercategories/bulkdelete",
            "CUSTOM_QUERY": {
                "CATEGORY": "retailercategories/customqueries/categoryquery/<retailerid>"
            }
        },

        "RETAILER_SERVICES_COST": {
            "LIST": "retailerservicescost/query",
            "CREATE": "retailerservicescost/create",
            "GET": "retailerservicescost/get/<retailerservicecostid>",
            "UPDATE": "retailerservicescost/update",
            "DELETE": "retailerservicescost/delete/<retailerservicecostid>",
            "BULK_CREATE": "retailerservicescost/bulkcreate",
            "BULK_UPDATE": "retailerservicescost/bulkupdate",
            "BULK_DELETE": "retailerservicescost/bulkdelete"
        },

        "RETAILER_COSTING_ACTIVITY": {
            "LIST": "get/retailer/costing-activity/mapping/<retailerId>/<first>/<limit>",
            "CREATE": "retailercostingactivity/create",
            "GET": "retailercostingactivity/get/<retailerCostingActivity>",
            "UPDATE": "retailercostingactivity/update",
            "DELETE": "retailercostingactivity/delete/<retailerCostingActivity>",
            "BULK_CREATE": "retailercostingactivity/bulkcreate",
            "BULK_UPDATE": "retailercostingactivity/bulkupdate",
            "BULK_DELETE": "retailercostingactivity/bulkdelete",
            "UPLOAD": "retailer-costing/upload/excel"
        },

        "RETAILER_SERVICEABLE_LOCATIONS": {
            "LIST": "retailerserviceablelocations/query",
            "CREATE": "retailerserviceablelocations/create",
            "GET": "retailerserviceablelocations/get/<retailerserviceablelocationid>",
            "UPDATE": "retailerserviceablelocations/update",
            "DELETE": "retailerserviceablelocations/delete/<retailerserviceablelocationid>",
            "BULK_CREATE": "retailerserviceablelocations/bulkcreate",
            "BULK_UPDATE": "retailerserviceablelocations/bulkupdate",
            "BULK_DELETE": "retailerserviceablelocations/bulkdelete",
        },
        "RETAILER_ODA_LOCATIONS": {
            "LIST": "retailerodalocations/query",
            "CREATE": "retailerodalocations/create",
            "GET": "retailerodalocations/get/<retailerserviceablelocationid>",
            "UPDATE": "retailerodalocations/update",
            "DELETE": "retailerodalocations/delete/<retailerserviceablelocationid>",
            "BULK_CREATE": "retailerodalocations/bulkcreate",
            "BULK_UPDATE": "retailerodalocations/bulkupdate",
            "BULK_DELETE": "retailerodalocations/bulkdelete",
        },
        "RETAILER_PACKING_COST": {
            "LIST": "retailerpackingcost/query",
            "CREATE": "retailerpackingcost/create",
            "GET": "retailerpackingcost/get/<retailerpackingcostid>",
            "UPDATE": "retailerpackingcost/update",
            "DELETE": "retailerpackingcost/delete/<retailerpackingcostid>",
            "BULK_CREATE": "retailerpackingcost/bulkcreate",
            "BULK_UPDATE": "retailerpackingcost/bulkupdate",
            "BULK_DELETE": "retailerpackingcost/bulkdelete"
        },
        "RETAILER_PACKING_SPECIFICATION": {
            "LIST": "retailerpackingspecification/query",
            "CREATE": "retailerpackingspecification/create",
            "GET": "retailerpackingspecification/get/<retailerpackingcostid>",
            "UPDATE": "retailerpackingspecification/update",
            "DELETE": "retailerpackingspecification/delete/<retailerpackingcostid>",
            "BULK_CREATE": "retailerpackingspecification/bulkcreate",
            "BULK_UPDATE": "retailerpackingspecification/bulkupdate",
            "BULK_DELETE": "retailerpackingspecification/bulkdelete"
        },
        "RETAILER_PROCESS_FIELD": {
            "LIST": "retailerprocessfield/query",
            "CREATE": "retailerprocessfield/create",
            "GET": "retailerprocessfield/get/<retailerpackingcostid>",
            "UPDATE": "retailerprocessfield/update",
            "DELETE": "retailerprocessfield/delete/<retailerpackingcostid>",
            "BULK_CREATE": "retailerprocessfield/bulkcreate",
            "BULK_UPDATE": "retailerprocessfield/bulkupdate",
            "BULK_DELETE": "retailerprocessfield/bulkdelete"
        },
        "RETAILER_PACKING_COST_BORNE": {
            "LIST": "retailerpayspackingcost/query",
            "CREATE": "retailerpayspackingcost/create",
            "GET": "retailerpayspackingcost/get/<retailerpackingcostborneid>",
            "UPDATE": "retailerpayspackingcost/update",
            "DELETE": "retailerpayspackingcost/delete/<retailerpackingcostborneid>",
            "BULK_CREATE": "retailerpayspackingcost/bulkcreate",
            "BULK_UPDATE": "retailerpayspackingcost/bulkupdate",
            "BULK_DELETE": "retailerpayspackingcost/bulkdelete"
        },
        "RETAILER_ADVANCE_REPLACEMENT_PRODUCTS": {
            "LIST": "retaileradvreplacementproducts/query",
            "CREATE": "retaileradvreplacementproducts/create",
            "GET": "retaileradvreplacementproducts/get/<advReplacementId>",
            "UPDATE": "retaileradvreplacementproducts/update",
            "DELETE": "retaileradvreplacementproducts/delete/<advReplacementId>",
            "BULK_CREATE": "retaileradvreplacementproducts/bulkcreate",
            "BULK_UPDATE": "retaileradvreplacementproducts/bulkupdate",
            "BULK_DELETE": "retaileradvreplacementproducts/bulkdelete"
        },
        "RETAILER_EXCHANGE_POLICY": {
            "LIST": "retailerexchangepolicy/query",
            "CREATE": "retailerexchangepolicy/create",
            "GET": "retailerexchangepolicy/get/<exchangePolicyId>",
            "UPDATE": "retailerexchangepolicy/update",
            "DELETE": "retailerexchangepolicy/delete/<exchangePolicyId>",
            "BULK_CREATE": "retailerexchangepolicy/bulkcreate",
            "BULK_UPDATE": "retailerexchangepolicy/bulkupdate",
            "BULK_DELETE": "retailerexchangepolicy/bulkdelete"
        },
        "RETAILER_RETURN_OR_PICKUP_LOCATIONS": {
            "LIST": "retailerreturnorpickuplocations/query",
            "CREATE": "retailerreturnorpickuplocations/create",
            "GET": "retailerreturnorpickuplocations/get/<RetailerReturnOrPickupLocationsId>",
            "UPDATE": "retailerreturnorpickuplocations/update",
            "DELETE": "retailerreturnorpickuplocations/delete/<RetailerReturnOrPickupLocationsId>",
            "BULK_CREATE": "retailerreturnorpickuplocations/bulkcreate",
            "BULK_UPDATE": "retailerreturnorpickuplocations/bulkupdate",
            "BULK_DELETE": "retailerreturnorpickuplocations/bulkdelete"
        },
        "RETAILER_SELLER_CATEGORIES": {
            "LIST": "retailersellercategories/query",
            "CREATE": "retailersellercategories/create",
            "GET": "retailersellercategories/get/<RetailerSellerCategoriesId>",
            "UPDATE": "retailersellercategories/update",
            "DELETE": "retailersellercategories/delete/<RetailerSellerCategoriesId>",
            "BULK_CREATE": "retailersellercategories/bulkcreate",
            "BULK_UPDATE": "retailersellercategories/bulkupdate",
            "BULK_DELETE": "retailersellercategories/bulkdelete"
        },
        "RETAILER_SERVICE_PROVIDER": {
            "LIST": "retailerserviceprovider/query",
            "CREATE": "retailerserviceprovider/create",
            "GET": "retailerserviceprovider/get/<RetailerServiceProviderId>",
            "UPDATE": "retailerserviceprovider/update",
            "DELETE": "retailerserviceprovider/delete/<RetailerServiceProviderId>",
            "BULK_CREATE": "retailerserviceprovider/bulkcreate",
            "BULK_UPDATE": "retailerserviceprovider/bulkupdate",
            "BULK_DELETE": "retailerserviceprovider/bulkdelete",
            "BY_RETAILER": "retailer/service-providers/<retailerId>/<firstResult>/<maxResults>",

            "PINCODES": {
                "LIST": "retailerserviceproviderpincode/query",
                "CREATE": "retailerserviceproviderpincode/create",
                "GET": "retailerserviceproviderpincode/get/<id>",
                "UPDATE": "retailerserviceproviderpincode/update",
                "DELETE": "retailerserviceproviderpincode/delete/<id>",
                "BULK_CREATE": "retailerserviceproviderpincode/bulkcreate",
                "BULK_UPDATE": "retailerserviceproviderpincode/bulkupdate",
                "BULK_DELETE": "retailerserviceproviderpincode/bulkdelete",
            }
        },
        "PRODUCT_PACKING": {
            "LIST": "productpacking/list",
            "CREATE": "productpacking/create",
            "GET": "productpacking/get/<RetailerServiceProviderId>",
            "UPDATE": "productpacking/update",
            "DELETE": "productpacking/delete/<RetailerServiceProviderId>",
            "BULK_CREATE": "productpacking/bulkcreate",
            "BULK_UPDATE": "productpacking/bulkupdate",
            "BULK_DELETE": "productpacking/bulkdelete",
        },
        "RETAILER_OTHER": {
            "LIST": "retailerother/query",
            "CREATE": "retailerother/create",
            "GET": "retailerother/get/<retailerOtherId>",
            "UPDATE": "retailerother/update",
            "DELETE": "retailerother/delete/<retailerOtherId>",
        },
        "RETAILER_CHEQUE_COLLECTION_CENTERS": {
            "LIST": "retailerchequecollectioncenter/query",
            "CREATE": "retailerchequecollectioncenter/create",
            "GET": "retailerchequecollectioncenter/get/<collectionCenterId>",
            "UPDATE": "retailerchequecollectioncenter/update",
            "DELETE": "retailerchequecollectioncenter/delete/<collectionCenterId>",
        },

        "RETAILER_CHEQUE_COLLECTION_CENTER_CITIES": {
            "LIST": "retailerchequecollectioncentercity/query",
            "CREATE": "retailerchequecollectioncentercity/create",
            "GET": "retailerchequecollectioncentercity/get/<collectionCenterCityId>",
            "UPDATE": "retailerchequecollectioncentercity/update",
            "DELETE": "retailerchequecollectioncentercity/delete/<collectionCenterCityId>",
            "BULK_CREATE": "retailerchequecollectioncentercity/bulkcreate",
            "BULK_UPDATE": "retailerchequecollectioncentercity/bulkupdate",
            "BULK_DELETE": "retailerchequecollectioncentercity/bulkdelete",
            "POPULATE_CITY": "retailer-cheque-collection-centers-cities/customqueries/<retailerId>"
        },
        "RETAILER_FAQS": {
            "LIST": "retailerfaq/query",
            "CREATE": "retailerfaq/create",
            "GET": "retailerfaq/get/<faqId>",
            "UPDATE": "retailerfaq/update",
            "DELETE": "retailerfaq/delete/<faqId>",
        },

        "RETAILER_SMS_EMAIL_TEMPLATE": {
            "LIST": "retailersmsemailtemplate/query",
            "CREATE": "retailersmsemailtemplate/create",
            "GET": "retailersmsemailtemplate/get/<templateId>",
            "UPDATE": "retailersmsemailtemplate/update",
            "DELETE": "retailersmsemailtemplate/delete/<templateId>",
        },

        "RETAILER_SMS_EMAIL_TRIGGER_CONFIG": {
            "LIST": "retailersmsemailtriggerconfig/query",
            "CREATE": "retailersmsemailtriggerconfig/create",
            "GET": "retailersmsemailtriggerconfig/get/<triggerId>",
            "UPDATE": "retailersmsemailtriggerconfig/update",
            "DELETE": "retailersmsemailtriggerconfig/delete/<triggerId>",
            "CONFIG_LIST_BY_SERVICE": "get/retailer/sms-email-configuration",
            "BULK_CREATE": "retailersmsemailtriggerconfig/bulkcreate",
            "BULK_UPDATE": "retailersmsemailtriggerconfig/bulkupdate",
            "BY_RETAILER": "get/sms-email-configuration/by-retailer/<retailerId>"
        },
        "RETAILER_PACKING_INTRA_CITY": {
            "PAGINATION": "product/intra-city/<retailerId>/<firstResult>/<maxResults>",
            "LIST": "retailerpackingintracity/query",
            "CREATE": "retailerpackingintracity/create",
            "GET": "retailerpackingintracity/get/<id>",
            "UPDATE": "retailerpackingintracity/update",
            "DELETE": "retailerpackingintracity/delete/<id>",
            "BULK_CREATE": "retailerpackingintracity/bulkcreate",
            "BULK_UPDATE": "retailerpackingintracity/bulkupdate",
            "BULK_DELETE": "retailerpackingintracity/bulkdelete"
        },
        "RETAILER_EMAIL_CONFIG": {
            "LIST": "retaileremailconfiguration/query",
            "CREATE": "retaileremailconfiguration/create",
            "GET": "retaileremailconfiguration/get/<id>",
            "UPDATE": "retaileremailconfiguration/update",
            "DELETE": "retaileremailconfiguration/delete/<id>"
        },
        "RETAILER_DOOR_STEP_SERVICE": {
            "LIST": "/list/door-step-services/<retailerId>/<firstResult>/<maxResults>",
            "CREATE": "retailerdoorstepservice/create",
            "GET": "retailerdoorstepservice/get/<id>",
            "UPDATE": "retailerdoorstepservice/update",
            "DELETE": "retailerdoorstepservice/delete/<id>"
        }
    },

    "SERVICE_PROVIDER": {
        "LIST": "serviceprovider/query",
        "CREATE": "serviceprovider/create",
        "GET": "serviceprovider/get/<serviceproviderid>",
        "UPDATE": "serviceprovider/update",
        "DELETE": "serviceprovider/delete/<serviceproviderid>",

        "LOCATION": {
            "LIST": "serviceproviderlocation/query",
            "CREATE": "serviceproviderlocation/create",
            "GET": "serviceproviderlocation/get/<ServiceProviderLocationId>",
            "UPDATE": "serviceproviderlocation/update",
            "DELETE": "serviceproviderlocation/delete/<ServiceProviderLocationId>",
            "ALL_LOCATIONS": "service/providers/list/<first>/<limit>"
        },

        "LOCATION_SERVICE": {
            "LIST": "serviceproviderlocationservice/query",
            "CREATE": "serviceproviderlocationservice/create",
            "GET": "serviceproviderlocationservice/get/<ServiceProviderLocationServiceId>",
            "UPDATE": "serviceproviderlocationservice/update",
            "DELETE": "serviceproviderlocationservice/delete/<ServiceProviderLocationServiceId>",
            "BY_SERVICEPROVIDER_ID": "servicelocations/by-service-provider/<serviceProviderId>"
        }
    },
    "INVENTORY": {
        "PACKAGING_MATERIAL": {
            "MASTER": {
                "LIST": "get/packaging-material/list/<firstResult>/<maxResults>",
                "CREATE": "packagingmaterialmaster/create",
                "GET": "packagingmaterialmaster/get/<packagingMaterialMasterId>",
                "UPDATE": "packaging-material/update",
                "DELETE": "packagingmaterialmaster/delete/<packagingMaterialMasterId>",
            },
            "INVENTORY": {
                "LIST": "get/packaging-inventory/details/<pageNo>/<maxResults>",
                "IMEI_LIST": "imeiinventory/query",
                "SEARCH_IMEI_INVENTORY": "search/imei-by-variable",
                "UPLOAD_IMEI_EXCEL": "upload/imei/excel",
                "DELETE_IMEI": "imeiinventory/delete/<imeiInventoryId>",
                "UPDATE_IMEI": "/update/imei-inventory",
                "CREATE": "packaging-material/inventory/add",
                "GET": "packagingmaterialinventory/get/<packagingMaterialInventoryId>",
                "UPDATE": "packagingmaterialinventory/update",
                "DELETE": "packagingmaterialinventory/delete/<packagingMaterialInventoryId>",
                "UPLOAD": "packaging-inventory/upload",
                "STATUS_UPDATE": "packaging-inventory/status/update",
                "GET_BY_BARCODE": "get/packaging-inventory/<barcode>"
            },
            "BARCODE_GENERATION": {
                "BULK_PRINT": "packaging-material/barcode/generate",
                "LIST": "barcode-generation/list/<firstResult>/<maxResults>"
            }
        }
    },
    "DOA": {
        "UPLOAD": "doa-form/upload",
        "DOWNLOAD": "attachment/download/DOA/<categoryId>/<brandId>",
        "LIST": "list/doa-forms/<firstResult>/<maxResults>",
        "GET": "doaform/get/<id>",
        "SEARCH": "doaform/query",
        "DELETE": "doaform/delete/<id>",
    },
    "AUTHLETTER": {
        "UPLOAD": "/auth-letter/upload"
    },
    "PURCHASEINVOICE": {
        "UPLOAD": "/purchase-invoice/upload",
        "AUTHUPLOAD": "upload/auth-letter"

    },
    "ADDITIONALFILE": {
        "UPLOAD": "/additional-file/upload",
    },
    "BITSBARCODEFILE": {
        "UPLOAD": "/bits/barcode/upload"
    },
    "EWASTEADDITIONALFILE": {
        "UPLOAD": "/ewaste-additional-file/upload"
    },
    "BULK_PICKUP": {
        "START": "/start/bulk-pickup-from-seller"
    },
    "MIS_REPORT": {
        "LIST": "/misbulktransfer/query"
    },
    "E_WASTE_MIS_REPORT": {
        "LIST": "/ewastemisbulktransfer/query"
    },
    "E_WASTE_BAG_HISTORY": {
        "LIST": "/baghistory/query"
    },
    "EVAL_RESULT": {
        "LIST": "/get/evaluation-result"
    },
    "TECH_EVAL_RESULT": {
        "LIST": "/get/tech-evaluation-result"
    },
    "PHY_EVAL_RESULT_AT_SERVICE": {
        "LIST": "/get/phy-evaluation-result/service-center"
    },
    "TECH_EVAL_RESULT_AT_SERVICE": {
        "LIST": "/get/tech-evaluation-result/service-center"
    },
    "PHY_EVAL_RESULT_STANDBY": {
        "LIST": "/get/phy-evaluation-result/standby-device"
    },
    "TECH_EVAL_RESULT_STANDBY": {
        "LIST": "/get/tech-evaluation-result/standby-device"
    },
    "EVAL_HISTORY": {
        "LIST": "/get/evaluation-history"
    },
    "EVAL_COMPARE": {
        "LIST": "/compare/evaluation-results"
    },
    "ATTACHMENT": {
        "UPLOAD": "attachment/upload",
        "DOWNLOAD": "attachment/download/<key1>/<key2>/<key3>",
        //"LIST" : "list/doa-forms/<firstResult>/<maxResults>",
        //"GET" : "doaform/get/<id>",
        //"SEARCH" : "doaform/query",
        //"DELETE": "doaform/delete/<id>",
    }
});