'use strict';


app.controller('ApplicationController', ['$scope', '$rootScope', '$state', 'LoginService','AuthTokenFactory', 'notification',
                                         'LIST_PAGE_SIZE', '$interval', 'TasksService', 'TokenFactory','UserService', '$cookies', 'AccessCheck',
                                        'ngDialog', 'Utility', '$timeout',
		function($scope, $rootScope, $state, LoginService, AuthTokenFactory, notification,
                 LIST_PAGE_SIZE, $interval, TasksService, TokenFactory, UserService, $cookies,AccessCheck, ngDialog, Utility, $timeout){
	
    $scope.Login = {};
    $rootScope.currentPage = 1;
    $rootScope.pageSize = LIST_PAGE_SIZE;
    $rootScope.total_count = 100;
    $rootScope.taskHeaderName = "";
    $rootScope.admin = false;
    $rootScope.host_base_url = host_base_url;

    
    $rootScope.getTitle = function(page) {
        return "Reverse Logistics : " + page;
    }
    
    $rootScope.dateTimePickerOptions = {
        icons:{
            next:'glyphicon glyphicon-arrow-right',
            previous:'glyphicon glyphicon-arrow-left',
            up:'glyphicon glyphicon-arrow-up',
            down:'glyphicon glyphicon-arrow-down'
        },
        sideBySide: true,
        toolbarPlacement: 'top',
        format: 'dd-MM-yyyy hh:mm'
    };
    
    //Handle default page if no login
    var token = AuthTokenFactory.isLoggedIn();
    if(!token)
    {
        $state.go('login');
        //return false;
    }
    else {
        Utility.populate_menubar($rootScope);
        $scope.loginInfo = JSON.parse(TokenFactory.getToken('loginInfo'));
    }
    
    $rootScope.concat_n_show = function(a, b) {
        return a + ' ' + b;
    }
    
    $rootScope.calculateFirstRecord = function(pageNo, limit) {			
        var first = 0;
        if (pageNo > 1) {
            first = (((parseInt(pageNo)-1))*parseInt(limit));
        }
        
        return first;
    }
    
	$scope.login = function(user) {
		LoginService.login(user, $scope, $state)
            .success(function (response){
                if (response) {
                    response = response.response;
                    $rootScope.roleCode = response.roleCode;
                    //Check if response is success
                    if (!response.success) {
                        notification.error("Unable to login. Either email or password is wrong!");
                        return;
                    }
                    
                    AuthTokenFactory.setToken(response.token);

                    response.username = (response.id.indexOf('@') !== -1) ? (response.id.split('@'))[0] : response.id;
                    //$rootScope.loginInfo.phone = response.phone;
                    $rootScope.loginInfo = $scope.loginInfo = (response)
					$rootScope.loginInfo.id = response.id;
                    $rootScope.loginInfo.phone = response.phone;
                    //Utility.get_assignee_number($scope.loginInfo.id,$scope);


                    TokenFactory.setToken('loginInfo',JSON.stringify( $scope.loginInfo ));
                    $rootScope.$broadcast ('populateMenubar');
                    
                    switch ($scope.loginInfo.roleCode) {
                        case 'superadmin' :
                        case 'admin' :
                        case 'ho' :
                        case 'readonly' :
                            $state.go('rl.manager-unclaimed', {first:1,limit:10});
                            break;
                        
                        case 'retailer' :
                            $state.go('rl.retailer-tasks', {first:1,limit:10});
                            break;
						case 'finance_head' :
                            $state.go('rl.finance-Party-view');
                            break;
                            case 'hub_manager' :
                            $state.go('rl.hub_manager-tasks', {first:1,limit:10});
                            break;
                        default :
                            $state.go('rl.tasks-assigned', {first:1,limit:10});
                            break;
                    }                    
                }
                
            })
            .error(function(error){
                $scope.Login.id = "";
                $scope.Login.password = "";
                $rootScope.handleError(error);
            }); //Call login service
	}

	$scope.logout = function(user) {
        console.log("Logging out!!!");
        AccessCheck.logout();
        console.log("Removed cookies");
		$state.go('login');
        console.log("State changed");
        return;
	}
    
    $rootScope.logout = function() {
        $scope.logout();
    }
    
    $rootScope.clearText = function(search) {
        search.text = "";
    }
    
    $rootScope.goToPage = function(page) {
        console.log(page);
        $state.go(page)
    }
    
    $rootScope.goToPageWithParams = function(page, params) {
        //console.log(page, params);
        $state.go(page, params);
    }
    
    $rootScope.redirectToPrev = function() {
        $state.go($state.current.redirectToState, $state.current.redirectParams);
    }
    
    $rootScope.handleError = function (error) {
    }
    
    $scope.changePassword = function(u) {
        var loginInfo = JSON.parse(TokenFactory.getToken('loginInfo'));//,JSON.stringify( $scope.loginInfo ));
        
        var changePassword = {email: loginInfo.id};
        changePassword.oldPassword = u.oldPassword;
        changePassword.newPassword = u.newPassword;
        UserService.change_my_password(changePassword)
                .success(function (response){
                    //notification.success("Successfully changed your password!")
                });
        
        return true;
    }

    $rootScope.get_selected_name = function(arr, selectedValue, valueFieldName, textFieldName) {
        for (var i = 0; i < arr.length; i++) {
            if (arr[i][valueFieldName] == selectedValue) {
                return arr[i][textFieldName];
            }
        }
        
        return "";
    };
    
    $rootScope.addNew = function(url) {
        $state.go(url);
    }
    
    $rootScope.goBack = function() {
        var token = AuthTokenFactory.getToken();
		if (!AuthTokenFactory.isLoggedIn()) {
            $state.go('login');
        }
        else
            $state.go('rl.home');
    }
    
    $rootScope.showSuccessMessage = function(response) {
        if (response !== undefined && response.response !== undefined) {
            if (response.message !== undefined && response.message !== null && response.message !== "" ) {
                notification.success(response.message);
            }
        }
    }
    
    $rootScope.showErrorMessage = function(response) {
        console.log(response);
        if (response.message !== undefined && response.message !== null && response.message !== "" )
            notification.error(response.message);
    }
    
    $rootScope.deleteDialogAndConfirm = function(msg, callback, dataToDelete) {
		ngDialog.openConfirm({
			template: '<div class="dialog-contents">' +
							msg + '<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
			controller: ['$scope', function($scope) { 
			  // Controller logic here
			}]
		}).then(function (success) {
			$rootScope.$broadcast (callback, dataToDelete);
		}, function (error) {
			// Error logic here
		});
	}
    
    var evtPopulateMenubar = $rootScope.$on('populateMenubar', function(e, args) {
        $rootScope.menubar = []; 
        LoginService
            .populate_menubar()
            .success(function(response){
                $rootScope.menubar = response;
                console.log($rootScope.menubar);
                $rootScope.$broadcast ('menuClick');
            });
    });
    var clickIndex = 0;
    var evtMenuClick = $rootScope.$on('menuClick', function() {
        $timeout(function(){
            $('ul.dropdown-menu [data-toggle=dropdown]').bind('click', function(event) { 
                event.preventDefault(); 
                event.stopPropagation(); 
                $(this).parent().siblings().removeClass('open');
                $(this).parent().toggleClass('open');
                
                // Re-add .open to parent sub-menu item
                $(this).parent().addClass('open');
                $(this).parent().find("ul").parent().find("li.dropdown").addClass('open');
                
            });
        }, 3000, true);
    });
	
	var evtDeleteDialogAndConfirmEvent = $rootScope.$on('deleteDialogAndConfirmEvent', function(e, msg, callback, dataToDelete) {
		console.log(msg, callback, dataToDelete);
		$scope.deleteDialogAndConfirm(msg, callback, dataToDelete);
	});
    
    var evtShowUploadReturnMessage = $rootScope.$on('showUploadReturnMessage', function(e, args){
        
        $rootScope.excelUploadErrors = null;
        
        var msg = ""; var type = "success";
        
        if (args.status == 200 && args.data.noOfRecordsInserted > 0) {
            msg += "Successfully uploaded " + args.data.noOfRecordsInserted + " item(s)!";
        }
        if (args.data.errorMessages.length > 0) {
            
            msg += " Problem in uploading " + args.data.errorMessages.length + " record(s)";
            
            type = "info";
            
            //notification.error("Error in uploading data! Please go through the error details for more information!");
            
            $rootScope.excelUploadErrors = {};
            $rootScope.excelUploadErrors.heading = "Excel Upload Errors"
            $rootScope.excelUploadErrors.errorMessages = args.data.errorMessages;
            
            //$timeout(function(){
            //    $scope.excelUploadErrors = null;
            //}, 8000);
        }
        else if (args.status == 200 && args.data.noOfRecordsInserted == 0) {
            notification.show("No records are uploaded!");// + args.data.noOfRecordsInserted + " item(s)!");
        }
        
        if (type == "success") {
            notification.success(msg);
        }
        else if (type == "info") {
            notification.info(msg);
        }
        
    });
    
    var evtShowSuccessMessage = $rootScope.$on('showSuccessMessage', function(e, args){
        console.log('broadcast from child in parent');
        $scope.showSuccessMessage(args);
    });
    
    var evtShowErrorMessage = $rootScope.$on('showErrorMessage', function(e, args){
        console.log('broadcast from child in parent');
        $scope.showErrorMessage(args);
    });
    
    $scope.$on('$destroy', function() {
        evtDeleteDialogAndConfirmEvent();
        evtShowSuccessMessage();
        evtShowErrorMessage();
        evtPopulateMenubar();
        evtShowUploadReturnMessage();
    });
    
    //$scope.$watch($state.current.name, function(val){
    //    console.log($state.current);
    //    if ($state.current.name == 'login') {
    //        AccessCheck.cancel();
    //    }
    //    else {
    //        AccessCheck.start($rootScope, $scope);
    //    }
    //});
}]);

app.controller('LogoutController', ['$scope', '$rootScope', function($scope, $rootScope) {
    $scope.logout = function() {
        $rootScope.logout();
    }
}]);