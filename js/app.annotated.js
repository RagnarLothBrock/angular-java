'use strict';

var host_base_url = "http://" + window.location.hostname;
if (window.location.port != "") {
    host_base_url += ":" + window.location.port;
}
if (window.location.pathname != "") {
    host_base_url += window.location.pathname;
}

var checkAccessAndReturnPath = function () {
    try {
        var loginInfo = JSON.parse(localStorage.getItem('loginInfo'));

        if (loginInfo === undefined || loginInfo === null) {
            return '/';
        }

        if (loginInfo.admin === true) {
            return '/tasks/manager/unclaimed/0/10'
        }
        else if (loginInfo.roleCode === 'retailer') {
            return ('/tasks/retailer/0/10');
        }
        else if (loginInfo.roleCode === 'hub_manager') {
            return ('/tasks/hub_manager/0/10');
        }
        else if (loginInfo.roleCode === 'finance_head') {
            return ('/tasks/search');
        }
        else {
            return ('/tasks/assigned/0/10');
        }
    }
    catch (e) {
        console.log(e);
        return '/';
    }
}

// Declare app level module which depends on views, and components
var app = angular.module('rLogisticsApp', [
    'ui.router',
    //  'ui.bootstrap',
    'ngResource',
    'ngDialog',
    'ngCookies',
    'angularjs-datetime-picker',
    'ngFileUpload',
    'rt.select2',
    'selectize',
    'daterangepicker',
    'ngMessages',
    'frapontillo.bootstrap-switch',
    'ckeditor',
    'ngSanitize',
    'angularUtils.directives.dirPagination',
    'ngNotify',
    'angular-bind-html-compile',
    'ngMap',
    'ngMaterial',
    'ngMessages',
    'ngAnimate',
    'ngAria',

]);

app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', '$locationProvider', '$windowProvider',
    function ($stateProvider, $urlRouterProvider, $httpProvider, $locationProvider, $windowProvider) {

        //$httpProvider.defaults.useXDomain = true;
        //delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $httpProvider.interceptors.push('AuthInterceptor');
        // $httpProvider.interceptors.push('myHttpInterceptor');

        // $locationProvider.html5Mode(true).hashPrefix('!')
        $stateProvider
        //Authenticated state
            .state('rl', {
                abstract: true,
                views: {
                    'page-main-content': {
                        templateUrl: 'partials/tpl/pages/home.tpl.html'
                    },
                    'top-header': {
                        templateUrl: 'partials/tpl/common/header.tpl.html'
                    },
                    'page-footer': {
                        templateUrl: 'partials/tpl/common/footer.tpl.html'
                    },
                    'left-sidebar': {
                        templateUrl: 'partials/tpl/common/sidebar.tpl.html'
                    }
                },
                data: {
                    layout: "site",
                    requireLogin: true,
                    checkAccess: true,
                    access_perm: null
                }
            })
            //Not authenticated state
            .state('rlna', {
                abstract: true,
                views: {
                    'top-header': {
                        templateUrl: 'partials/tpl/common/login-header.tpl.html'
                    },
                    'main-content': {
                        templateUrl: 'partials/tpl/common/login.tpl.html'
                    },
                    'page-footer': {
                        templateUrl: 'partials/tpl/common/footer.tpl.html'
                    }
                },
                data: {
                    layout: "login",
                    requireLogin: false,
                    checkAccess: false,
                    access_perm: null
                }
            })
            .state('rlna.not-found', {
                url: '/not-found',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/common/not_found.tpl.html'
                    },
                    'main-content@': {
                        templateUrl: 'partials/tpl/common/not_found.tpl.html'
                    },
                },
                controller: 'ApplicationController',
                data: {
                    page: "Page Not Found"
                }
            })

            .state('rl.change-password', {
                url: '/change-password',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/change_password.tpl.html'
                    }
                },
                controller: 'ApplicationController',
                data: {
                    page: "Change Password",
                    requireLogin: true,
                    checkAccess: false
                }
            });

        var urlNotFoundRule = function ($injector, $location, $window, $state, $rootScope) {
            var path = checkAccessAndReturnPath();
            $location.path(path);
        }

        //$urlRouterProvider.otherwise('/');
        $urlRouterProvider.otherwise(urlNotFoundRule);


        //$locationProvider.html5Mode({
        //    enabled: true,
        //    requireBase: false
        //});
    }]);

app.config(['ngDialogProvider', function (ngDialogProvider) {
    ngDialogProvider.setDefaults({
        className: 'ngdialog-theme-default',
        plain: true,
        showClose: true,
        closeByDocument: true,
        closeByEscape: true
    });
}]);
app.config(['$httpProvider', '$windowProvider', function ($httpProvider, $windowProvider) {
    // $httpProvider.interceptors.push('myHttpInterceptor');
    var spinnerFunction = function (data, headersGetter) {
        // todo start the spinner here
        //alert('start spinner');
        var $window = $windowProvider.$get();
        $window.scrollTo(0, 0);
        $('#mydiv').show();
        return data;
    };
    $httpProvider.defaults.transformRequest.push(spinnerFunction);
}]);
//app.factory('myHttpInterceptor', function ($q, $window) {
//        return {
//            'response': function (response) {
//                    // do something on success
//                    // todo hide the spinner
//                    //alert('stop spinner');
//                    $('#mydiv').hide();
//                    $('#mydiv').height($(document).height()+20);
//                    return response;
//                },
//            'responseError':function (response) {
//                // do something on error
//                // todo hide the spinner
//                //alert('stop spinner');
//                $('#mydiv').hide();
//                $('#mydiv').height($(document).height()+20);
//                return response;
//            }
//        };
//    });
app.value('selectizeConfig', {
    delimiter: ',',
    create: true,
    valueField: 'id',
    labelField: 'name',
    onInitialize: function (selectize) {
        // receives the selectize object as an argument 
    },
});

app.config(["$sceProvider", function ($sceProvider) {
    // Completely disable SCE.  For demonstration purposes only!
    // Do not use in new projects.
    $sceProvider.enabled(false);
}]);

app.config(["paginationTemplateProvider", function (paginationTemplateProvider) {
    paginationTemplateProvider.setPath('partials/tpl/common/dirPagination.tpl.html');
}]);

