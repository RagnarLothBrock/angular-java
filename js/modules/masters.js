'use strict'

app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {

		$stateProvider
			.state('rl.masters', {
				url: '/masters',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/list.tpl.html'
					}
				},
				data: {
					page: "Master Tables",
					requireLogin : true
				}
			});
    }]);
app.controller('MasterController', ['$scope', function($scope) {
	$scope.breadcrumbs = [
		{
			title: "Master Tables",
			link: "/masters"
		}
	];
}]);