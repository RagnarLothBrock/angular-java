'use strict'

app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {

		$stateProvider
			.state('rl.retailer-onboarding', {
				url: '/retailer/onboarding',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/retailer/onboarding.tpl.html'
					}
				},
				controller: 'RetailerController',
				data: {
					page: "Retailer On Boarding",
					requireLogin : true
				}
			})
			.state('rl.retailer-edit', {
				url: '/retailer/get/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/retailer/onboarding.tpl.html'
					}
				},
				controller: 'RetailerController',
				data: {
					page: "Update Retailer",
					requireLogin : true
				}
			})
			.state('rl.retailers', {
				url: '/retailers',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/retailer/list.tpl.html'
					}
				},
				controller: 'RetailerController',
				data: {
					page: "Retailer On Boarding",
					requireLogin : true
				}
			});
    }]);

app.controller('RetailerController',
	['$scope', '$rootScope', '$state', 'notification', 'AuthTokenFactory','$location', 'logger','$q', '$timeout', 'ngDialog', 'Utils',
	 'ServiceMaster', 'ProductCategory', 'PackagingType', 'BrandService', 'Retailer', 'Product', 'RetailerServices',
	 'ServiceProvider', 'ServiceProviderLocation', 'RetailerCategories', 'RetailerServiceableLocations', 'RetailerOdaLocations', 'RetailerServicesCost',
	 'RetailerPackingCost', 'RetailerPaysPackingCost', 'RetailerAdvReplacementProducts', 'ProductSubCategory', 'RetailerExchangePolicy',
	 'RetailerReturnOrPickupLocations', 'RetailerSellerCategories', 'RetailerServiceProvider', 'CityService', 'RetailerOther', 'ProductPacking',
	 'RetailerChequeCollectionCenters', 'RetailerChequeCollectionCenterCities', 'RetailerFAQs', 'Utility', 'ServiceProviderLocationService', '$http',
	 'LIST_PAGE_SIZE', 'MAX_DROPDOWN_SIZE', 'RetailerServiceProviderPincode','MasterCategory','TasksService',
    function($scope, $rootScope, $state,  notification, AuthTokenFactory, $location, logger, $q, $timeout,ngDialog, Utils,
		ServiceMaster, ProductCategory, PackagingType, BrandService, Retailer, Product, RetailerServices,
		ServiceProvider, ServiceProviderLocation, RetailerCategories, RetailerServiceableLocations, RetailerOdaLocations, RetailerServicesCost,
		RetailerPackingCost, RetailerPaysPackingCost, RetailerAdvReplacementProducts, ProductSubCategory, RetailerExchangePolicy,
		RetailerReturnOrPickupLocations, RetailerSellerCategories, RetailerServiceProvider, CityService, RetailerOther, ProductPacking,
		RetailerChequeCollectionCenters, RetailerChequeCollectionCenterCities, RetailerFAQs, Utility, ServiceProviderLocationService, $http,
		LIST_PAGE_SIZE, MAX_DROPDOWN_SIZE, RetailerServiceProviderPincode,MasterCategory,TasksService
	) {
    $scope.cities = [];
	//$scope Variables
		$scope.pageSize = LIST_PAGE_SIZE;
		$scope.total_count = 0;
		$scope.currentPage = 1;
		
        $scope.notification = $rootScope.notification;
		$scope.retailers = [];
		$scope.retailer = new retailer();
		$scope.currentStep = 1;
		$scope.currentStepName = "Registration";
		$scope.steps = [
			{name:"Basic Info",url:"registration"},
			{name:"Products",url:"products"},
			{name:"Services Availed",url:"services_availed"},
			{name:"Categories Serviced",url:"categories_serviced"},
			//{name:"Cost Per Service",url:"cost_per_service"},
			{name:"Service Costing",url:"service_costing"},
			{name:"Serviceable Locations",url:"serviceable_locations"},
			{name:"ODA Locations",url:"oda_locations"},
			{name:"Packing Cost",url:"packing_cost"},
			{name:"Packing Cost Borne",url:"packing_cost_borne"},
			{name:"Advance Replacement",url:"advance_replacement"},
			{name:"Replacement Policy",url:"exchange_policy"},
			{name:"Return/Pickup Locations",url:"return_pickup_location"},
			{name:"Service Provider",url:"service_provider"},
			{name:"Cheque Collection Centers",url:"cheque_collection_centers"},
			{name:"Door Step Services",url:"door_step_services"},
			{name:"Message",url:"faq"},
			{name:"Other Settings",url:"other"},
			{name:"Email Configuration",url:"email_configuration"},
			{name:"SMS/Email Templates",url:"sms_email_template"},
			{name:"SMS/Email Notification Settings",url:"sms_email_trigger_config"},
			{name:"Packing - Intra City",url:"packing_intra_city"},
			{name:"Mapping - Process fields",url:"process_fields"},
			{name:"Packing Specifications",url:"packing_specifications"},
			{name:"Product Packing",url:"product_packing"},
		];
	
	$scope.breadcrumbs = [
		{
			title: "Retailers",
			link: "/retailers"
		}
	];
	
	$scope.getAllRetailersList = function(first, limit) {
		console.log("($scope.loginInfo.roleCode"+$scope.loginInfo.roleCode);
		$scope.currentPage = first;
		first = $rootScope.calculateFirstRecord(first, limit);
		// if ($scope.loginInfo.roleCode === 'hub_manager') {
		// 	var city= $scope.loginInfo.locationName;
		// Retailer.list({'query':{'city':city},'firstResult':first, maxResults: limit}).success(function(response) {
		// 	//$scope.retailers = response;
		// 	if (response !== undefined && response.data !== undefined && response.data.length > 0){
		// 		$scope.retailers = response.data;
		// 		$scope.total_count = response.totalRecords;
		// 	}
		// });
		// }

		// var loginInfo = JSON.parse(TokenFactory.getToken('loginInfo'));
		// if ($scope.loginInfo.roleCode !== 'hub_manager') {
			Retailer.list({'query':{},'firstResult':first, maxResults: limit}).success(function(response) {
			//$scope.retailers = response;
			if (response !== undefined && response.data !== undefined && response.data.length > 0){
				$scope.retailers = response.data;
				$scope.total_count = response.totalRecords;
			}
		});
		// }
	}
	
	//Retailer List
	if ($state.current.url == '/retailers') {
		$scope.showAddNew = true;
		$scope.addNewUrl = "/retailer/onboarding";
		$scope.addNewState = "rl.retailer-onboarding"
		$scope.getAllRetailersList($scope.currentPage, $scope.pageSize);
	}
	
	if ($state.current.url == '/retailer/get/:id') {
		try {
				$scope.$watch('authFile.file', function () {
					$scope.authFile.selectedFileName = ($scope.authFile.file !== undefined) ? $scope.authFile.file.name : "";
				});
			}
			catch(e) {
				
			}
	}
	
	var hasErrorsExchangePolicy = function(retailer) {
		$scope.retailer.list.exchangePolicyProducts.bulkCreate = [],
		$scope.retailer.list.exchangePolicyProducts.bulkUpdate = [];
		$scope.retailer.list.exchangePolicyProducts.errors = [];
		var selectedObj = $.unique(retailer.list.exchangePolicyProducts.selectedObj);
		for(var i = 0; i < selectedObj.length; i++) {
			if (selectedObj[i] === undefined) {
                continue;
            }
			var e = {};
			if (selectedObj[i].productId == null) {
                e.productId = "Please select a product";
            }
			if (e.productId !== undefined ) {
                $scope.retailer.list.exchangePolicyProducts.errors[i] = e;
            }
			
			$scope.retailer.list.exchangePolicyProducts.selectedObj[i].retailerId = $scope.retailer.retailer.id;
			
			var item = angular.copy(selectedObj[i]);
			if (selectedObj[i].id == null) {
                $scope.retailer.list.exchangePolicyProducts.bulkCreate.push(item);
            }
			else {
				$scope.retailer.list.exchangePolicyProducts.bulkUpdate.push(item);
			}
		}
		
		if ($scope.retailer.list.exchangePolicyProducts.errors.length > 0) {
			return true;
		}
		return false;
	}
	
	var hasErrorsReturnOrPickupLocations = function(retailer) {
		var templateObj = retailer.list.returnOrPickupLocations.template;
		var errors = $scope.retailer.list.returnOrPickupLocations.errors = {};
		
		if (isEmpty(templateObj.sellerName,'sellerName',"Enter seller name",$scope.notification)) {
			errors.sellerName = "Enter seller name";
		}
		if (isEmpty(templateObj.sellerCode,'sellerCode',"Enter seller code",$scope.notification)) {
			errors.sellerCode = "Enter seller code";
		}
		if (isEmpty(templateObj.sellerLocation,'sellerLocation',"Enter seller location",$scope.notification)) {
			errors.sellerLocation = "Enter seller location";
		}
		if (isEmpty(templateObj.sellerAddress,'sellerAddress',"Enter seller address",$scope.notification)) {
			errors.sellerAddress = "Enter seller address";
		}
		if (isEmpty(templateObj.sellerPhoneNumber,'sellerPhoneNumber',"Enter seller address",$scope.notification)) {
			errors.sellerPhoneNumber = "Enter seller phone number";
		}
		
		
		if (isEmpty(templateObj.cityId,'cityId')) {
			errors.cityId = "Select seller city";
		}
		if (isEmpty(templateObj.sellerContactPerson,'sellerContactPerson',"Enter seller contact person",$scope.notification)) {
			errors.sellerContactPerson = "Enter contact person";
		}
		
		if (isEmpty(templateObj.sellerEmail,'sellerEmail',"Enter seller email",$scope.notification)) {
			errors.sellerEmail = "Enter valid seller email";
		}
		if (isEmpty(templateObj.sellerPincode,'sellerPincode',"Enter seller address",$scope.notification)) {
			errors.sellerPincode = "Enter seller pincode";
		}
		else if (templateObj.sellerPincode.length !== 6) {
			errors.sellerPincode = "Pincode should be 6 digits!";
		}
		
		$scope.retailer.list.returnOrPickupLocations.errors = errors;
		if (Object.getOwnPropertyNames($scope.retailer.list.returnOrPickupLocations.errors).length > 0) {
			return true;
		}
		return false;
	}

	var hasErrorsProductPacking = function(retailer) {
		var templateObj = $scope.retailer.productPacking;
		var errors = $scope.errors = {};
		
		if (isEmpty(templateObj.productId,'productId',"Select Product",$scope.notification)) {
			errors.sellerName = "Select Product";
		}
		if (isEmpty(templateObj.packagingTypeId,'packagingTypeId',"Select Packing Material",$scope.notification)) {
			errors.sellerCode = "Select Packing Material";
		}
		if (isEmpty(templateObj.type,'type',"Choose packing type",$scope.notification)) {
			errors.sellerLocation = "Choose packing type";
		}
		if (isEmpty(templateObj.packingSpec,'packingSpec',"Select Packing Specification",$scope.notification)) {
			errors.sellerAddress = "Select Packing Specification";
		}
		
		$scope.errors = errors;
		if (Object.getOwnPropertyNames($scope.errors).length > 0) {
			return true;
		}
		return false;
	}
	
	var hasErrorsServiceProviderLocation = function(retailer) {
		var templateObj = $scope.retailer.serviceProvider;
		$scope.errors = {};
		
		delete(retailer.serviceProvider.serviceProviderId);
		delete(retailer.serviceProvider.area);
		delete(retailer.serviceProvider.brandName);
		delete(retailer.serviceProvider.categoryName);
		delete(retailer.serviceProvider.serviceProviderCode);
		delete(retailer.serviceProvider.subCategoryName);
		$scope.retailer.serviceProvider.city = "";
		for(var i=0; i< $scope.retailer.serviceProviderCityList.length; i++) {
			console.log($scope.retailer.serviceProviderCityList[i].id, templateObj.cityId,$scope.retailer.serviceProviderCityList[i].name);
			if (templateObj.cityId == $scope.retailer.serviceProviderCityList[i].id) {
                $scope.retailer.serviceProvider.city = $scope.retailer.serviceProviderCityList[i].name;
            }
		}
		return false;
	}
	/*** Error Validation Data Block **/
	
	
	/** Loading Page Data Block **/
	
	var populateCategoryList = function() {
		ProductCategory.list({'query':{}}).success(function(response) {
			$scope.retailer.list.productCategory.list = response;
		});
	}
	
	var populateSubCategoryList = function(categoryId) {
		var params = {'query':{'productCategoryId':categoryId}};
		ProductSubCategory.list(params).success(function(response) {
			$scope.retailer.list.subCategory.list = response;
		});
	}
	
	var populateProductList = function() {
		Utility.populate_products({}, $scope);
	}
	$scope.populateServiceProviderList = function(first, limit) {
		$scope.retailer.serviceProviderList = [];
		$scope.initOtherDropdwons();
		ServiceProvider.list({'query':{}, firstResult: first, maxResults:MAX_DROPDOWN_SIZE}).success(function(response){
			if (response !== undefined && response.data !== undefined && response.data.length > 0)
				$scope.retailer.serviceProviderList = response.data;
		});
	}
	
	$scope.initOtherDropdwons = function() {
		$scope.retailer.serviceProviderLocationList=[];
		$scope.retailer.serviceProviderCategoryList=[];
		$scope.retailer.serviceProviderSubCategoryList= [];
		$scope.retailer.serviceProviderBrandList= [];
		$scope.retailer.serviceProvider.serviceProviderLocationId = "";
		$scope.retailer.serviceProvider.categoryId = "";
		$scope.retailer.serviceProvider.brandId = "";
		$scope.retailer.serviceProvider.subCategoryId  = "";
		$scope.retailer.serviceProvider.cityId = "";
		$scope.retailer.serviceProvider.city = "";
	}
	
	$scope.populateServiceProviderLocation = function(providerId, cityId) {
		//Populate Location
		$scope.retailer.serviceProviderLocationList = [];
		$scope.retailer.serviceProviderCategoryList = [];
		$scope.retailer.serviceProviderSubCategoryList = [];
		$scope.retailer.serviceProviderBrandList= [];
		
		ServiceProviderLocation
			.list({'query':{'serviceProviderId': providerId, 'cityId' : cityId}})
			.success(function(response) {
				if (response !== undefined && response.data !== undefined && response.data.length > 0)
					$scope.retailer.serviceProviderLocationList = response.data;
		});
	}
	
	$scope.populateServiceProviderCategory = function(serviceProvider) {		
		//Populate Category
		$scope.retailer.serviceProviderCategoryList = [];
		$scope.retailer.serviceProviderSubCategoryList = [];		
		ProductCategory
			.byServiceProviderLocation({serviceProviderLocationId: serviceProvider.serviceProviderLocationId})
			.success(function(response) {
				//if (response !== undefined && response.data !== undefined && response.data.length > 0)
					$scope.retailer.serviceProviderCategoryList = response;
		});
		
		$scope.populateServiceProviderBrandList(serviceProvider);
	}
	
	$scope.populateServiceProviderSubCategory = function(serviceProvider) {		
		//Populate Category
		$scope.retailer.serviceProviderSubCategoryList = [];
		//$scope.retailer.serviceProvider.subCategoryId  = "";
		var params = {
			serviceProviderLocationId: serviceProvider.serviceProviderLocationId,
			categoryId: serviceProvider.categoryId
			
		}
		ProductSubCategory
			.byServiceProviderLocation(params)
			.success(function(response) {
				//if (response !== undefined && response.data !== undefined && response.data.length > 0)
					$scope.retailer.serviceProviderSubCategoryList = response;
		});
		
		var p = {
			serviceProviderLocationId: serviceProvider.serviceProviderLocationId,
			categoryId: serviceProvider.categoryId,
			brandId: null,
			subCategoryId: null
		}
		ServiceProviderLocationService
			.list({query:p})
			.success(function(response){
				if (response.length > 0) {
                    $scope.serviceProviderLocationService = response[0];
                }
		});
	}
	
	$scope.populateServiceProviderBrandList = function(serviceProvider) {
		$scope.retailer.serviceProviderBrandList = [];
		//$scope.retailer.serviceProvider.brandId = "";
		BrandService.byServiceProviderLocation({serviceProviderLocationId: serviceProvider.serviceProviderLocationId})
			.success(function(response) {
				//if (response !== undefined && response.data !== undefined && response.data.length > 0)
					$scope.retailer.serviceProviderBrandList = response;
		});
	}
	
	$scope.populateRetailerServiceProviderList = function(first, limit) {
		 $scope.currentPage = first ;// initialize page no to 1
        //$scope.total_count = 100;
        first = $scope.calculateFirstRecord($scope.currentPage, limit);
		$scope.retailer.retailerServiceProviderList = [];
		RetailerServiceProvider
			.by_retailer({'retailerId':$state.current.data.params.id, firstResult: first, maxResults: limit})
			.success(function(response){
				if (response !== undefined && response.data !== undefined && response.data.length > 0) {
					$scope.retailer.retailerServiceProviderList = response.data;
					 $scope.total_count = response.totalRecords;
                    
				}
			});
	}

	$scope.populateProductPackingList = function(first, limit) {
		 $scope.currentPage = first ;// initialize page no to 1
        //$scope.total_count = 100;
        first = $scope.calculateFirstRecord($scope.currentPage, limit);
		ProductPacking
			.list({'query':{'retailerId':$state.current.data.params.id}, firstResult:first, maxResults:limit})
			.success(function(response){
				if (response !== undefined && response.data !== undefined && response.data.length > 0) {
					$scope.retailer.productPackingList = response.data;
					$scope.packingList = $scope.retailer.productPackingList;
					 $scope.total_count = response.totalRecords;
                    
				}
			});
	}
	/*** Loading Page Data Block **/
	
	/*** Save Page Data Block **/	
	
	
	
	//Save retailer exchange policy product
	var saveRetailerExchangePolicy = function(retailer) {
		//Validate Retailer
		$scope.retailer.list.exchangePolicyProducts.errors = [];
		
		var hasErrors = hasErrorsExchangePolicy(retailer);
		
		if (!hasErrors) {
			
			if ($scope.retailer.list.exchangePolicyProducts.bulkUpdate.length > 0) {
                //Save Details
				RetailerExchangePolicy
					.bulkupdate($scope.retailer.list.exchangePolicyProducts.bulkUpdate)
					.success(function(response) {
					});
            }
			
			if ($scope.retailer.list.exchangePolicyProducts.bulkCreate.length > 0) {
                //Save Details
				RetailerExchangePolicy
					.bulkcreate($scope.retailer.list.exchangePolicyProducts.bulkCreate)
					.success(function(response) {
						getRetailerExchangePolicyProducts();
					});
            }
        }
	}
	
	var saveReturnOrPickupLocation = function(retailer) {
		//Validate Retailer
		$scope.retailer.list.returnOrPickupLocations.errors = {};
		
		$scope.retailer.list.returnOrPickupLocations.template.retailerId = $state.current.data.params.id;
		
		var hasErrors = hasErrorsReturnOrPickupLocations(retailer);
		
		if (!hasErrors) {
			var templateObj = $scope.retailer.list.returnOrPickupLocations.template;
			templateObj.city = Utils.get_city_name($scope.cities,templateObj.cityId);
			
			if (templateObj.id == undefined || templateObj.id == "" || templateObj.id == null) {
				delete(templateObj.id);
				//Continue with retailer return/pickup location create
				RetailerReturnOrPickupLocations.create(templateObj).success(function(response) {
					console.log(response);
					//Save seller categories
					saveSellerCategories(response.response.id);					
				});
			}
			else {
				//Continue with retailer return/pickup location Update
				RetailerReturnOrPickupLocations.update(templateObj).success(function(response) {
					console.log(response);
					//Save seller categories
					saveSellerCategories(response.response.id);
				});
			}
			
		}
	}

	
	$scope.saveProductPacking = function(retailer) {
		//Validate Retailer
		console.log("Inside save packing function",retailer);
		var hasErrors = hasErrorsProductPacking(retailer);
		$scope.retailer.productPacking.retailerId = $state.current.data.params.id;
		if (!hasErrors) {
			var templateObj = $scope.retailer.productPacking;
			// var params={id:$scope.retailer.productPacking.id,
			//             packagingTypeId:$scope.retailer.productPacking.packagingTypeId,
			// 		    packingSpec:$scope.retailer.productPacking.packingSpec,
			// 		    productId:$scope.retailer.productPacking.productId,
			// 		    type:$scope.retailer.productPacking.type,
			// 		    retailerId:$scope.retailer.productPacking.retailerId}
			
			// var templateObj =params;
			console.log("templateObj",templateObj);
			
			if (templateObj.id == undefined || templateObj.id == "" || templateObj.id == null) {
				delete(templateObj.id);
				//Continue with retailer return/pickup location create
				ProductPacking.create(templateObj).success(function(response) {
						$scope.retailer.productPacking = {};
						$scope.populateProductPackingList(1,10);
				});
			}
			else {
				//Continue with retailer return/pickup location Update
				ProductPacking.update(templateObj).success(function(response) {
						$scope.retailer.productPacking = {};	
				});
			}
			
		}
	}
	
	var saveSellerCategories = function(sellerId) {
		var categoryIds = $scope.retailer.list.returnOrPickupLocations.sellerCategories.selectedIds;
		if (categoryIds.length > 0) {
			for (var i=0; i < categoryIds.length; i++) {
				var obj = angular.copy($scope.retailer.list.returnOrPickupLocations.sellerCategories.template);
			  
				obj.categoryId = categoryIds[i];
				obj.retailerId = $state.current.data.params.id;
				obj.sellerId = sellerId;
				
				$scope.retailer.list.returnOrPickupLocations.sellerCategories.selectedObj.push(obj);
			}
			
			//Bulk Delete
			if ($scope.retailer.list.returnOrPickupLocations.sellerCategories.sellerCategoryIds.length > 0) {
                RetailerSellerCategories
					.bulkdelete($scope.retailer.list.returnOrPickupLocations.sellerCategories.sellerCategoryIds)
					.success(function(response) {
						console.log(response);
					});
            }
			
			//Bulk Create
			RetailerSellerCategories
				.bulkcreate($scope.retailer.list.returnOrPickupLocations.sellerCategories.selectedObj)
				.success(function(response) {
					console.log(response);
					initReturnPickupLocation();
					$scope.getAllRetailerReturnOrPickupLocation(1, $scope.pageSize);
				});
        }
	}
	
	$scope.saveServiceProviderLocation = function(retailer) {
		//Validate Retailer
		$scope.errors = {};
		
		$scope.retailer.serviceProvider.retailerId = $state.current.data.params.id;
		
		var hasErrors = hasErrorsServiceProviderLocation(retailer);
		
		if (!hasErrors) {
			var templateObj = $scope.retailer.serviceProvider;
			if (templateObj.id == undefined || templateObj.id == "" || templateObj.id == null) {
				delete(templateObj.id);
				if (templateObj.brandId == "") {
                    templateObj.brandId = null;
                }
				
				if (templateObj.subCategoryId == "") {
                    templateObj.subCategoryId = null;
                }
				//Continue with retailer return/pickup location create
				RetailerServiceProvider.create(templateObj).success(function(response) {
					console.log(response);
					$scope.loadStepData(12,"Service Provider");
					$scope.serviceProviderForm = {};
					$scope.retailer.serviceProvider = {
							id: null,
							retailerId: null,
							serviceProviderLocationId : null,
							categoryId: null,
							brandId: null,
							subCategoryId : null,
							cityId: null,
							city: null
						};
				});
			}
			else {
				if (templateObj.brandId == "") {
                    templateObj.brandId = null;
                }
				
				if (templateObj.subCategoryId == "") {
                    templateObj.subCategoryId = null;
                }
				//Continue with retailer return/pickup location Update
				RetailerServiceProvider.update(templateObj).success(function(response) {
					console.log(response);
					$scope.loadStepData(12,"Service Provider");
					$scope.retailer.serviceProvider = {
							id: null,
							retailerId: null,
							serviceProviderLocationId : null,
							categoryId: null,
							brandId: null,
							subCategoryId : null,
							cityId: null,
							city: null
						};
					$scope.serviceProviderForm = {};
				});
			}
			
		}
	}
	/*** Save Page Data Block **/
	
	/*** Delete Data **/
	var evtDeleteRetailerExchangePolicysProduct = $rootScope.$on('deleteRetailerExchangePolicysProduct', function(e, args) {
            $scope.deleteRetailerExchangePolicysProduct(args);
        });
	$scope.deleteRetailerExchangePolicysProduct = function(index) {
		var obj = $scope.retailer.list.exchangePolicyProducts.selectedObj[index];
		RetailerExchangePolicy
				.del(obj)
				.success(function(response) {
					if (response.response == true) {
                        $scope.retailer.list.exchangePolicyProducts.selectedObj.splice(index,1);
						if ($scope.retailer.list.exchangePolicyProducts.selectedObj.length == 0) {
							$scope.addMoreExchangePolicyProducts({},0);
						}
                    }
				});
	}
	/** Delete Functions **/
	var getRetailerExchangePolicyProducts = function(first, limit) {
		$scope.currentPage = first ;// initialize page no to 1
        //$scope.total_count = 100;
        first = $scope.calculateFirstRecord($scope.currentPage, limit);
		RetailerExchangePolicy
				.list({'query':{'retailerId' : $scope.retailer.retailer.id},firstResult:first, maxResults:limit})
				.success(function(response) {
					if (response !== undefined && response.data !== undefined && response.data.length > 0){
                        $scope.retailer.list.exchangePolicyProducts.selectedObj = response.data;
                        $scope.total_count = response.totalRecords;
                    }
					
					if ($scope.retailer.list.exchangePolicyProducts.selectedObj.length == 0) {
						$scope.addMoreExchangePolicyProducts({},0);
					}
				});
	}
	
	$scope.getAllRetailerReturnOrPickupLocation = function(first, limit) {
		$scope.currentPage = first ;// initialize page no to 1
        //$scope.total_count = 100;
        first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
		RetailerReturnOrPickupLocations
				.list({'query':{'retailerId' : $scope.retailer.retailer.id}, firstResult:first, maxResults:limit})
				.success(function(response) {
					if (response !== undefined && response.data !== undefined && response.data.length > 0){
                        $scope.retailer.list.returnOrPickupLocations.list = response.data;
                        $scope.total_count = response.totalRecords;
                    }
				});
	}
	
	var getRetailerReturnOrPickupLocationById = function(id) {
		RetailerReturnOrPickupLocations
				.get({'id':id})
				.success(function(response) {
					//console.log(response);
					$scope.retailer.list.returnOrPickupLocations.template = response;
					getRetailerSellerCategories(id);
				});
	}
	
	var getRetailerSellerCategories = function(sellerId) {
		RetailerSellerCategories
				.list({'query':{'retailerId' : $scope.retailer.retailer.id,
								'sellerId' : sellerId}})
				.success(function(response) {
					//console.log(response);
					$scope.retailer.list.returnOrPickupLocations.sellerCategories.list = response.data;
					for (var j=0; j<response.data.length; j++) {
						$scope.retailer.list.returnOrPickupLocations.sellerCategories.sellerCategoryIds.push(response.data[j].id);
						$scope.toggleReturnPickupCategoriesOffered(response.data[j].categoryId);
						//$scope.retailer.list.returnOrPickupLocations.list.selectedIds.push(response[j].categoryId);
					}
				});
	}
	
	$scope.getRetailerServiceProvider = function(serviceProvider) {
		$scope.retailer.serviceProvider = serviceProvider;
		$scope.populateServiceProviderLocation(serviceProvider.serviceProviderId,serviceProvider.cityId);
		$scope.populateServiceProviderCategory(serviceProvider);
		$scope.populateServiceProviderSubCategory(serviceProvider);
		$scope.retailer.serviceProvider = serviceProvider;
	}

	$scope.getRetailerProductPacking = function(productPacking) {
		$scope.id = productPacking.id;
		ProductPacking.get({'id':$scope.id}).success(function(response) {
					$scope.retailer.productPacking = response;
					console.log("$scope.retailer.productPacking",$scope.retailer.productPacking);	
				});
		
	}
	
	var initReturnPickupLocation = function() {
		$scope.retailer.list.returnOrPickupLocations = {
			list:[],
			template: {
				id : "",
				retailerId : "",
				sellerCode : "",
				sellerName : "",
				sellerLocation : "",
				sellerAddress : "",
				sellerPhoneNumber : "",
				sellerAlternatePhoneNumber : "",
				sellerEmail : "",
				sellerPincode : ""
			},
			errors: [],
			sellerCategories: {
				list: [],
				selectedObj: [],
				selectedIds: [],
				sellerCategoryIds : [],
				template: {
					retailerId : "",
					categoryId : "",
					sellerId: ""
				}
			}
		};
	}
	
	$scope.getRetailerReturnOrPickupLocation = function(location) {
		$scope.retailer.list.returnOrPickupLocations.template = location;
		getRetailerSellerCategories(location.id);
	}
	/*** Scope Function ***/
	
	
	$scope.saveRetailerExchangePolicy = function(retailer) {
		saveRetailerExchangePolicy(retailer);
	}
	
	$scope.saveReturnPickupLocation = function(retailer) {
		saveReturnOrPickupLocation(retailer);
		console.log(retailer);
	}
	
	$scope.populateSubCategoryList = function(categoryId) {
		populateSubCategoryList(categoryId);
	}
	
	$scope.showPage = function(stepId, stepName) {
		
		var s = $scope.steps[stepId-1];
		stepName = s.name;
		$scope.currentStep = stepId;
		$scope.currentStepName = stepName;
		$(".onboarding-steps").hide();
		$(".onboarding-steps:nth-of-type("+stepId+")").show();
		$(".list-group-item").removeClass('active');
		$(".list-group-item:nth-of-type("+stepId+")").addClass('active');
		
		//Load Step Data
		$scope.loadStepData(stepId,stepName);
	}
	
	//Load Page data
	$scope.loadStepData = function(stepId,stepName) {
		var s = $scope.steps[stepId-1];
		stepName = s.name;
		$scope.currentStep = stepId;
		$scope.currentStepName = stepName;
		
		switch (stepName) {
			case "Basic Info":
				$rootScope.$broadcast ('getRetailer',$state.current.data.params.id);
				break;
			
			case "Products":
				$rootScope.$broadcast ('getAllRetailerProducts',$state.current.data.params.id);
				break;
			
			case "Services Availed":
				$scope.retailer.list.serviceMaster.selectedIds = [];
				$scope.retailer.list.serviceMaster.selectedObj = [];
				$rootScope.$broadcast ('populateServicesListByRetailer');
				break;
			
			case "Categories Serviced":	
				$scope.retailer.list.productCategory.selectedIds = [];
				$scope.retailer.list.productCategory.selectedObj = [];
				$rootScope.$broadcast('populateMasterCategory');
				$rootScope.$broadcast('populateCategoriesByRetailer');
				break;
			
			case "Cost Per Service":				
				$scope.retailer.list.costPerService.selectedObj = [];
				$rootScope.$broadcast('populateRetailerAvailedServiceList');
				$rootScope.$broadcast('populateCategoriesServicedList');
				$rootScope.$broadcast('getRetailerServicesCost');
				break;
			
			case "Service Costing":
				$rootScope.$broadcast('populateRetailerCostingActivityList');
				break;
					
			case "Serviceable Locations":
				$rootScope.$broadcast('populateServiceableLocationsList');
				break;
				
			case "ODA Locations":
				$rootScope.$broadcast('populateOdaLocationsList');
				break;
			
			case "Packing Cost":
				$rootScope.$broadcast('populatePackagingType');
				$rootScope.$broadcast('getRetailerPackingCost');				
				break;
				
			case "Packing Specifications":
				$rootScope.$broadcast ('populateServicesListByRetailer');
				$rootScope.$broadcast('populateCategoriesByRetailer');
				$rootScope.$broadcast('getRetailerPackingSpecification');				
				break;
				
			case "Mapping - Process fields":
				$rootScope.$broadcast('getRetailerProcessField');				
				break;
			
			case "Packing Cost Borne":
				$rootScope.$broadcast('populateCategoriesServicedList');
				Utility.populate_brands({},$scope);
				$rootScope.$broadcast('getRetailerPackingCostBorne');				
				break;
			
			case "Advance Replacement":
				Utility.populate_products({retailerId:$state.current.data.params.id}, $scope);
				$rootScope.$broadcast('getRetailerAdvanceReplacementProducts');	
				break;
			
			case "Replacement Policy":
				Utility.populate_products({retailerId:$state.current.data.params.id}, $scope);
				getRetailerExchangePolicyProducts(0, MAX_DROPDOWN_SIZE);
				if ($scope.retailer.list.exchangePolicyProducts.selectedObj.length == 0) {
                    $scope.addMoreExchangePolicyProducts({},0);
                }
				break;
			
			case "Return/Pickup Locations":
				Utility.populate_city({}, $scope);
				initReturnPickupLocation();
				$rootScope.returnPickupLocations = true;
				$rootScope.authUpload = false;
				$rootScope.$broadcast('populateCategoriesByRetailer');
				$scope.getAllRetailerReturnOrPickupLocation(1,$scope.pageSize);
				break;
			
			case "Service Provider":
				$rootScope.showServiceProvierPincodes = false;
				$rootScope.showServiceProvider = true;
				$scope.populateServiceProviderList(0,$scope.pageSize);
				Utility.populate_city({},$scope);
				
				$scope.populateRetailerServiceProviderList(1,$scope.pageSize);
				break;

			case "Product Packing":
				Utility.populate_products({'retailerId':$state.current.data.params.id},$scope);
				Utility.populate_packging_material_type({},$scope);
				$scope.populateProductPackingList(1,$scope.pageSize);
				break;
			
			case "Cheque Collection Centers":
				$scope.populateChequeCollectionCenterCityList();
				$scope.getChequeCollectionCenterList(1,$scope.pageSize);
				break;
			
			case "Message":
				$scope.getFAQ();
				break;
			
			case "Other Settings":
				RetailerOther.list({'query':{'retailerId':$scope.retailer.retailer.id}, firstResult:0, maxResults: MAX_DROPDOWN_SIZE}	).success(function(response){
					if (response !== undefined && response.data !== undefined && response.data.length > 0){					
                        $scope.retailer.other = response.data[0];
                    }
				});
				break;
			
			case "Email Configuration":
				$rootScope.$broadcast ('getRetailerEmailConfiguration');
				break;
			
			case "SMS/Email Templates":
				$scope.$broadcast ('getSMSEmailTemplateList');
				break;
			
			case "SMS/Email Notification Settings":
				//This will call Child controller function
				$scope.$broadcast ('populateConfigList');
				break;
			
			case "Packing - Intra City":
				Utility.populate_products({retailerId:$state.current.data.params.id}, $scope);
				$rootScope.$broadcast ('getRetailerPackingIntraCity');
				break;
			
			case "Door Step Services":
				Utility.populate_brands({}, $scope);
				$rootScope.$broadcast ('getRetailerDoorStepServicesByBrand');
				break;
        }
	}
	
	$scope.openAttachPincodes = function(serviceProvider) {
		$rootScope.returnPickupLocations = false;
		$rootScope.showServiceProvierPincodes = true;
		$scope.retailerServiceProviderPincode = {};
		$scope.retailerServiceProviderPincode.serviceProvider = serviceProvider;
		$scope.populateServiceProviderPincodeList(1,$scope.pageSize, serviceProvider.serviceProviderId)
		console.log("serviceProvider",serviceProvider);
	}
	
	$scope.openAuthUpload = function(location) {
		$rootScope.returnPickupLocations = false;
		$rootScope.authUpload = true;
		$scope.pickupLocation = location;
	}
	
	$scope.uploadAuth = function(authFile) {
		$rootScope.returnPickupLocations = false;
		$rootScope.authUpload = true;
		var id = $state.current.data.params.id;
		var params = { data: {key1:'services',key2:'AuthLetter',key3:id }, file: authFile.file };
		// var params = { data: {processId:'5644331'}, file: authFile.file };

		console.log("var userLocId = "+$state.current.data.params.id);
		console.log("Auth File "+authFile.file);
		// console.log("PickupLocation " + $scope.pickupLocation.sellerLocation + " " + $scope.pickupLocation.sellerName);
		// RetailerServices.uploadAuth(params)
			TasksService.uploadAuthLetterRetailer(params)//data, status, headers, config
				.progress(function (evt) {
						var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
						$scope.uploadPercentage = progressPercentage;
					})
					.success(function (data, status, headers, config) {
						if (status == 200 && data.success === true) {
							notification.success("Successfully uploaded Auth Letter!");
						}

						if (data.errorMessages !== undefined && data.errorMessages.length > 0) {
							notification.error("Unable To Upload");
						}
						$scope.authFile = {};
						$scope.uploadPercentage = 0;
					});
	}
	
	$scope.populateServiceProviderPincodeList = function(first, limit, serviceProviderId) {
		$scope.currentPage = first // initialize page no to 1
			
		first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
		var params = {query:{retailerId:$scope.retailer.retailer.id,retailerServiceProviderId:serviceProviderId},
		firstResult: first, maxResults: limit}
		$scope.retailerServiceProvoderPincodeList = {};
		RetailerServiceProviderPincode
				.list(params)
				.success(function(response){
					if (response !== undefined && response.data !== undefined && response.data.length > 0){
						$scope.retailerServiceProvoderPincodeList = response.data;
						$scope.total_count = response.totalRecords;
					}
                   
				});
	}
	
	$scope.saveRetailerServiceProviderPincode = function(retailerServiceProviderPincode) {
		var findComma = retailerServiceProviderPincode.pincode.indexOf(",");
		$scope.retailerServiceProviderPincode.errors ={};
		var pincodeTemplate = {
			id : "",
			retailerId: $scope.retailer.retailer.id,
			retailerServiceProviderId: "",
			pincode: ""
		}
		var pincodeList = [];
		if (findComma !== -1) {
			
            var pincodes = retailerServiceProviderPincode.pincode.split(",");
			
			for(var i in pincodes) {
				var p = angular.copy(pincodeTemplate);
				delete(p.id);
				if (pincodes[i] !== "" && pincodes[i].length === 6) {
                    p.pincode = pincodes[i];
					p.retailerId = $scope.retailer.retailer.id;
					p.retailerServiceProviderId = retailerServiceProviderPincode.serviceProvider.serviceProviderId;
					
					pincodeList.push(p);
                }
				
				if (pincodes[i] != "" && pincodes[i].length !== 6) {
					$scope.retailerServiceProviderPincode.errors.pincode = "Data contains invalid pincodes!. Pincode should be 6 characters long."
					  setTimeout(function() {
						$scope.retailerServiceProviderPincode.errors.pincode = "";
					  },3000);
                }
			}
			
			if (pincodeList.length > 0) {
                //Save data in DBs
				RetailerServiceProviderPincode
					.bulkcreate(pincodeList)
					.success(function(response){
						if (response && response.response) {
							$rootScope.$broadcast ('showSuccessMessage',response);
							$scope.retailerServiceProviderPincode.pincode = "";
							$scope.populateServiceProviderPincodeList(1, $scope.pageSize, retailerServiceProviderPincode.serviceProvider.serviceProviderId);
						}
					});
            }
			
        }
		else {
			var p = angular.copy(pincodeTemplate);
			p.pincode = retailerServiceProviderPincode.pincode;
			p.retailerId = $scope.retailer.retailer.id;
			p.retailerServiceProviderId = retailerServiceProviderPincode.serviceProvider.serviceProviderId;
			delete(p.id);
			
			if (p.pincode === "" || p.pincode.length !== 6) {
				$scope.retailerServiceProviderPincode.errors.pincode = "Data contains invalid pincodes!. Pincode should be 6 characters long."
					  setTimeout(function() {
						$scope.retailerServiceProviderPincode.errors.pincode = "";
					  },3000);
				return;
			}
			
			$scope.retailerServiceProviderPincode.pincode = null;
			
			//Save data in DB
			RetailerServiceProviderPincode
				.create(p)
				.success(function(response){
                    if (response && response.response.id) {
                        $rootScope.$broadcast ('showSuccessMessage',response);
                        $scope.populateServiceProviderPincodeList(1, $scope.pageSize, retailerServiceProviderPincode.serviceProvider.serviceProviderId);
                    }
				});
		}
	}
	
	$scope.toggleReturnPickupCategoriesOffered = function(id) {
		console.log(id);
		var idx = $scope.retailer.list.returnOrPickupLocations.sellerCategories.selectedIds.indexOf(id);
		// is currently selected
		if (idx > -1) {
		  $scope.retailer.list.returnOrPickupLocations.sellerCategories.selectedIds.splice(idx, 1);
		}	
		// is newly selected
		else {
		  $scope.retailer.list.returnOrPickupLocations.sellerCategories.selectedIds.push(id);		  
		}
		console.log($scope.retailer.list.returnOrPickupLocations.sellerCategories.selectedIds);
	}
	
	
	
	
	
	$scope.addMoreExchangePolicyProducts = function(arProduct, index) {
		if($scope.retailer.list.exchangePolicyProducts.selectedObj.length > 0) {
			if (arProduct !== undefined && arProduct.categoryId !== undefined) {
				$scope.retailer.list.exchangePolicyProducts.selectedObj[index] = arProduct;
            }
		}
		$scope.retailer.list.exchangePolicyProducts.selectedObj
			.push(angular.copy($scope.retailer.list.exchangePolicyProducts.template) );
	}
	$scope.addMoreExchangePolicyProducts($scope.arProduct,0);
	
	$scope.deleteExchangePolicyProduct = function(index) {
		var obj = $scope.retailer.list.exchangePolicyProducts.selectedObj[index];
		
		//Delete Object
		//Delete from DB
		if (obj !== undefined && obj.id !== null && obj.id !== undefined) {
			$rootScope.$broadcast ('deleteDialogAndConfirmEvent',"Are you sure want to delete retailer e product?",
								   'deleteRetailerExchangePolicysProduct', index);
			return;
        }
		$scope.retailer.list.exchangePolicyProducts.selectedObj.splice(index,1);
	}
	
	
	
	$scope.saveRetailerOther = function(retailer) {
		retailer.other.retailerId = retailer.retailer.id;
		if (retailer.other.id == '' || retailer.other.id == undefined || retailer.other.id == null) {
			delete(retailer.other.id);
            RetailerOther.create(retailer.other).success(function(response){
				
			});
        }
		else {
			RetailerOther.update(retailer.other).success(function(response){
				
			});
		}
		
	}
	
	$scope.populateChequeCollectionCenterCityList = function() {
		$scope.retailer.chequeCollectionCenterCityList = [];
		RetailerChequeCollectionCenterCities
			.populate_city({'retailerId':$scope.retailer.retailer.id})
			.success(function(response){
				$scope.retailer.chequeCollectionCenterCityList = response;
			});
	}
	
	$scope.getChequeCollectionCenterList = function(first, limit) {
		$scope.retailer.chequeCollectionCenterList = [];
		$scope.currentPage = first ;// initialize page no to 1
        first = $scope.calculateFirstRecord($scope.currentPage, limit);
		RetailerChequeCollectionCenters
			.list({'query':{'retailerId':$scope.retailer.retailer.id}, firstResult: first, maxResults: limit})
			.success(function(response){
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.total_count = response.totalRecords;
                   
					$scope.retailer.chequeCollectionCenterList = response.data;
					for( var i = 0; i < $scope.retailer.chequeCollectionCenterList.length; i++) {					
						$scope.getRetailerChequeCollectionCenterCities(i);
					}
				}
			});
	}
	
	$scope.getRetailerChequeCollectionCenterCities  = function(index) {
		var item = $scope.retailer.chequeCollectionCenterList[index];
		$scope.retailer.chequeCollectionCenterList[index].cities = [];
		RetailerChequeCollectionCenterCities
			.list({query:{chequeCollectionCenterId:item.id}, firstResult:0, maxResults:MAX_DROPDOWN_SIZE})
			.success(function(cities) {
				if (cities !== undefined && cities.data !== undefined && cities.data.length > 0){
					$scope.retailer.chequeCollectionCenterList[index].cities = cities.data;
				}
			});
	}
	
	$scope.getRetailerChequeCollectionCenter = function(collectionCenter) {
		
		$scope.retailer.chequeCollectionCenter = {};
		$scope.populateChequeCollectionCenterCityList();
		RetailerChequeCollectionCenters
			.get({'id':collectionCenter.id})
			.success(function(response){
				$scope.retailer.chequeCollectionCenter = response;
				$scope.retailer.chequeCollectionCenter.pincode = parseInt(response.pincode);
				$scope.retailer.chequeCollectionCenter.phoneNumber = parseInt(response.phoneNumber);
				$scope.retailer.chequeCollectionCenter.alternatePhoneNumber = parseInt(response.alternatePhoneNumber);
			});
		
		var cityIds = [], cityPrimaryIds = [];
		RetailerChequeCollectionCenterCities
			.list({query:{chequeCollectionCenterId : collectionCenter.id}, firstResult:0, maxResults:MAX_DROPDOWN_SIZE})
			.success(function(response){
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
                    if (response.data.length > 0) {
                        for(var i = 0; i < response.data.length; i ++) {
							cityIds.push(response.data[i].cityId);
							cityPrimaryIds.push(response.data[i].id);
							$scope.retailer.chequeCollectionCenterCityList.push({cityId:response.data[i].cityId,cityName:response.data[i].cityName})
						}
						$scope.retailer.chequeCollectionCenter.cityId = cityIds;
						$scope.retailer.chequeCollectionCenter.cityPrimaryIds = cityPrimaryIds;
                    }
                }
			});
	}
	
	$scope.saveChequeCollectionCenter = function(retailer) {
		var chequeCollectionCenter = angular.copy(retailer.chequeCollectionCenter);
		chequeCollectionCenter.retailerId = retailer.retailer.id;
		
		var chequeCollectionCityIds = chequeCollectionCenter.cityId;
		var chequeCollectionCenterCityPrimaryIds = chequeCollectionCenter.cityPrimaryIds;
		delete(chequeCollectionCenter.cityId);
		
		if (chequeCollectionCenter.id == '' || chequeCollectionCenter.id == undefined || chequeCollectionCenter.id == null) {
			RetailerChequeCollectionCenters
				.create(chequeCollectionCenter)
				.success(function(response) {
					console.log(response);
					if (response) {
                        response = response.response;
                    }
					
					if (response.id != undefined) {
						$scope.saveChequeCollectionCenterCities(chequeCollectionCityIds, response, chequeCollectionCenterCityPrimaryIds);
						$scope.resetCollectionCenterForm();
						$scope.getChequeCollectionCenterList();
                    }
					else {
						notification.error("Error in saving data!");
					}
					
			});
        }
		else {
			delete(chequeCollectionCenter.cityPrimaryIds);
			RetailerChequeCollectionCenters
				.update(chequeCollectionCenter)
				.success(function(response){
					console.log(response);
					if (response) {
                        response = response.response;
                    }
					//$scope.retailer.chequeCollectionCenter = response;
					
					$scope.saveChequeCollectionCenterCities(chequeCollectionCityIds, response, chequeCollectionCenterCityPrimaryIds);
					$scope.resetCollectionCenterForm();
					$scope.getChequeCollectionCenterList();
			});
		}
	}
	
	$scope.deleteChequeCollectionCenterCities = function(cityPrimaryIds) {
		RetailerChequeCollectionCenterCities
			.bulkdelete(cityPrimaryIds)
			.success(function(response){
				
			});
	}
	
	$scope.deleteRetailerChequeCollectionCenter = function(cillectionCenter) {
		
	}
	
	$scope.resetCollectionCenterForm = function() {
		
		$scope.chequeCollectionform.$setPristine();
		$scope.chequeCollectionform.$setUntouched();
		$scope.chequeCollectionform.$valid = true;
		$scope.retailer.chequeCollectionCenter = {};
		$scope.populateChequeCollectionCenterCityList();
	}
	
	$scope.saveChequeCollectionCenterCities = function(chequeCollectionCityIds, response, chequeCollectionCenterCityPrimaryIds) {
		var city = {"retailerId": $scope.retailer.retailer.id,"chequeCollectionCenterId": "","cityId": ""};
		var cities = [];
		for(var i = 0; i < chequeCollectionCityIds.length; i++) {
			var c = angular.copy(city);
			c.chequeCollectionCenterId = response.id;
			c.cityId = chequeCollectionCityIds[i];			
			c.cityName = Utils.get_selected_name($scope.retailer.chequeCollectionCenterCityList,c.cityId,'cityId','cityName');
			
			cities.push(c);
		}
		
		if (chequeCollectionCenterCityPrimaryIds != undefined && chequeCollectionCenterCityPrimaryIds.length > 0) {
			$scope.deleteChequeCollectionCenterCities(chequeCollectionCenterCityPrimaryIds);
		}
		
		RetailerChequeCollectionCenterCities
			.bulkcreate(cities)
			.success(function(response){
				if (response.response == "") {
                    notification.error("Error! Could not save cheque collection center cities.");
					return;
                }
			});
	}
	
	$scope.retailer.faq = {};
	$scope.saveRetailerFAQ = function(retailer) {
		var rfaq = {};
		rfaq.retailerId = $scope.retailer.retailer.id;
		rfaq.faq = retailer.faq.faq;
		rfaq.id = retailer.faq.id;
		
		if (rfaq.id == '' || rfaq.id == undefined || rfaq.id == null) {
            RetailerFAQs
				.create(rfaq)
				.success(function(response) {
					$scope.getFAQ();
				});
        }
		else {
			RetailerFAQs
				.update(rfaq)
				.success(function(response) {
					$scope.getFAQ();
				});
		}
		
	}

	$scope.openPopUp = function(){
		console.log("Inside PopUp Method");
	}
	
	$scope.getFAQ = function() {
		RetailerFAQs
			.list({query:{retailerId: $scope.retailer.retailer.id}})
			.success(function(response) {
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.retailer.faq = response.data[0];
				}
			});
	}
	
	if ($state.current.url == '/retailer/get/:id' || $state.current.url == '/retailer/onboarding') {
		//Show default page
		$scope.showPage(1);		
		$scope.breadcrumbs.push({'title' : $state.current.data.page, link: $state.current.url});
	}
	
	$scope.$on('$destroy', function() {
        evtDeleteRetailerExchangePolicysProduct();
    });
}]);