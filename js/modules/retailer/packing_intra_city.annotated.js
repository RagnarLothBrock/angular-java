'use strict';
app.controller('RetailerPackingIntraCityController', 
               ['$scope', '$rootScope', 'RestHTTPService', 'API', '$state', 'LIST_PAGE_SIZE',
                function($scope, $rootScope, RestHTTPService, API, $state, LIST_PAGE_SIZE){
    $scope.errors = {};
    $scope.pageSize = LIST_PAGE_SIZE;
    $scope.total_count = 0;
    $scope.currentPage = 1;
    
    $scope.saveRetailerPackingIntraCity = function(retailerPackingIntraCity) {
        if (retailerPackingIntraCity === undefined || retailerPackingIntraCity.productId === undefined) {
            $scope.errors.productId = "Please select a product to continue!"
            return false;
        }
        
        retailerPackingIntraCity.retailerId = $state.current.data.params.id;
        
        RestHTTPService
            .post(API.RETAILER.RETAILER_PACKING_INTRA_CITY.CREATE, {'retailerPackingIntraCity':retailerPackingIntraCity})
            .success(function(response) {
                $scope.populateRetailerPackingIntraCityList(1, $scope.pageSize);
            });
    }
    
    $scope.populateRetailerPackingIntraCityList = function(first, limit) {
		$scope.currentPage = first;
		first = $rootScope.calculateFirstRecord(first, limit);
        
        var params = {
            'query': {
                'retailerId':$state.current.data.params.id
            },
            'firstResult':first,
            'maxResults': limit
        }
        
        var apiUrl = API.RETAILER.RETAILER_PACKING_INTRA_CITY.PAGINATION
                        .replace('<retailerId>', $state.current.data.params.id)
                        .replace('<firstResult>', first)
                        .replace('<maxResults>', limit);
		RestHTTPService
            .post(apiUrl,params)
            .success(function(response) {
			if (response !== undefined && response.data !== undefined && response.data.length > 0){
				$scope.retailerPackingIntraCityList = response.data;
				$scope.total_count = response.totalRecords;
			}
		});
	}
    
    $scope.deleteRetailerPackingIntraCityProduct = function(index, packingIntraCity) {
        var param = {id: packingIntraCity.id};
        RestHTTPService
            .del(API.RETAILER.RETAILER_PACKING_INTRA_CITY.DELETE,param,'id')
            .success(function(response) {
			$scope.populateRetailerPackingIntraCityList(1, $scope.pageSize);
		});
    }
    
    
    var evtGetRetailerPackingIntraCity = $rootScope.$on('getRetailerPackingIntraCity', function(){
        $scope.populateRetailerPackingIntraCityList(1, $scope.pageSize);
    });
    
    $scope.$on('$destroy', function() {
        evtGetRetailerPackingIntraCity();
    });
}]); 