'use strict';
app.controller('RetailerAdvanceReplacementController',[
                '$scope', '$rootScope', '$state', 'notification', 'RetailerAdvReplacementProducts', 'ngDialog', 'MAX_DROPDOWN_SIZE',
function(   $scope, $rootScope, $state, notification, RetailerAdvReplacementProducts, ngDialog, MAX_DROPDOWN_SIZE
){
    $scope.pageSize = MAX_DROPDOWN_SIZE;
    $scope.total_count = 0;
    $scope.currentPage = 1;
    
    $scope.getRetailerAdvanceReplacementProducts = function(first, limit) {
        $scope.currentPage = first ;// initialize page no to 1
        //$scope.total_count = 100;
        first = $scope.calculateFirstRecord($scope.currentPage, limit);
		RetailerAdvReplacementProducts
				.list({'query':{'retailerId' : $scope.retailer.retailer.id}, firstResult:first, maxResults:limit})
				.success(function(response) {
					if (response !== undefined && response.data !== undefined && response.data.length > 0){
                        $scope.retailer.list.advanceReplacementProducts.selectedObj = response.data;
                        $scope.total_count = response.totalRecords;
                    }
					if ($scope.retailer.list.advanceReplacementProducts.selectedObj.length == 0) {
						$scope.addMoreAdvanceReplacementProducts({},0);
					}
				});
	}
    
    $scope.addMoreAdvanceReplacementProducts = function(arProduct, index) {
        
		if($scope.retailer.list.advanceReplacementProducts.selectedObj.length > 0) {
			if (arProduct !== undefined && arProduct.categoryId !== undefined) {
				$scope.retailer.list.advanceReplacementProducts.selectedObj[index] = arProduct;
            }
		}
        
		$scope.retailer.list.advanceReplacementProducts.selectedObj
			.push(angular.copy($scope.retailer.list.advanceReplacementProducts.template) );
        console.log($scope.retailer.list.advanceReplacementProducts.selectedObj);
	}
    
   $scope.deleteRetailerAdvanceReplacementProduct = function(index) {
        var obj = $scope.retailer.list.advanceReplacementProducts.selectedObj[index];
        RetailerAdvReplacementProducts
                .del(obj)
                .success(function(response) {
                    if (response && response.response == true) {
                        $scope.retailer.list.advanceReplacementProducts.selectedObj.splice(index,1);
                        if ($scope.retailer.list.advanceReplacementProducts.selectedObj.length == 0) {
                            $scope.addMoreAdvanceReplacementProducts({},0);
                        }
                    }
                });
    }
	
	$scope.deleteAdvanceReplacementProduct = function(index) {
		var obj = $scope.retailer.list.advanceReplacementProducts.selectedObj[index];
		
		//Delete Object
		//Delete from DB
		if (obj !== undefined && obj.id !== null && obj.id !== undefined) {
            var msg = "Are you sure want to delete retailer advance replacement product?";
            var callback = 'deleteRetailerAdvanceReplacementProduct';
            var dataToDelete = index;
            ngDialog.openConfirm({
                template: '<div class="dialog-contents">' +
                                msg + '<br/><br/>' +
                                '<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
                                '<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
                            '</div>',
                controller: ['$scope', function($scope) { 
                  // Controller logic here
                  console.log($scope);
                     
                }]
            }).then(function (success) {
                console.log($scope);
                $scope.deleteRetailerAdvanceReplacementProduct(dataToDelete);
            }, function (error) {
                // Error logic here
            });
//			$rootScope.$broadcast ('deleteDialogAndConfirm',
//                                   "Are you sure want to delete retailer advance replacement product?",
//								   'deleteRetailerAdvanceReplacementProduct', index);
			return;
        }
		
	}
    
    var hasErrorsAdvanceReplacementProducts = function(retailer) {
		$scope.retailer.list.advanceReplacementProducts.bulkCreate = [],
		$scope.retailer.list.advanceReplacementProducts.bulkUpdate = [];
		$scope.retailer.list.advanceReplacementProducts.errors = [];
		var selectedObj = $.unique(retailer.list.advanceReplacementProducts.selectedObj);
		for(var i = 0; i < selectedObj.length; i++) {
			if (selectedObj[i] === undefined) {
                continue;
            }
			var e = {};
			if (selectedObj[i].productId == null) {
                e.productId = "Please select a product";
            }
			if (e.productId !== undefined ) {//|| e.categoryId !== undefined || e.subCategoryId !== undefined) {
                $scope.retailer.list.advanceReplacementProducts.errors[i] = e;
            }
			
			$scope.retailer.list.advanceReplacementProducts.selectedObj[i].retailerId = $scope.retailer.retailer.id;
			
			var item = angular.copy(selectedObj[i]);
			
			if (selectedObj[i].id == null) {
                $scope.retailer.list.advanceReplacementProducts.bulkCreate.push(item);
            }
			else {
				$scope.retailer.list.advanceReplacementProducts.bulkUpdate.push(item);
			}
		}
		
		if ($scope.retailer.list.advanceReplacementProducts.errors.length > 0) {
			return true;
		}
		return false;
	}
    
    //Save retailer advance replacement product
	$scope.saveRetailerAdvanceReplacement = function(retailer) {
		//Validate Retailer
		$scope.retailer.list.advanceReplacementProducts.errors = [];
		
		var hasErrors = hasErrorsAdvanceReplacementProducts(retailer);
		
		if (!hasErrors) {
			//$scope.retailer.list.advanceReplacementProducts.selectedObj = [];
			if ($scope.retailer.list.advanceReplacementProducts.bulkUpdate.length > 0) {
                //Save Details
				RetailerAdvReplacementProducts
					.bulkupdate($scope.retailer.list.advanceReplacementProducts.bulkUpdate)
					.success(function(response) {
					});
            }
			
			if ($scope.retailer.list.advanceReplacementProducts.bulkCreate.length > 0) {
                //Save Details
				RetailerAdvReplacementProducts
					.bulkcreate($scope.retailer.list.advanceReplacementProducts.bulkCreate)
					.success(function(response) {
						$scope.getRetailerAdvanceReplacementProducts();
					});
            }
        }
	}
    
    var evtGetRetailerAdvanceReplacementProducts = $rootScope.$on('getRetailerAdvanceReplacementProducts', function(e, args) {
        $scope.getRetailerAdvanceReplacementProducts(1, $scope.pageSize);
        
        if ($scope.retailer.list.advanceReplacementProducts.selectedObj.length == 0) {
            $scope.addMoreAdvanceReplacementProducts({},0);
        }
    });
    
    $scope.$on('$destroy', function() {
        evtGetRetailerAdvanceReplacementProducts();
    });
    
}]);