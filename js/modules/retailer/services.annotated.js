'use strict';

app.factory('Retailer', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.ADD, {'retailer':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.GET.replace('<retailerid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.UPDATE, {'retailer':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.DELETE.replace('<retailerid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		}
	}
}]);

app.factory('RetailerServices', ["$http", "REST_API_URL", "API", "Upload", function($http, REST_API_URL, API,Upload) {

	return {
		uploadAuth:function(params) {
			console.log(params);
			return Upload.upload({
				 url : REST_API_URL + API.RETAILER.RETAILER_SERVICES.UPLOADAUTH,
				 data: params.data,
				 file: params.file});
		},
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICES.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICES.CREATE, {'retailerServices':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.RETAILER_SERVICES.GET.replace('<retailerServiceId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICES.UPDATE, {'retailerServices':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.RETAILER_SERVICES.DELETE.replace('<retailerServiceId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		bulkcreate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICES.BULK_CREATE, {'retailerServicesList':params});
		},
		bulkdelete:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICES.BULK_DELETE, {'retailerServicesIdList':params});
		},
		customServiceQuery: function(params) {
			var url = API.RETAILER.RETAILER_SERVICES.CUSTOM_QUERY.SERVICE.replace('<retailerid>', params.id);
			return $http.post(REST_API_URL + url, {});
		}
	}
}]);

app.factory('RetailerCategories', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_CATEGORY.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_CATEGORY.CREATE, {'RetailerServiceableLocations':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.RETAILER_CATEGORY.GET.replace('<retailerCategoryId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_CATEGORY.UPDATE, {'RetailerServiceableLocations':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.RETAILER_CATEGORY.DELETE.replace('<retailerCategoryId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		bulkcreate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_CATEGORY.BULK_CREATE, {'retailerCategoriesList':params});
		},
		bulkdelete:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_CATEGORY.BULK_DELETE, {'retailerCategoriesIdList':params});
		},
		customCategoryQuery: function(params) {
			var url = API.RETAILER.RETAILER_CATEGORY.CUSTOM_QUERY.CATEGORY.replace('<retailerid>', params.id);
			return $http.post(REST_API_URL + url, {});
		}
	}
}]);

app.factory('RetailerServicesCost', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICES_COST.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICES_COST.CREATE, {'RetailerServicesCost':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.RETAILER_SERVICES_COST.GET.replace('<retailerservicecostid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICES_COST.UPDATE, {'RetailerServicesCost':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.RETAILER_SERVICES_COST.DELETE.replace('<retailerservicecostid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		bulkcreate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICES_COST.BULK_CREATE, {'retailerServicesCostList':params});
		},
		bulkupdate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICES_COST.BULK_UPDATE, {'retailerServicesCostList':params});
		},
		bulkdelete:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICES_COST.BULK_DELETE, {'retailerServicesCostIdList':params});
		}
	}
}]);

app.factory('RetailerServiceableLocations', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICEABLE_LOCATIONS.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICEABLE_LOCATIONS.CREATE,
                              {'retailerServiceableLocations':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.RETAILER_SERVICEABLE_LOCATIONS.GET.replace('<retailerserviceablelocationid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICEABLE_LOCATIONS.UPDATE,
                              {'retailerServiceableLocations':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.RETAILER_SERVICEABLE_LOCATIONS.DELETE.replace('<retailerserviceablelocationid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		bulkcreate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICEABLE_LOCATIONS.BULK_CREATE,
                              {'retailerServiceableLocationsList':params});
		},
		bulkdelete:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICEABLE_LOCATIONS.BULK_DELETE,
                              {'retailerServiceableLocationsIdList':params});
		}
	}
}]);

app.factory('RetailerOdaLocations', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_ODA_LOCATIONS.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_ODA_LOCATIONS.CREATE,
                              {'retailerOdaLocations':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.RETAILER_ODA_LOCATIONS.GET.replace('<retailerserviceablelocationid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_ODA_LOCATIONS.UPDATE,
                              {'retailerOdaLocations':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.RETAILER_ODA_LOCATIONS.DELETE.replace('<retailerserviceablelocationid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		bulkcreate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_ODA_LOCATIONS.BULK_CREATE,
                              {'retailerOdaLocationsList':params});
		},
		bulkdelete:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_ODA_LOCATIONS.BULK_DELETE,
                              {'retailerOdaLocationsIdList':params});
		}
	}
}]);

app.factory('RetailerPackingCost', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PACKING_COST.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PACKING_COST.CREATE, {'RetailerPackingCost':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.RETAILER_PACKING_COST.GET.replace('<retailerpackingcostid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PACKING_COST.UPDATE, {'RetailerPackingCost':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.RETAILER_PACKING_COST.DELETE.replace('<retailerpackingcostid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		bulkcreate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PACKING_COST.BULK_CREATE, {'retailerPackingCostList':params});
		},
		bulkupdate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PACKING_COST.BULK_UPDATE, {'retailerPackingCostList':params});
		},
		bulkdelete:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PACKING_COST.BULK_DELETE, {'retailerPackingCostIdList':params});
		}
	}
}]);

app.factory('RetailerProcessField', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PROCESS_FIELD.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PROCESS_FIELD.CREATE, {'retailerProcessField':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.RETAILER_PROCESS_FIELD.GET.replace('<retailerpackingcostid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PROCESS_FIELD.UPDATE, {'retailerProcessField':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.RETAILER_PROCESS_FIELD.DELETE.replace('<retailerpackingcostid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		bulkcreate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PROCESS_FIELD.BULK_CREATE, {'retailerProcessFieldList':params});
		},
		bulkupdate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PROCESS_FIELD.BULK_UPDATE, {'retailerProcessFieldList':params});
		},
		bulkdelete:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PROCESS_FIELD.BULK_DELETE, {'retailerProcessFieldListIdList':params});
		}
	}
}]);

app.factory('RetailerPackingSpecification', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PACKING_SPECIFICATION.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PACKING_SPECIFICATION.CREATE, {'retailerPackingSpecification':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.RETAILER_PACKING_SPECIFICATION.GET.replace('<retailerpackingcostid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PACKING_SPECIFICATION.UPDATE, {'retailerPackingSpecification':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.RETAILER_PACKING_SPECIFICATION.DELETE.replace('<retailerpackingcostid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		bulkcreate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PACKING_SPECIFICATION.BULK_CREATE, {'retailerPackingSpecificationList':params});
		},
		bulkupdate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PACKING_SPECIFICATION.BULK_UPDATE, {'retailerPackingSpecificationList':params});
		},
		bulkdelete:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PACKING_SPECIFICATION.BULK_DELETE, {'retailerPackingSpecificationIdList':params});
		}
	}
}]);

app.factory('RetailerPaysPackingCost', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PACKING_COST_BORNE.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PACKING_COST_BORNE.CREATE, {'RetailerPaysPackingCost':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.RETAILER_PACKING_COST_BORNE.GET.replace('<retailerpackingcostborneid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PACKING_COST_BORNE.UPDATE, {'RetailerPaysPackingCost':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.RETAILER_PACKING_COST_BORNE.DELETE.replace('<retailerpackingcostborneid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		bulkcreate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PACKING_COST_BORNE.BULK_CREATE, {'retailerPaysPackingCostList':params});
		},
		bulkupdate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PACKING_COST_BORNE.BULK_UPDATE, {'retailerPaysPackingCostList':params});
		},
		bulkdelete:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_PACKING_COST_BORNE.BULK_DELETE, {'retailerPaysPackingCostIdList':params});
		}
	}
}]);

app.factory('RetailerAdvReplacementProducts', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_ADVANCE_REPLACEMENT_PRODUCTS.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_ADVANCE_REPLACEMENT_PRODUCTS.CREATE, {'RetailerAdvReplacementProducts':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.RETAILER_ADVANCE_REPLACEMENT_PRODUCTS.GET.replace('<advReplacementId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_ADVANCE_REPLACEMENT_PRODUCTS.UPDATE, {'RetailerAdvReplacementProducts':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.RETAILER_ADVANCE_REPLACEMENT_PRODUCTS.DELETE.replace('<advReplacementId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		bulkcreate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_ADVANCE_REPLACEMENT_PRODUCTS.BULK_CREATE, {'retailerAdvReplacementProductsList':params});
		},
		bulkupdate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_ADVANCE_REPLACEMENT_PRODUCTS.BULK_UPDATE, {'retailerAdvReplacementProductsList':params});
		},
		bulkdelete:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_ADVANCE_REPLACEMENT_PRODUCTS.BULK_DELETE, {'retailerAdvReplacementProductsIdList':params});
		}
	}
}]);

app.factory('RetailerExchangePolicy', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_EXCHANGE_POLICY.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_EXCHANGE_POLICY.CREATE, {'RetailerExchangePolicy':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.RETAILER_EXCHANGE_POLICY.GET.replace('<exchangePolicyId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_EXCHANGE_POLICY.UPDATE, {'RetailerExchangePolicy':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.RETAILER_EXCHANGE_POLICY.DELETE.replace('<exchangePolicyId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		bulkcreate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_EXCHANGE_POLICY.BULK_CREATE, {'retailerExchangePolicyList':params});
		},
		bulkupdate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_EXCHANGE_POLICY.BULK_UPDATE, {'retailerExchangePolicyList':params});
		},
		bulkdelete:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_EXCHANGE_POLICY.BULK_DELETE, {'retailerExchangePolicyIdList':params});
		}
	}
}]);
app.factory('RetailerReturnOrPickupLocations', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_RETURN_OR_PICKUP_LOCATIONS.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_RETURN_OR_PICKUP_LOCATIONS.CREATE, {'retailerReturnOrPickupLocations':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.RETAILER_RETURN_OR_PICKUP_LOCATIONS.GET.replace('<RetailerReturnOrPickupLocationsId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_RETURN_OR_PICKUP_LOCATIONS.UPDATE, {'retailerReturnOrPickupLocations':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.RETAILER_RETURN_OR_PICKUP_LOCATIONS.DELETE.replace('<RetailerReturnOrPickupLocationsId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		bulkcreate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_RETURN_OR_PICKUP_LOCATIONS.BULK_CREATE, {'RetailerReturnOrPickupLocationsList':params});
		},
		bulkupdate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_RETURN_OR_PICKUP_LOCATIONS.BULK_UPDATE, {'RetailerReturnOrPickupLocationsList':params});
		},
		bulkdelete:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_RETURN_OR_PICKUP_LOCATIONS.BULK_DELETE, {'RetailerReturnOrPickupLocationsIdList':params});
		}
	}
}]);
app.factory('RetailerSellerCategories', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SELLER_CATEGORIES.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SELLER_CATEGORIES.CREATE, {'RetailerSellerCategories':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.RETAILER_SELLER_CATEGORIES.GET.replace('<RetailerSellerCategoriesId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SELLER_CATEGORIES.UPDATE, {'RetailerSellerCategories':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.RETAILER_SELLER_CATEGORIES.DELETE.replace('<RetailerSellerCategoriesId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		bulkcreate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SELLER_CATEGORIES.BULK_CREATE, {'retailerSellerCategoriesList':params});
		},
		bulkdelete:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SELLER_CATEGORIES.BULK_DELETE, {'retailerSellerCategoriesIdList':params});
		}
	}
}]);

app.factory('RetailerServiceProvider', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICE_PROVIDER.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICE_PROVIDER.CREATE, {'retailerServiceProvider':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.RETAILER_SERVICE_PROVIDER.GET.replace('<RetailerServiceProviderId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICE_PROVIDER.UPDATE, {'retailerServiceProvider':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.RETAILER_SERVICE_PROVIDER.DELETE.replace('<RetailerServiceProviderId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		bulkcreate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICE_PROVIDER.BULK_CREATE, {'retailerServiceProviderList':params});
		},
		bulkdelete:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICE_PROVIDER.BULK_DELETE, {'retailerServiceProviderIdList':params});
		},
		by_retailer:function(params) {
			var getUrl = API.RETAILER.RETAILER_SERVICE_PROVIDER.BY_RETAILER
							.replace('<retailerId>', params.retailerId)
							.replace('<firstResult>', params.firstResult)
							.replace('<maxResults>',  params.maxResults);;
			return $http.post(REST_API_URL + getUrl, {});
		},
	}
}]);

app.factory('ProductPacking', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.PRODUCT_PACKING.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.PRODUCT_PACKING.CREATE, {'productPacking':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.PRODUCT_PACKING.GET.replace('<RetailerServiceProviderId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.PRODUCT_PACKING.UPDATE, {'productPacking':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.PRODUCT_PACKING.DELETE.replace('<RetailerServiceProviderId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		bulkcreate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.PRODUCT_PACKING.BULK_CREATE, {'productPackingList':params});
		},
		bulkdelete:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.PRODUCT_PACKING.BULK_DELETE, {'productPackingIdList':params});
		},
	}
}]);

app.factory('RetailerOther', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_OTHER.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_OTHER.CREATE, {'retailerOther':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.RETAILER_OTHER.GET.replace('<retailerOtherId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_OTHER.UPDATE, {'retailerOther':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.RETAILER_OTHER.DELETE.replace('<retailerOtherId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
	}
}]);

app.factory('RetailerChequeCollectionCenters', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_CHEQUE_COLLECTION_CENTERS.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_CHEQUE_COLLECTION_CENTERS.CREATE, {'retailerChequeCollectionCenter':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.RETAILER_CHEQUE_COLLECTION_CENTERS.GET.replace('<collectionCenterId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_CHEQUE_COLLECTION_CENTERS.UPDATE, {'retailerChequeCollectionCenter':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.RETAILER_CHEQUE_COLLECTION_CENTERS.DELETE.replace('<collectionCenterId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
	}
}]);

app.factory('RetailerChequeCollectionCenterCities', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_CHEQUE_COLLECTION_CENTER_CITIES.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_CHEQUE_COLLECTION_CENTER_CITIES.CREATE, {'retailerChequeCollectionCenterCity':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.RETAILER_CHEQUE_COLLECTION_CENTER_CITIES.GET.replace('<collectionCenterCityId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_CHEQUE_COLLECTION_CENTER_CITIES.UPDATE, {'retailerChequeCollectionCenterCity':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.RETAILER_CHEQUE_COLLECTION_CENTER_CITIES.DELETE.replace('<collectionCenterCityId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		bulkcreate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_CHEQUE_COLLECTION_CENTER_CITIES.BULK_CREATE, {'retailerChequeCollectionCenterCityList':params});
		},
		bulkdelete:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_CHEQUE_COLLECTION_CENTER_CITIES.BULK_DELETE, {'retailerChequeCollectionCenterCityIdList':params});
		},
		populate_city: function(params) {			
			var getUrl = API.RETAILER.RETAILER_CHEQUE_COLLECTION_CENTER_CITIES.POPULATE_CITY.replace('<retailerId>', params.retailerId);
			return $http.post(REST_API_URL + getUrl, {});
		}
	}
}]);

app.factory('RetailerFAQs', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_FAQS.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_FAQS.CREATE, {'retailerFAQ':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.RETAILER_FAQS.GET.replace('<faqId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_FAQS.UPDATE, {'retailerFAQ':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.RETAILER_FAQS.DELETE.replace('<faqId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
	}
}]);

app.factory('RetailerSMSEmailTemplate', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SMS_EMAIL_TEMPLATE.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SMS_EMAIL_TEMPLATE.CREATE, {'retailerSmsEmailTemplate':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.RETAILER_SMS_EMAIL_TEMPLATE.GET.replace('<templateId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SMS_EMAIL_TEMPLATE.UPDATE, {'retailerSmsEmailTemplate':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.RETAILER_SMS_EMAIL_TEMPLATE.DELETE.replace('<templateId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		copy_master_templates:function(params) {
			var getUrl = API.MASTERS.SMS_EMAIL_TEMPLATE.COPY_MASTER_TEMPLATES.replace('<retailerId>', params.retailerId);
			return $http.post(REST_API_URL + getUrl, {});
		},
	}
}]);

app.factory('RetailerSMSEmailTriggerConfig', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SMS_EMAIL_TRIGGER_CONFIG.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SMS_EMAIL_TRIGGER_CONFIG.CREATE, {'retailerSmsEmailTemplate':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.RETAILER_SMS_EMAIL_TRIGGER_CONFIG.GET.replace('<templateId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SMS_EMAIL_TRIGGER_CONFIG.UPDATE, {'retailerSmsEmailTemplate':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.RETAILER_SMS_EMAIL_TRIGGER_CONFIG.DELETE.replace('<templateId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		bulkcreate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SMS_EMAIL_TRIGGER_CONFIG.BULK_CREATE, {'retailerSmsEmailTriggerConfigList':params});
		},
		bulkupdate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SMS_EMAIL_TRIGGER_CONFIG.BULK_UPDATE, {'retailerSmsEmailTriggerConfigList':params});
		},
		by_retailer: function(params) {
			var getUrl = API.RETAILER.RETAILER_SMS_EMAIL_TRIGGER_CONFIG.BY_RETAILER.replace('<retailerId>', params.retailerId);
			return $http.post(REST_API_URL + getUrl, {});
		},
	}
}]);

app.factory('RetailerServiceProviderPincode', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICE_PROVIDER.PINCODES.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICE_PROVIDER.PINCODES.CREATE,
                              {'retailerServiceProviderPincode':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.RETAILER_SERVICE_PROVIDER.PINCODES.GET.replace('<id>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICE_PROVIDER.PINCODES.UPDATE,
                              {'retailerServiceProviderPincode':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.RETAILER_SERVICE_PROVIDER.PINCODES.DELETE.replace('<id>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		bulkcreate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICE_PROVIDER.PINCODES.BULK_CREATE,
                              {'retailerServiceProviderPincodeList':params});
		},
		bulkdelete:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_SERVICE_PROVIDER.PINCODES.BULK_DELETE,
                              {'retailerServiceProviderPincodeIdList':params});
		}
	}
}]);