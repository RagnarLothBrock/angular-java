'use strict';

app.factory('RetailerCostingActivity', ["$http", "REST_API_URL", "API", "Upload", function($http, REST_API_URL, API, Upload) {
	return {
        list: function(params) {
            var getUrl = API.RETAILER.RETAILER_COSTING_ACTIVITY.LIST
								.replace('<retailerId>', params.id)
								.replace('<first>', params.firstResult)
								.replace('<limit>', params.maxResults);
			return $http.post(REST_API_URL + getUrl, {query:params.query});
        },
        bulkcreate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_COSTING_ACTIVITY.BULK_CREATE, {'retailerCostingActivityList':params});
		},
		bulkupdate:function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_COSTING_ACTIVITY.BULK_UPDATE, {'retailerCostingActivityList':params});
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_COSTING_ACTIVITY.CREATE, {'retailerCostingActivity':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.RETAILER_COSTING_ACTIVITY.GET.replace('<retailerCostingActivityId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.RETAILER_COSTING_ACTIVITY.UPDATE, {'retailerCostingActivity':params});
		},
		upload: function(params) {
			return Upload.upload({
                url: REST_API_URL + API.RETAILER.RETAILER_COSTING_ACTIVITY.UPLOAD,
                data: params.data,
                file: params.file
            })
		}
	}
}]);

app.controller('RetailerCostingActivityController',[
                '$scope', '$rootScope', '$state', 'notification', '$location', 'RetailerCostingActivity', 'LIST_PAGE_SIZE', 'ngDialog', 'Utility',
    function(   $scope, $rootScope, $state, notification, $location, RetailerCostingActivity, LIST_PAGE_SIZE, ngDialog, Utility
    ){
    $scope.serviceCosts = [];
    $scope.retailerServiceCosts = [];
	$scope.pageSize = 25;
	$scope.currentPage = 1;
	$scope.costingActivity = {};
	$scope.showServiceCostingList = true;
	$scope.showServiceCostingUpload = false;
	
	$rootScope.fnShowServiceCostingList = function(){
		$scope.showServiceCostingList = true;
		$scope.showServiceCostingUpload = false;
	}
	
	$rootScope.fnShowServiceCostingUpload = function() {
		$scope.showServiceCostingList = false;
		$scope.showServiceCostingUpload = true;
	}
    
    $scope.getRetailerCostingActivityList = function(first, limit) {
		$scope.currentPage = first // initialize page no to 1
			
		first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
        $scope.serviceCosts = [];
        RetailerCostingActivity
            .list({id:$state.current.data.params.id, firstResult: first, maxResults: limit, query: $scope.costingActivity})
            .success(function(response) {
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.pages = 1;
					$scope.total_count = response.totalRecords;
					$scope.serviceCosts = response.data;
					
					//$scope.initRetailerServiceCosts ($scope.serviceCosts);
					
				}
        });
    }
	
	$scope.filterRetailerCostingActivity = function(costingActivity) {
		$scope.getRetailerCostingActivityList(1, $scope.pageSize);
	}
	
	$scope.clearSearchFields = function() {
		$scope.form = {};
		$scope.costingActivity = {};
		$scope.getRetailerCostingActivityList(1, $scope.pageSize);
	}
	
	$scope.showEditDialog = function(costing, index) {
		//console.log(costing);
		var item = {};
		item.id = costing.retailerCostingActivityId;
		item.mappingId = costing.mappingId;
		item.retailerId = $state.current.data.params.id;
		item.costingActivityCode = costing.costingActivityCode;
		item.categoryId = costing.categoryId;
		item.serviceId = costing.serviceId;
		item.cost = (costing.cost == null) ? 0 : parseFloat(costing.cost);
		item.status = (costing.status == null) ? 1 : parseInt(costing.status);
		
		$scope.retServiceCosting = item;
		$scope.editIndex = index;
		
		var html = '<div class="dialog-contents" ng-model="retServiceCosting">' +
							'<div class="panel panel-default" ng-model="editIndex">' +
								'<div class="panel-heading">Edit Service Costing</div>' +
								'<div class="panel-body" ng-model="serviceCosts">' +
									'<div class="form-group">'+
										'<div><label>Category: </label>  ' + costing.productCategoryName + '</div>'+
									'</div>'+
									'<div class="form-group">'+
										'<div><label>Service: </label>  ' + costing.serviceName + '</div>'+
									'</div>'+
									'<div class="form-group">'+
										'<div><label>Activity: </label>  ' + costing.costingActivityName + '</div>'+
									'</div>'+
									'<div class="form-group">'+
										'<div><label>Amount: </label>  ' +
											'<div class="input-group">' +
												'<span class="input-group-addon" id="basic-addon1"><i class="fa fa-inr"></i></span>' +
												'<input spellcheck="true" type="number" name="cost" class="form-control" style="" id="cost" min="0"' +
													'placeholder="Enter Cost" value="' + item.cost +'" ng-model="retServiceCosting.cost"' +
													'maxlength="10"  required="required" step="0.01"/>' +
												'</div>' +										
											'</div>'+
										'</div>'+
									'</div>'+
									'<div class="form-group">'+
										'<div><label>Enable?: </label>  ' +
											'<div class="radio-inline" style="">' +
												'<label>' +
													'<input type="radio" name="status" id="status1" value="1" ng-checked="retServiceCosting.status==1" ng-model="retServiceCosting.status" required />' +
													'Yes' +
												'</label>' +
											'</div>' +
											'<div class="radio-inline" style="">' +
												'<label>' +
													'<input type="radio" name="status" id="status0" value="0" ng-checked="retServiceCosting.status!=1"  ng-model="retServiceCosting.status" />' +
													'No' +
												'</label>' +
											'</div>' +
										'</div>'+
									'</div>'+
								'</div>'+
							  '</div>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="save(retServiceCosting)" class="btn btn-primary">Save</button>' +
						'</div>'
		ngDialog.openConfirm({
				template: html,
				scope: $scope,
				controller: ['$scope', '$rootScope', 'RetailerCostingActivity', function($scope, $rootScope, RetailerCostingActivity) {
					//console.log($scope);
					$scope.save = function(retServiceCosting) {
						//console.log($scope);
						delete(retServiceCosting.mappingId);
						if (retServiceCosting.id !== null) {
                            //Update
							RetailerCostingActivity
								.update(retServiceCosting)
								.success(function(respose){
								$scope.serviceCosts[$scope.editIndex].id = retServiceCosting.id;
								$scope.serviceCosts[$scope.editIndex].cost = retServiceCosting.cost;
								$scope.serviceCosts[$scope.editIndex].status = retServiceCosting.status;
							});
                        }
						else {
							//Create
							delete(retServiceCosting.id);
							RetailerCostingActivity
								.create(retServiceCosting)
								.success(function(respose){
								retServiceCosting.id = respose.response.id;
								$scope.serviceCosts[$scope.editIndex].id = retServiceCosting.id;
								$scope.serviceCosts[$scope.editIndex].cost = retServiceCosting.cost;
								$scope.serviceCosts[$scope.editIndex].status = retServiceCosting.status;
							});
						}
						
						ngDialog.close();
					}
				  
				}]
			}).then(function (success) {
				// Success logic here
				console.log(item,index,costing);
				
			}, function (error) {
				// Error logic here
			});
	}
    
    $scope.initRetailerServiceCosts = function(serviceCosts) {
        var retailerServiceCosts = [];
        for (var i = 0; i < serviceCosts.length; i++) {
            var item = {};
            item.id = serviceCosts[i].retailerCostingActivityId;
            item.mappingId = serviceCosts[i].mappingId;
            item.retailerId = $state.current.data.params.id;
            item.costingActivityCode = serviceCosts[i].costingActivityCode;
            item.categoryId = serviceCosts[i].categoryId;
            item.serviceId = serviceCosts[i].serviceId;
            item.cost = (serviceCosts[i].cost == null) ? 0 : parseFloat(serviceCosts[i].cost);
            item.status = (serviceCosts[i].status == null) ? 1 : parseInt(serviceCosts[i].status);
            
            retailerServiceCosts.push(item);
        }
		//$scope.rServiceCosts = retailerServiceCosts;
		$scope.retailerServiceCosts = retailerServiceCosts.splice(0, $scope.pageSize);
		//$scope.getServiceCostingPage(1, $scope.pageSize);
    }
    
    $scope.saveRetailerCostingActivity = function(retailerServiceCosts) {
        if ($scope.form.$invalid) {
            return;
        }
        var bulkCreate = [], bulkUpdate = [];
        for (var i = 0; i < retailerServiceCosts.length; i++) {
            
            delete(retailerServiceCosts[i].mappingId);
            if (retailerServiceCosts[i].id !== null) {
                
                bulkUpdate.push(retailerServiceCosts[i]);
            }
            else {
                delete(retailerServiceCosts[i].id);
                bulkCreate.push(retailerServiceCosts[i]);
            }
        }
        
        if (bulkUpdate.length > 0) {
           RetailerCostingActivity
                .bulkupdate(bulkUpdate)
                .success(function(response){
                });
        }
        
        if (bulkCreate.length > 0) {
           RetailerCostingActivity
                .bulkcreate(bulkCreate)
                .success(function(response){
                    $rootScope.$broadcast('populateRetailerCostingActivityList');
                });
        }
    }
	
	$scope.costingActivityMappingUpload = {};
	$scope.upload = function(costingActivityMappingUpload) {
		$scope.costingActivityMappingUpload.retailerId = $state.current.data.params.id;
		var param = {data : $scope.costingActivityMappingUpload, file: $scope.costingActivityMappingUpload.file}
		RetailerCostingActivity.upload(param)
			.progress(function (evt) {
				var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
				$scope.uploadPercentage = progressPercentage;
				console.log(progressPercentage);
			})
			.success(function (data, status, headers, config) {
				console.log(data);
				$rootScope.$broadcast('showUploadReturnMessage', {data:data, status:status, headers:headers, config:config});
				$scope.costingActivityMappingUpload = {data:{}, file:{}};
			})
			.error(function(error){
				var errorM = "";
				$(error.message).each(function(){
					errorM += this +"<br/>";
				});
				notification.error(errorM)
			});
	}
	
	$scope.$watch('costingActivityMappingUpload.file', function () {
		$scope.costingActivityMappingUpload.selectedFileName = ($scope.costingActivityMappingUpload.file !== undefined) ? $scope.costingActivityMappingUpload.file.name : "";
	});
    
    var evtPopulateRetailerCostingActivityList = $rootScope.$on('populateRetailerCostingActivityList', function(e, args) {
        $scope.getRetailerCostingActivityList(1, $scope.pageSize);
		Utility.populate_category({},$scope);
		Utility.populate_costing_activity({},$scope);
		Utility.populate_master_services({},$scope);
    });
	$scope.$on('$destroy', function() {
		evtPopulateRetailerCostingActivityList();
	});
}]);