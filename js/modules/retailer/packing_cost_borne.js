'use strict';
app.controller('RetailerPackingCostBorneController',[
        '$scope', '$rootScope', '$state', 'notification', 'RetailerPaysPackingCost', 'MAX_DROPDOWN_SIZE',
function($scope, $rootScope, $state, notification, RetailerPaysPackingCost, MAX_DROPDOWN_SIZE)
{
    $scope.pageSize = MAX_DROPDOWN_SIZE;
    $scope.total_count = 0;
    $scope.currentPage = 1;
    
    var hasErrorsRetailerPackingCostBorne = function(retailer) {
		$scope.retailer.list.packingCostBorne.bulkCreate = [],
		$scope.retailer.list.packingCostBorne.bulkUpdate = [];
		$scope.retailer.list.packingCostBorne.errors = [];
		var selectedObj = $.unique(retailer.list.packingCostBorne.selectedObj);
		for(var i = 0; i < selectedObj.length; i++) {
			if (selectedObj[i] === undefined) {
                continue;
            }
			var e = {};
			if (selectedObj[i].categoryId == null) {
                e.categoryId = "Please select a category";
            }
			if (selectedObj[i].brandId == null) {
                e.brandId = "Please select a brand";
            }
			if (e.brandId !== undefined || e.categoryId !== undefined) {
                $scope.retailer.list.packingCostBorne.errors[i] = e;
            }
			
			$scope.retailer.list.packingCostBorne.selectedObj[i].retailerId = $scope.retailer.retailer.id;
			
			if (selectedObj[i].id == null) {
                $scope.retailer.list.packingCostBorne.bulkCreate.push(selectedObj[i]);
            }
			else {
				$scope.retailer.list.packingCostBorne.bulkUpdate.push(selectedObj[i]);
			}
		}
		
		if ($scope.retailer.list.packingCostBorne.errors.length > 0) {
			return true;
		}
		return false;
	}
    
    $scope.saveRetailerPackingCostBorne = function(retailer) {
		//Validate Retailer
		$scope.retailer.list.packingCostBorne.errors = [];
		
		var hasErrors = hasErrorsRetailerPackingCostBorne(retailer);
		
		if (!hasErrors) {
			if ($scope.retailer.list.packingCostBorne.bulkUpdate.length > 0) {
                //Save Details
				RetailerPaysPackingCost
					.bulkupdate($scope.retailer.list.packingCostBorne.bulkUpdate)
					.success(function(response) {
                        if (response !== undefined && response.response !== undefined && response.response !== null) {
                            $rootScope.$broadcast ('showSuccessMessage',response);
                        }
					});
            }
			
			if ($scope.retailer.list.packingCostBorne.bulkCreate.length > 0) {
                //Save Details
				RetailerPaysPackingCost
					.bulkcreate($scope.retailer.list.packingCostBorne.bulkCreate)
					.success(function(response) {
                        if (response !== undefined && response.response !== undefined && response.response !== null) {
                            $rootScope.$broadcast ('showSuccessMessage',response);
                            $scope.getRetailerPackingCostBorne();
                        }
					});
            }
        }
	}
    
    $scope.deleteRetailerPackingCostBorne = function(index) {
		var obj = $scope.retailer.list.packingCostBorne.selectedObj[index];
		RetailerPaysPackingCost
				.del(obj)
				.success(function(response) {
					if (response !== undefined && response.response !== undefined && response.response !== null) {
                        $rootScope.$broadcast ('showSuccessMessage',response);
                        $scope.retailer.list.packingCostBorne.selectedObj.splice(index,1);
						if ($scope.retailer.list.packingCostBorne.selectedObj.length == 0) {
							$scope.addMorePackingCostBorne({},0);
						}
                    }
				});
	}
    
    $scope.getRetailerPackingCostBorne = function(first, limit) {
        $scope.currentPage = first ;// initialize page no to 1
        //$scope.total_count = 100;
        first = $scope.calculateFirstRecord($scope.currentPage, limit);
		RetailerPaysPackingCost
				.list({'query':{'retailerId' : $scope.retailer.retailer.id}, firstResult:first, maxResults:limit})
				.success(function(response) {
					if (response !== undefined && response.data !== undefined && response.data.length > 0){
                        $scope.retailer.list.packingCostBorne.selectedObj = response.data;
                        $scope.total_count = response.totalRecords;
                    }
					if ($scope.retailer.list.packingCostBorne.selectedObj.length == 0) {
						$scope.addMorePackingCostBorne({},0);
					}
				});
	}
    
    $scope.addMorePackingCostBorne = function(pCostBorne,index) {
		if($scope.retailer.list.packingCostBorne.selectedObj.length > 0) {
			if (pCostBorne !== undefined && pCostBorne.categoryId !== undefined) {
				$scope.retailer.list.packingCostBorne.selectedObj[index] = pCostBorne;
            }
		}
		$scope.retailer.list.packingCostBorne.selectedObj
			.push(angular.copy($scope.retailer.list.packingCostBorne.template) );
	}
	
	$scope.deletePackingCostBorne = function(index) {
		var obj = $scope.retailer.list.packingCostBorne.selectedObj[index];
		
		//Delete Object
		//Delete from DB
		if (obj !== undefined && obj.id !== null && obj.id !== undefined) {
			$rootScope.$broadcast ('deleteDialogAndConfirmEvent',"Are you sure want to delete retailer packing cost borne?", 'deleteRetailerPackingCostBorne', index);
        }
		else {
			$scope.retailer.list.packingCostBorne.selectedObj.splice(index,1);
		}
	}
    
    var evtDeleteRetailerPackingCostBorne = $rootScope.$on('deleteRetailerPackingCostBorne', function(e,index) {
        $scope.deleteRetailerPackingCostBorne(index);
    });
    
    var evtGetRetailerPackingCostBorne = $rootScope.$on('getRetailerPackingCostBorne', function(e,index) {
        $scope.getRetailerPackingCostBorne(0,$scope.pageSize);
        if ($scope.retailer.list.packingCostBorne.selectedObj.length == 0) {
            $scope.addMorePackingCostBorne({},0);
        }
    });
    
    $scope.$on('$destroy', function() {
        evtDeleteRetailerPackingCostBorne();
        evtGetRetailerPackingCostBorne();
    });
}]);