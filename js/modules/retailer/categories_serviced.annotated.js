'use strict';
app.controller('RetailerCategoriesServicedController',[
                '$scope', '$rootScope', '$state', 'notification', 'RetailerCategories', 'ProductCategory','MasterCategory','Utility',
    function(   $scope, $rootScope, $state, notification, RetailerCategories, ProductCategory ,MasterCategory ,Utility
    ){
        
    $scope.populateCategoriesByRetailer = function() {
		ProductCategory.byRetailer({'id':$state.current.data.params.id}).success(function(response) {
			$scope.retailer.list.productCategory.list = response;
			console.log("respo from cat.ser",response);
			for(var i=0; i < $scope.retailer.list.productCategory.list.length; i++) {
				if ($scope.retailer.list.productCategory.list[i].retailerCategoryId !== null) {
                    $scope.retailer.list.productCategory.selectedIds.push($scope.retailer.list.productCategory.list[i].categoryId)
                }
			}
		});
	}
	$scope.populateMasterCategory =function(){
		console.log("Success not called");
		$scope.masterCategory=[];
		MasterCategory.list({query:{}}).success(function(response){
			$scope.masterCategory=response.data;
			console.log("res $.masterCategory",$scope.masterCategory);
		});
	}

	$scope.populateCategoriesServicedList = function() {
		RetailerCategories.customCategoryQuery({'id':$state.current.data.params.id}).success(function(response) {
			$scope.retailer.list.costPerService.categoriesServiced = response;
			console.log("response from categoryServiceList",response);
		});
	}
    
    var evtPopulateCategoriesByRetailer = $rootScope.$on('populateCategoriesByRetailer', function(e, args) {
        $scope.populateCategoriesByRetailer(args);
    });
	
	var evtPopulateCategoriesServicedList = $rootScope.$on('populateCategoriesServicedList', function(e, args) {
        $scope.populateCategoriesServicedList(args);
    });

	var evtpopulateMasterCategory = $rootScope.$on('populateMasterCategory', function(e, args) {
        $scope.populateMasterCategory(args);
    });
	
	$scope.$on('$destroy', function() {
        evtPopulateCategoriesByRetailer();
        evtPopulateCategoriesServicedList();
    });

    //Validate Retailer Category Data
	var hasErrorsRetailerCategoriesServiced = function(retailer) {
		var retailerCategories = [],
			selectedIds = retailer.list.productCategory.selectedIds;
		
		if (selectedIds.length == 0) {
            $scope.retailer.list.productCategory.errors.categoriesServiced = "Please select any one category to continue!";
			return false;
        }
		selectedIds = $.unique(selectedIds);
		for(var i = 0; i < selectedIds.length; i++) {
			if (selectedIds[i] != undefined &&
				selectedIds[i] != null &&
				selectedIds[i] != "") {
                //code
				var cat = {
					retailerId: retailer.retailer.id,
					categoryId: selectedIds[i]
				};
				retailerCategories.push(cat);
            }
		}
		retailerCategories = $.unique(retailerCategories);
		return retailerCategories;
	}
    
    //Save Categories Serviced by a Retailer
	$scope.saveRetailerCategoriesServiced = function(retailer) {
		//Validate Retailer
		$scope.retailer.list.productCategory.errors = {};
		
		var categories = hasErrorsRetailerCategoriesServiced(retailer);
		
		if(!categories) return false;
		
		var selectedObj = retailer.list.productCategory.selectedObj;
		
		//Execute bulk delete with ids
		var deleteIdList = [];
		for(var i = 0; i < $scope.retailer.list.productCategory.list.length; i++) {
			if ($scope.retailer.list.productCategory.list[i].retailerCategoryId != null) {
                deleteIdList.push($scope.retailer.list.productCategory.list[i].retailerCategoryId);
            }
		}
		
		if (deleteIdList.length > 0) {
            RetailerCategories.bulkdelete(deleteIdList).success(function(response) {
				console.log(response);
			});
        }
		//Uncomment this once bulkcreate is implemented
		RetailerCategories.bulkcreate(categories).success(function(response) {
			if (response && response.response.length > 0) {
               $rootScope.$broadcast ('showSuccessMessage',response);
				$scope.populateCategoriesByRetailer();
            }
		});        
	}
	
	$scope.toggleCategoriesServiced = function(id) {
		
		var found = 0; var ids = [];
        if ($scope.retailer.list.productCategory.selectedIds.length > 0) {
            angular.forEach($scope.retailer.list.productCategory.selectedIds,function(item, key){
                
                if (item === id) {
                    found = 1;
                }
                else {
                    ids.push(item);
                }
            });
        }
        else {
            ids.push(id);
        }
        if (found == 0) {
            ids.push(id);
        }
        
        $scope.retailer.list.productCategory.selectedIds = ids;
	}
}]);