'use strict';
app.controller('RetailerDoorStepServicesController',[
                '$scope', '$rootScope', '$state', 'notification', 'LIST_PAGE_SIZE', 'API', 'RestHTTPService',
function(   $scope, $rootScope, $state, notification, LIST_PAGE_SIZE, API, RestHTTPService
){
    $scope.pageSize = LIST_PAGE_SIZE;
    $scope.total_count = 0;
    $scope.currentPage = 1;
    $scope.doorStepServicesList = [];
    
    $scope.saveRetailerDoorStepServices = function(retailerDoorStepServices) {
        if ($scope.retailerDoorStepServicesForm.$invalid) {
            return false;
        }
        retailerDoorStepServices.retailerId = $state.current.data.params.id;
        
        RestHTTPService
            .post(API.RETAILER.RETAILER_DOOR_STEP_SERVICE.CREATE, {retailerDoorStepService: retailerDoorStepServices})
            .success(function(response) {
               // $scope.doorStepServicesList = [];
                $scope.populateRetailerDoorStepServicesList(1, $scope.pageSize);
            });
    }
    
    $scope.populateRetailerDoorStepServicesList = function(first, limit) {
        $scope.currentPage = first // initialize page no to 1
			
		first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
        //$scope.doorStepServicesList = [];
		
		var params = {
            'query': {
                'retailerId':$state.current.data.params.id
            },
            'firstResult':first,
            'maxResults': limit
        }
        
        var apiUrl = API.RETAILER.RETAILER_DOOR_STEP_SERVICE.LIST
                        .replace('<retailerId>', $state.current.data.params.id)
                        .replace('<firstResult>', first)
                        .replace('<maxResults>', limit);
		
        RestHTTPService
            .post(apiUrl, {query: {retailerId:$state.current.data.params.id}})
            .success(function(response) {
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.pages = 1;
					$scope.total_count = response.totalRecords;
					$scope.doorStepServicesList = response.data;
				}
        });
    }
    
    $scope.deleteRetailerDoorStepServiceCallBack = function(index) {
        var param = {id: $scope.doorStepServicesList[index].id};
        RestHTTPService
            .del(API.RETAILER.RETAILER_DOOR_STEP_SERVICE.DELETE,param,'id')
            .success(function(response) {
			$scope.populateRetailerDoorStepServicesList(1, $scope.pageSize);
		});
    }
    
    $scope.deleteRetailerDoorStepServiceClick = function(index, doorStepService) {
        $rootScope.$broadcast ('deleteDialogAndConfirmEvent',"Are you sure want to delete this record?",
								   'deleteRetailerDoorStepServiceCallBack', index);
    }
    
    var evtGetRetailerDoorStepServicesByBrand = $rootScope.$on('getRetailerDoorStepServicesByBrand', function(e, args) {
        $scope.populateRetailerDoorStepServicesList(1, $scope.pageSize)
    });
    
    var evtDeleteRetailerDoorStepServiceCallBack = $rootScope.$on('deleteRetailerDoorStepServiceCallBack', function(e, args) {
        $scope.deleteRetailerDoorStepServiceCallBack(args);
    });
    
    $scope.$on('$destroy', function() {
        evtGetRetailerDoorStepServicesByBrand();
        evtDeleteRetailerDoorStepServiceCallBack();
    });
}]);