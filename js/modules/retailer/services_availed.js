'use strict';
app.controller('RetailerServicesAvailedController',[
                '$scope', '$rootScope', '$state', 'notification', 'ServiceMaster', 'RetailerServices',
    function(   $scope, $rootScope, $state, notification, ServiceMaster , RetailerServices
    ){
    
    //This function will all the services in the system along with details of whether a retailer is availed a service or not
    $scope.populateServicesListByRetailer = function() {
        $scope.retailer.list.serviceMaster.selectedIds = [];
        ServiceMaster.byRetailer({'id':$state.current.data.params.id}).success(function(response) {
            $scope.retailer.list.serviceMaster.list = response;
            for(var i=0; i < $scope.retailer.list.serviceMaster.list.length; i++) {
                if ($scope.retailer.list.serviceMaster.list[i].retailerServiceId !== null) {
                    $scope.retailer.list.serviceMaster.selectedIds.push($scope.retailer.list.serviceMaster.list[i].serviceId)
                }
            }
        });
    }
	
	$scope.retailerServiceList = [];
	$scope.populateServicesListByRetailerForRetailer = function() {
        $scope.retailer.list.serviceMaster.selectedIds = [];
        ServiceMaster.byRetailer({'id':$scope.loginInfo.retailerId}).success(function(response) {
            $scope.retailerServiceList = response;
			console.log("Services " + retailerServiceList);
        });
    }
    
    //This function will list services availed by retailer
    $scope.populateRetailerAvailedServiceList = function() {
		RetailerServices.customServiceQuery({'id':$state.current.data.params.id}).success(function(response) {
			$scope.retailer.list.costPerService.availedServices = response;
		});
	}
    
    var evtPopulateServicesListByRetailer = $rootScope.$on('populateServicesListByRetailer', function(e, args) {
        $scope.populateServicesListByRetailer(args);
    });
    
    var evtPopulateRetailerAvailedServiceList = $rootScope.$on('populateRetailerAvailedServiceList', function(e, args) {
        $scope.populateRetailerAvailedServiceList(args);
    });
    
    $scope.isSelectedService = function(serviceId) {
       // console.log($scope.retailer.list.serviceMaster.selectedIds);
        for(var i = 0; i < $scope.retailer.list.serviceMaster.list.length; i++) {
           // console.log($scope.retailer.list.serviceMaster.list[i].serviceName,$scope.retailer.list.serviceMaster.list[i].retailerId);
            if ( serviceId == $scope.retailer.list.serviceMaster.list[i].serviceId &&
                $scope.retailer.list.serviceMaster.list[i].retailerId !== null) {
                return true;
            }
        }
        return false;
    }
    
    $scope.toggleServicesAvailed = function(id) {
        
        var addItem = function(array, item) {
            var index = array.indexOf(item);
            if (index === -1) {
                array.push(item);
            }
            return array;
        }
        
        var found = 0; var ids = [];
        if ($scope.retailer.list.serviceMaster.selectedIds.length > 0) {
            angular.forEach($scope.retailer.list.serviceMaster.selectedIds,function(item, key){
                
                if (item === id) {
                    found = 1;
                }
                else {
                    ids = addItem(ids,item);
                }
            });
        }
        else {
            ids = addItem(ids,id);
        }
        if (found == 0) {
            ids = addItem(ids,id);
        }
        
        $scope.retailer.list.serviceMaster.selectedIds = [];
        $scope.retailer.list.serviceMaster.selectedIds = angular.copy( ids );
	}
    
    //Validate Retailer Services Data
	var hasErrorsRetailerServices = function(retailer) {
		var retailerServices = [],
			selectedIds = retailer.list.serviceMaster.selectedIds;
		
		if (selectedIds.length == 0) {
            $scope.retailer.list.serviceMaster.errors.servicesAvailed = "Please select any one service to continue!";
			return false;
        }
		selectedIds.forEach( function( item ) {  
			if (item != undefined &&
				item != null &&
				item != "") {
                //code
				var ser = {
					retailerId: retailer.retailer.id,
					serviceId: item
				};
				retailerServices.push(ser);
            }
		});
		return retailerServices;
	}
    
    var bulkCreate = function(services) {
        //Uncomment this once bulkcreate is implemented
        RetailerServices.bulkcreate(services).success(function(response) {
            
            if (response && response.response.length > 0) {
                $rootScope.$broadcast ('showSuccessMessage',response);
                $scope.populateServicesListByRetailer();
            }
        });
    }
    
    //Save Retailer Services Availed
	$scope.saveRetailerServicesAvailed = function(retailer) {
		//Validate Retailer
		$scope.retailer.list.serviceMaster.errors = {};
		var selectedObj = $scope.retailer.list.serviceMaster.selectedObj;
		
		var services = hasErrorsRetailerServices(retailer);
		
		if(!services) return false;
		
		//Execute bulk delete with ids
		var deleteIdList = [];
		for(var i = 0; i < $scope.retailer.list.serviceMaster.list.length; i++) {
			if ($scope.retailer.list.serviceMaster.list[i].retailerServiceId != null) {
                deleteIdList.push($scope.retailer.list.serviceMaster.list[i].retailerServiceId);
            }
		}
		if (deleteIdList.length > 0) {
            RetailerServices.bulkdelete(deleteIdList).success(function(response) {
                bulkCreate(services);
			});
        }
        else {
            bulkCreate(services);
        }
	}
    
    $scope.$on('$destroy', function() {
		evtPopulateServicesListByRetailer();
        evtPopulateRetailerAvailedServiceList();
	});
}]);