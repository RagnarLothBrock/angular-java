'use strict';
app.controller('RetailerPackingCostController',[
                '$scope', '$rootScope', '$state', 'notification', 'Utility', 'RetailerPackingCost','RetailerProcessField','RetailerPackingSpecification','MAX_DROPDOWN_SIZE',
function(   $scope, $rootScope, $state, notification, Utility, RetailerPackingCost, RetailerProcessField, RetailerPackingSpecification, MAX_DROPDOWN_SIZE
){
    $scope.pageSize = MAX_DROPDOWN_SIZE;
    $scope.total_count = 0;
    $scope.currentPage = 1;
    
    var hasErrorsRetailerPackingCost = function(retailer) {
		$scope.retailer.list.packingMaterialType.bulkCreate = [],
		$scope.retailer.list.packingMaterialType.bulkUpdate = [];
		$scope.retailer.list.packingMaterialType.errors = [];
		var selectedObj = $.unique(retailer.list.packingMaterialType.selectedObj);
		for(var i = 0; i < selectedObj.length; i++) {
			if (selectedObj[i] === undefined) {
                continue;
            }
			var e = {};
			if (selectedObj[i].packingMaterialId == null) {
                e.packingMaterialId = "Please select a service";
            }
			if (selectedObj[i].materialCost == null) {
                e.materialCost = "Please enter valid material cost";
            }
            
			if (selectedObj[i].packingCost == null) {
                e.packingCost = "Please enter valid packing cost";
            }
			if (e.packingMaterialId !== undefined && e.materialCost !== undefined && e.packagingCost !== undefined &&
                e.packingMaterialId !== null && e.materialCost !== null && e.packagingCost !== null &&
                e.packingMaterialId !== "" && e.materialCost !== "" && e.packagingCost !== "")
            {
                $scope.retailer.list.packingMaterialType.errors[i] = e;
            }
			
			$scope.retailer.list.packingMaterialType.selectedObj[i].retailerId = $scope.retailer.retailer.id;
			
			if (selectedObj[i].id == null) {
                $scope.retailer.list.packingMaterialType.bulkCreate.push(selectedObj[i]);
            }
			else {
				$scope.retailer.list.packingMaterialType.bulkUpdate.push(selectedObj[i]);
			}
		}
		
		if ($scope.retailer.list.packingMaterialType.errors.length > 0) {
			return true;
		}
		return false;
	}
	
	
	 var hasErrorsRetailerPackingSpecification = function(retailer) {
		$scope.retailer.list.packingSpecification.bulkCreate = [],
		$scope.retailer.list.packingSpecification.bulkUpdate = [];
		$scope.retailer.list.packingSpecification.errors = [];
		var selectedObj = $.unique(retailer.list.packingSpecification.selectedObj);
		for(var i = 0; i < selectedObj.length; i++) {
			if (selectedObj[i] === undefined) {
                continue;
            }
			
			
			$scope.retailer.list.packingSpecification.selectedObj[i].retailerId = $scope.retailer.retailer.id;
			
			if (selectedObj[i].id == null) {
                $scope.retailer.list.packingSpecification.bulkCreate.push(selectedObj[i]);
            }
			else {
				$scope.retailer.list.packingSpecification.bulkUpdate.push(selectedObj[i]);
			}
		}
		
		return false;
	}
    
    $scope.deleteRetailerPackingCost = function(index) {
		var obj = $scope.retailer.list.packingMaterialType.selectedObj[index];
		RetailerPackingCost
				.del(obj)
				.success(function(response) {
					if (response !== undefined && response.response !== undefined &&
                        response.response !== null && response.response === true) {
                        $rootScope.$broadcast ('showSuccessMessage',response);
                        $scope.retailer.list.packingMaterialType.selectedObj.splice(index,1);
						if ($scope.retailer.list.packingMaterialType.selectedObj.length == 0) {
							$scope.addMorePackingMaterial({},0);
						}
                    }
				});
	}
	
	$scope.deleteRetailerPackingSpecification = function(index) {
		var obj = $scope.retailer.list.packingSpecification.selectedObj[index];
		RetailerPackingSpecification
				.del(obj)
				.success(function(response) {
					if (response !== undefined && response.response !== undefined &&
                        response.response !== null && response.response === true) {
                        $rootScope.$broadcast ('showSuccessMessage',response);
                        $scope.retailer.list.packingSpecification.selectedObj.splice(index,1);
						if ($scope.retailer.list.packingSpecification.selectedObj.length == 0) {
							$scope.addMorePackingSPecification({},0);
						}
                    }
				});
	}
    
    //Save Retailer Packing Cost
	$scope.saveRetailerPackingCost = function(retailer) {
		//Validate Retailer
		$scope.retailer.list.packingMaterialType.errors = [];
		
		var hasErrors = hasErrorsRetailerPackingCost(retailer);
		console.log($scope.retailer.list.packingMaterialType, hasErrors);
		if (!hasErrors) {
			if ($scope.retailer.list.packingMaterialType.bulkUpdate.length > 0) {
                //Save Details
				RetailerPackingCost
					.bulkupdate($scope.retailer.list.packingMaterialType.bulkUpdate)
					.success(function(response) {
                        if (response !== undefined && response.response !== undefined && response.response !== null) {
                            $rootScope.$broadcast ('showSuccessMessage',response);
                        }
					});
            }
			
			if ($scope.retailer.list.packingMaterialType.bulkCreate.length > 0) {
                //Save Details
				RetailerPackingCost
					.bulkcreate($scope.retailer.list.packingMaterialType.bulkCreate)
					.success(function(response) {
                        if (response !== undefined && response.response !== undefined && response.response !== null) {
                            $rootScope.$broadcast ('showSuccessMessage',response);
                            $scope.getRetailerPackingCost(0, $scope.pageSize);
                        }
					});
            }
        }
	}
    
	
	//Save Retailer Packing Specification
	$scope.saveRetailerPackingSpecification = function(retailer) {
		//Validate Retailer
		$scope.retailer.list.packingSpecification.errors = [];
		
		var hasErrors = hasErrorsRetailerPackingSpecification(retailer);
		
		if (!hasErrors) {
			if ($scope.retailer.list.packingSpecification.bulkUpdate.length > 0) {
                //Save Details
				RetailerPackingSpecification
					.bulkupdate($scope.retailer.list.packingSpecification.bulkUpdate)
					.success(function(response) {
                        if (response !== undefined && response.response !== undefined && response.response !== null) {
                            $rootScope.$broadcast ('showSuccessMessage',response);
                        }
					});
            }
			
			if ($scope.retailer.list.packingSpecification.bulkCreate.length > 0) {
                //Save Details
				RetailerPackingSpecification
					.bulkcreate($scope.retailer.list.packingSpecification.bulkCreate)
					.success(function(response) {
                        if (response !== undefined && response.response !== undefined && response.response !== null) {
                            $rootScope.$broadcast ('showSuccessMessage',response);
                            $scope.getRetailerPackingSpecification(0, $scope.pageSize);
                        }
					});
            }
        }
	}
	
	
    //Get retailer services cost
	$scope.getRetailerPackingCost = function(first, limit) {
        
        $scope.currentPage = first ;// initialize page no to 1
        //$scope.total_count = 100;
        first = $scope.calculateFirstRecord($scope.currentPage, limit);
		RetailerPackingCost
				.list({'query':{'retailerId' : $scope.retailer.retailer.id}, firstResult:first, maxResults:limit})
				.success(function(response) {
					if (response !== undefined && response.data !== undefined && response.data.length > 0){
                        $scope.retailer.list.packingMaterialType.selectedObj = response.data;
                        $scope.total_count = response.totalRecords;
                    }
                    if ($scope.retailer.list.packingMaterialType.selectedObj.length == 0) {
                        $scope.addMorePackingMaterial({},0);
                    }
				});
	}
	
	//Get retailer packing specification
	$scope.getRetailerPackingSpecification = function(first, limit) {
        
        $scope.currentPage = first ;// initialize page no to 1
        //$scope.total_count = 100;
        first = $scope.calculateFirstRecord($scope.currentPage, limit);
		RetailerPackingSpecification
				.list({'query':{'retailerId' : $scope.retailer.retailer.id}, firstResult:first, maxResults:limit})
				.success(function(response) {
					if (response !== undefined && response.data !== undefined && response.data.length > 0){
                        $scope.retailer.list.packingSpecification.selectedObj = response.data;
                        $scope.total_count = response.totalRecords;
                    }
                    if ($scope.retailer.list.packingSpecification.selectedObj.length == 0) {
                        $scope.addMorePackingSpecification({},0);
                    }
				});
	}
    
    $scope.addMorePackingMaterial = function(pMaterial, index) {
		if($scope.retailer.list.packingMaterialType.selectedObj.length > 0) {
			if (pMaterial !== undefined && pMaterial.packingMaterialId !== undefined) {
				$scope.retailer.list.packingMaterialType.selectedObj[index] = pMaterial;
            }
		}
		$scope.retailer.list.packingMaterialType.selectedObj
			.push(angular.copy($scope.retailer.list.packingMaterialType.template) );
	}
	
	$scope.addMorePackingSpecification = function(pMaterial, index) {
		if($scope.retailer.list.packingSpecification.selectedObj.length > 0) {
			if (pMaterial !== undefined) {
				$scope.retailer.list.packingSpecification.selectedObj[index] = pMaterial;
            }
		}
		$scope.retailer.list.packingSpecification.selectedObj
			.push(angular.copy($scope.retailer.list.packingSpecification.template) );
	}
	
	$scope.deletePackingMaterial = function(index) {
		var obj = $scope.retailer.list.packingMaterialType.selectedObj[index];
		
		//Delete Object
		//Delete from DB
		if (obj !== undefined && obj.id !== null && obj.id !== undefined) {
			$rootScope.$broadcast ('deleteDialogAndConfirmEvent', "Are you sure want to delete retailer packing cost?", 'deleteRetailerPackingCost', index);
        }
	}
	
	$scope.deletePackingSpecification = function(index) {
		var obj = $scope.retailer.list.packingSpecification.selectedObj[index];
		
		//Delete Object
		//Delete from DB
		if (obj !== undefined && obj.id !== null && obj.id !== undefined) {
			$rootScope.$broadcast ('deleteDialogAndConfirmEvent', "Are you sure want to delete retailer packing specification?", 'deleteRetailerPackingSpecification', index);
        }
	}
    
    var evtPopulatePackagingType = $rootScope.$on('populatePackagingType', function(e, args) {
        Utility.populate_packging_material_type({},$scope);
    });
    var evtGetRetailerPackingCost = $rootScope.$on('getRetailerPackingCost', function(e, args) {
        $scope.getRetailerPackingCost(0, $scope.pageSize);
    });
    var evtDeleteRetailerPackingCost = $rootScope.$on('deleteRetailerPackingCost', function(e,index) {
        $scope.deleteRetailerPackingCost(index);
    });
    
    if ($scope.retailer.list.packingMaterialType.selectedObj.length == 0) {
        $scope.addMorePackingMaterial({},0);
    }
    
    $scope.$on('$destroy', function() {
        evtPopulatePackagingType();
        evtGetRetailerPackingCost();
        evtDeleteRetailerPackingCost();
    });
	
	
	var evtGetRetailerPackingSpecification = $rootScope.$on('getRetailerPackingSpecification', function(e, args) {
        $scope.getRetailerPackingSpecification(0, $scope.pageSize);
    });
    var evtDeleteRetailerPackingSpecification = $rootScope.$on('deleteRetailerPackingSpecification', function(e,index) {
        $scope.deleteRetailerPackingSpecification(index);
    });
    
    if ($scope.retailer.list.packingSpecification.selectedObj.length == 0) {
        $scope.addMorePackingSpecification({},0);
    }
    
    $scope.$on('$destroy', function() {
        evtGetRetailerPackingSpecification();
        evtDeleteRetailerPackingSpecification();
    });
	
	
	//process Fields
	$scope.getRetailerProcessField = function(first, limit) {
        
        $scope.currentPage = first ;// initialize page no to 1
        //$scope.total_count = 100;
        first = $scope.calculateFirstRecord($scope.currentPage, limit);
		RetailerProcessField
				.list({'query':{'retailerId' : $scope.retailer.retailer.id}, firstResult:first, maxResults:limit})
				.success(function(response) {
					if (response !== undefined && response.data !== undefined && response.data.length > 0){
                        $scope.retailer.list.processField.selectedObj = response.data;
                        $scope.total_count = response.totalRecords;
                    }
                    if ($scope.retailer.list.processField.selectedObj.length == 0) {
                        $scope.addMoreProcessField({},0);
                    }
				});
	}
	
	var evtGetRetailerProcessField = $rootScope.$on('getRetailerProcessField', function(e, args) {
        $scope.getRetailerProcessField(0, $scope.pageSize);
    });
	
	
	$scope.addMoreProcessField = function(pMaterial, index) {
		console.log("pMaterial : "+pMaterial);
		if($scope.retailer.list.packingMaterialType.selectedObj.length > 0) {
			if (pMaterial !== undefined && pMaterial.retailerField !== undefined) {
				$scope.retailer.list.processField.selectedObj[index] = pMaterial;
            }
		}
		$scope.retailer.list.processField.selectedObj
			.push(angular.copy($scope.retailer.list.processField.template) );
	}
	
	$scope.deleteProcessField = function(index) {
		var obj = $scope.retailer.list.processField.selectedObj[index];
		
		//Delete Object
		//Delete from DB
		if (obj !== undefined && obj.id !== null && obj.id !== undefined) {
			$rootScope.$broadcast ('deleteDialogAndConfirmEvent', "Are you sure want to delete retailer process field?", 'deleteRetailerProcessField', index);
        }
	}
	
	
	$scope.deleteRetailerProcessField = function(index) {
		var obj = $scope.retailer.list.processField.selectedObj[index];
		RetailerProcessField
				.del(obj)
				.success(function(response) {
					if (response !== undefined && response.response !== undefined &&
                        response.response !== null && response.response === true) {
                        $rootScope.$broadcast ('showSuccessMessage',response);
                        $scope.retailer.list.processField.selectedObj.splice(index,1);
						if ($scope.retailer.list.processField.selectedObj.length == 0) {
							$scope.addMoreProcessField({},0);
						}
                    }
				});
	}
	
	
	var hasErrorsRetailerProcessField = function(retailer) {
		console.log("retailer : "+retailer);
		$scope.retailer.list.processField.bulkCreate = [],
		$scope.retailer.list.processField.bulkUpdate = [];
		$scope.retailer.list.processField.errors = [];
		var selectedObj = $.unique(retailer.list.processField.selectedObj);
		for(var i = 0; i < selectedObj.length; i++) {
			if (selectedObj[i] === undefined) {
                continue;
            }
			var e = {};
			var response = {};
			console.log("selectedObj[i].canonicalField "+selectedObj[i].canonicalField);
			if (selectedObj[i].canonicalField == null || selectedObj[i].canonicalField.trim().length === 0) {
                e.canonicalField = "Please enter canonical field";
				response.message = "Please enter canonical field";
				$rootScope.$broadcast ('showErrorMessage',response);
				return true;
				
            }
			console.log("selectedObj[i].retailerField "+selectedObj[i].retailerField);
			if (selectedObj[i].retailerField == null || selectedObj[i].retailerField.trim().length === 0) {
                e.retailerField = "Please enter retailer field";
				response.message = "Please enter retailer field";
				$rootScope.$broadcast ('showErrorMessage',response);
				 return true;
				
            }
			if (e.retailerField !== undefined && e.canonicalField !== undefined && e.retailerField !== null && e.canonicalField !== null)
            {
                $scope.retailer.list.processField.errors[i] = e;
				
            }
            
			$scope.retailer.list.processField.selectedObj[i].retailerId = $scope.retailer.retailer.id;
			
			if (selectedObj[i].id == null) {
                $scope.retailer.list.processField.bulkCreate.push(selectedObj[i]);
            }
			else {
				$scope.retailer.list.processField.bulkUpdate.push(selectedObj[i]);
			}
		}
		if ($scope.retailer.list.processField.errors.length > 0) 
		{
			return true;
		}
		return false;
	}
	
	
	$scope.saveRetailerProcessField = function(retailer) {
		//Validate Retailer
		$scope.retailer.list.processField.errors = [];
		
		var hasErrors = hasErrorsRetailerProcessField(retailer);
		console.log($scope.retailer.list.processField, hasErrors);
		if (!hasErrors) {
			if ($scope.retailer.list.processField.bulkUpdate.length > 0) {
                //Save Details
				RetailerProcessField
					.bulkupdate($scope.retailer.list.processField.bulkUpdate)
					.success(function(response) {
                        if (response !== undefined && response.response !== undefined && response.response !== null) {
                            $rootScope.$broadcast ('showSuccessMessage',response);
                        }
					});
            }
			
			if ($scope.retailer.list.processField.bulkCreate.length > 0) {
                //Save Details
				RetailerProcessField
					.bulkcreate($scope.retailer.list.processField.bulkCreate)
					.success(function(response) {
                        if (response !== undefined && response.response !== undefined && response.response !== null) {
                            $rootScope.$broadcast ('showSuccessMessage',response);
                            $scope.getRetailerProcessField(0, $scope.pageSize);
                        }
					});
            }
        }
	}
	
	
	
	
	var evtDeleteRetailerProcessField = $rootScope.$on('deleteRetailerProcessField', function(e,index) {
        $scope.deleteRetailerProcessField(index);
    });
    
    if ($scope.retailer.list.processField.selectedObj.length == 0) {
        $scope.addMoreProcessField({},0);
    }
	
	$scope.$on('$destroy', function() {
        evtGetRetailerProcessField();
        evtDeleteRetailerProcessField();
    });
}]);
