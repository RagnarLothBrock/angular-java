'use strict';
app.controller('RetailerEmailConfigurationController', 
               ['$scope', '$rootScope', 'RestHTTPService', 'API', '$state',
                function($scope, $rootScope, RestHTTPService, API, $state){
    $scope.errors = {};
    
    $scope.saveRetailerEmailConfig = function(retailerEmailConfig) {
        if ($scope.retailerEmailConfigForm.$invalid) {
            return false;
        }
        retailerEmailConfig.retailerId = $state.current.data.params.id;
        if(retailerEmailConfig.id !== undefined &&
           retailerEmailConfig.id !== null &&
           retailerEmailConfig.id !== ""){
            //Update record
            var apiUrl = API.RETAILER.RETAILER_EMAIL_CONFIG.UPDATE;
            
            
            RestHTTPService
                .post(apiUrl,{'retailerEmailConfiguration':retailerEmailConfig})
                .success(function(response) {
                    $scope.getRetailerEmailConfig()
                });
        }
        else {
            delete(retailerEmailConfig.id);
            //Create record
            var apiUrl = API.RETAILER.RETAILER_EMAIL_CONFIG.CREATE;           
            
            //Create record
            RestHTTPService
                .post(apiUrl,{'retailerEmailConfiguration':retailerEmailConfig})
                .success(function(response) {
                    $scope.getRetailerEmailConfig();
                });
        }
    }
    
    $scope.getRetailerEmailConfig = function() {
        var apiUrl = API.RETAILER.RETAILER_EMAIL_CONFIG.LIST;
            
        //Create record
        RestHTTPService
            .post(apiUrl,{'query':{retailerId: $state.current.data.params.id}})
            .success(function(response) {
                if (response !== undefined && response.data !== undefined && response.data.length > 0){
                    $scope.retailerEmailConfig = response['data'][0];
                }
            });
    }
    var evtGetRetailerEmailConfiguration = $rootScope.$on('getRetailerEmailConfiguration', function(){
        $scope.getRetailerEmailConfig();
    });
    
    $scope.$on('$destroy', function() {
        evtGetRetailerEmailConfiguration();
    });
}]);