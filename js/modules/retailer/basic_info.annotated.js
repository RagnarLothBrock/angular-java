'use strict';
app.controller('RetailerBasicInfoController',[
                '$scope', '$rootScope', '$state', 'notification', 'Retailer','$location'
,    function(   $scope, $rootScope, $state, notification, Retailer ,$location
    ){
    
    
    /*** Error Validation Data Block **/
	//Validate Retailer Details
	var hasErrorsRetailer = function(retailer) {
		if (isEmpty(retailer.name,'name',"Enter retailer name",$scope.notification)) {
			$scope.errors.name = "Enter retailer name";
		}
		if (isEmpty(retailer.code,'code',"Enter retailer code",$scope.notification)) {
			$scope.errors.code = "Enter retailer code";
		}
		if (isEmpty(retailer.address1,'address1',"Enter retailer address",$scope.notification)) {
			$scope.errors.address1 = "Enter retailer address";
		}
		if (isEmpty(retailer.city,'city',"Enter retailer city",$scope.notification)) {
			$scope.errors.city = "Enter retailer city";
		}
		if (isEmpty(retailer.pincode,'pincode',"Enter retailer pincode",$scope.notification)) {
			$scope.errors.pincode = "Enter retailer pincode";
		}
		else if (retailer.pincode.length !== 6) {
			$scope.errors.pincode = "Pincode should be 6 digits!";
		}
		if (isEmpty(retailer.email,'email',"Enter retailer email",$scope.notification)) {
			$scope.errors.email = "Enter retailer email";
		}
		else if (!validEmail(retailer.email)) {
            $scope.errors.email = "Enter valid email";
        }
		if (isEmpty(retailer.telephoneNumber,'telephoneNumber',"Enter retailer telephone number",$scope.notification)) {
			$scope.errors.telephoneNumber = "Enter retailer telephone number";
		}
		
		
		if (Object.getOwnPropertyNames($scope.errors).length > 0) {
			return true;
		}
		return false;
	}
    
    /** Delete Data Block **/
	$scope.deleteRetailer = function(retailer,index) {
		ngDialog.openConfirm({
			template: '<div class="dialog-contents">' +
							'Do you want to delete this retailer?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
			controller: ['$scope', function($scope) { 
			  // Controller logic here
			}]
		}).then(function (success) {
			// Success logic here
			Retailer.del(retailer).success(function(response) {
				console.log(response);
				$scope.retailers.splice(index,1);
			});
		}, function (error) {
			// Error logic here
		});
	}
    
    //Save Retailer Data
	$scope.saveRetailer = function(retailer) {
		//Validate Retailer
		$scope.errors = {};
		
		if(hasErrorsRetailer(retailer.retailer)) return false;
		
		if (retailer.retailer.id == undefined || retailer.retailer.id == "" || retailer.retailer.id == null) {
			//Continue with retailer create
			Retailer.create(retailer.retailer).success(function(response) {
                if (response !== undefined && response.response !== undefined && response.response !== null) {
                    $rootScope.$broadcast ('showSuccessMessage',response);
				
                    $scope.retailer.retailer.id = response.response.id;
                    
                    //$scope.retailer = response;
                    $location.path('/retailer/get/'+response.response.id);
                }
			});
		}
		else {
			//Continue with retailer Update
			Retailer.update(retailer.retailer).success(function(response) {
                if (response !== undefined && response.response !== undefined && response.response !== null) {
                    $rootScope.$broadcast ('showSuccessMessage',response);
                    //Check if templates are copied or not
                    //$rootScope.$broadcast ('checkAndCopySMSEmailTemplates');
                }
			});
		}
	}
    
    var getRetailer = function(id) {
        Retailer.get({id : id}).success(function(response) {
			$scope.retailer.retailer = angular.extend($scope.retailer.retailer,response);
            $scope.breadcrumbs.push({'title' : $scope.retailer.retailer.name});
		});
    }
    
    var evtGetRetailer = $rootScope.$on('getRetailer', function(e, args) {
        getRetailer(args);
    });
    
    //Get Retailer details
	if ($state.current.url == '/retailer/get/:id') {
		$scope.retailer.retailer.id = $state.current.data.params.id;
		$rootScope.$broadcast ('getRetailer',$state.current.data.params.id);
	}
    
    $scope.$on('$destroy', function() {
        evtGetRetailer();
    });
}]);