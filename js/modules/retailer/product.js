'use strict';

app.factory('Product', function($http, REST_API_URL, API, Upload) {

	return {
		list: function(params) {
			var getUrl = API.RETAILER.PRODUCT.LIST
					.replace('<retailerId>', params.query.retailerId)
					.replace('<first>', params.firstResult)
					.replace('<limit>', params.maxResults);
			return $http.post(REST_API_URL + getUrl, params);
			//return $http.post(REST_API_URL + API.RETAILER.PRODUCT.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.RETAILER.PRODUCT.CREATE, {'product':params});
		},
		get: function(params) {
			var getUrl = API.RETAILER.PRODUCT.GET.replace('<productid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.RETAILER.PRODUCT.UPDATE, {'product':params});
		},
		del: function(params) {
			var getUrl = API.RETAILER.PRODUCT.DELETE.replace('<productid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		upload: function(params) {
			return Upload.upload({
                url: REST_API_URL + API.RETAILER.PRODUCT.UPLOAD,
                data: params.data,
                file: params.file
            })
		}
	}
});
app.controller('RetailerProductController',
	['$scope', '$rootScope', '$state', 'notification', 'AuthTokenFactory', 'ngDialog', '$location', 'logger',
		'Product', 'PackagingType', 'ProductSubCategory', 'BrandService', 'ProductCategory', 'LIST_PAGE_SIZE', 'Utility',
    function($scope, $rootScope, $state,  notification, AuthTokenFactory, ngDialog, $location, logger,
			 Product, PackagingType, ProductSubCategory, BrandService, ProductCategory, LIST_PAGE_SIZE, Utility) {
		console.log("COntroller Called");
		$scope.pageSize = LIST_PAGE_SIZE;
		$scope.total_count = 0;
		$scope.currentPage = 1;
		
		$scope.uploadPercentage = 0;
        $scope.notification = $rootScope.notification;        
        $scope.products = [];
        $scope.pageActionHeader = "";
		$scope.errors = {};
		$scope.product = {};
		$scope.productFilter = {};
		$scope.showProductList = false;
		$scope.showProductSave = false;
		$scope.showProductUpload = false;
		$rootScope.showAddNewbtn = false;
		
		$scope.makePageVisible = function(page) {
			function hide() {
                $scope.showProductList = false;
				$scope.showProductSave = false;
				$scope.showProductUpload = false;
            }
			
			hide();
			$scope[page]= true;
			console.log(page, $scope);
		}
		
		$rootScope.fnShowProductSave = function() {
			$rootScope.showAddNewbtn = false;
			$scope.makePageVisible('showProductSave');
			$scope.product = {};
			$scope.form.$invalid = false;
			$scope.populateDropdowns();
		}
		
		$rootScope.fnShowProductUpload = function() {
			$rootScope.showAddNewbtn = false;
			$scope.makePageVisible('showProductUpload');
			$scope.product = {};
			//$scope.form.$invalid = false;
			//$scope.populateDropdowns();
		}
		$rootScope.fnShowProductList = function() {
			$rootScope.showAddNewbtn = false;
			$scope.makePageVisible('showProductList');
			$scope.getProductList(1, $scope.pageSize);
			$scope.productFilter = {};
			$scope.retailerProductFilterForm.$invalid = false;
			$scope.populateDropdowns();
		}
		
		$scope.getProductList = function(first, limit) {
			$scope.makePageVisible('showProductList');
			$rootScope.showAddNewbtn = true;
			
			$scope.currentPage = first // initialize page no to 1
			
			var query = $scope.productFilter;
			query.retailerId = $state.current.data.params.id;
			
			first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
			Product.list({'query':query,firstResult: first, maxResults: limit}).success(function(response) {
				if (response !== undefined && response.data !== undefined){
					$scope.products = response.data;
					$scope.total_count = response.totalRecords;
				}
			});
		}
		
		$scope.getRetailerProduct = function(product) {
			//$scope.product = product;
			$scope.makePageVisible('showProductSave');
			//$scope.makePageVisible('showProductUpload');
			
			$scope.getProductDetails(product.id);
		}
		
		$scope.populateDropdowns = function() {
			Utility.populate_packging_material_type({},$scope);
			Utility.populate_brands({},$scope);
			Utility.populate_category({},$scope);
			Utility.populate_brands({},$scope);
		}
		
		$scope.populateSubCategory = function(categoryId) {
			Utility.populate_sub_category({productCategoryId:categoryId},$scope);
		}
		
		$scope.getProductDetails = function(productId) {
			$rootScope.currentStepName = "Update Product";
			$scope.populateDropdowns();
			
			$scope.product.id = productId;
			Product.get($scope.product).success(function(response) {
				$scope.product = response;				
				var subCat = {id: response.subCategoryId};
				ProductSubCategory.get(subCat).success(function(response) {
					$scope.product.categoryId = response.productCategoryId;
					
					$scope.populateSubCategory($scope.product.categoryId);
				});
			});
		}
		
		$scope.populateSubCategory = function(cat) {
			Utility.populate_sub_category({productCategoryId:cat},$scope);
		}
		
		//Save Product Details
		$scope.saveProductDetails = function(product) {
			if ($scope.form.$invalid) {
                return false;
            }
			
			$scope.errors = {};
			//Validate Product
			if(hasErrors(product)) return false;
			
			delete product.categoryId;
			delete product.selectedFileName;
			
			product.retailerId = $state.current.data.params.id;
			
			if (product.id == undefined || product.id == "" || product.id == null) {
                //Continue with product create
				Product.create(product).success(function(response) {
					if (response == "") {
                        notification.error("Error in creating new product!");
						return;
                    }
					$scope.productFilter = {};
					$scope.filterRetailerProducts();
				});
            }
			else {
				//Continue with product Update
				Product.update(product).success(function(response) {
					if (response == "") {
                        notification.error("Error in saving product data!");
						return;
                    }
					$scope.productFilter = {};
					$scope.filterRetailerProducts();
				});
			}
		}
		
		//Delete Existing Product
		$scope.deleteProduct = function(product, index) {
			ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete this product?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				console.log(product,index);
				Product.del(product).success(function(response) {
					if (response) {
                        $state.go('rl.masters-products');
                    }
				});
			}, function (error) {
				// Error logic here
			});
			
		}
		
		//Validate Product Details
		var hasErrors = function(product) {
			if (isEmpty(product.name,'name',"Enter product name",$scope.notification)) {
                $scope.errors.name = "Enter product name";
            }
			if (isEmpty(product.code,'code',"Enter product code",$scope.notification)) {
                $scope.errors.code = "Enter product code";
            }
			if (isEmpty(product.packagingTypeBoxId,'packagingTypeBoxId',"Select packaging type box",$scope.notification)) {
                $scope.errors.packagingTypeBoxId = "Select packaging type box";
            }
			
			if (isEmpty(product.categoryId,'categoryId',"Select category",$scope.notification)) {
                $scope.errors.categoryId = "Select category";
            }
			if (isEmpty(product.subCategoryId,'subCategoryId',"Select sub category",$scope.notification)) {
                $scope.errors.subCategoryId = "Select sub category";
            }
			if (isEmpty(product.brandId,'brandId',"Select brand",$scope.notification)) {
                $scope.errors.brandId = "Select brand";
            }
			
			if (Object.getOwnPropertyNames($scope.errors).length > 0) {
				return true;
			}
			return false;
		}
		
		$scope.filterRetailerProducts = function(productFilter) {			
			$scope.populateDropdowns();
			$scope.getProductList(1, $scope.pageSize);
		}
		
		$scope.clearSearchFields = function() {
			$scope.retailerProductFilterForm = {};
			$scope.productFilter = {};
			$scope.getProductList(1, $scope.pageSize);
		}
		
		$scope.upload = function(product) {
			product.retailerId = $state.current.data.params.id;
			Product.upload({data:product, file: product.file})
				.progress(function (evt) {
					var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
					$scope.uploadPercentage = progressPercentage;
					console.log(progressPercentage);
				})
				.success(function (data, status, headers, config) {
					$rootScope.$broadcast('showUploadReturnMessage', {data:data, status:status, headers:headers, config:config});
					$scope.product = {data:{}, file:{}};
				})
				.error(function(error){
					var errorM = "";
					$(error.message).each(function(){
						errorM += this +"<br/>";
					});
					notification.error(errorM)
				});
		}
		
		$scope.$watch('product.file', function () {
			$scope.product.selectedFileName = ($scope.product.file !== undefined) ? $scope.product.file.name : "";
		});
		
		var evtGetAllRetailerProducts = $rootScope.$on('getAllRetailerProducts', function(e, args) {
			$scope.filterRetailerProducts();
		});
		
		$scope.$on('$destroy', function() {
			evtGetAllRetailerProducts();
		});
    }]);