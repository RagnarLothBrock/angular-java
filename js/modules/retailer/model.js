'use strict';

var retailer = function(){
	return {
		retailer : {
			"address1":"",
			"address2" : "",
			"altTelephoneNumber" :"",
			"city" :"",
			"code" :"",
			"email" :"" ,
			"name" : "",
			"pincode" : "",
			"telephoneNumber" : ""
		},		
		packingCostBorne : [],
		advanceReplacementProducts : [],
		//exchangePolicyProducts : [],
		returnPickupLocations : [],
		serviceProviders: [],
		list: {
			serviceMaster : {
				list : [],
				selectedIds : [],
				selectedObj : [],
				errors : {}
			},
			productCategory : {
				list : [],
				selectedIds : [],
				selectedObj : [],
				errors : {}
			},
			costPerService : {
				availedServices: [],
				categoriesServiced : [],
				selectedObj : [],
				errors : [],
				template : {
					serviceId: null,
					categoryId: null,
					cost: null,
					retailerId: null,
					id: null
				},
				bulkCreate: [],
				bulkUpdate: []
			},
			serviceableLocations : {
				list: [],
				selectedIds: [],
				errors: {},
				pincode: "",
				template: {
					pincode: "",
					retailerId: ""
				}
			},
			odaLocations : {
				list: [],
				selectedIds: [],
				errors: {},
				pincode: "",
				template: {
					pincode: "",
					retailerId: ""
				}
			},
			packingMaterialType: {
				list : [],
				selectedObj: [],
				errors: [],
				template : {
					id: null,
					retailerId: null,
					packingMaterialId: null,
					materialCost: null,
					packingCost: null
				},
				bulkCreate: [],
				bulkUpdate: []
			},
			packingSpecification: {
				list : [],
				selectedObj: [],
				errors: [],
				template : {
					id: null,
					retailerId: null,
					categoryId: null,
					serviceId: null,
					oldPickupLocal:null,
					oldPickupOutstation:null,
					newPickupLocal:null,
					newPickupOutstation:null
				},
				bulkCreate: [],
				bulkUpdate: []
			},
			processField: {
				list : [],
				selectedObj: [],
				errors: [],
				template : {
					id: null,
					retailerId: null,
					canonicalField: null,
					retailerField: null
				},
				bulkCreate: [],
				bulkUpdate: []
			},
			packingCostBorne: {
				selectedObj:[],
				template : {
					id: null,
					brandId: null,
					retailerId: null,
					categoryId: null
				},
				errors: [],
				bulkCreate: [],
				bulkUpdate: []
			},
			advanceReplacementProducts: {
				selectedObj:[],
				template : {
					id: null,
					retailerId: null,
					productId: null
				},
				errors: [],
				bulkCreate: [],
				bulkUpdate: []
			},
			exchangePolicyProducts: {
				selectedObj:[],
				template : {
					id: null,
					retailerId: null,
					productId: null
				},
				errors: [],
				bulkCreate: [],
				bulkUpdate: []
			},
			product: {
				list: []
			},
			brand: {
				list: []
			},
			subCategory: {
				list:[]
			},
			returnOrPickupLocations: {
				list:[],
				template: {
					id : "",
					retailerId : "",
					sellerCode : "",
					sellerName : "",
					sellerLocation : "",
					sellerAddress : "",
					sellerPhoneNumber : "",
					sellerAlternatePhoneNumber : "",
					sellerEmail : "",
					sellerPincode : ""
				},
				errors: [],
				sellerCategories: {
					list: [],
					selectedObj: [],
					selectedIds: [],
					sellerCategoryIds : [],
					template: {
						retailerId : "",
						categoryId : "",
						sellerId: ""
					}
				}
			}
		},
		serviceProviderList: [],
		serviceProviderCityList: [],
		serviceProviderLocationList:[],
		serviceProviderCategoryList:[],
		serviceProviderSubCategoryList: [],
		serviceProviderBrandList: [],
		serviceProvider: {
			id: null,
			retailerId: null,
			serviceProviderLocationId : null,
			categoryId: null,
			brandId: null,
			subCategoryId : null,
			cityId: null,
			city: null
		},
		productPacking: {
			id: null,
			productId: null,
			retailerId: null,
			packagingTypeId : null,
			type: null,
			packingSpec: null
		},
		retailerServiceProviderList:[],
		productPackingList:[],
		other: {
			id : "",
			retailerId: "",
			isAllowOnlyASP : ""
		}
	}
}
