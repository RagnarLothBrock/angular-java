app.controller('SmsEmailTriggerTemplateController',
               ['$scope', 'Utility', 'notification', 'RetailerSMSEmailTemplate', 'ngDialog', 'SMSEmailTemplate', '$rootScope', 'MAX_DROPDOWN_SIZE',
        function($scope, Utility, notification, RetailerSMSEmailTemplate, ngDialog,SMSEmailTemplate, $rootScope, MAX_DROPDOWN_SIZE) {
    $scope.smsEmailTriggerConfig = {retailerId : $scope.retailer.retailer.id};
    $scope.smsEmailTemplate = {};
    $scope.smsEmailTriggerCodeList = [];
    $scope.paramsList = [];
    //$scope.smsEmailTemplateList = [];
    $scope.smsCKEditorOptions = {
        height: 100,
        toolbarCanCollapse:true,
        toolbarGroups : [
            { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
            { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
            { name: 'links', groups: [ 'links' ] },
            { name: 'insert', groups: [ 'insert' ] },
            { name: 'forms', groups: [ 'forms' ] },
            { name: 'tools', groups: [ 'tools' ] },
            { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
            { name: 'others', groups: [ 'others' ] },
            '/',
            { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
            { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
            { name: 'styles', groups: [ 'styles' ] },
            { name: 'colors', groups: [ 'colors' ] },
            { name: 'about', groups: [ 'about' ] }
        ],
        removeButtons : 'Underline,Subscript,Superscript,Link,Unlink,Anchor,Image,Table,HorizontalRule,SpecialChar,Maximize,Strike,RemoveFormat,NumberedList,BulletedList,Blockquote,Outdent,Indent,About'
    };
    
    $scope.saveRetailerSmsEmailTemplate = function(smsEmailTemplate) {
        if (isEmpty(smsEmailTemplate.emailTemplate, "emailTemplate") &&
            isEmpty(smsEmailTemplate.smsTemplate, "smsTemplate")) {
            $scope.SmsEmailTemplateForm.$invalid = true;
            $scope.SmsEmailTemplateForm.emailTemplate.$error.requiredSMSEmail = true;
            return false;
        }
        
        if ($scope.SmsEmailTemplateForm.$invalid) {
            notification.error("Form data is invalid. Please correct it.");
            return false;
        }
        
        $scope.smsEmailTemplate.retailerId = $scope.retailer.retailer.id;
        
        if ($scope.smsEmailTemplate.id !== undefined &&
            $scope.smsEmailTemplate !== null &&
            $scope.smsEmailTemplate !== "") {
            RetailerSMSEmailTemplate
                .update(smsEmailTemplate)
                .success(function(response) {
                    if (response == undefined) {
                        notification.error("Error in saving data! There might be a record with the same SMS/Email Trigger Code!");
                        return;
                    }
                    notification.success("Successfully saved all data!");
                    $scope.populateSMSEmailTemplateList();
                });
        }
        else {
            RetailerSMSEmailTemplate
                .create(smsEmailTemplate)
                .success(function(response) {
                    if (response == undefined) {
                        notification.error("Error in saving data! There might be a record with the same SMS/Email Trigger Code!");
                        return;
                    }
                    notification.success("Successfully saved all data!");
                    $scope.populateSMSEmailTemplateList();
                });
        }
    }
    
    $scope.populateSMSEmailTriggerCode = function() {
        Utility.populate_sms_email_trigger_code({}, $scope);
    }
    
    $scope.populateSMSEmailTemplateList = function() {
        
        RetailerSMSEmailTemplate
            .list({'query':{'retailerId':$scope.retailer.retailer.id}, firstResult:0, maxResults:MAX_DROPDOWN_SIZE})
            .success(function(response) {
                if (response !== undefined && response.data !== undefined && response.data.length > 0){
                    $scope.smsEmailTemplateList = response.data;
                }
            });
    }
    
    $scope.getRetailerSMSEmailTemplate = function(template) {
        $scope.smsEmailTemplate = template;
        $scope.smsEmailTemplate.retailerId = $scope.retailer.retailer.id;
        console.log("@@@@@@@@@@@@@@",template);
        $scope.$broadcast ('populateSMSEmailTemplate', {triggerCode:template.triggerCode});
    }
    
    $scope.deleteRetailerSMSEmailTemplate = function(template, index) {
        console.log(template);
        RetailerSMSEmailTemplate
            .del(template)
            .success(function(response) {
                if (response == "") {
                    notification.error("Error in deleting record!");
                    return;
                }
                notification.success("Successfully deleted this record!");
                $scope.smsEmailTemplateList.splice(index, 1);
            });
    }
    
    $scope.clearFields = function() {
        $scope.smsEmailTemplate = {};
        $scope.SmsEmailTemplateForm.$invalid = false;
    }
    
    $scope.$on('getSMSEmailTemplateList', function(e) {  
        $scope.populateSMSEmailTemplateList();
        $scope.populateSMSEmailTriggerCode();
    });
    
    $scope.populateRetailerSMSEmailTemplate = function(triggerCode) {
        $scope.smsEmailTemplate = {};
        $scope.smsEmailTemplate.smsEmailTriggerCode = triggerCode;
        $scope.populateSMSEmailTemplate(triggerCode);
        //var p = triggerCode.split("~");
        //$scope.description = p[1];
        //triggerCode = p[0];    
        RetailerSMSEmailTemplate
            .list({query:{'smsEmailTriggerCode':triggerCode,'retailerId' : $scope.retailer.retailer.id},
                  firstResult:0, maxResults:MAX_DROPDOWN_SIZE})
            .success(function(response) {
                if (response !== undefined && response.data !== undefined && response.data.length > 0){
                    $scope.smsEmailTemplate = response.data[0];
                    $scope.smsEmailTemplate.retailerId = $scope.retailer.retailer.id;
                }
            });
    }
    
    $scope.populateSMSEmailTemplate = function(triggerCode) {
        
        if (triggerCode === undefined) {
            $scope.masterSMSTemplate = {};
            $scope.paramsList = [];
            $scope.smsEmailTemplate.smsEmailTriggerCode = "";
            return;
        }
        
        //var p = triggerCode.split("~");
        //$scope.description = p[1];
        //triggerCode = p[0];        
        $scope.paramsList = [];
        
        SMSEmailTemplate
        .list({query:{'smsEmailTriggerCode':triggerCode}})
        .success(function(response) {
            if (response !== undefined && response.data !== undefined && response.data.length > 0){
                //$scope.smsEmailTemplate = response[0];
                $scope.masterSMSTemplate = response.data["0"];
                var params = response.data["0"];
                if (params.params.indexOf(",") !== -1) {
                    $scope.paramsList = params.params.split(",");
                }
                else
                    $scope.paramsList.push(params.params);
                console.log($scope.paramsList);
            }
        });
    }
    
    $scope.addParamToTemplate = function(param) {
        if ($scope.smsEmailTemplate.smsTemplate == null) {
            $scope.smsEmailTemplate.smsTemplate = "";
        }
        if ($scope.smsEmailTemplate.emailTemplate == null) {
            $scope.smsEmailTemplate.emailTemplate = "";
        }
        
        param = param.replace(/^\s+|\s+$/g, '');
        
        $scope.smsEmailTemplate.emailTemplate = $scope.smsEmailTemplate.emailTemplate + '  <p>${' + param + '}</p>';
        
        $scope.smsEmailTemplate.smsTemplate = $scope.smsEmailTemplate.smsTemplate + '  ${' + param + '}';
    }
    
    $scope.$on('populateSMSEmailTemplate', function(e, args){
        $scope.populateSMSEmailTemplate(args.triggerCode);
    });
    
    //$rootScope.$on('copySMSEmailTemplates', function(e){
    //     RetailerSMSEmailTemplate
    //        .copy_master_templates({retailerId:$scope.retailer.retailer.id})            
    //        .success(function(response) {
    //            
    //        });
    //});
    //
    //$rootScope.$on('checkAndCopySMSEmailTemplates', function(e){
    //    RetailerSMSEmailTemplate
    //        .list({'query':{'retailerId':$scope.retailer.retailer.id}})
    //        .success(function(response) {
    //            if (response !== undefined && response.length === 0) {
    //                $rootScope.$broadcast ('copySMSEmailTemplates');
    //            }
    //        });
    //});
}]);

app.controller('SmsEmailTriggerConfigController',
               ['$scope', 'Utility', 'notification', 'RetailerSMSEmailTriggerConfig', 'SMSEmailTriggerConfig', '$document',
        function($scope, Utility, notification, RetailerSMSEmailTriggerConfig, SMSEmailTriggerConfig, $document) {
    $scope.SMSEmailConfigList = [];
    $scope.getConfigList = function() {
        RetailerSMSEmailTriggerConfig
            .by_retailer({'retailerId':$scope.retailer.retailer.id})
            .success(function(response) {
                if (response == undefined) {
                    notification.error("Error in loading SMS/Email Configuration Settings!");
                    return;
                }
                $scope.SMSEmailConfigList = response;
            });
    }
    
    $scope.$on('populateConfigList', function(e) {  
        $scope.getConfigList();            
    });
    
    $scope.SMSEmailTriggerConfigTemplate = {
        id : "",
        retailerId : "",
        smsEmailTriggerCode: "",
        serviceId: "",
        isEmailRequired: 0,
        isSmsRequired: 0
    }
    $scope.saveRetailerSmsEmailTriggerConfig = function(SMSEmailConfigList) {
        var InsertArr = [], UpdateArr = [];
        for(var i in $scope.SMSEmailConfigList) {
            var item = $scope.SMSEmailConfigList[i];
            
            var templateItem = angular.copy($scope.SMSEmailTriggerConfigTemplate);
            templateItem.smsEmailTriggerCode = item.smsEmailTriggerCode;
            templateItem.retailerId = $scope.retailer.retailer.id;
            templateItem.serviceId = item.serviceId;
            templateItem.isEmailRequired = item.retailerIsEmailRequired;
            templateItem.isSmsRequired = item.retailerIsSmsRequired;
            templateItem.id = item.retailerSmsEmailTriggerConfigId;
            if (item.retailerSmsEmailTriggerConfigId == null) {
                delete templateItem.id;
                InsertArr.push(templateItem);
            }
            else
                UpdateArr.push(templateItem);
        }
        
        if (InsertArr.length > 0) {
            RetailerSMSEmailTriggerConfig
                .bulkcreate(InsertArr)
                .success(function(response) {
                    if (response == undefined) {
                        notification.error("Error while saving data!.");
                        return;
                    }
                    notification.success("Successfully saved data!");
                });
        }
        
        if (UpdateArr.length > 0) {
            RetailerSMSEmailTriggerConfig
                .bulkupdate(UpdateArr)
                .success(function(response) {
                    if (response == undefined) {
                        notification.error("Error while saving data!.");
                        return;
                    }
                    notification.success("Successfully saved data!");
                });
        }
    }
    
    $scope.checkAll = function(type) {
        for(var i in $scope.SMSEmailConfigList) {
            if (type == 'selectAllEmailRequired') {
                $scope.SMSEmailConfigList[i].retailerIsEmailRequired = $scope.selectAllEmailRequired;
            }
            if (type == 'selectAllSMSRequired') {
                $scope.SMSEmailConfigList[i].retailerIsSmsRequired = $scope.selectAllSMSRequired;
            }
        }
    }
}]);

