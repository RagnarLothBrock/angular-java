'use strict';
app.controller('RetailerServiceableLocationController',[
        '$scope', '$rootScope', '$state', 'notification', 'RetailerServiceableLocations', 'RetailerOdaLocations', 'LIST_PAGE_SIZE',
function($scope, $rootScope, $state, notification, RetailerServiceableLocations, RetailerOdaLocations, LIST_PAGE_SIZE)
{
    $scope.pageSize = LIST_PAGE_SIZE;
    $scope.total_count = 0;
    $scope.currentPage = 1;
    //$scope.getAllRetailerServiceableLocationsList = function(first)
    
	$scope.populateServiceableLocationsList = function(first, limit) {
        $scope.currentPage = first;
		first = $rootScope.calculateFirstRecord(first, limit);
		RetailerServiceableLocations
			.list({'query':{'retailerId':$scope.retailer.retailer.id},'firstResult':first, maxResults: limit})
			.success(function(response) {
                if (response !== undefined && response.data !== undefined && response.data.length > 0){
                    $scope.retailer.list.serviceableLocations.list = response.data;
                    $scope.total_count = response.totalRecords;
                    $scope.retailer.list.serviceableLocations.errors.pincode = null;
                }
				//$scope.retailer.list.serviceableLocations.list = response;
			});
	}
    
   
    
    $scope.saveRetailerServiceableLocations = function(retailer) {
		var findComma = retailer.list.serviceableLocations.pincode.indexOf(",");
		var pincodeList = [];
		if (findComma !== -1) {
            var pincodes = retailer.list.serviceableLocations.pincode.split(",");
			
			
			for(var i in pincodes) {
				var p = angular.copy($scope.retailer.list.serviceableLocations.template);
				console.log(pincodes[i].length);
				if (pincodes[i] !== "" && pincodes[i].length === 6) {
                    p.pincode = pincodes[i];
					p.retailerId = $scope.retailer.retailer.id;
					
					pincodeList.push(p);
                }
				
				if (pincodes[i] != "" && pincodes[i].length !== 6) {
					$scope.retailer.list.serviceableLocations.errors.pincode = "Data contains invalid pincodes!. Pincode should be 6 characters long."
					  setTimeout(function() {
						$scope.retailer.list.serviceableLocations.errors.pincode = "";
					  },3000);
                }
			}
			//Save data in DB
			RetailerServiceableLocations
				.bulkcreate(pincodeList)
				.success(function(response){
                    if (response && response.response) {
                        $rootScope.$broadcast ('showSuccessMessage',response);
                        retailer.list.serviceableLocations.pincode = "";
                        $scope.populateServiceableLocationsList(1, $scope.pageSize);
                    }
				});
        }
		else {
			var p = angular.copy($scope.retailer.list.serviceableLocations.template);
			p.pincode = retailer.list.serviceableLocations.pincode;
			p.retailerId = $scope.retailer.retailer.id;
			
			if (p.pincode === "" || p.pincode.length !== 6) {
				$scope.retailer.list.serviceableLocations.errors.pincode = "Data contains invalid pincodes!. Pincode should be 6 characters long."
				  setTimeout(function() {
					$scope.retailer.list.serviceableLocations.errors.pincode = "";
				  },3000);
				  
				  return;
			}
			
			$scope.retailer.list.serviceableLocations.pincode = null;
			
			//Save data in DB
			RetailerServiceableLocations
				.create(p)
				.success(function(response){
                    if (response && response.response.id) {
                        $rootScope.$broadcast ('showSuccessMessage',response);
                        p.id = response.response.id;
                        $scope.retailer.list.serviceableLocations.list.push(p);
                    }
				});
		}
	}
	$scope.searchSearvicablePincode =function(retailer){
        $scope.retailer.list.serviceableLocations.list = [];
        $scope.retailer.list.serviceableLocations.errors.pincode = "";
        var p = angular.copy($scope.retailer.list.serviceableLocations.template);
        p.pincode = retailer.list.serviceableLocations.pincode;
        p.retailerId = $scope.retailer.retailer.id;

        if (p.pincode === "" || p.pincode.length !== 6) {
            $scope.retailer.list.serviceableLocations.errors.pincode = "Data contains invalid pincodes!. Pincode should be 6 characters long."
            setTimeout(function() {
                $scope.retailer.list.serviceableLocations.errors.pincode = "";
            },3000);

            return;
        }
        RetailerServiceableLocations
            .list({'query':{'retailerId':$scope.retailer.retailer.id,'pincode':$scope.retailer.list.serviceableLocations.pincode}})
            .success(function(response) {
                if (response !== undefined && response.data !== undefined && response.data.length > 0){
                    $scope.retailer.list.serviceableLocations.list = response.data;
                    $scope.total_count = response.totalRecords;
                }else {
                    $scope.retailer.list.serviceableLocations.errors.pincode ="Not Found";
				}
                //$scope.retailer.list.serviceableLocations.list = response;
            });
	}
    
    $rootScope.deleteServiceableLocation = function(index) {
		
		var pincode = $scope.retailer.list.serviceableLocations.list[index];
			//Delete data in DB
			RetailerServiceableLocations
				.del(pincode)
				.success(function(response){
                    if (response && response.response === true) {
                        $rootScope.$broadcast ('showSuccessMessage',response);
                        $scope.retailer.list.serviceableLocations.list.splice(index,1);	
                    }		
				});
		
	}
    
	$scope.deleteRetailerServiceableLocation = function(index) {
		//deleteRetailerServiceableLocation(index);
		$rootScope.$broadcast ('deleteDialogAndConfirmEvent',"Do you want to delete this location?",
							   'deleteServiceableLocation',
							   index);
	}
	
	$scope.deleteAllRetailerServiceableLocation = function(selectedIds) {
		$rootScope.$broadcast ('deleteDialogAndConfirmEvent',"Do you want to delete all selected locations?",
							   'deleteAllServiceableLocation',
							   selectedIds);
    }
    
	$scope.deleteAllServiceableLocation = function(selectedIds) {
		if (selectedIds.length <= 0) {
            return;
        }
		//Delete all selected data in DB
		RetailerServiceableLocations
			.bulkdelete(selectedIds)
			.success(function(response){
				for(var index in $scope.retailer.list.serviceableLocations.list) {
					if (selectedIds.indexOf($scope.retailer.list.serviceableLocations.list[index].id) !== -1) {
						$scope.retailer.list.serviceableLocations.list.splice(index,1);
                    }
					
				}
			});
	}
    
    var evtPopulateServiceableLocationsList = $rootScope.$on('populateServiceableLocationsList', function(e, args) {
            $scope.populateServiceableLocationsList(1, $scope.pageSize);
        });
    var evtDeleteServiceableLocation = $rootScope.$on('deleteServiceableLocation', function(e, args) {
            $scope.deleteServiceableLocation(args);
        });
    var evtDeleteAllServiceableLocation = $rootScope.$on('deleteAllServiceableLocation', function(e, args) {
            $scope.deleteAllServiceableLocation(args);
        });
	$scope.$on('$destroy', function() {
		evtPopulateServiceableLocationsList();
        evtDeleteServiceableLocation();
        evtDeleteAllServiceableLocation();
	});
	
	
	$scope.populateOdaLocationsList = function(first, limit) {
        $scope.currentPage = first;
		first = $rootScope.calculateFirstRecord(first, limit);
		RetailerOdaLocations
			.list({'query':{'retailerId':$scope.retailer.retailer.id},'firstResult':first, maxResults: limit})
			.success(function(response) {
                if (response !== undefined && response.data !== undefined && response.data.length > 0){
                    $scope.retailer.list.odaLocations.list = response.data;
                    $scope.total_count = response.totalRecords;
                }
				//$scope.retailer.list.serviceableLocations.list = response;
			});
	}
    
   
    
    $scope.saveRetailerOdaLocations = function(retailer) {
		var findComma = retailer.list.odaLocations.pincode.indexOf(",");
		var pincodeList = [];
		if (findComma !== -1) {
            var pincodes = retailer.list.odaLocations.pincode.split(",");
			
			
			for(var i in pincodes) {
				var p = angular.copy($scope.retailer.list.odaLocations.template);
				console.log(pincodes[i].length);
				if (pincodes[i] !== "" && pincodes[i].length === 6) {
                    p.pincode = pincodes[i];
					p.retailerId = $scope.retailer.retailer.id;
					
					pincodeList.push(p);
                }
				
				if (pincodes[i] != "" && pincodes[i].length !== 6) {
					$scope.retailer.list.odaLocations.errors.pincode = "Data contains invalid pincodes!. Pincode should be 6 characters long."
					  setTimeout(function() {
						$scope.retailer.list.odaLocations.errors.pincode = "";
					  },3000);
                }
			}
			
			//Save data in DB
			RetailerOdaLocations
				.bulkcreate(pincodeList)
				.success(function(response){
                    if (response && response.response) {
                        $rootScope.$broadcast ('showSuccessMessage',response);
                        retailer.list.odaLocations.pincode = "";
                        $scope.populateOdaLocationsList(1, $scope.pageSize);
                    }
				});
        }
		else {
			var p = angular.copy($scope.retailer.list.odaLocations.template);
			p.pincode = retailer.list.odaLocations.pincode;
			p.retailerId = $scope.retailer.retailer.id;
			
			if (p.pincode === "" || p.pincode.length !== 6) {
				$scope.retailer.list.odaLocations.errors.pincode = "Data contains invalid pincodes!. Pincode should be 6 characters long."
				  setTimeout(function() {
					$scope.retailer.list.odaLocations.errors.pincode = "";
				  },3000);
				  
				  return;
			}
			
			$scope.retailer.list.odaLocations.pincode = null;
			
			//Save data in DB
			RetailerOdaLocations
				.create(p)
				.success(function(response){
                    if (response && response.response.id) {
                        $rootScope.$broadcast ('showSuccessMessage',response);
                        p.id = response.response.id;
                        $scope.retailer.list.odaLocations.list.push(p);
                    }
				});
		}
	}
    
    $rootScope.deleteOdaLocation = function(index) {
		
		var pincode = $scope.retailer.list.odaLocations.list[index];
			//Delete data in DB
			RetailerOdaLocations
				.del(pincode)
				.success(function(response){
                    if (response && response.response === true) {
                        $rootScope.$broadcast ('showSuccessMessage',response);
                        $scope.retailer.list.odaLocations.list.splice(index,1);	
                    }		
				});
		
	}
    
	$scope.deleteRetailerOdaLocation = function(index) {
		//deleteRetailerServiceableLocation(index);
		$rootScope.$broadcast ('deleteDialogAndConfirmEvent',"Do you want to delete this location?",
							   'deleteOdaLocation',
							   index);
	}
	
	$scope.deleteAllRetailerOdaLocation = function(selectedIds) {
		$rootScope.$broadcast ('deleteDialogAndConfirmEvent',"Do you want to delete all selected locations?",
							   'deleteAllOdaLocation',
							   selectedIds);
    }
    
	$scope.deleteAllOdaLocation = function(selectedIds) {
		if (selectedIds.length <= 0) {
            return;
        }
		//Delete all selected data in DB
		RetailerOdaLocations
			.bulkdelete(selectedIds)
			.success(function(response){
				for(var index in $scope.retailer.list.odaLocations.list) {
					if (selectedIds.indexOf($scope.retailer.list.odaLocations.list[index].id) !== -1) {
						$scope.retailer.list.odaLocations.list.splice(index,1);
                    }
					
				}
			});
	}
    
    var evtPopulateOdaLocationsList = $rootScope.$on('populateOdaLocationsList', function(e, args) {
            $scope.populateOdaLocationsList(1, $scope.pageSize);
        });
    var evtDeleteOdaLocation = $rootScope.$on('deleteOdaLocation', function(e, args) {
            $scope.deleteOdaLocation(args);
        });
    var evtDeleteAllOdaLocation = $rootScope.$on('deleteAllOdaLocation', function(e, args) {
            $scope.deleteAllOdaLocation(args);
        });
	$scope.$on('$destroy', function() {
		evtPopulateOdaLocationsList();
        evtDeleteOdaLocation();
        evtDeleteAllOdaLocation();
	});
}]);