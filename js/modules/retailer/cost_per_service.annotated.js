'use strict';
app.controller('RetailerCostPerServiceController',[
        '$scope', '$rootScope', '$state', 'notification', 'RetailerServicesCost',
function($scope, $rootScope, $state, notification, RetailerServicesCost)
{
    //Get Retailer Cost Per Services
    $scope.getRetailerServicesCost = function() {
        RetailerServicesCost
				.list({'query':{'retailerId' : $scope.retailer.retailer.id}})
				.success(function(response) {
					//console.log(response);
					$scope.retailer.list.costPerService.selectedObj = response;
					if ($scope.retailer.list.costPerService.selectedObj.length == 0) {
						$scope.addMoreCostPerService({},0);
					}
				});
    }
    
    var evtGetRetailerServicesCost = $rootScope.$on('getRetailerServicesCost', function(e, args) {
        $scope.getRetailerServicesCost(args);
        if ($scope.retailer.list.costPerService.selectedObj.length == 0) {
            $scope.addMoreCostPerService({},0);
        }
    });
    
    
    
    $scope.addMoreCostPerService = function(cPerService,index) {
		if($scope.retailer.list.costPerService.selectedObj.length > 0)
		{
			if (cPerService != undefined && cPerService.serviceId !== undefined) {
                $scope.retailer.list.costPerService.selectedObj[index] = cPerService;
            }
		}
		$scope.retailer.list.costPerService.selectedObj
			.push(angular.copy($scope.retailer.list.costPerService.template) );
	}
    
    $scope.deleteRetailerServicesCost = function(index) {
		var obj = $scope.retailer.list.costPerService.selectedObj[index];
		RetailerServicesCost
				.del(obj)
				.success(function(response) {
					if (response && response.response === true) {
                        $scope.retailer.list.costPerService.selectedObj.splice(index,1);
						if ($scope.retailer.list.costPerService.selectedObj.length == 0) {
							$scope.addMoreCostPerService({},0);
						}
                    }
				});
	}
    
    var evtDeleteRetailerServicesCost = $rootScope.$on('deleteRetailerServicesCost', function(e,index) {
        $scope.deleteRetailerServicesCost(index);
    });
    
	$scope.deleteCostPerService = function(index) {
		var obj = $scope.retailer.list.costPerService.selectedObj[index];
		//Delete from DB
		if (obj !== undefined  && obj.id !== null && obj.id !== undefined) {
           $rootScope.$broadcast ('deleteDialogAndConfirmEvent', "Are you sure want to delete retailer services cost?", 'deleteRetailerServicesCost', index);
        }
	}
    
    //Save Retailer Cost Per Service
	$scope.saveRetailerCostPerService = function(retailer) {
		//Validate Retailer
		$scope.retailer.list.costPerService.errors = {};
		
		var hasErrors = hasErrorsRetailerCostPerService(retailer);
		
		if (!hasErrors) {
			if ($scope.retailer.list.costPerService.bulkUpdate.length > 0) {
                //Save Details
				RetailerServicesCost
					.bulkupdate($scope.retailer.list.costPerService.bulkUpdate)
					.success(function(response) {
					});
            }
			
			if ($scope.retailer.list.costPerService.bulkCreate.length > 0) {
                //Save Details
				RetailerServicesCost
					.bulkcreate($scope.retailer.list.costPerService.bulkCreate)
					.success(function(response) {
						getRetailerServicesCost();
					});
            }
        }
	}
    
    //Validate Retailer Cost Per Service
	var hasErrorsRetailerCostPerService = function(retailer) {
		$scope.retailer.list.costPerService.bulkCreate = [],
		$scope.retailer.list.costPerService.bulkUpdate = [];
		$scope.retailer.list.costPerService.errors = [];
		var selectedObj = $.unique(retailer.list.costPerService.selectedObj);
		for(var i = 0; i < selectedObj.length; i++) {
			if (selectedObj[i] === undefined) {
                continue;
            }
			var e = {};
			if (selectedObj[i].serviceId == null) {
                e.serviceId = "Please select a service";
            }
			if (selectedObj[i].categoryId == null) {
                e.categoryId = "Please select a category";
            }
			if (selectedObj[i].cost == null) {
                e.cost = "Please enter cost";
            }
			if (e.categoryId !== undefined || e.serviceId !== undefined || e.cost !== undefined) {
                $scope.retailer.list.costPerService.errors[i] = e;
            }
			
			$scope.retailer.list.costPerService.selectedObj[i].retailerId = $scope.retailer.retailer.id;
			
			if (selectedObj[i].id == null) {
                $scope.retailer.list.costPerService.bulkCreate.push(selectedObj[i]);
            }
			else {
				$scope.retailer.list.costPerService.bulkUpdate.push(selectedObj[i]);
			}
		}
		
		if ($scope.retailer.list.costPerService.errors.length > 0) {
			return true;
		}
		return false;
	}
    
    $scope.$on('$destroy', function() {
        evtGetRetailerServicesCost();
        evtDeleteRetailerServicesCost();
    });
}]);
