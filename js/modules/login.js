'use strict'

app.config(['$stateProvider', '$urlRouterProvider','$httpProvider',
    function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $stateProvider
        .state( 'login', {
			url: '/login',
			views: {
				'top-header': {
					templateUrl: 'partials/tpl/common/login-header.tpl.html'
				},
				'main-content': {
					templateUrl: 'partials/tpl/common/login.tpl.html'
				},
				'page-footer': {
					templateUrl: 'partials/tpl/common/footer.tpl.html'
				}
            },
			controller : 'loginCntrl',			
			data : {
				layout : "login",
				requireLogin : false,
                page : "Login"
			}
		})
		.state( 'root', {
			url: '/',
			views: {
				'top-header': {
					templateUrl: 'partials/tpl/common/login-header.tpl.html'
				},
				'main-content': {
					templateUrl: 'partials/tpl/common/login.tpl.html'
				},
				'page-footer': {
					templateUrl: 'partials/tpl/common/footer.tpl.html'
				}
            },
			controller : 'loginCntrl',			
			data : {
				layout : "login",
				requireLogin : false,
                page : "Login"
			}
		})
		.state('rlna.forgot-password', {
				url: '/forgot-password',
				views: {
					'main-content@': {
						templateUrl: 'partials/tpl/common/forgot_password.tpl.html'
					}
				},
				controller: 'ForgotPasswordController',
				data : {
					layout : "login",
					requireLogin : false,
					page : "Forgot Password"
				}
			})
		.state('rlna.reset-password', {
				url: '/reset-password/:key/:email',
				views: {
					'main-content@': {
						templateUrl: 'partials/tpl/common/reset_password.tpl.html'
					}
				},
				controller: 'ResetPasswordController'
			})
    }]);

app.controller('loginCntrl', function() {
});

app.controller('ForgotPasswordController', ['$scope', 'UserService', 'notification', function($scope, UserService, notification){
	$scope.forgotPassword = function(email) {
		UserService
			.generate_reset_password_token({email:email})
			.success(function(response) {
				notification.success("Password reset link has been sent to your registered email. Follow the link to reset your password. This link will be active for next 24 hours.");
			});
	}
}]);

app.controller('ResetPasswordController',
			   ['$scope', '$state', 'UserService', 'notification',
		function($scope, $state, UserService, notification){
	$scope.user = {'email' : $state.current.data.params.email, "token" : $state.current.data.params.key}
	$scope.resetPassword = function(user) {
		UserService
			.reset_password(user)
			.success(function(response) {
				notification.success("Successfully reset your password. Please login to continue!");
				$state.go('login');
			});
	}
}]);

app.factory('LoginService', function($http, REST_API_URL, API){
	return {
		login : function(user) {
			return $http({
					url:REST_API_URL + API.LOGIN,
					method:"POST",
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded'
					},
					data: user
				});
		},
        logout : function() {
            return $http.post( REST_API_URL + API.LOGOUT, {} );
        },
		sessionCheck : function() {
			return $http.post(REST_API_URL + API.SESSION_CHECK );
		},
		populate_menubar: function() {
			return $http.post( REST_API_URL + API.POPULATE_MENU, {} );
		},
		has_access: function(menu,feature) {
			return $http.post( REST_API_URL + API.HAS_ACCESS, {menubar:menu, feature:feature});
		}
	}
});