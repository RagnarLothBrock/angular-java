'use strict'

app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
    function ($stateProvider, $urlRouterProvider, $httpProvider) {

        $stateProvider
            .state('rl.version', {
                url: '/admin/version',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/version.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Release Summary",
                    requireLogin: true,
                    checkAccess: false
                }
            })
            .state('rl.tasks', {
                url: '/tasks/retailer/completed:first/:limit',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/completed.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Tasks - completed",
                    status: 'completed',
                    requireLogin: true,
                    checkAccess: false
                }
            })
            .state('rl.hub', {
                url: '/hub/completed:first/:limit',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/completed.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Tasks - completed",
                    status: 'completed',
                    requireLogin: true,
                    checkAccess: false
                }
            })
            .state('rl.tasks-completed', {
                url: '/tasks/involved/:first/:limit',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/involved.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Tasks - Unclaimed",
                    status: 'involved',
                    requireLogin: true,
                    checkAccess: false
                }
            })
            .state('rl.tasks-assigned', {
                url: '/tasks/assigned/:first/:limit',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/assigned.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Tasks - Assigned",
                    status: 'assigned',
                    requireLogin: true,
                    checkAccess: false
                }
            })
            .state('rl.task-view-assigned', {
                url: '/task/:id/view-assigned',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/view.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Task - View",
                    status: 'assigned',
                    processId: "",
                    assigneeNumber: null,
                    customerNumber: null,
                    taskName: "",
                    requireLogin: true,
                    checkAccess: false
                }
            })
            .state('rl.task-view-involved', {
                url: '/task/:id/view-involved',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/view.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Task - View",
                    status: 'involved',
                    processId: "",
                    taskName: "",
                    requireLogin: true,
                    checkAccess: false
                }
            })
            .state('rl.task-view', {
                url: '/task/:id/view',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/view_searched_task.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Task - View",
                    status: 'view',
                    processId: "",
                    taskName: "",
                    requireLogin: true,
                    checkAccess: false
                }
            })
            .state('rl.tasks-search', {
                url: '/tasks/search',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/search/search.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Search Tickets",
                    requireLogin: true,
                    checkAccess: false
                }
            })

            //Now Added For Report Generation
            .state('rl.report-generate', {
                url: '/generate/report',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/search/search.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Search Tickets",
                    requireLogin: true,
                    checkAccess: false
                }
            })
            .state('rl.tasks-search-completed', {
                url: '/tasks/search/completed',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/completedSearch/completed_search.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Search Completed Tickets",
                    requireLogin: true,
                    checkAccess: false
                }
            })
            .state('rl.finance-location-view', {
                url: '/locationview',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/locationViewSearch/search.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Location View",
                    requireLogin: true,
                    checkAccess: false
                }
            })
            // .state('rl.finance-completed-ticket-view-search', {
            // 	url: '/completedticketview',
            // 	views: {
            // 		'page-main-content@': {
            // 			templateUrl: 'partials/tpl/pages/tasks/completedTicketViewSearch/search_completed_ticket.tpl.html'
            // 		}
            // 	},
            // 	controller: 'TasksController',
            // 	data: {
            // 		page: "Ticket View",
            // 		requireLogin: true,
            // 		checkAccess: false
            // 	}
            // })
            .state('rl.finance-Party-view', {
                url: '/partyview',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/partyViewSearch/search.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Party View",
                    requireLogin: true,
                    checkAccess: false
                }
            })

            //Hum Manager Finance Menu Template
            .state('rl.finance-hub-view', {
                url: '/finance/hub',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/partyViewSearch/search.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Finance View",
                    requireLogin: true,
                    checkAccess: false
                }
            })

            // .state('rl.finance-account-payable-search', {
            // 	url: '/accountspayable',
            // 	views: {
            // 		'page-main-content@': {
            // 			templateUrl: 'partials/tpl/pages/tasks/accountPayableSearch/search.tpl.html'
            // 		}
            // 	},
            // 	controller: 'TasksController',
            // 	data: {
            // 		page: "Account Payable",
            // 		requireLogin: true,
            // 		checkAccess: false
            // 	}
            // })
            // .state('rl.finance-account-receivable-search', {
            // 	url: '/accountsreceivable',
            // 	views: {
            // 		'page-main-content@': {
            // 			templateUrl: 'partials/tpl/pages/tasks/accountReceivableSearch/search.tpl.html'
            // 		}
            // 	},
            // 	controller: 'TasksController',
            // 	data: {
            // 		page: "Account Receivable",
            // 		requireLogin: true,
            // 		checkAccess: false
            // 	}
            // })
            .state('rl.my-completed', {
                url: '/tasks/my-completed/:first/:limit',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/my-completed.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "My Completed Tickets",
                    requireLogin: true,
                    checkAccess: false
                }
            })
            .state('rl.manager-unclaimed', {
                url: '/tasks/manager/unclaimed/:first/:limit',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/manager/unclaimed.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "All Unclaimed Tickets",
                    requireLogin: true,
                    checkAccess: false

                }
            })
            .state('rl.manager-assigned', {
                url: '/tasks/manager/assigned/:first/:limit',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/manager/assigned.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "All Assigned Tickets",
                    requireLogin: true,
                    checkAccess: false
                }
            })
            .state('rl.manager-completed', {
                url: '/tasks/manager/completed/:first/:limit',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/manager/completed.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "All Completed Tickets",
                    requireLogin: true,
                    checkAccess: false
                }
            })
            .state('rl.retailer-tasks', {
                url: '/tasks/retailer/:first/:limit',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/retailer/tickets.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Retailer Tickets",
                    requireLogin: true,
                    checkAccess: false,
                    first: 0,
                    limit: 10
                }
            })
            .state('rl.hub_manager-tasks', {
                url: '/tasks/hub_manager/:first/:limit',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/retailer/tickets.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Retailer Tickets",
                    requireLogin: true,
                    checkAccess: false,
                    first: 0,
                    limit: 10
                }
            })
            .state('rl.retailer-ticket-status', {
                url: '/ticketStatus',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/search/searchStatus.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Status",
                    requireLogin: true,
                    checkAccess: false,
                }
            }).state('rl.mis-ticket-search', {
            url: '/mis/ticket-search',
            views: {
                'page-main-content@': {
                    templateUrl: 'partials/tpl/pages/tasks/mis-report/search.tpl.html'
                }
            },
            controller: 'TasksController',
            data: {
                page: "MIS Report",
                requireLogin: true,
                checkAccess: false,
            }
        })
            .state('rl.auth-file-upload', {
                url: '/task/authorization-file/upload/:id',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/AUthFileUpload.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Authorization Letter",
                    requireLogin: true,
                    processId: ""
                }
            })
            .state('rl.updateCost', {
                url: '/task/productCost/update/:id/:cost',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/updateCost.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Update Product Cost",
                    requireLogin: true,
                    processId: ""
                }
            })

            .state('rl.damageCost', {
                url: '/task/damageCost/update/:id',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/updateDamageCost.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Update Product Cost",
                    requireLogin: true,
                    processId: ""
                }
            })

            .state('rl.additional-file-upload', {
                url: '/task/additional-file/upload/:id',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/AdditionalFile.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Additional File upload",
                    requireLogin: true,
                    processId: ""
                }
            })
            .state('rl.CallToCustomer', {
                url: '/task/call/:processId',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/CallToCustomer.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Call Customer",
                    requireLogin: true,
                    processId: ""
                }
            })
            .state('rl.ewaste-additional-file', {
                url: '/tasks/ewaste-additional-file/:id',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/ewasteAdditionalFile.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Release Summary",
                    requireLogin: true,
                    checkAccess: false
                }
            })
            .state('rl.bits-barcode-file', {
                url: '/tasks/bits-barcode-file/:id',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/bitsBarcodeFile.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Upload Barcode",
                    requireLogin: true,
                    checkAccess: false
                }
            })
            .state('rl.purchase-invoice-upload', {
                url: '/task/purchase-invoice/upload/:id',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/PurchaseInvoiceUpload.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Purchase Invoice",
                    requireLogin: true,
                    processId: ""
                }
            }).state('rl.download-retailer-tickets', {
            url: '/download/tickets',
            views: {
                'page-main-content@': {
                    templateUrl: 'partials/tpl/pages/tasks/download/search.tpl.html'
                }
            },
            controller: 'TasksController',
            data: {
                page: "Downloads",
                requireLogin: true,
                checkAccess: false,
            }
        })
            .state('rl.mis-report', {
                url: '/view/mis-report/:id',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/MisReport.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "MIS Report",
                    requireLogin: true,
                    processId: "",
                    first: 0,
                    limit: 10
                }
            })
            .state('rl.ewaste-mis-report', {
                url: '/view/ewaste-Mis/:id',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/ewasteMis.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "E-Waste Mis",
                    requireLogin: true,
                    processId: "",
                    first: 0,
                    limit: 10
                }
            })
            .state('rl.sub-ticket', {
                url: '/view/sub-ticket/:id',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/subTicket.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Sub Ticket",
                    requireLogin: true,
                    processId: "",
                    first: 0,
                    limit: 10
                }
            })
            .state('rl.evaluation-result', {
                url: '/task/evaluation/:category',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/EvalResult.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Evaluation Result",
                    requireLogin: true
                }
            })
            .state('rl.tech-evaluation-result', {
                url: '/task/tech_evaluation/:category',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/TechEvalResult.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Technical Evaluation Result",
                    requireLogin: true
                }
            })
            .state('rl.evaluation-history', {
                url: '/task/evaluation-history',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/EvalHistory.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Evaluation History",
                    requireLogin: true
                }
            })
            .state('rl.evaluation-compare', {
                url: '/task/evaluation-compare/:e1/:e2',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/EvalCompare.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Evaluation Comparison",
                    requireLogin: true
                }
            })
            .state('rl.bulk-pickup-tickets', {
                url: '/tickets/bulk-pickup',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/bulk-pickup/search.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Bulk Pickup",
                    requireLogin: true,
                    checkAccess: false,
                }
            })
            .state('rl.device-info', {
                url: '/device/info',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/device_details.tpl.html'
                    }
                },
                controller: 'TasksController',
                data: {
                    page: "Device Details",
                    requireLogin: true
                }
            })
            .state('rl.filtered_search_result', {
                url: '/search/result',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/tasks/search/test.tpl.html'
                    }
                },
                controller: 'TaskController',
                data: {
                    page: "Filtered Search",
                    requiredLogin: true
                },
                params: {
                    task: null
                }
            })
        /*.state('rl.retailer-unclaimed', {
				url: '/tasks/retailer/unclaimed/:first/:limit',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/tasks/retailer/unclaimed.tpl.html'
					}
				},
				controller: 'TasksController',
				data: {
					page: "All Unclaimed Tickets",
					requireLogin : true,
					checkAccess : false

				}
			})
			.state('rl.retailer-assigned', {
				url: '/tasks/retailer/assigned/:first/:limit',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/tasks/retailer/assigned.tpl.html'
					}
				},
				controller: 'TasksController',
				data: {
					page: "All Assigned Tickets",
					requireLogin : true,
					checkAccess : false
				}
			})
			.state('rl.retailer-completed', {
				url: '/tasks/retailer/completed/:first/:limit',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/tasks/retailer/completed.tpl.html'
					}
				},
				controller: 'TasksController',
				data: {
					page: "All Completed Tickets",
					requireLogin : true,
					checkAccess : false
				}
			})*/;
    }
]);

app.factory('TasksService', ["$http", "REST_API_URL", "AuthTokenFactory", "API", "Upload", function ($http, REST_API_URL, AuthTokenFactory, API, Upload) {

    return {
        involved: function (params, first, limit) {
            //return $http.post(REST_API_URL + API.TASKS.INVOLVED, {});
            var Url = API.TASKS.INVOLVED.replace('<first>', first).replace('<limit>', limit);
            return $http.post(REST_API_URL + Url, params);
        },
        getFinance: function (params) {
            var Url = API.TASKS.GET_FINANCE_DETAILS;
            return $http.post(REST_API_URL + Url, params);
            //return $http.post(REST_API_URL + API.TASKS.FIND_BY_VARIABLE_VALUES, params);
        },
        makePayment: function (params) {
            var Url = API.TASKS.PAY_TO_CUSTOMER;
            return $http.post(REST_API_URL + Url, params);
            //return $http.post(REST_API_URL + API.TASKS.FIND_BY_VARIABLE_VALUES, params);
        },
        makeCall: function (params) {
            var Url = API.TASKS.CALL_TO_CUSTOMER;
            return $http.post(REST_API_URL + Url, params);
            //return $http.post(REST_API_URL + API.TASKS.FIND_BY_VARIABLE_VALUES, params);
        },
        makeCallToCustomer: function (params) {
            var Url = API.TASKS.CALL_CUSTOMER.replace('<processId>', params.processId);
            return $http.post(REST_API_URL + Url, params);


        },
        checkStatus: function (params) {
            var Url = API.TASKS.PAYMENT_STATUS;
            return $http.post(REST_API_URL + Url, params);
            //return $http.post(REST_API_URL + API.TASKS.FIND_BY_VARIABLE_VALUES, params);
        },
        getFinanceDetails: function (params) {
            var Url = API.TASKS.GET_SINGLE_FINANCE_DETAILS;
            return $http.post(REST_API_URL + Url, params);
            //return $http.post(REST_API_URL + API.TASKS.FIND_BY_VARIABLE_VALUES, params);
        },

        assigned: function (first, limit) {
            //return $http.post(REST_API_URL + API.TASKS.ASSIGNED, {});
            var Url = API.TASKS.ASSIGNED.replace('<first>', first).replace('<limit>', limit);
            return $http.post(REST_API_URL + Url, {});
        },
        my_completed: function (first, limit, variablesOfInterest) {
            var Url = API.TASKS.MY_COMPLETED.replace('<first>', first).replace('<limit>', limit);
            return $http.post(REST_API_URL + Url, {variablesOfInterest: variablesOfInterest});
        },
        manager_assigned: function (first, limit) {
            var Url = API.TASKS.MANAGER_ASSIGNED.replace('<first>', first).replace('<limit>', limit);
            return $http.post(REST_API_URL + Url, {});
        },
        manager_unclaimed: function (first, limit) {
            var Url = API.TASKS.MANAGER_UNCLAIMED.replace('<first>', first).replace('<limit>', limit);
            return $http.post(REST_API_URL + Url, {});
        },
        manager_completed: function (first, limit) {
            var Url = API.TASKS.MANAGER_COMPLETED.replace('<first>', first).replace('<limit>', limit);
            return $http.post(REST_API_URL + Url, {variablesOfInterest: ["consumerName", "problemDescription", "productCategory", "product", "retailer", "telephoneNumber", "natureOfComplaint", "rlTicketStatus", "dateOfComplaint", "dateOfPurchase", "rlTicketNo", "ticketPriority"]});
        },
        retailer_assigned: function (first, limit) {
            var Url = API.TASKS.MANAGER_ASSIGNED.replace('<first>', first).replace('<limit>', limit);
            return $http.post(REST_API_URL + Url, {});
        },
        retailer_unclaimed: function (first, limit) {
            var Url = API.TASKS.MANAGER_UNCLAIMED.replace('<first>', first).replace('<limit>', limit);
            return $http.post(REST_API_URL + Url, {});
        },
        retailer_completed: function (first, limit) {
            var Url = API.TASKS.MANAGER_COMPLETED.replace('<first>', first).replace('<limit>', limit);
            return $http.post(REST_API_URL + Url, {variablesOfInterest: ["consumerName", "problemDescription", "productCategory", "product", "retailer", "telephoneNumber", "natureOfComplaint", "rlTicketStatus", "dateOfComplaint", "dateOfPurchase", "rlTicketNo"]});
        },
        claim: function (taskId) {
            var claimUrl = API.TASK.CLAIM.replace('<taskid>', taskId);
            return $http.post(REST_API_URL + claimUrl, {});
        },
        firstgroup: function (taskId, fieldValues) {
            var firstgroupUrl = API.TASK.FIRSTGROUP.replace('<taskid>', taskId);
            return $http.post(REST_API_URL + firstgroupUrl, fieldValues);
        },
        nextgroup: function (taskId, fillSessionId, fieldValues) {
            var nextgroupUrl = API.TASK.NEXTGROUP.replace('<taskid>', taskId).replace('<fillSessionId>', fillSessionId);
            return $http.post(REST_API_URL + nextgroupUrl, fieldValues);
        },
        complete: function (taskId, fillSessionId, fieldValues) {
            var completeUrl = API.TASK.COMPLETE.replace('<taskid>', taskId).replace('<fillSessionId>', fillSessionId);
            return $http.post(REST_API_URL + completeUrl, fieldValues);
        },
        find_by_variable_values: function (params) {
            var Url = API.TASKS.FIND_BY_VARIABLE_VALUES.replace('<first>', params.firstResult).replace('<limit>', params.maxResults);
            return $http.post(REST_API_URL + Url, params);
            //return $http.post(REST_API_URL + API.TASKS.FIND_BY_VARIABLE_VALUES, params);
        },

        get_rl_process_status: function (params) {
            var Url = API.TASKS.GET_PROCESS_STATUS;
            return $http.post(REST_API_URL + Url, params);
            //return $http.post(REST_API_URL + API.TASKS.FIND_BY_VARIABLE_VALUES, params);
        },

        generateSearchExcel: function (params) {
            var Url = API.TASKS.GENERATE_SEARCH_EXCEL;
            return $http.post(REST_API_URL + Url, params);
            //return $http.post(REST_API_URL + API.TASKS.FIND_BY_VARIABLE_VALUES, params);
        },
        generateCompletedSearchExcel: function (params) {
            var Url = API.TASKS.GENERATE_EXCEL_FOR_COMPLETED_TICKETS;
            return $http.post(REST_API_URL + Url, params);
            //return $http.post(REST_API_URL + API.TASKS.FIND_BY_VARIABLE_VALUES, params);
        },
        find_completed_by_variable_values: function (params) {
            var Url = API.TASKS.FIND_COMPLETED_BY_VARIABLE_VALUES.replace('<first>', params.firstResult).replace('<limit>', params.maxResults);
            return $http.post(REST_API_URL + Url, params);
        },
        updateFeBlocking: function (params) {
            var Url = API.TASKS.UPDATING_FE_TICKET_STATUS;
            return $http.post(REST_API_URL + Url, params);
            //return $http.post(REST_API_URL + API.TASKS.FIND_BY_VARIABLE_VALUES, params);
        },
        find_by_process_id: function (processId, variablesOfInterest) {
            var findUrl = API.TASK.FIND_BY_PROCESS_ID.replace('<processId>', processId);
            return $http.post(REST_API_URL + findUrl, {variablesOfInterest: variablesOfInterest});
        },
        delete_task: function (processId, email) {
            var findUrl = API.TASK.DELETE_TASK.replace('<processId>', processId);
            return $http.post(REST_API_URL + findUrl, {email: email});
        },
        viewLocationDetail: function (processId, key3, key4) {
            var findUrl = API.TASK.GET_LOCATION.replace('<processId>', processId).replace('<key3>', key3).replace('<key4>', key4);
            return $http.post(REST_API_URL + findUrl, {});
        },
        download_retailer_tickets: function (params) {
            var findUrl = API.TASK.DOWNLOAD_RETAILER_TICKETS;
            return $http.post(REST_API_URL + findUrl, params);
        },
        bulk_pickup_tickets: function (params) {
            var findUrl = API.TASK.BULK_PICKUP_TICKETS;
            return $http.post(REST_API_URL + findUrl, params);
        },
        start_bulk_pickup: function (params) {
            var findUrl = API.BULK_PICKUP.START;
            return $http.post(REST_API_URL + findUrl, params);
        },
        listMisReport: function (params) {
            var findUrl = API.MIS_REPORT.LIST;
            return $http.post(REST_API_URL + findUrl, params);
        },
        ewasteMisList: function (params) {
            var findUrl = API.E_WASTE_MIS_REPORT.LIST;
            return $http.post(REST_API_URL + findUrl, params);
        },
        bag_history: function (params) {
            var findUrl = API.E_WASTE_BAG_HISTORY.LIST;
            return $http.post(REST_API_URL + findUrl, params);
        },

        getPickDropLoc: function (params) {
            var findUrl = API.TASK.PICKDROPLIST;
            return $http.post(REST_API_URL + findUrl, params);
        },

        listEvalResult: function (params) {
            var findUrl = API.EVAL_RESULT.LIST;
            return $http.post(REST_API_URL + findUrl, params);
        },
        listTechEvalResult: function (params) {
            var findUrl = API.TECH_EVAL_RESULT.LIST;
            return $http.post(REST_API_URL + findUrl, params);
        },

        listPhyEvalAtService: function (params) {
            var findUrl = API.PHY_EVAL_RESULT_AT_SERVICE.LIST;
            return $http.post(REST_API_URL + findUrl, params);
        },

        getTechEvalAtService: function (params) {
            var findUrl = API.TECH_EVAL_RESULT_AT_SERVICE.LIST;
            return $http.post(REST_API_URL + findUrl, params);
        },

        getPhyEvalOfStandby: function (params) {
            var findUrl = API.PHY_EVAL_RESULT_STANDBY.LIST;
            return $http.post(REST_API_URL + findUrl, params);
        },

        getTechEvalOfStandby: function (params) {
            var findUrl = API.TECH_EVAL_RESULT_STANDBY.LIST;
            return $http.post(REST_API_URL + findUrl, params);
        },
        compareEvalResults: function (params) {
            var findUrl = API.EVAL_COMPARE.LIST;
            return $http.post(REST_API_URL + findUrl, params);
        },
        listEvalHistory: function (params) {
            var findUrl = API.EVAL_HISTORY.LIST;
            return $http.post(REST_API_URL + findUrl, params);
        },
        dummyPage: function (params) {
            //	var findUrl = API.MIS_REPORT.LIST;
            return $http.post("184.171.164.146:11080/rlogistics-execution/rlservice/test", params);
        },
        genrateDec: function (processId) {
            var url = API.TASK.GENERATE_DEC;
            return $http.post(REST_API_URL + url, {processId: processId});
        },
        newProductCost: function (params) {
            var findUrl = API.TASK.UPDATE_COST;
            return $http.post(REST_API_URL + findUrl, params);
        },

        damageCost: function (params) {
            var findUrl = API.TASK.UPDATE_DAMAGE_COST;
            return $http.post(REST_API_URL + findUrl, params);
        },
        upload: function (params) {
            console.log(params);
            return Upload.upload({
                url: REST_API_URL + API.AUTHLETTER.UPLOAD,
                data: params.data,
                file: params.file
            })
        },
        uploadPurchaseInvoice: function (params) {
            console.log(params);
            return Upload.upload({
                url: REST_API_URL + API.PURCHASEINVOICE.UPLOAD,
                data: params.data,
                file: params.file
            })
        },

        uploadAuthLetterRetailer: function (params) {
            console.log(params);
            return Upload.upload({
                url: REST_API_URL + API.PURCHASEINVOICE.AUTHUPLOAD,
                data: params.data,
                file: params.file
            })
        },

        uploadAdditionalFile: function (params) {
            console.log(params);
            return Upload.upload({
                url: REST_API_URL + API.ADDITIONALFILE.UPLOAD,
                data: params.data,
                file: params.file
            })
        },
        uploadBitsBarcodeFile: function (params) {
            console.log(params);
            return Upload.upload({
                url: REST_API_URL + API.BITSBARCODEFILE.UPLOAD,
                data: params.data,
                file: params.file
            })
        },
        uploadEwasteAdditionalFile: function (params) {
            console.log(params);
            return Upload.upload({
                url: REST_API_URL + API.EWASTEADDITIONALFILE.UPLOAD,
                data: params.data,
                file: params.file
            })
        },
        viewBitsPhotos: function (params) {
            var findUrl = API.TASK.GETIMAGE;
            return $http.post(REST_API_URL + findUrl, params);
        },
        assigneeNumber: function (params) {
            var url = API.TASKS.GET_ASSIGNEE_NUMBER;
            return $http.post(REST_API_URL + url, params);

        },
        deviceInfo: function (params) {
            return $http.post(REST_API_URL + API.REPORT.GET_DEVICE_INFO, params)

        },
        viewPhotos: function (params) {
            return $http.post(REST_API_URL + API.TICKET_IMAGES.LIST, params)

        }
    }
}]);

app.controller('TasksController',
    ['$scope', '$rootScope', '$state', 'TasksService', 'notification', 'AuthTokenFactory', 'ngDialog', '$location', 'logger',
        'TokenFactory', 'ProcessService', 'Utility', '$window', 'UserService', 'LIST_PAGE_SIZE', 'RestHTTPService', 'API', 'RetailerReturnOrPickupLocations', '$cookies',
        function ($scope, $rootScope, $state, TasksService, notification, AuthTokenFactory, ngDialog, $location, logger, TokenFactory,
                  ProcessService, Utility, $window, UserService, LIST_PAGE_SIZE, RestHTTPService, API, RetailerReturnOrPickupLocations, $cookies) {
            $scope.tasks = [];

            $scope.searchedTasksNew = [];
            //console.log($cookies);
            $scope.fieldValues = {};
            $scope.task = {};
            $scope.pagination = {};
            $scope.pageSize = LIST_PAGE_SIZE;
            $scope.total_count = 0;
            $scope.currentPage = 1;
            $scope.compareEvalIds = [];
            $scope.showVal = 1;
            // $scope.displayVal = 1;
            $scope.startDateRange = "";
            $scope.endDateRange = "";
            $scope.latitude = 0;
            $scope.longitude = 0;
            $scope.getLocation = false;
            $scope.task.retailer = [];
            $scope.searchDownTask = {};
            $scope.searchDown2Task = {};
            $scope.processStatus = [];
            $scope.filtervar = {};
            var customerNumber;
            var customerAlternateNumber;
            var variablesOfInterest = [
                "consumerName", "productCategory", "product", "retailer", "telephoneNumber", "rlLocationName", "rlRetailerCode",
                "natureOfComplaint", "rlTicketStatus", "rlProcessStatus", "dateOfComplaint", "rlTicketNo", "consumerComplaintNumber",
                "addressLine1", "addressLine2", "city", "dropLocCity", "dropLocAddress1", "dropLocAddress1", "pincode", "brand", "model", "productCode", "ticketCreationDate",
                "dateOfPurchase", "rlAuthorizationLetterFileName", "rlPurchaseInvoiceFileName", "isDeclaired", "ticketPriority",
                "problemDescription", "ticketCreationDate", "amtPaidToCustomer", "ticketCreationDate", "rlReportingCity", "orderNumber", "rlInvolvedUser", "dropLocCity"
            ];
            //I remove from variablesOfInterest var are:
            var variablesOfInterestTicketView = [
                "rlTicketNo", "rlTicketStatus", "rlProcessStatus", "rlCallTranscript", "assignee", "rlAppointmentDate", "rlClosingDate",
                "rlTicketCost", "rlTicketCosting", "rlCostingMap", "rlTicketCostMap", "dropLocCity", "dropLocAddress1", "dropLocAddress1", "dropLocState", "dropLocPincode",
                "consumerName", "emailId", "addressLine1", "addressLine2", "landmark", "city", "pincode", "telephoneNumber", "alternateTelephoneNumber", "dropLocContactPerson", "dropLocContactNo",
                "consumerComplaintNumber", "dateOfComplaint", "natureOfComplaint", "problemDescription", "rlRetailerCode", "rlAppointmentDateForFV", "amtPaidToCustomer",
                "product", "productCategory", "brand", "retailer", "rlRetailerCode", "dateOfPurchase", "latitude", "longitude", "isBlockToFe", "ticketCreationDate",
                "rlAMPMDetailsMap", "identificationNo", "isDeclaired", "amount", "name", "status", "email", "maxValueToBeOffered", "txnId", "payePhone", "payeEmail", "dttimeStampInMs",
                "sellerContactNumber", "customerContactNumber", "custAltPhoneNo", "buyerContactNo", "bulkMode", "retailerPhoneNo", "rlValueOffered", "isCostUpdated", "physicalEvaluation", "TechEvalRequired",
                "_PhotographBefore", "_PhotoBefore", "_PhotographAfter", "_Signature", "_ServiceCenterSignature", "_CustomerSignature", "rlAuthorizationLetterFileName", "rlPurchaseInvoiceFileName", "model", "rlReturnLocationName", "rlReturnLocationAddress", "rlReturnLocationPincode", "rlPickupLocationName", "rlPickupLocationAddress", "rlPickupLocationPincode", "productName2", "brand2", "model2", "wgf_FP_PhotographBefore", "wgf_FP_PhotographAfter", "wgf_FP_CustomerSignature", "orderNumber"
            ];
            var evalIdList = [];


            $scope.breadcrumbs = [
                {
                    title: "Tickets"
                }
            ];
            $scope.breadcrumbs.push({'title': $state.current.data.page});

            $scope.selectre = function (retailer, first, limit) {
                $scope.filtervar.rlRetailerId = retailer;
                $scope.involved($scope.filtervar, first, limit);
            }
            $scope.selectpe = function (priority, first, limit) {
                $scope.filtervar.ticketPriority = priority;
                $scope.involved($scope.filtervar, first, limit);

            }
            $scope.priority = ['High','Medium','Low'];
            $scope.filtervar.pincode = null;
            $scope.reset = function (first, limit) {
                $scope.filtervar.rlRetailerId = null;
                $scope.filtervar.ticketPriority = null;
                $scope.involved($scope.filtervar, first, limit);
            }


            $scope.isOpen = false;
            $scope.datefiltervar = null;
            $scope.involved = function (filter, first, limit) {
                var l = [];
                if ($scope.datefiltervar != undefined || $scope.datefiltervar != null) {
                    var lItem1 = {
                        variable: 'ticketCreationDate',
                        value: '<=' + formatDate(new Date($scope.datefiltervar))
                    }
                    l.push(lItem1);
                    var lItem2 = {
                        variable: 'ticketCreationDate',
                        value: '>=' + formatDate(new Date($scope.datefiltervar))
                    }
                    l.push(lItem2)
                }

                $scope.tasks = [];
                $scope.total_count = 0;
                console.log("filter fields" + filter);
                for (var key in filter) {
                    if (!isEmpty(filter[key], key) && filter[key].indexOf('%') === -1) {
                        var qItem = {
                            variable: key,
                            value: filter[key] + '*'
                        }
                        l.push(qItem);
                    }
                }
                $scope.currentPage = first // initialize page no to 1

                first = $rootScope.calculateFirstRecord($scope.currentPage, limit);

                TasksService.involved({"query": l}, first, limit).success(function (response) {
                    $scope.tasks = response.data;
                    $scope.total_count = response.totalRecords;
                });
            }
            $scope.select = function (index) {
                $scope.id = event.target.id;
                $(".list-group-item").removeClass('active');
                angular.element(document.querySelector('#' + $scope.id)).addClass('active');
            };
            $scope.select2 = function (index) {
                $scope.id2 = index.target.id;
                console.log("Id : " + $scope.id2);
                $(".list-group-item2").removeClass('active');
                angular.element(document.querySelector('#' + $scope.id2)).addClass('active');
            };

            //Populating the DropDown For Pick And Drop Location
            $scope.populatePickDropLocation = function (retailerId) {
                console.log("retailerId = :", retailerId);
                TasksService.getPickDropLoc({'query': {'retailerId': retailerId}}).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0) {
                        console.log("bagHistory Respomse : ", response);
                        $scope.pickDrop = response.data;
                        console.log("rewertuigfghjk", $scope.pickDrop);
                        $scope.total_count = response.totalRecords;
                    }
                });

            }

            console.log($scope.loginInfo.locationName);

            $scope.checkDown = function () {
                console.log("$scope.searchDownTask ::" + $scope.searchDownTask);
                $scope.generateSearchExcel($scope.searchDownTask);
            }
            $scope.checkDown2 = function () {
                console.log("$scope.searchDown2Task ::" + $scope.searchDown2Task);
                $scope.generateCompletedSearchExcel($scope.searchDown2Task);
            }
            //Generating the Search Ticket Excel-Sheet
            $scope.generateSearchExcel = function (task) {
                if (task === undefined) {
                    return;
                }
                if ($scope.loginInfo.roleCode == 'retailer') {
                    task.rlRetailerId = $scope.loginInfo.retailerId;
                    console.log("$scope.loginInfo :" + $scope.loginInfo.retailerId);
                }
                if (isEmpty(task.consumerName, 'consumerName') &&
                    isEmpty(task.product, 'product') &&
                    isEmpty(task.telephoneNumber, 'telephoneNumber') &&
                    isEmpty(task.emailId, 'emailId') &&
                    isEmpty(task.productCategory, 'productCategory') &&
                    isEmpty(task.rlRetailerId, 'retailer') &&
                    isEmpty(task.rlRetailerId, 'retailers.code') &&
                    isEmpty(task.rlHomeLocationId, 'rlHomeLocationId') &&
                    isEmpty(task.dropLocCity, 'dropLocCity') &&
                    isEmpty(task.city, 'city') &&
                    isEmpty(task.rlProcessStatus, 'rlProcessStatus') &&
                    isEmpty(task.rlTicketNo, 'rlTicketNo') &&
                    isEmpty(task.natureOfComplaint, 'natureOfComplaint') &&
                    isEmpty(task.rlLocationName, 'rlLocationName') &&
                    isEmpty($scope.datePicker.dateOfComplaint.startDate, 'startDate') &&
                    isEmpty($scope.datePicker.dateOfComplaint.endDate, 'endDate') &&
                    isEmpty(task.rlReportingCity, 'rlReportingCity')) {
                    return;
                }


                var q = [];
                console.log("taskqwet", task, 'consumerName');
                // $scope.clearSearchFields();
                console.log("task");
                for (var key in task) {
                    if (!isEmpty(task[key], key) && task[key].indexOf('%') === -1) {
                        var qItem = {
                            variable: key,
                            value: task[key] + '*'
                        };
                        //					if (key == 'rlTicketNo') {
                        //                        qItem.value = task[key];
                        //                    }
                        q.push(qItem);
                    }
                }
                if (!isEmpty($scope.datePicker.dateOfComplaint.startDate, 'startDate')) {
                    var qItem = {
                        variable: 'ticketCreationDate',
                        value: ">=" + formatDate(new Date($scope.datePicker.dateOfComplaint.startDate))
                    };
                    q.push(qItem);
                }
                if (!isEmpty($scope.datePicker.dateOfComplaint.endDate, 'endDate')) {
                    var qItem = {
                        variable: 'ticketCreationDate',
                        value: "<=" + formatDate(new Date($scope.datePicker.dateOfComplaint.endDate))
                    };
                    q.push(qItem);
                }
                TasksService
                    .generateSearchExcel({
                        'query': q,
                        'variablesOfInterest': variablesOfInterest,
                    })
                    .success(function (response) {
                        // && response.data.totalRecords > 0
                        if (response !== undefined && response.data !== undefined && response.totalRecords) {
                            console.log("response.data:  :" + response);
                            $scope.total_count = response.totalRecords;
                            // notification.success(response.message);
                            // $window.location.href = "http://localhost/rl_angular/server/attachment/download/search/abc/bca";
                            $window.location.href = host_base_url + API.EXCEL_URL;

                        }
                    });
                console.log("generateSearchExcel : " + q);
            }

            //Generating the Search Ticket Excel-Sheet FOR COMPLETED TICKETS
            $scope.generateCompletedSearchExcel = function (task) {
                if (task === undefined) {
                    return;
                }
                if ($scope.loginInfo.roleCode == 'retailer') {
                    task.rlRetailerId = $scope.loginInfo.retailerId;
                    console.log("$scope.loginInfo :" + $scope.loginInfo.retailerId);
                }
                if (isEmpty(task.consumerName, 'consumerName') &&
                    isEmpty(task.product, 'product') &&
                    isEmpty(task.telephoneNumber, 'telephoneNumber') &&
                    isEmpty(task.emailId, 'emailId') &&
                    isEmpty(task.productCategory, 'productCategory') &&
                    isEmpty(task.rlRetailerId, 'retailer') &&
                    isEmpty(task.rlRetailerId, 'retailers.code') &&
                    isEmpty(task.rlHomeLocationId, 'rlHomeLocationId') &&
                    isEmpty(task.dropLocCity, 'dropLocCity') &&
                    isEmpty(task.city, 'city') &&
                    isEmpty(task.rlProcessStatus, 'rlProcessStatus') &&
                    isEmpty(task.rlTicketNo, 'rlTicketNo') &&
                    isEmpty(task.natureOfComplaint, 'natureOfComplaint') &&
                    isEmpty(task.rlLocationName, 'rlLocationName') &&
                    isEmpty($scope.datePicker.dateOfComplaint.startDate, 'startDate') &&
                    isEmpty($scope.datePicker.dateOfComplaint.endDate, 'endDate') &&
                    isEmpty(task.ticketCreationDate, 'ticketCreationDate')
                ) {
                    return;
                }


                var q = [];
                console.log("taskqwet", task, 'consumerName');
                // $scope.clearSearchFields();
                console.log("task");
                for (var key in task) {
                    if (!isEmpty(task[key], key) && task[key].indexOf('%') === -1) {
                        var qItem = {
                            variable: key,
                            value: task[key] + '*'
                        };
                        //					if (key == 'rlTicketNo') {
                        //                        qItem.value = task[key];
                        //                    }
                        q.push(qItem);
                    }
                }
                if (!isEmpty($scope.datePicker.dateOfComplaint.startDate, 'startDate')) {
                    var qItem = {
                        variable: 'ticketCreationDate',
                        value: ">=" + formatDate(new Date($scope.datePicker.dateOfComplaint.startDate))
                    };
                    q.push(qItem);
                }
                if (!isEmpty($scope.datePicker.dateOfComplaint.endDate, 'endDate')) {
                    var qItem = {
                        variable: 'ticketCreationDate',
                        value: "<=" + formatDate(new Date($scope.datePicker.dateOfComplaint.endDate))
                    };
                    q.push(qItem);
                }
                TasksService
                    .generateCompletedSearchExcel({
                        'query': q,
                        'variablesOfInterest': variablesOfInterest,
                    })
                    .success(function (response) {

                        if (response !== undefined && response.data !== undefined && response.totalRecords > 0) {
                            $scope.total_count = response.totalRecords;
                            $window.location.href = host_base_url + API.EXCEL_URL;
                            notification.success(response.message);
                        }
                    });
                console.log("generateCompletedSearchExcel : " + q);
            }


            $scope.bagHistory = function (first, limit) {
                console.log("inside bagHistory", first, limit);
                $scope.currentPage = first;// initialize page no to 1
                first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
                TasksService.bag_history({
                    'query': {'processId': $state.current.data.params.id},
                    firstResult: first,
                    maxResults: limit
                }).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0) {
                        console.log("bagHistory Respomse : ", response);
                        $scope.bagHis = response.data;
                        $scope.total_count = response.totalRecords;
                    }
                });
            }
            $scope.assigned = function (first, limit) {
                $scope.currentPage = first;// initialize page no to 1

                first = $rootScope.calculateFirstRecord($scope.currentPage, limit);

                TasksService.assigned(first, limit).success(function (response) {
                    $scope.tasks = response.data;
                    $scope.total_count = response.totalRecords;
                });
            }

            $scope.my_completed = function (first, limit) {
                $scope.currentPage = first;// initialize page no to 1
                //$scope.total_count = 100;
                first = $rootScope.calculateFirstRecord($scope.currentPage, limit);

                TasksService.my_completed(first, limit, variablesOfInterest).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0) {
                        $scope.tasks = response.data;
                        $scope.total_count = response.totalRecords;
                    }
                });
            }

            $scope.deleteTask = function (task) {
                /*
                var response = {};
                response.message = "Task deleted successfully";
                */
                var processId = task.process.processId
                console.log("Task : + " + task);
                console.log("Deleted by :-" + $rootScope.loginInfo.id);
                TasksService.delete_task(processId, $rootScope.loginInfo.id).success(function (response) {
                    if (response !== undefined && response.message !== undefined) {
                        notification.success(response.message);
                        console.log($scope.currentPage);
                        $scope.search($scope.task, $scope.currentPage, $scope.pageSize);
                        $state.go('rl.tasks-search');
                    }
                });
            }


            $scope.deleteTaskConfirm = function (task) {
                var processId = task.process.processId;
                console.log("The task to be deleted with id:-" + processId);
                /*
                $rootScope.$broadcast ('deleteDialogAndConfirmEvent',"Are you sure to delete this ticket",
                                       'deleteTask',
                                       processId);
                */
                ngDialog.openConfirm({
                    template: '<div class="dialog-contents">' +
                    "Are you sure to delete this ticket, you will not get it back once deleted" + '<br/><br/>' +
                    '<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
                    '<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
                    '</div>',
                    controller: ['$scope', function ($scope) {
                        // Controller logic here
                    }]
                }).then(function (success) {
                    $scope.deleteTask(task);
                }, function (error) {
                    // Error logic here
                });


            }


            $scope.dummyPage = function (task) {
                var processId = task.process.processId;
                console.log("The task to be deleted with id:-" + processId);
                /*
                $rootScope.$broadcast ('deleteDialogAndConfirmEvent',"Are you sure to delete this ticket",
                                       'deleteTask',
                                       processId);
                */
                TasksService.dummyPage({'processId': processId}).success(function (response) {

                });

            }

            $scope.managerUnclaimed = function (first, limit) {
                $scope.currentPage = first // initialize page no to 1

                first = $rootScope.calculateFirstRecord($scope.currentPage, limit);

                TasksService.manager_unclaimed(first, limit).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0) {
                        $scope.pages = 1;
                        $scope.tasks = response.data;
                        $scope.total_count = response.totalRecords;
                    }
                });
            }


            $scope.managerAssigned = function (first, limit) {
                $scope.currentPage = first // initialize page no to 1

                first = $rootScope.calculateFirstRecord($scope.currentPage, limit);

                TasksService.manager_assigned(first, limit).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0) {
                        $scope.pages = 1;
                        $scope.tasks = response.data;
                        $scope.total_count = response.totalRecords;
                    }
                });
            }

            $scope.managerCompleted = function (first, limit) {
                $scope.currentPage = first // initialize page no to 1
                first = $rootScope.calculateFirstRecord($scope.currentPage, limit);

                TasksService.manager_completed(first, limit).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0) {
                        $scope.pages = 1;
                        $scope.tasks = response.data;
                        // for(var d = 0; d < response.data.length; d++) {
                        // 	var processEndTime = new Date(response.data[d].processEndTime);
                        // 	response.data[d].processEndTime = processEndTime;
                        // }

                        $scope.total_count = response.totalRecords;
                    }
                });
            }

            $scope.retailerUnclaimed = function (first, limit) {
                $scope.currentPage = first // initialize page no to 1

                first = $rootScope.calculateFirstRecord($scope.currentPage, limit);

                TasksService.retailer_unclaimed(first, limit).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0) {
                        $scope.pages = 1;
                        $scope.tasks = response.data;
                        $scope.total_count = response.totalRecords;
                    }
                });
            }

            $scope.retailerAssigned = function (first, limit) {
                $scope.currentPage = first // initialize page no to 1

                first = $rootScope.calculateFirstRecord($scope.currentPage, limit);

                TasksService.retailer_assigned(first, limit).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0) {
                        $scope.pages = 1;
                        $scope.tasks = response.data;
                        $scope.total_count = response.totalRecords;
                    }
                });
            }

            $scope.retailerCompleted = function (first, limit) {
                $scope.currentPage = first // initialize page no to 1
                first = $rootScope.calculateFirstRecord($scope.currentPage, limit);

                TasksService.retailer_completed(first, limit).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0) {
                        $scope.pages = 1;
                        $scope.tasks = response.data;
                        $scope.total_count = response.totalRecords;
                    }
                });
            }

            if ($state.current.url == "/tasks/search" || $state.current.url == "/tasks/search/completed") {
                Utility.populate_category({}, $scope);
                Utility.populate_retailer({}, $scope);
                Utility.populate_city({}, $scope);
                Utility.populate_master_services({}, $scope);
                // Utility.populate_rlprocess_status({}, $scope);
                TasksService.get_rl_process_status({
                    query: {},
                    firstResult: 0,
                    maxResults: 1000
                }).success(function (response) {
                    $scope.processStatus = response.data;
                    // console.log("sdfgjkloiygthjkiuyigjklogbjkiguopu" + $scope.processStatus);
                });
                console.log("sdfgjkloiygthjkiuyigjklogbjkiguopu" + $scope.processStatus);
            }

            if ($state.current.url == "/mis/ticket-search") {
                Utility.populate_retailer({}, $scope);
            }

            if ($state.current.url == "/download/tickets") {
                Utility.populate_retailer({}, $scope);
            }

            if ($state.current.url == "/tickets/bulk-pickup") {
                Utility.populate_retailer({}, $scope);
            }
            if ($state.current.url == "/locationview") {
                Utility.populate_location({}, $scope);
            }
            if ($state.current.url == "/partyview") {
                Utility.populate_retailer({}, $scope);
            }

            if ($state.current.url == "/finance/hub") {
                Utility.populate_retailer({}, $scope);
            }

            if ($state.current.url == "/completedticketview") {
                Utility.populate_retailer({}, $scope);
                Utility.populate_location({}, $scope);
            }
            if ($state.current.url == "/retailerview") {
                Utility.populate_retailer({}, $scope);
                Utility.populate_location({}, $scope);
            }
            if ($state.current.url == "/accountspayable") {
                Utility.populate_retailer({}, $scope);
                Utility.populate_location({}, $scope);
            }
            if ($state.current.url == "/accountsreceivable") {
                Utility.populate_retailer({}, $scope);
                Utility.populate_location({}, $scope);
            }

            $scope.populateRetailerReturnOrPickupLocations = function (retailerId) {
                var first = 0;
                var limit = 1000;
                RetailerReturnOrPickupLocations
                    .list({'query': {'retailerId': retailerId}, firstResult: first, maxResults: limit})
                    .success(function (response) {
                        if (response !== undefined && response.data !== undefined && response.data.length > 0) {
                            $scope.retailerReturnOrPickupLocations = response.data;
                            $scope.total_count = response.totalRecords;
                        }
                    });
            }


            if ($state.current.url == "/task/:id/view-" + $state.current.data.status) {
                $cookies.remove("CustomerNumber");
                $cookies.remove("CustomerAlternateNumber");

                var taskId = $state.current.data.params.id;
                TasksService.firstgroup(taskId, {'fieldValues': $scope.fieldValues}).success(function (response) {

                    $scope.datepickerOptions = {
                        format: 'yyyy-mm-dd',
                        language: 'fr',
                        startDate: "2012-10-01",
                        endDate: "2012-10-31",
                        autoclose: true,
                        weekStart: 0
                    }

                    $scope.task = formatResponse(response);
                    for (var i = 0; i < $scope.task.properties.length; i++) {
                        if ($scope.task.properties[i].name == "Phone No") {
                            console.log($scope.task.properties[i].value);
                            customerNumber = $scope.task.properties[i].value;
                            $cookies.put("CustomerNumber", customerNumber);
                        }
                        if ($scope.task.properties[i].name == "Alternate Phone No") {
                            console.log($scope.task.properties[i].value);
                            customerAlternateNumber = $scope.task.properties[i].value;
                            $cookies.put("CustomerAlternateNumber", customerAlternateNumber);
                        }


                    }
                    //console.log($scope.task.properties.);
                    $scope.taskId = taskId;
                    $scope.task.headerName = TokenFactory.getToken('taskHeaderName');
                    $scope.task.processId = TokenFactory.getToken('processId');
                    $scope.task.status = $state.current.data.status;
                    //logger.log($scope.task, Object.getOwnPropertyNames($scope.task).length);
                });
            }
            ;


            if ($state.current.url == "/task/authorization-file/upload/:id") {

                $scope.breadcrumbs.push({title: "Upload"});

                try {
                    $scope.$watch('authFile.file', function () {
                        $scope.authFile.selectedFileName = ($scope.authFile.file !== undefined) ? $scope.authFile.file.name : "";
                    });
                }
                catch (e) {

                }
            }
            if ($state.current.url == "/task/productCost/update/:id/:cost") {
                $scope.oldCost = $state.current.data.params.cost;
                $scope.breadcrumbs.push({title: "Update Cost"});
            }

            $scope.misDetails = [];

            $scope.viewMisReport = function (first, limit) {
                $scope.currentPage = first // initialize page no to 1
                first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
                TasksService.listMisReport({
                    'query': {'processId': $state.current.data.params.id},
                    firstResult: first,
                    maxResults: limit
                }).success(function (response) {
                    $scope.misDetails = response.data;
                    $scope.total_count = response.totalRecords;
                    $scope.ticketNo = $scope.misDetails[0].ticketNo;
                    $scope.retailer = $scope.misDetails[0].retailer;
                    console.log($scope.ticketNo);
                    console.log($scope.retailer);
                });
            }

            $scope.unBlockFETicket = function (task) {
                console.log(task.process.processId);
                TasksService.updateFeBlocking({
                    'query': {
                        'processId': task.process.processId,
                        'calling': '1'
                    }
                }).success(function (response) {
                    notification.success("Successfully Unblocked");

                });
            }

            $scope.viewSubTicket = function (first, limit) {
                $scope.showSubTicket = true;
                $scope.showBarcodes = false;
                $scope.currentPage = first // initialize page no to 1
                first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
                console.log("Inside Called Methods");
                TasksService.listMisReport({
                    'query': {
                        'processId': $state.current.data.params.id,
                        'actionType': 'bitsBox'
                    }, firstResult: first, maxResults: limit
                }).success(function (response) {
                    $scope.misDetails = response.data;
                    $scope.total_count = response.totalRecords;
                });
            }

            $scope.misBulkQueryId = function (id) {
                $scope.showSubTicket = false;
                $scope.showBarcodes = true;
                $scope.id = id // initialize page no to 1
                console.log("Inside Called Methods");
                TasksService.listMisReport({
                    'query': {'subTicketNo': $scope.id},
                    firstResult: 0,
                    maxResults: 10000
                }).success(function (response) {
                    $scope.misDetails = response.data;
                    $scope.total_count = response.totalRecords;
                });
                TasksService.viewBitsPhotos({
                    'query': {'key1': 'box', 'key2': $scope.id},
                    firstResult: 0,
                    maxResults: 10000
                }).success(function (response) {
                    $scope.misImages = response.response;
                    console.log("$scope.misImages" + $scope.misImages);
                });
            }

//Older implementation
// $scope.getAllProcessPhotos = function (id) {
//     $scope.processId = id;
//     console.log("$scope.processId" + $scope.processId);
//     TasksService.viewBitsPhotos({
//         'query': {'key1': 'process', 'key2': $scope.processId},
//         firstResult: 0,
//         maxResults: 10000
//     }).success(function (response) {8
//         $scope.processImage = response.response;
//         console.log("$scope.processImage" + $scope.processImage[0].key2);
//     });
// }
            /**
             * New Implementation to view photos from s3
             */

            $scope.getAllProcessPhotos = function (id) {

                $scope.processId = id;
                TasksService.viewPhotos({
                    'query': {'procInstId': $scope.processId},
                    'firstResult': 0,
                    'maxResults': 1000
                }).success(function (response) {
                    $scope.processImage = response.data;
                })
            }

            $scope.viewMisReportFilter = function (task, first, limit) {
                $scope.currentPage = first // initialize page no to 1
                first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
                $scope.task = task;
                var status = $scope.task.status;
                TasksService.listMisReport({
                    'query': {'retailerId': $scope.task.retailerId, 'status': status},
                    firstResult: first,
                    maxResults: limit
                }).success(function (response) {
                    $scope.misDetails = response.data;
                    $scope.total_count = response.totalRecords;
                });
            }
            $scope.viewEwasteMis = function (first, limit) {
                $scope.currentPage = first // initialize page no to 1
                first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
                TasksService.ewasteMisList({
                    'query': {'processId': $state.current.data.params.id},
                    firstResult: first,
                    maxResults: limit
                }).success(function (response) {
                    $scope.ewasteMisDetails = response.data;
                    $scope.total_count = response.totalRecords;
                    $scope.ticketNo = $scope.ewasteMisDetails[0].ticketNo;
                    $scope.retailer = $scope.ewasteMisDetails[0].retailer;
                    console.log($scope.ticketNo);
                });
            }

            if ($state.current.url == "/view/mis-report/:id") {
                /*	var first = $rootScope.calculateFirstRecord($scope.currentPage, $scope.pageSize);
                    TasksService.listMisReport({'query':{'processId':$state.current.data.params.id}, firstResult:first, maxResults:$scope.pageSize}).success(function(response) {
                        $scope.misDetails = response.data;
                    }); */
                $scope.processId = $state.current.data.params.id;
                $scope.viewMisReport(1, $scope.pageSize);
            }
            if ($state.current.url == "/view/ewaste-Mis/:id") {
                $scope.processId = $state.current.data.params.id;
                $scope.viewEwasteMis(1, $scope.pageSize);
            }

            if ($state.current.url == "/view/sub-ticket/:id") {
                console.log("Inside Calling Methods");
                $scope.processId = $state.current.data.params.id;
                $scope.viewSubTicket(1, $scope.pageSize);
            }

            $scope.evalEnggRes = [];
            $scope.evalCustRes = [];
//	$scope.engineerEvalResult = [];
// if ($state.current.url == "/task/evaluation/:category") {
            /*	var first = $rootScope.calculateFirstRecord($scope.currentPage, $scope.pageSize);
                TasksService.listMisReport({'query':{'processId':$state.current.data.params.id}, firstResult:first, maxResults:$scope.pageSize}).success(function(response) {
                    $scope.misDetails = response.data;
                }); */

// 	TasksService.listEvalResult({'category':$state.current.data.params.category}).success(function(response) {
// 		console.log("Response from server : ",response.response.cust_response);
// 		if(!response.success) {

// 		} else {
// 			$scope.evalEnggRes = response.response.engg_response;
// 			$scope.evalCustRes = response.response.cust_response;
// 			console.log("$scope.evalResult",$scope.evalEnggRes);
// 	//		$scope.engineerEvalResult = response.response.engineer_response;
// 		}
// 	});
// }

            $scope.evalHistory = [];
            if ($state.current.url == "/task/evaluation-history") {
                /*	var first = $rootScope.calculateFirstRecord($scope.currentPage, $scope.pageSize);
                    TasksService.listMisReport({'query':{'processId':$state.current.data.params.id}, firstResult:first, maxResults:$scope.pageSize}).success(function(response) {
                        $scope.misDetails = response.data;
                    }); */
                TasksService.listEvalHistory().success(function (response) {
                    console.log("Response from server : " + response.response);
                    if (!response.success) {

                    } else {
                        $scope.evalHistory = response.response;
                        //		$scope.engineerEvalResult = response.response.engineer_response;
                    }
                });
            }


            $scope.toggleEvalIds = function (id) {

                var found = 0;
                var ids = $scope.compareEvalIds;
                if ($scope.compareEvalIds.length > 0) {
                    angular.forEach($scope.compareEvalIds, function (item, key) {

                        if (item === id) {
                            found = 1;
                            var index = ids.indexOf(item);
                            ids.splice(index, 1);
                        }
                    });
                    if (found == 0) {
                        ids.push(id);
                    }
                }
                else {
                    ids.push(id);
                }

                $scope.compareEvalIds = ids;
                evalIdList = $scope.compareEvalIds;
                console.log("Before compare 1: " + $scope.compareEvalIds[0]);
                console.log("Before compare 2: " + $scope.compareEvalIds[1]);
                console.log($scope.compareEvalIds);
            }

            if ($state.current.url == "/task/evaluation-compare/:e1/:e2") {
                /*	var first = $rootScope.calculateFirstRecord($scope.currentPage, $scope.pageSize);
                    TasksService.listMisReport({'query':{'processId':$state.current.data.params.id}, firstResult:first, maxResults:$scope.pageSize}).success(function(response) {
                        $scope.misDetails = response.data;
                    }); */
                var cat = "mob";
                TasksService.compareEvalResults({
                    'eval1': $state.current.data.params.e1,
                    'eval2': $state.current.data.params.e2
                }).success(function (response) {
                    console.log("Response from server : " + response.response);
                    if (!response.success) {

                    } else {
                        $scope.evalResult = response.response;
                        //			console.log("After compare : " + evalIdList);
                        $scope.eval1 = $state.current.data.params.e1;
                        $scope.eval2 = $state.current.data.params.e2;
                    }
                });
            }

            $scope.compare = function (evalId1, evalId2) {

                console.log("e1 : " + evalId1);
                console.log("e1 : " + evalId2);
                //		$scope.compareEvalIds = compareEvalIds;
                $state.go('rl.evaluation-compare');
            }


            if ($state.current.url == "/task/additional-file/upload/:id") {

                $scope.breadcrumbs.push({title: "Upload"});

                try {
                    $scope.$watch('additionalFile.file', function () {
                        $scope.additionalFile.selectedFileName = ($scope.additionalFile.file !== undefined) ? $scope.additionalFile.file.name : "";
                    });
                }
                catch (e) {

                }
            }

            if ($state.current.url == "/tasks/ewaste-additional-file/:id") {

                $scope.breadcrumbs.push({title: "Upload"});

                try {
                    $scope.$watch('additionalFile.file', function () {
                        $scope.additionalFile.selectedFileName = ($scope.additionalFile.file !== undefined) ? $scope.additionalFile.file.name : "";
                    });
                }
                catch (e) {

                }
            }
            if ($state.current.url == "tasks")

                if ($state.current.url == "/tasks/bits-barcode-file/:id") {

                    $scope.breadcrumbs.push({title: "Upload"});

                    try {
                        $scope.$watch('additionalFile.file', function () {
                            $scope.additionalFile.selectedFileName = ($scope.additionalFile.file !== undefined) ? $scope.additionalFile.file.name : "";
                        });
                    }
                    catch (e) {

                    }
                }

            /*if ($state.current.url == "/tasks/ewaste-additional-file/:id") {

                $scope.breadcrumbs.push({ title: "Upload" });

                try {
                    $scope.$watch('ewasteFileUpload.file', function () {
                        $scope.ewasteFileUpload.selectedFileName = ($scope.ewasteFileUpload.file !== undefined) ? $scope.ewasteFileUpload.file.name : "";
                    });
                }
                catch (e) {

                }
            } */

            if ($state.current.url == "/task/purchase-invoice/upload/:id") {

                $scope.breadcrumbs.push({title: "Upload"});

                try {
                    $scope.$watch('purchaseInvoice.file', function () {
                        $scope.purchaseInvoice.selectedFileName = ($scope.purchaseInvoice.file !== undefined) ? $scope.purchaseInvoice.file.name : "";
                    });
                }
                catch (e) {

                }
            }
            $scope.vars = null;
            if ($state.current.url == "/task/call/:processId") {
                //$scope.processId = $state.current.data.params.id;

                $scope.id = $state.current.data.params.processId;
                TasksService
                    .find_by_process_id($state.current.data.params.processId, variablesOfInterestTicketView)
                    .success(function (response) {
                        var vars = response.variables;
                        $scope.vars = vars;
                        //console.log("inside call page"+ processId);
                        console.log("inside call page" + taskId);
                        console.log(vars);
                    });

            }

            $scope.ticketImages = [];
            $scope.vars = null;
            if ($state.current.url == "/task/:id/view") {
                TasksService
                    .find_by_process_id($state.current.data.params.id, variablesOfInterestTicketView)
                    .success(function (response) {
                        var vars = response.variables;
                        $scope.vars = vars;
                        console.log("vvvariablesOfInterestTicketView", $scope.vars);

                        if (!isNaN(vars.rlAppointmentDateForFE)) {
                            vars.rlAppointmentDateForFE = convertToDateTimeString(vars.rlAppointmentDateForFE, 'dd-MM-yyyy hh:mm');
                        }
                        // if(vars.isCostUpdated!=='yes'){
                        // 	$scope.prevCost = vars.rlValueOffered;
                        // 	console.log("$scope.prevCost",$scope.prevCost);
                        // }
                        console.log("$vars.rlValueOffered", vars.rlValueOffered);
                        response.variables = vars;
                        $scope.task = response;
                        $scope.prevCost = vars.maxValueToBeOffered;


                        //Get ticket service costing heads
                        //var costingHeads = $.trim($scope.task.variables.rlTicketCosting);
                        //var costingHeadsArr = costingHeads.replace( /\n/g, "~~" ).split( "~~" );
                        //console.log(costingHeads, costingHeadsArr);

                        if (response.completed == false) {
                            Utility.populate_roles({}, $scope);
                        }

                        var images = [];
                        angular.forEach(response.variables, function (value, key) {
                            if (key.indexOf("_PhotographBefore") !== -1 ||
                                key.indexOf("_PhotoBefore") !== -1 ||
                                key.indexOf("_PhotographAfter") !== -1 ||
                                key.indexOf("_Signature") !== -1 ||
                                key.indexOf("_ServiceCenterSignature") !== -1 ||
                                key.indexOf("_PhotoId") !== -1 ||
                                key.indexOf("_CustomerSignature") !== -1) {

                                images.push(key);
                            }
                        }, images);

                        $scope.ticketImages = images;
                        if (response.variables.physicalEvaluation !== null && response.variables.physicalEvaluation.toUpperCase() === 'YES') {
                            TasksService.listEvalResult({'category': $state.current.data.params.id}).success(function (response) {
                                console.log("Response from server : ", response.response.cust_response);
                                if (!response.success) {

                                } else {
                                    $scope.evalEnggRes = response.response.engg_response;
                                    $scope.evalCustRes = response.response.cust_response;
                                    console.log("$scope.evalResult", $scope.evalEnggRes);
                                    //		$scope.engineerEvalResult = response.response.engineer_response;
                                }

                            });
                        }
                    });

            }
            $scope.techEvalEnggRes = [];
            $scope.getTechEvalResult = function () {
                console.log("getTechEvalResult Called");
                TasksService.listTechEvalResult({'category': $state.current.data.params.id}).success(function (response) {
                    console.log("Response from server : ", response);
                    $scope.techEvalEnggRes = response.response;
                    console.log("$scope.techEvalEnggRes" + $scope.techEvalEnggRes);
                });
            }

//Method Call For Physical Evaluation At Service Center
            $scope.phyEvalAtService = [];
            $scope.getPhyEvalAtService = function () {
                $scope.displayVal = 1;
                console.log("phyEvalAtService Called");
                TasksService.listPhyEvalAtService({'category': $state.current.data.params.id}).success(function (response) {
                    console.log("Response from server : ", response);
                    $scope.phyEvalAtService = response.response;
                    console.log("$scope.techEvalEnggRes" + $scope.phyEvalAtService);
                });
            }

//Method Call For Techinacl Evaluation At Service Center
            $scope.techEvalAtService = [];
            $scope.getTechEvalAtService = function () {
                $scope.displayVal = 5;
                console.log("techEvalAtService Called");
                TasksService.getTechEvalAtService({'category': $state.current.data.params.id}).success(function (response) {
                    console.log("Response from server : ", response);
                    $scope.techEvalAtService = response.response;
                    console.log("$scope.techEvalEnggRes" + $scope.techEvalAtService);
                });
            }

//Method Call For Physical Evaluation For Stand-By Device
            $scope.phyEvalOfStandby = [];
            $scope.getPhyEvalOfStandby = function () {
                console.log("phyEvalOfStandby Called");
                TasksService.getPhyEvalOfStandby({'category': $state.current.data.params.id}).success(function (response) {
                    console.log("Response from server : ", response);
                    $scope.phyEvalOfStandby = response.response;
                    console.log("$scope.techEvalEnggRes" + $scope.phyEvalOfStandby);
                });
            }

//Method Call For Technical Evaluation For Stand-By Device
            $scope.techEvalOfStandby = [];
            $scope.getTechEvalOfStandby = function () {
                console.log("techEvalOfStandby Called");
                TasksService.getTechEvalOfStandby({'category': $state.current.data.params.id}).success(function (response) {
                    console.log("Response from server : ", response);
                    $scope.techEvalOfStandby = response.response;
                    console.log("$scope.techEvalEnggRes" + $scope.techEvalOfStandby);
                });
            }

            $scope.populateUsersByRole = function (roleCode) {
                UserService.list({'query': {roleCode: roleCode}}).success(function (response) {
                    $scope.users = response.data;
                });
            }

            if ($state.current.url == "/tickets/bulk-pickup") {
                $scope.populateUsersByRole('delivery_boy');
            }


//Changes


            $scope.viewTask = function (task) {
                var taskId = task.taskId;
                TokenFactory.setToken('taskHeaderName', task.name);
                TokenFactory.setToken('processId', task.processId);
                if ($state.current.data.status == undefined) {

                    $location.path("/task/" + taskId + "/view");
                }
                else {
                    $location.path("/task/" + taskId + "/view-" + $state.current.data.status);

                    highlightTab($state.current.data.status);
                    //console.log($scope.loginInfo.id);


                }
            }


            $scope.claimTask = function (taskId, taskName, processId) {
                TasksService.claim(taskId).success(function (response) {
                    if (response == "true" || response == true) {
                        TokenFactory.setToken('taskHeaderName', taskName);
                        TokenFactory.setToken('processId', processId);
                        $location.path("/task/" + taskId + "/view-assigned");
                        highlightTab('assigned');
                    }
                    else {
                        notification.error("Unable to claim");
                    }
                });
            }


            $scope.completeData = {fillSessionId: "", fieldValues: {}};
            $scope.save = function (task) {
                console.log($state);
                var taskId = $state.current.data.params.id;
                var postdata = {};
                $scope.errorMessages = [];
                $scope.errors = {};

                if ($scope.taskFrom.$invalid) {
                    return false;
                }

                var fillResponse = $scope.fill_fieldValues(task);

                if (Object.getOwnPropertyNames($scope.errors).length > 0) {
                    return;
                }
                console.log("Field Values--", $scope.fieldValues);
                postdata.fillSessionId = task.fillSessionId;
                postdata.fieldValues = $scope.fieldValues;

                $scope.completeData = angular.extend($scope.completeData, postdata);
                if (!task.lastGroup) {
                    TasksService.nextgroup(taskId, task.fillSessionId, {'fieldValues': $scope.fieldValues}).success(function (response) {
                        $scope.taskFrom.$submitted = false;
                        $scope.taskFrom.$invalid = false;
                        //Check if there are any error messages to be displayed
                        if (response != undefined && response.messages.length > 0) {
                            $scope.errorMessages = response.messages
                            notification.taskErrorMessage($scope.errorMessages);
                            $window.scrollTo(0, 0);
                            return;
                        }
                        if (response != undefined && response.properties != undefined) {

                            if (response.properties.length == 0 && response.lastGroup == true) {

                                if (response.notifications.length > 0) {
                                    notification.taskMessage(response.notifications[0]);
                                    $window.scrollTo(0, 0);
                                }

                                //Close the task
                                completeTask(taskId, response.fillSessionId, postdata);
                            }

                            $scope.task = formatResponse(response);
                            $scope.taskId = taskId;
                            $scope.task.headerName = TokenFactory.getToken('taskHeaderName');
                            $scope.task.processId = TokenFactory.getToken('processId');
                            $scope.task.status = $state.current.data.status;
                        }
                    });
                }
                else {

                    completeTask(taskId, task.fillSessionId, postdata);
                }
            }

            $scope.fill_fieldValues = function (task) {


                var savedFieldValues = [];
                for (var i = 0; i < task.properties.length; i++) {
                    var prop = task.properties[i];

                    //Validate Mandatory fileds
                    if (!prop.readOnly) {
                        if (isEmpty(prop.value, prop.id)) {
                            if (prop.mandatory)
                                $scope.errors[prop.id] = decodeHtmlEntity(prop.name) + " is required!";
                        }
                        else if (prop.type === 'double') {
                            if (parseFloat(prop.value) < 0) {
                                $scope.errors[prop.id] = "Enter valid data for " + decodeHtmlEntity(prop.name);
                            }
                            else {
                                savedFieldValues[prop.id] = prop.value;
                            }
                        }
                        else if (prop.type === 'long') {
                            if (parseInt(prop.value) < 0) {
                                $scope.errors[prop.id] = "Enter valid data for " + decodeHtmlEntity(prop.name);
                            }
                            else {
                                savedFieldValues[prop.id] = prop.value;
                            }
                        }
                        else if (prop.type === 'barcode') {
                            if (!isNumber(prop.value)) {
                                $scope.errors[prop.id] = "Barcode can contain only numeric values!";
                            }
                            else {
                                if (prop.value.length > 10) {
                                    prop.value = prop.value.substr(1, 10);
                                }

                                if (prop.value.length <= 10) {
                                    savedFieldValues[prop.id] = prop.value;
                                }
                            }
                        }
                        else if (prop.type === 'multiline-barcode') {
                            var parts = prop.value.split("\n") || [];
                            console.log("parts " + parts);
                            console.log("parts lenght " + parts.length);
                            for (var j = 0; j < parts.length; j++) {
                                if (!isNumber(parts[j])) {
                                    $scope.errors[prop.id] = "Error at line " + j + " Barcode can contain only numeric values!";
                                }
                                else {
                                    if (parts[j].length > 10) {
                                        var current = new Number(j);
                                        if (current == 0) {
                                            prop.value = parts[j].substr(1, 10);
                                            console.log("Fisrt part " + prop.value);
                                        } else {
                                            prop.value = prop.value + "\n" + parts[j].substr(1, 10);
                                        }
                                    }
                                    if (parts[j].length <= 10) {
                                        var current = new Number(j);
                                        if (current == 0) {
                                            prop.value = parts[j];
                                        } else {
                                            prop.value = prop.value + "\n" + parts[j];
                                        }
                                    }
                                }

                            }
                            savedFieldValues[prop.id] = prop.value;
                            console.log("Property value : " + prop.value);
                        }
                        else {
                            savedFieldValues[prop.id] = prop.value;
                        }
                    }

                }
                //console.log(savedFieldValues);
                $scope.fieldValues = angular.extend({}, savedFieldValues);
            }

            function completeTask(taskId, fillSessionId, postdata) {
                TasksService
                    .complete(taskId, fillSessionId, {'fieldValues': postdata.fieldValues})
                    .success(function (response) {
                        logger.log(response);

                        if (!response.success) {

                            if (response.groupDescription.messages.length > 0) {
                                $scope.errorMessages = response.groupDescription.messages;
                                notification.taskErrorMessage($scope.errorMessages);
                                $window.scrollTo(0, 0);
                            }
                            return;
                        }

                        if (response.success == 'true' || response.success == true) {

                            if (response.notifications.length > 0) {
                                notification.taskMessage(response.notifications[0]);
                                $window.scrollTo(0, 0);
                            }

                            //Get my next task
                            ProcessService
                                .nexttask(TokenFactory.getToken('processId'))

                                .success(function (response) {
                                    if (response !== undefined && response.response !== undefined) {
                                        response = response.response;
                                    }
                                    //
                                    if (response !== undefined && response.taskId !== undefined && response.taskId != null) {
                                        TokenFactory.setToken('taskHeaderName', response.name);
                                        $location.path("/task/" + response.taskId + "/view-assigned");
                                    }
                                    else {
                                        $state.go('rl.tasks-assigned', {first: 1, limit: 10});
                                    }
                                })
                                .error(function (error) {
                                    console.log(error);
                                    $state.go('rl.tasks-assigned', {first: 1, limit: 10});
                                });
                        }
                        else {
                            notification.taskErrorMessage('Unable to complete the task');
                        }
                    });
            }

            function convertToDateTimeString(value, format) {
                var d = new Date(value);
                // Hours part from thy9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999e timestamp
                var hours = d.getHours();
                // Minutes part from the timestamp
                var minutes = "0" + d.getMinutes();
                // Seconds part from the timestamp
                var seconds = "0" + d.getSeconds();

                // Will display time in 10:30:23 format
                var formattedTime = hours + ':' + minutes.substr(-2);// + ':' + seconds.substr(-2);

                var newval = d.getDate() + '-' + (d.getMonth() + 1) + '-' + d.getFullYear();

                if (format != undefined && format == 'dd-MM-yyyy hh:mm') {
                    newval += " " + formattedTime
                }

                return newval;
            }

            function formatResponse(response) {
                var readOnlyFieldsCount = 0, editableFieldsCount = 0;
                for (var i = 0; i < response.properties.length; i++) {

                    if (response.properties[i].readOnly) {
                        readOnlyFieldsCount++;
                    }
                    else {
                        editableFieldsCount++;
                    }

                    if (response.properties[i].type == 'url' &&
                        (response.properties[i].value !== undefined &&
                            response.properties[i].value !== null &&
                            response.properties[i].value !== "")) {
                        if (response.properties[i].value.indexOf("attachment/download") !== -1) {
                            response.properties[i].value = host_base_url + 'server/' + response.properties[i].value;
                        }
                    }

                    if ((response.properties[i].type == 'date' &&
                        response.properties[i].value != null)) {
                        if (!isNaN(response.properties[i].value)) {
                            response.properties[i].longDate = response.properties[i].value;

                            response.properties[i].value = convertToDateTimeString(response.properties[i].value, response.properties[i].format);
                        }
                    }
                    if ((response.properties[i].type == 'enum' || response.properties[i].type == 'dynamic-enum')
                        && !response.properties[i].mandatory && !response.properties[i].readOnly
                    ) {
                        var index = 0, valueExistsInOptions = false;
                        if (response.properties[i].value != null) {
                            for (var key in response.properties[i].allowedValues) {
                                if (response.properties[i].value.toLocaleLowerCase() === key.toLocaleLowerCase()) {
                                    valueExistsInOptions = true;
                                }
                            }
                            if (!valueExistsInOptions) {
                                response.properties[i].value = null;
                            }
                        }
                        else {
                            for (var key in response.properties[i].allowedValues) {
                                if (response.properties[i].allowedValues.hasOwnProperty(key) && index == 0) {
                                    response.properties[i].value = key;
                                    index++;
                                    break;
                                }
                            }
                        }
                    }
                    if ((response.properties[i].type == 'enum' || response.properties[i].type == 'dynamic-enum') &&
                        response.properties[i].readOnly && response.properties[i].value == null) {
                        var keys = [];
                        for (var key in response.properties[i].allowedValues) {
                            keys.push(key);
                        }
                        if (keys.length == 1) {
                            response.properties[i].value = keys[0];
                        }
                        console.log(keys, response.properties[i]);
                    }
                    if (response.properties[i].type == 'long') {
                        response.properties[i].value = parseInt(response.properties[i].value);
                    }
                    if (response.properties[i].type == 'enum' || response.properties[i].type == 'dynamic-enum') {
                        var valueExistsInOptions = false;
                        if (response.properties[i].value != null) {
                            for (var key in response.properties[i].allowedValues) {
                                if (response.properties[i].value.toLocaleLowerCase() === key.toLocaleLowerCase()) {
                                    valueExistsInOptions = true;
                                }
                            }
                            if (!valueExistsInOptions) {
                                response.properties[i].value = null;
                            }
                        }
                    }
                    //itp_UD_DocketNo
                    if (response.properties[i].type == 'string' && response.properties[i].id == "itp_UD_DocketNo") {
                        if (response.properties[i].value !== null) {
                            $scope.trackProduct = {};
                            $scope.trackProduct.docketNo = response.properties[i].value;
                            response.properties[i].value = '<track-product docket-no="{{trackProduct}}"></track-product>';
                        }
                    }
                }
                $scope.readOnlyFieldsCount = readOnlyFieldsCount;
                $scope.editableFieldsCount = editableFieldsCount;
                return response;
            }

            $scope.datePicker = {};
            $scope.searchedDownloadTasks = [];

            $scope.datePicker.taskDateRange = {startDate: null, endDate: null};
            $scope.searchForDownload = function (task) {
                if (task === undefined) {
                    return;
                }
                var startDate = formatDate(new Date($scope.datePicker.taskDateRange.startDate));
                var endDate = formatDate(new Date($scope.datePicker.taskDateRange.endDate));
                console.log("Start Date : " + startDate);
                console.log("End Date : " + endDate);
                console.log("RetailerId : " + task.retailer);

                //search for download
                TasksService.download_retailer_tickets({
                    'startDate': startDate,
                    'endDate': endDate,
                    'retailerId': task.retailer
                })
                    .success(function (response) {
                        if (response !== undefined && response.response !== undefined && response.response.length > 0) {
                            $scope.searchedDownloadTasks = response.response;
                        }
                    });
            }

            $scope.bulkPickupTasks = [];
            $scope.searchBulkPickup = function (task) {
                if (task === undefined) {
                    return;
                }

                //search for download
                TasksService.bulk_pickup_tickets({
                    'pickupLocationId': $scope.task.pickupLocationId,
                    'deliveryBoyEmail': $scope.task.deliveryBoyEmail
                })
                    .success(function (response) {
                        if (response !== undefined && response.response !== undefined && response.response.length > 0) {
                            $scope.bulkPickupTasks = response.response;
                        }
                    });
            }

            $scope.datePicker.dateOfComplaint = {startDate: null, endDate: null};
            if ($scope.loginInfo.roleCode !== 'admin') {
                $scope.task.rlReportingCity = $scope.loginInfo.locationName;
            }
            $scope.search = function (task, first, limit, identity) {
                console.log($scope.datePicker.dateOfComplaint.$valid);
                $scope.searchDownTask = task;
                $scope.currentPage = first // initialize page no to 1
                //console.log("retgvytufkeluvry", task);
                first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
                if (task === undefined) {
                    return;
                }
                if ($scope.loginInfo.roleCode == 'retailer') {
                    task.rlRetailerId = $scope.loginInfo.retailerId;
                    console.log("$scope.loginInfo :" + $scope.loginInfo.retailerId);
                }
                if ($scope.loginInfo.roleCode == 'hub_manager') {
                    task.rlLocationName = $scope.loginInfo.locationName;
                    console.log("$scope.locationName :" + $scope.loginInfo.locationName);
                }
                var q = [];
                if (identity !== 5) {

                    if (isEmpty(task.consumerName, 'consumerName') &&
                        isEmpty(task.product, 'product') &&
                        isEmpty(task.telephoneNumber, 'telephoneNumber') &&
                        isEmpty(task.emailId, 'emailId') &&
                        isEmpty(task.productCategory, 'productCategory') &&
                        isEmpty(task.rlRetailerId, 'retailer') &&
                        isEmpty(task.rlRetailerId, 'retailers.code') &&
                        isEmpty(task.rlHomeLocationId, 'rlHomeLocationId') &&
                        isEmpty(task.dropLocCity, 'dropLocCity') &&
                        isEmpty(task.city, 'city') &&
                        isEmpty(task.pincode, 'pincode') &&
                        isEmpty(task.rlProcessStatus, 'rlProcessStatus') &&
                        isEmpty(task.rlTicketNo, 'rlTicketNo') &&
                        isEmpty(task.natureOfComplaint, 'natureOfComplaint') &&
                        isEmpty(task.rlLocationName, 'rlLocationName') &&
                        isEmpty($scope.datePicker.dateOfComplaint.startDate, 'startDate') &&
                        isEmpty($scope.datePicker.dateOfComplaint.endDate, 'endDate') &&
                        isEmpty(task.rlReportingCity, 'task.rlReportingCity')) {
                        return;
                    }
                }
                else {
                    // This Is For Showing Ticket from a particular date
                    // var qItem = {
                    // 	variable: 'ticketCreationDate',
                    // 	value: ">=25-12-2017"
                    // };
                    // q.push(qItem);
                }
                if (task.natureOfComplaint === "repair") {
                }

                // var q = [];
                console.log("taskqwet", task, 'consumerName');
                // $scope.clearSearchFields();
                console.log("task");
                for (var key in task) {
                    if (!isEmpty(task[key], key) && task[key].indexOf('%') === -1) {
                        if (task.natureOfComplaint === "Repair") {
                            var qItem = {
                                variable: key,
                                value: task[key]
                            };

                        }
                        else {
                            var qItem = {
                                variable: key,
                                value: task[key] + '*'
                            };
                        }
                        //					if (key == 'rlTicketNo') {
                        //                        qItem.value = task[key];
                        //                    }
                        q.push(qItem);
                    }
                }
                if (!isEmpty($scope.datePicker.dateOfComplaint.startDate, 'startDate')) {
                    var qItem = {
                        variable: 'ticketCreationDate',
                        value: ">=" + formatDate(new Date($scope.datePicker.dateOfComplaint.startDate))
                    };
                    q.push(qItem);
                }
                if (!isEmpty($scope.datePicker.dateOfComplaint.endDate, 'endDate')) {
                    var qItem = {
                        variable: 'ticketCreationDate',
                        value: "<=" + formatDate(new Date($scope.datePicker.dateOfComplaint.endDate))
                    };
                    q.push(qItem);
                }

                console.log(q);
                //Search tasks
                TasksService
                    .find_by_variable_values({
                        'query': q,
                        'variablesOfInterest': variablesOfInterest,
                        'firstResult': first,
                        maxResults: limit
                    })
                    .success(function (response) {

                        if (response !== undefined && response.data !== undefined && response.data.length > 0) {
                            $scope.pages = 1;
                            $scope.searchedTasks = response.data;

                            $scope.test = "abcccccc";
                            $scope.total_count = response.totalRecords;
                            console.log("inside https://www.google.com/");
                            // $window.location.href = "http://localhost/rl_angular//server/attachment/download/search/abc/bca";
                            console.log("outside https://www.google.com/");
                        }
                    });
            }

            $scope.searchCompleted = function (task, first, limit) {
                $scope.searchDown2Task = task;
                console.log("inside Search Completed", task.rlLocationName);
                $scope.currentPage = first // initialize page no to 1

                first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
                if (task === undefined) {
                    return;
                }
                if ($scope.loginInfo.roleCode == 'retailer') {
                    task.rlRetailerId = $scope.loginInfo.retailerId;
                    console.log("$scope.loginInfo :" + $scope.loginInfo.retailerId);
                }
                if ($scope.loginInfo.roleCode == 'hub_manager') {
                    task.rlLocationName = $scope.loginInfo.locationName;
                    console.log("$scope.locationName :" + $scope.loginInfo.locationName);
                }
                if (isEmpty(task.consumerName, 'consumerName') &&
                    isEmpty(task.product, 'product') &&
                    isEmpty(task.telephoneNumber, 'telephoneNumber') &&
                    isEmpty(task.emailId, 'emailId') &&
                    isEmpty(task.productCategory, 'productCategory') &&
                    isEmpty(task.retailer, 'retailer') &&
                    isEmpty(task.rlTicketNo, 'rlTicketNo') &&
                    isEmpty(task.rlRetailerId, 'rlRetailerId') &&
                    isEmpty(task.rlHomeLocationId, 'rlHomeLocationId') &&
                    isEmpty(task.rlProcessStatus, 'rlProcessStatus') &&
                    // isEmpty(task.rlRetailerId, 'retailers.code') &&
                    isEmpty(task.dropLocCity, 'dropLocCity') &&
                    isEmpty(task.city, 'city') &&
                    isEmpty(task.pincode, 'pincode') &&
                    isEmpty(task.natureOfComplaint, 'natureOfComplaint') &&
                    isEmpty(task.rlLocationName, 'rlLocationName') &&
                    isEmpty($scope.datePicker.dateOfComplaint.startDate, 'startDate') &&
                    isEmpty($scope.datePicker.dateOfComplaint.endDate, 'endDate') &&
                    isEmpty(task.rlReportingCity, 'rlReportingCity')) {
                    return;
                }


                var q = [];
                console.log("task", task, 'consumerName');
                for (var key in task) {
                    if (!isEmpty(task[key], key) && task[key].indexOf('%') === -1) {
                        var qItem = {
                            variable: key,
                            value: task[key] + '*'
                        };
                        q.push(qItem);
                    }
                }
                if (!isEmpty($scope.datePicker.dateOfComplaint.startDate, 'startDate')) {
                    var qItem = {
                        variable: 'ticketCreationDate',
                        value: ">=" + formatDate(new Date($scope.datePicker.dateOfComplaint.startDate))
                    };
                    q.push(qItem);
                }
                if (!isEmpty($scope.datePicker.dateOfComplaint.endDate, 'endDate')) {
                    var qItem = {
                        variable: 'ticketCreationDate',
                        value: "<=" + formatDate(new Date($scope.datePicker.dateOfComplaint.endDate))
                    };
                    q.push(qItem);
                }


                //Search tasks
                TasksService
                    .find_completed_by_variable_values({
                        'query': q,
                        'variablesOfInterest': variablesOfInterest,
                        'firstResult': first,
                        maxResults: limit
                    })
                    .success(function (response) {

                        if (response !== undefined && response.data !== undefined && response.data.length > 0) {
                            $scope.pages = 1;
                            $scope.completedTasks = response.data;
                            $scope.total_count = response.totalRecords;
                            console.log("$scope.total_count", $scope.total_count);
                            console.log("$scope.completedTasks", $scope.completedTasks);
                        }
                    });
            }

            $scope.searchTicketStatus = function (task, first, limit) {
                $scope.currentPage = first // initialize page no to 1

                first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
                if (task === undefined) {
                    return;
                }
                task.retailer = $scope.loginInfo.retailerName;
                console.log("Retailer name: " + task.retailer);
                if (isEmpty(task.consumerName, 'consumerName') &&
                    isEmpty(task.product, 'product') &&
                    isEmpty(task.telephoneNumber, 'telephoneNumber') &&
                    isEmpty(task.emailId, 'emailId') &&
                    isEmpty(task.productCategory, 'productCategory') &&
                    isEmpty(task.retailer, 'retailer') &&
                    isEmpty(task.rlTicketNo, 'rlTicketNo') &&
                    isEmpty($scope.datePicker.dateOfComplaint.startDate, 'startDate') &&
                    isEmpty($scope.datePicker.dateOfComplaint.endDate, 'endDate')) {
                    return;
                }


                var q = [];
                for (var key in task) {
                    if (!isEmpty(task[key], key) && task[key].indexOf('%') === -1) {
                        var qItem = {
                            variable: key,
                            value: task[key] + '*'
                        };
                        //					if (key == 'rlTicketNo') {
                        //                        qItem.value = task[key];
                        //                    }
                        q.push(qItem);
                    }
                }
                if (!isEmpty($scope.datePicker.dateOfComplaint.startDate, 'startDate')) {
                    var qItem = {
                        variable: 'ticketCreationDate',
                        value: ">=" + formatDate(new Date($scope.datePicker.dateOfComplaint.startDate))
                    };
                    q.push(qItem);
                }
                if (!isEmpty($scope.datePicker.dateOfComplaint.endDate, 'endDate')) {
                    var qItem = {
                        variable: 'ticketCreationDate',
                        value: "<=" + formatDate(new Date($scope.datePicker.dateOfComplaint.endDate))
                    };
                    q.push(qItem);
                }


                //Search tasks
                TasksService
                    .find_by_variable_values({
                        'query': q,
                        'variablesOfInterest': variablesOfInterest,
                        'firstResult': first,
                        'roleCode': $scope.loginInfo.roleCode,
                        maxResults: limit
                    })
                    .success(function (response) {
                        if (response.data.length == 0 && $scope.loginInfo.roleCode == 'retailer') {
                            notification.error("Ticket no is not valid");
                        }

                        if (response !== undefined && response.data !== undefined && response.data.length > 0) {
                            $scope.pages = 1;
                            $scope.searchedTasks = response.data;
                            $scope.total_count = response.totalRecords;
                        }
                    });
            }

            $scope.viewTaskDetails = function (task) {
                var q = [{
                    variable: 'taskId',
                    value: task.taskId
                }]
                //Search tasks
                TasksService
                    .find_by_variable_values({
                        'query': q
                    })
                    .success(function (response) {


                        $scope.task = response;

                    });
            }

            $scope.clearSearchFields = function () {
                $scope.task = {};
                $scope.datePicker = {};
                $scope.datePicker.dateOfComplaint = {startDate: null, endDate: null};
                $scope.searchedTasks = [];
            }


            $scope.openLightboxModal = function (processId, key) {
                var d = "closeThisDialog();";
                ngDialog.open({
                    className: ' modal2',
                    showClose: false,
                    closeByDocument: true,
                    closeByEscape: true,
                    template:
                    '<div id="myModal2" class="modal2">' +
                    '<span class="ngdialog-close" ng-click="' + d + '"></span>' +
                    '<img class="modal-content2" id="img02" src="' + host_base_url + 'server/attachment/access/process/' + processId + '/' + key + '">' +
                    '<div id="caption">' + key + '</div>' +
                    '</div>',
                    controller: ['$scope', function ($scope) {
                        // Controller logic here
                        $scope.closeThisDialog = function () {

                        }
                    }]
                });
            }

            $scope.openLightboxModal2 = function (url, type) {
                var d = "closeThisDialog();";
                ngDialog.open({
                    className: ' modal2',
                    showClose: false,
                    closeByDocument: true,
                    closeByEscape: true,
                    template:
                    '<div id="myModal2" class="modal2">' +
                    '<span class="ngdialog-close" ng-click="' + d + '"></span>' +
                    '<img class="modal-content2" id="img02" src="' + url + '">' +
                    '<div id="caption">' + type + '</div>' +
                    '</div>',
                    controller: ['$scope', function ($scope) {
                        // Controller logic here
                        $scope.closeThisDialog = function () {

                        }
                    }]
                });
            }

            $scope.openLightboxModal3 = function (processId, key2, key3, key4) {
                var d = "closeThisDialog();";
                console.log("Inside Model3");
                ngDialog.open({
                    className: ' modal2',
                    showClose: false,
                    closeByDocument: true,
                    closeByEscape: true,
                    template:
                    '<div id="myModal2" class="modal2">' +
                    '<span class="ngdialog-close" ng-click="' + d + '"></span>' +
                    '<img class="modal-content2" id="img02" src="' + host_base_url + 'server/attachment/access/' + processId + '/' + key2 + '/' + key3 + '/' + key4 + '">' +
                    '<div id="caption">' + key3 + '</div>' +
                    '</div>',
                    controller: ['$scope', function ($scope) {
                        // Controller logic here
                        $scope.closeThisDialog = function () {

                        }
                    }]
                });
            }
            $scope.latlang = [];
            $scope.callLocation = function (processId, key3, key4) {
                console.log("inside CallLocation");
                console.log(processId, key3, key4);
                TasksService.viewLocationDetail(processId, key3, key4).success(function (response) {
                    console.log("viewLocationDetail response : " + response);
                    $scope.latlang = response.response;
                    // $scope.longitude = response.response.longitude;
                    console.log("viewLocationDetail response : " + $scope.latlang.longitude);

                });
            }

            $scope.showTrackingDetails = function (docketNo) {
                var d = "closeThisDialog();";
                $scope.docketNo = docketNo;
                RestHTTPService
                    .get(API.AMPM.TRACK_PRODUCT, {conNo: $scope.docketNo}, 'conNo')
                    .success(function (response) {
                        $scope.productTrackingDetails = response.response;
                        ngDialog.open({
                            className: "tracking-dialog ngdialog-theme-default",
                            showClose: false,
                            closeByDocument: true,
                            closeByEscape: true,
                            scope: $scope,
                            id: "ampmDialog",
                            template:
                            '<div class="dialog-contents">' +
                            '<div class="panel panel-default">' +
                            '<div class="panel-heading">' +
                            '<h3 class="panel-title" >AMPM Tracking : <span ng-bind="docketNo"></span></h3>' +
                            '<span class="ngdialog-close pull-right" ng-click="' + d + '"></span>' +
                            '</div>' +
                            '<div class="panel-body">' +
                            '<table class="table">' +
                            '<tr>' +
                            '<th>#</th>' +
                            '<th>Consignment No.</th>' +
                            '<th>Ref. No.</th>' +
                            '<th>Mf. No.</th>' +
                            '<th>Type</th>' +
                            '<th>Date</th>' +
                            '<th>Location</th>' +
                            '<th>Status</th>' +

                            '</tr>' +
                            '<tr ng-repeat="detail in productTrackingDetails">' +
                            '<td ng-bind="$index+1"></td>' +
                            '<td ng-bind="detail.conNo"></td>' +
                            '<td ng-bind="detail.refNo"></td>' +
                            '<td ng-bind="detail.mfNo"></td>' +
                            '<td ng-bind="detail.type"></td>' +
                            '<td><span ng-bind="detail.conNoDate"></span>&nbsp;<span ng-bind="detail.conNoTime"></span></td>' +
                            '<td><span ng-bind="detail.cityName"></span>&nbsp;<span ng-bind="detail.location"></span></td>' +
                            '<td ng-bind="detail.status"></td>' +
                            '</tr>' +
                            '</table>' +
                            '<div class="text-center">' +
                            '<button ng-click="closeThisDialog()" class="btn btn-default">Close</button>&nbsp;&nbsp;' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>',
                            controller: ['$scope', function ($scope) {


                                // Controller logic here
                                $scope.closeThisDialog = function () {
                                    ngDialog.close();
                                }
                            }]
                        });
                    });


            }

            $scope.refreshData = function (first, limit) {


                if ($state.current.url == "/tasks/manager/unclaimed/:first/:limit") {
                    $scope.pages = 1;
                    $scope.showRefresh = true;
                    $scope.managerUnclaimed(first, limit);
                }

                if ($state.current.url == "/tasks/manager/assigned/:first/:limit") {
                    $scope.pages = 1;
                    $scope.showRefresh = true;
                    $scope.managerAssigned(first, limit);
                }

                if ($state.current.url == "/tasks/manager/completed/:first/:limit") {
                    $scope.pages = 1;
                    $scope.showRefresh = true;
                    $scope.managerCompleted(first, limit);
                }


                //if ($state.current.url == "/tasks/retailer/unclaimed/:first/:limit") {
                //	$scope.pages = 1;
                //	$scope.showRefresh = true;
                //	$scope.retailerUnclaimed(first, limit);
                //}
                //
                //if ($state.current.url == "/tasks/retailer/assigned/:first/:limit") {
                //	$scope.pages = 1;
                //	$scope.showRefresh = true;
                //	$scope.retailerAssigned(first, limit);
                //}
                //
                //if ($state.current.url == "/tasks/retailer/completed/:first/:limit") {
                //	$scope.pages = 1;
                //	$scope.showRefresh = true;
                //	$scope.retailerCompleted(first, limit);
                //}

                if ($state.current.url == "/tasks/my-completed/:first/:limit") {
                    $scope.pages = 1;
                    $scope.showRefresh = true;
                    $scope.my_completed(first, limit);
                }

                if ($state.current.url == "/tasks/involved/:first/:limit") {
                    $scope.filtervar = [];
                    $scope.datefiltervar = null;
                    $scope.involved($scope.filtervar, first, limit);
                    Utility.populate_retailer({}, $scope);
                    $scope.showRefresh = true;
                    $(document).ready(function () {
                        $('[data-toggle="tooltip"]').tooltip();
                    });
                }

                if ($state.current.url == "/tasks/assigned/:first/:limit") {
                    $scope.assigned(first, limit);
                    $scope.showRefresh = true;
                    var assigneeNumber = null;
                    $cookies.remove('AssigneeNumber');
                    TasksService.assigneeNumber({id: $scope.loginInfo.id}).success(function (response) {
                        $cookies.put('AssigneeNumber', assigneeNumber);
                        //$rootScope.loginInfo.number = assigneeNumber;
                        // $scope.loginInfo.number = assigneeNumber;
                        // console.log($rootScope.loginInfo.number);
                        // console.log($scope.loginInfo.number);
                    })


                }

                if ($state.current.url == "/tasks/retailer/:first/:limit") {
                    $scope.showRefresh = true;
                    var loginInfo = JSON.parse(TokenFactory.getToken('loginInfo'));
                    if (loginInfo.roleCode == 'retailer') {
                        $scope.task = {retailer: loginInfo.retailerName};
                        // $scope.task = { ticketCreationDate: ">=2017/12/22" };
                        $scope.search($scope.task, 1, $scope.pageSize, 5);
                    }
                }

                if ($state.current.url == "/tasks/hub_manager/:first/:limit") {
                    $scope.showRefresh = true;
                    console.log("Inside /tasks/hub_manager/:first/:limit");
                    var loginInfo = JSON.parse(TokenFactory.getToken('loginInfo'));
                    if (loginInfo.roleCode == 'hub_manager') {
                        $scope.task = {rlLocationName: loginInfo.locationName};
                        // $scope.task = { ticketCreationDate: ">=2017/12/22" };
                        $scope.search($scope.task, 1, $scope.pageSize, 5);
                    }
                }

                if ($state.current.url == "/tasks/retailer/completed:first/:limit") {
                    $scope.showRefresh = true;
                    var loginInfo = JSON.parse(TokenFactory.getToken('loginInfo'));
                    if (loginInfo.roleCode == 'retailer') {
                        $scope.task = {retailer: loginInfo.retailerName};
                        $scope.searchCompleted($scope.task, 1, $scope.pageSize);
                    }
                }

                if ($state.current.url == "/hub/completed:first/:limit") {
                    console.log("Inside Hub Completed")
                    $scope.showRefresh = true;
                    var loginInfo = JSON.parse(TokenFactory.getToken('loginInfo'));
                    if (loginInfo.roleCode == 'hub_manager') {
                        $scogtpe.task = {rlLocationName: loginInfo.locationName};
                        $scope.searchCompleted($scope.task, 1, $scope.pageSize);
                    }
                }

            }


            if ($state.current.data.params.limit !== undefined) {
                $state.current.data.params.limit = ($state.current.data.params.limit === ":limit") ? LIST_PAGE_SIZE : $state.current.data.params.limit;
            }

            if ($state.current.data.params.first !== undefined) {
                $state.current.data.params.first = ($state.current.data.params.first === ":first") ? 1 : $state.current.data.params.first;
            }

            $scope.upload = function (authFile) {
                if ($scope.authFileElement.$invalid) {
                    return;
                }
                console.log("processId-->> " + $state.current.data.params.id);

                var param = {data: {processId: $state.current.data.params.id}, file: authFile.file};


                TasksService.upload(param)
                    .progress(function (evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        $scope.uploadPercentage = progressPercentage;
                    })
                    .success(function (data, status, headers, config) {
                        if (status == 200 && data.success === true) {
                            notification.success("Successfully uploaded Authorization letter!");
                        }

                        if (data.errorMessages !== undefined && data.errorMessages.length > 0) {
                            notification.error(data.errorMessages[0]);
                        }
                        $scope.doaForm = {};
                        $scope.uploadPercentage = 0;
                        $state.go("rl.retailer-tasks");
                    })
                    .error(function (error) {
                        var errorM = "";
                        $(error.message).each(function () {
                            errorM += this + "<br/>";
                        });
                        notification.error(errorM);
                    });

            }

            $scope.uploadAdditionalFile = function (additionalFile) {
                if ($scope.additionalFileElement.$invalid) {
                    return;
                }
                console.log("processId-->> " + $state.current.data.params.id);

                var param = {data: {processId: $state.current.data.params.id}, file: additionalFile.file};

                TasksService.uploadAdditionalFile(param)
                    .progress(function (evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        $scope.uploadPercentage = progressPercentage;
                    })
                    .success(function (data, status, headers, config) {
                        console.log("its done");
                        //                if (data !== undefined && data !== null && data !== "") {
                        //
                        //					if (status == 200 && data.noOfRecordsInserted > 0) {
                        //						if (data.noOfRecordsInserted == 1) {
                        //                            //code
                        //                        }
                        //						notification.success("Successfully uploaded " + data.noOfRecordsInserted + ( (data.noOfRecordsInserted == 1) ? " ticket!" : " tickets!") );
                        //					}
                        //
                        //					if (data.errorMessages.length > 0) {
                        //						notification.error(data.errorMessages[0]);
                        //					}
                        //				}

                        $rootScope.$broadcast('showUploadReturnMessage', {
                            data: data,
                            status: status,
                            headers: headers,
                            config: config
                        });
                        console.log("data", data);
                        console.log("status", status);
                        console.log("headers", headers);
                        console.log("config", config);
                        $scope.uploadPercentage = 0;
                    })
                    .error(function (error) {
                        var errorM = "";
                        $(error.message).each(function () {
                            errorM += this + "<br/>";
                            console.log("errorMIF", errorM);
                        });
                        notification.error(errorM);
                        console.log("errorMElse", errorM);
                    });


            }

            $scope.uploadBitsBarcodeFile = function (additionalFile) {
                if ($scope.additionalFileElement.$invalid) {
                    return;
                }
                console.log("processId-->> " + $state.current.data.params.id);

                var param = {data: {processId: $state.current.data.params.id}, file: additionalFile.file};

                TasksService.uploadBitsBarcodeFile(param)
                    .progress(function (evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        $scope.uploadPercentage = progressPercentage;
                    })
                    .success(function (data, status, headers, config) {
                        console.log("its done");
                        //                if (data !== undefined && data !== null && data !== "") {
                        //
                        //					if (status == 200 && data.noOfRecordsInserted > 0) {
                        //						if (data.noOfRecordsInserted == 1) {
                        //                            //code
                        //                        }
                        //						notification.success("Successfully uploaded " + data.noOfRecordsInserted + ( (data.noOfRecordsInserted == 1) ? " ticket!" : " tickets!") );
                        //					}
                        //
                        //					if (data.errorMessages.length > 0) {
                        //						notification.error(data.errorMessages[0]);
                        //					}
                        //				}

                        $rootScope.$broadcast('showUploadReturnMessage', {
                            data: data,
                            status: status,
                            headers: headers,
                            config: config
                        });
                        console.log("data", data);
                        console.log("status", status);
                        console.log("headers", headers);
                        console.log("config", config);
                        $scope.uploadPercentage = 0;
                    })
                    .error(function (error) {
                        var errorM = "";
                        $(error.message).each(function () {
                            errorM += this + "<br/>";
                            console.log("errorMIF", errorM);
                        });
                        notification.error(errorM);
                        console.log("errorMElse", errorM);
                    });


            }

            $scope.uploadEwasteAdditionalFile = function (additionalFile) {
                if ($scope.additionalFileElement.$invalid) {
                    return;
                }
                console.log("processId-->> " + $state.current.data.params.id);

                var param = {data: {processId: $state.current.data.params.id}, file: additionalFile.file};

                TasksService.uploadEwasteAdditionalFile(param)
                    .progress(function (evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        $scope.uploadPercentage = progressPercentage;
                    })
                    .success(function (data, status, headers, config) {
                        $rootScope.$broadcast('showUploadReturnMessage', {
                            data: data,
                            status: status,
                            headers: headers,
                            config: config
                        });
                        console.log("data", data);
                        console.log("status", status);
                        console.log("headers", headers);
                        console.log("config", config);
                        $scope.uploadPercentage = 0;
                    })
                    .error(function (error) {
                        var errorM = "";
                        $(error.message).each(function () {
                            errorM += this + "<br/>";
                            console.log("errorMIF", errorM);
                        });
                        notification.error(errorM);
                        console.log("errorMElse", errorM);
                    });
            }

            $scope.uploadPurchaseInvoice = function (purchaseInvoice) {
                if ($scope.purchaseInvoiceElement.$invalid) {
                    return;
                }
                console.log("processId-->> " + $state.current.data.params.id);

                var param = {data: {processId: $state.current.data.params.id}, file: purchaseInvoice.file};


                TasksService.uploadPurchaseInvoice(param)
                    .progress(function (evt) {
                        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                        $scope.uploadPercentage = progressPercentage;
                    })
                    .success(function (data, status, headers, config) {
                        if (status == 200 && data.success === true) {
                            notification.success("Successfully uploaded Purchase Invoice!");
                        }

                        if (data.errorMessages !== undefined && data.errorMessages.length > 0) {
                            notification.error(data.errorMessages[0]);
                        }
                        $scope.doaForm = {};
                        $scope.uploadPercentage = 0;
                        $state.go("rl.retailer-tasks");
                    })
                    .error(function (error) {
                        var errorM = "";
                        $(error.message).each(function () {
                            errorM += this + "<br/>";
                        });
                        notification.error(errorM);
                    });

            }


            $scope.processIds = [];
            $scope.toggleBulkPickupTicket = function (id) {

                var found = 0;
                var ids = [];
                if ($scope.processIds.length > 0) {
                    angular.forEach($scope.processIds, function (item, key) {

                        if (item === id) {
                            found = 1;
                        }
                        else {
                            ids.push(item);
                        }
                    });
                }
                else {
                    ids.push(id);
                }

                $scope.processIds = ids;
            }

            $scope.isMarked = 0;
            $scope.markUnmarkAll = function (bulkPickupTasks) {
                var ids = [];

                //mark all
                if ($scope.isMarked == 0) {
                    for (var i = 0; i < bulkPickupTasks.length; i++) {
                        ids.push(bulkPickupTasks[i].processId);
                    }

                    $scope.processIds = ids;
                    $scope.isMarked = 1;
                }

                //unmark all
                else {
                    $scope.processIds = [];
                    $scope.isMarked = 0;
                }

            }

            $scope.startBulkPickup = function (processIds) {
                for (var i = 0; i < processIds.length; i++) {
                    if (processIds[i] != undefined &&
                        processIds[i] != null &&
                        processIds[i] != "") {
                        //code
                        console.log("ProcessId: " + processIds[i]);
                    }
                }

                TasksService
                    .start_bulk_pickup({
                        'processIds': processIds
                    })
                    .success(function (response) {
                        console.log("Old Response " + response);

                    })
            }

            $scope.generateDeclaration = function (processId) {
                var processId = processId;
                TasksService.genrateDec(processId).success(function (response) {
                    var res = response;
                    if (response !== undefined && response.message !== undefined) {
                        notification.success(response.message);
                    }

                });
                //    $state.go('rl.task-view');
                TasksService
                    .find_by_process_id($state.current.data.params.id, variablesOfInterestTicketView)
                    .success(function (response) {
                        var vars = response.variables;

                        if (!isNaN(vars.rlAppointmentDateForFE)) {
                            vars.rlAppointmentDateForFE = convertToDateTimeString(vars.rlAppointmentDateForFE, 'dd-MM-yyyy hh:mm');
                        }
                        response.variables = vars;
                        $scope.task = response;


                        //Get ticket service costing heads
                        //var costingHeads = $.trim($scope.task.variables.rlTicketCosting);
                        //var costingHeadsArr = costingHeads.replace( /\n/g, "~~" ).split( "~~" );
                        //console.log(costingHeads, costingHeadsArr);

                        if (response.completed == false) {
                            Utility.populate_roles({}, $scope);
                        }

                        var images = [];
                        angular.forEach(response.variables, function (value, key) {
                            if (key.indexOf("_PhotographBefore") !== -1 ||
                                key.indexOf("_PhotoBefore") !== -1 ||
                                key.indexOf("_PhotographAfter") !== -1 ||
                                key.indexOf("_Signature") !== -1 ||
                                key.indexOf("_ServiceCenterSignature") !== -1 ||
                                key.indexOf("_PhotoId") !== -1 ||
                                key.indexOf("_CustomerSignature") !== -1) {

                                images.push(key);
                            }
                        }, images);

                        $scope.ticketImages = images;
                    });
            }

// Finance Head Controller Logic
            if ($state.current.url == "/partyview") {
                Utility.populate_retailer({}, $scope);
                Utility.populate_location({}, $scope);
                console.log($scope.locations);
            }

//Finance Menu For Hub Manager
            if ($state.current.url == "/finance/hub") {
                Utility.populate_retailer({}, $scope);
                Utility.populate_location({}, $scope);
                console.log($scope.locations);
            }

            $scope.searchFinance = function (finance, first, limit) {
                console.log("finance :" + finance);
                $scope.currentPage = first;// initialize page no to 1
                first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
                var q = [];
                console.log("taskqwet", finance, 'finance');
                // $scope.clearSearchFields();
                console.log("finance");
                var retailerId = "";
                var locationId = "";
                var startDate = "";
                var endDate = "";

                //Adding hub location
                if ($scope.loginInfo.roleCode === 'hub_manager') {
                    locationId = $scope.loginInfo.locationName;
                }

                if (finance.retailerId !== null && finance.retailerId !== undefined) {
                    retailerId = finance.retailerId;
                }
                if (finance.locationId !== null && finance.locationId !== undefined) {
                    locationId = finance.locationId;
                }
                if (!isEmpty($scope.datePicker.dateOfTransaction.startDate, 'startDate')) {
                    startDate = ">=" + formatDate(new Date($scope.datePicker.dateOfTransaction.startDate));
                    $scope.startDateRange = startDate;
                }
                if (!isEmpty($scope.datePicker.dateOfTransaction.endDate, 'endDate')) {
                    endDate = "<=" + formatDate(new Date($scope.datePicker.dateOfTransaction.endDate));
                    $scope.endDateRange = endDate;
                }
                TasksService.getFinance({
                    query: {
                        "retailerId": retailerId,
                        "locationId": locationId,
                        "startDate": startDate,
                        "endDate": endDate
                    }, firstResult: first, maxResults: limit
                }).success(function (response) {

                    if (response.response !== undefined && response.response !== undefined && response.response.length > 0) {
                        $scope.financeDetails = response.response;
                        $scope.total_count = response.response.totalRecords;
                        console.log("$scope.total_count", $scope.total_count);
                        console.log("$scope.completedTasks", $scope.financeDetails);
                    }
                });

            }

            $scope.clearFinanceFields = function () {
                $scope.financeDetails = [];
                $scope.financeDetail = [];
                $scope.finance = {};
                $scope.datePicker.dateOfTransaction = {};
                // $scope.datePicker.dateOfTransaction.endDate = {};
            }

            if ($state.current.url == "/partyview/:retailer/:locationId") {
                console.log("Inside getFinDetails");
                var retailerId = $state.current.data.params.retailer;
                var locationId = $state.current.data.params.locationId;
                console.log("retailerId :" + retailerId);
                console.log("locationId :" + locationId);
                var startDate = $scope.endDateRange;
                var endDate = $scope.endDateRange;
                TasksService.getFinanceDetails({
                    query: fin,
                    startDate: startDate,
                    EndDate: endDate,
                    firstResult: 1,
                    maxResults: 10
                }).success(function (response) {

                    if (response !== undefined && response !== undefined && response.length > 0) {
                        $scope.financeDetailsByRetailer = response;
                        $scope.total_count = response.totalRecords;
                        console.log("$scope.total_count", $scope.total_count);
                        console.log("$scope.completedTasks", $scope.financeDetails);
                    }
                });
            }

// Finance Head Controller Logic Ends Here
            $scope.updateNewCost = function (updateCost) {
                console.log("processId-->> " + $state.current.data.params.id);
                console.log("updated Cost", updateCost);
                $scope.errors = {};
                //Validate City
                if (hasErrors(updateCost)) return false;

                var param = {processId: $state.current.data.params.id, newprodCost: updateCost, oldPrice: $scope.pro};


                TasksService.newProductCost(param).success(function (response) {
                    console.log("responses: ", response.success);
                    if (response.success === true) {
                        notification.success(response.message);
                        $state.go('rl.retailer-tasks');
                    }
                    else {
                        notification.success("response.message");
                    }

                    // if (response.errorMessages !== undefined && response.errorMessages.length > 0) {
                    // 	notification.error(data.errorMessages[0]);
                    // }
                })
                    .error(function (error) {
                        var errorM = "";
                        $(error.message).each(function () {
                            errorM += this + "<br/>";
                        });
                        notification.error(errorM);
                    });

            }
//Changes For RepairStandBy Damaged Cost

            $scope.updateDamageCost = function (updateCost) {
                console.log("updated Cost", updateCost);
                $scope.errors = {};
                //Validate City
                if (hasErrors(updateCost)) return false;

                var param = {processId: $state.current.data.params.id, damageCost: updateCost};


                TasksService.damageCost(param).success(function (response) {
                    console.log("responses: ", response.success);
                    if (response.success === true) {
                        notification.success(response.message);
                        $state.go('rl.retailer-tasks');
                    }
                    else {
                        notification.success("response.message");
                    }

                    // if (response.errorMessages !== undefined && response.errorMessages.length > 0) {
                    // 	notification.error(data.errorMessages[0]);
                    // }
                })
                    .error(function (error) {
                        var errorM = "";
                        $(error.message).each(function () {
                            errorM += this + "<br/>";
                        });
                        notification.error(errorM);
                    });

            }

//Changes For RepairStandBy Damage Cost End Here

//CHANGES FOR PAYMENT GATEWAY
            $scope.makePayment = function (processId) {
                console.log("Process Id :" + processId);
                TasksService.makePayment({processId: processId}).success(function (response) {
                    console.log("responses: ", response.success);
                    $scope.payToResponse = response;
                    console.log("$scope.payToResponse : " + $scope.payToResponse);
                    if (response.success === true) {
                        notification.success(response.message);
                    }
                    else {
                        notification.success(response.message);
                    }
                })
            }

            $scope.paymentStatus = function (processId) {
                console.log("Process Id :" + processId);
                TasksService.checkStatus({processId: processId}).success(function (response) {
                    console.log("responses: ", response);
                    $scope.payToResponse = response.response;
                    console.log("$scope.payToResponse : " + $scope.payToResponse);
                    if (response.success === true) {
                        notification.success(response.message);
                    }
                    else {
                        notification.success(response.message);
                    }
                })
            }

//CHANGES FOR PAYMENT GATEWAY END HERE
//validate UpdateCost Error
            var hasErrors = function (updateCost) {
                if (isEmpty(updateCost, 'updateCost', "Enter Updated Cost", $scope.notification)) {
                    $scope.errors.updateCost = "Enter updateCost";
                }

                if (Object.getOwnPropertyNames($scope.errors).length > 0) {
                    return true;
                }
                return false;
            }

            $scope.callToCustomer = function (processId, contNo) {
                var processId = processId;
                var contNo = contNo;
                console.log(contNo + "inside callToCustomer :" + processId);
                TasksService.makeCall({'processId': processId, 'contNo': contNo}).success(function (response) {
                    console.log("responses: ", response.success);
                    $scope.callResponse = response;
                    if (response.success === true) {
                        notification.success(response.message);
                    }
                    else {
                        notification.success(response.message);
                    }
                })
            }
            $scope.button = {};
            $scope.button.disabled = false;
            $scope.cnumber = "currentNumber";
            $scope.caltnumber = "altNumber";
            $scope.callCustomer = function (value) {
                $scope.button.disabled = true;
                var processIdf = $state.current.data.params.id;
                var num = value
                var processId = TokenFactory.getToken('processId');
                //console.log($state.current.data.params.processId);
                // console.log(num);
                var cnumber = null;
                if (num === "currentNumber") {
                    cnumber = $cookies.get("CustomerNumber");
                } else {
                    cnumber = $cookies.get("CustomerAlternateNumber");
                }
                // console.log(processId + "Inside callCustomer" + "********");
                var anumber = $cookies.get('AssigneeNumber');
                //console.log($scope.loginInfo);
                TasksService.makeCallToCustomer({
                    'processId': processId,
                    'to': num,
                    'assigneeNumber': $scope.loginInfo.phone,
                    'customerNumber': cnumber
                }).success(function (response) {
                    console.log("response: ", response.success);
                    $scope.callResponse1 = response;
                    if (response.success === true) {
                        notification.success(response.message);
                    }
                    else {
                        notification.success(response.message);
                    }
                    if (response.message === "Try Again") {
                        $scope.button.disabled = false;
                    }

                })
            }


            $scope.currentPage = $state.current.data.params.first;

            $scope.refreshData($state.current.data.params.first, $state.current.data.params.limit);


            $scope.showInfo = function (value) {
                TasksService.deviceInfo({'groupBy': value}).success(function (response) {
                    console.log(response);
                    $scope.deviceData = response.response;
                })
            }

            if ($state.current.url == "/device/info") {
                $scope.show = 1;
                $scope.showInfo("version");

            }
//$scope.searchedTasksNew = {};
            $scope.showDialog = function (ev, data) {
                if (ev === 'orderNumber') {
                    $scope.task.orderNumber = data;
                }
                else {
                    $scope.task.consumerName = data;
                }

                console.log($scope.searchedTasks);

                $scope.search($scope.task, 1, 10, 5);
            }


        }

    ])
;