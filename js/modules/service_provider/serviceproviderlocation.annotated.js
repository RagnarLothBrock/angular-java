'use strict';

app.factory('ServiceProviderLocation', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {
	return {
        list: function(params) {
			return $http.post(REST_API_URL + API.SERVICE_PROVIDER.LOCATION.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.SERVICE_PROVIDER.LOCATION.CREATE, {'serviceProviderLocation':params});
		},
		get: function(params) {
			var getUrl = API.SERVICE_PROVIDER.LOCATION.GET.replace('<ServiceProviderLocationId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.SERVICE_PROVIDER.LOCATION.UPDATE, {'serviceProviderLocation':params});
		},
		del: function(params) {
			var getUrl = API.SERVICE_PROVIDER.LOCATION.DELETE.replace('<ServiceProviderLocationId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		all_locations:function(params) {
			var getUrl = API.SERVICE_PROVIDER.LOCATION.ALL_LOCATIONS
						.replace('<first>', params.firstResult)
						.replace('<limit>', params.maxResults);
			return $http.post(REST_API_URL + getUrl, params);
			return $http.post(REST_API_URL + API.SERVICE_PROVIDER.LOCATION.ALL_LOCATIONS, params);
		}
	}
}]);