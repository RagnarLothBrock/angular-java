'use strict';

app.factory('ServiceProviderLocationService', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {
	return {
        list: function(params) {
			return $http.post(REST_API_URL + API.SERVICE_PROVIDER.LOCATION_SERVICE.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.SERVICE_PROVIDER.LOCATION_SERVICE.CREATE, {'serviceProviderLocationService':params});
		},
		get: function(params) {
			var getUrl = API.SERVICE_PROVIDER.LOCATION_SERVICE.GET.replace('<ServiceProviderLocationServiceId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.SERVICE_PROVIDER.LOCATION_SERVICE.UPDATE, {'serviceProviderLocationService':params});
		},
		del: function(params) {
			var getUrl = API.SERVICE_PROVIDER.LOCATION_SERVICE.DELETE.replace('<ServiceProviderLocationServiceId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		by_serviceprovider_id: function(params) {
			var getUrl = API.SERVICE_PROVIDER.LOCATION_SERVICE.BY_SERVICEPROVIDER_ID.replace('<serviceProviderId>', params.serviceProviderId);
			return $http.post(REST_API_URL + getUrl, {});
		}
	}
}]);