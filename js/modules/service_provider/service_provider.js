'use strict'

app.config(['$stateProvider', '$urlRouterProvider','$httpProvider',
    function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $stateProvider
        .state('rl.service-providers', {
			url: '/service-providers',
			views: {
				'page-main-content@' : {
					templateUrl: 'partials/tpl/pages/service_provider/list.tpl.html'
				}
            },
			controller : 'ServiceProviderController',
			data : {
				page : "Service Providers",
				layout : "site",
				requireLogin : true
			}
		})
        .state('rl.service-provider-onboarding', {
			url: '/service-provider/onboarding',
			views: {
				'page-main-content@' : {
					templateUrl: 'partials/tpl/pages/service_provider/onboarding.tpl.html'
				}
            },
			controller : 'ServiceProviderController',
			data : {
				page : "Service Center Onboarding",
				layout : "site",
				requireLogin : true
			}
		})
        .state('rl.service-provider-get', {
			url: '/service-provider/get/:id',
			views: {
				'page-main-content@' : {
					templateUrl: 'partials/tpl/pages/service_provider/onboarding.tpl.html'
				}
            },
			controller : 'ServiceProviderController',
			data : {
				page : "Service Center Update",
				layout : "site",
				requireLogin : true
			}
		})
		.state('rl.all-service-provider-locations', {
			url: '/service-provider/list-all-locations',
			views: {
				'page-main-content@' : {
					templateUrl: 'partials/tpl/pages/service_provider/all_locations.tpl.html'
				}
            },
			controller : 'ServiceProviderController',
			data : {
				page : "Service Center All Locations",
				layout : "site",
				requireLogin : true
			}
		});
    }]);

app.controller('ServiceProviderController', [
                    '$scope','$rootScope', '$state', 'notification', 'ServiceProvider', 'ServiceProviderLocation', 'CityService',
					'ProductCategory', 'ProductSubCategory', 'BrandService', 'ServiceProviderLocationService', 'LIST_PAGE_SIZE',
					'Utility',
                    function($scope,$rootScope, $state, notification, ServiceProvider, ServiceProviderLocation, CityService,
					ProductCategory, ProductSubCategory, BrandService, ServiceProviderLocationService, LIST_PAGE_SIZE,
					Utility){
    
    $scope.breadcrumbs = [
		{
			title: "Service Centers",
			link: '/service-providers'
		}
	];
	
	$scope.pageSize = LIST_PAGE_SIZE;
	$scope.total_count = 0;
	$scope.currentPage = 1;

    $scope.serviceProvider = {};
	$scope.serviceProvider.location = {};
    $scope.steps = [
        "Basic Info",
        "Service Center Location",
        "Service Center Authorization"
    ];
    $scope.currentStep = 1;
	$scope.currentStepName = $scope.steps[$scope.currentStep-1];
	$scope.service_provider_locations = [];
	
	$scope.serviceProviderList = function(first, limit) {
		$scope.currentPage = first // initialize page no to 1
			
		first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
		ServiceProvider.list({'query':{}, firstResult: first, maxResults: limit}).success(function(response) {
			if (response !== undefined && response.data !== undefined && response.data.length > 0){
				$scope.service_providers = response.data;
				$scope.total_count = response.totalRecords;
			}
			
		});
	}
    
    if ($state.current.name == 'rl.service-providers') {
		
		$scope.showAddNew = true;
		$scope.addNewUrl = "/service-provider/onboarding";
		$scope.addNewState = "rl.service-provider-onboarding"
		
        $scope.serviceProviderList(1, $scope.pageSize);
    }
	
	$scope.getServiceProviderLocationsList = function(first, limit) {
		$scope.currentPage = first // initialize page no to 1
		
		first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
		ServiceProviderLocation.all_locations({'query':{},firstResult: first, maxResults: limit}).success(function(response) {
			if (response !== undefined && response.data !== undefined && response.data.length > 0){
				$scope.service_provider_locations = response.data;
				$scope.total_count = response.totalRecords;
			}
		});
	}
	
	if ($state.current.name == 'rl.all-service-provider-locations') {
        $scope.getServiceProviderLocationsList(1, $scope.pageSize);
		$scope.breadcrumbs.push({"title":"All Locations"})
    }
    
    
    //Save Service Center Basic Info
    $scope.save = function(serviceProvider) {
        $scope.errors = {};
        
        //Validate Service Center
        if(hasErrors(serviceProvider)) return false;
        
        if (serviceProvider.id == undefined || serviceProvider.id == "" || serviceProvider.id == null) {
            //Continue with service Provider create
            ServiceProvider
				.create(serviceProvider)
				.success(function(response) {
					if (response !== undefined && response.response !== undefined && response.response !== null) {
						$rootScope.$broadcast ('showSuccessMessage',response);				
						$state.go('rl.service-providers');
					}
				})
				.error(function(errors){
					console.log(errors);
				});
        }
        else {
            //Continue with service Provider Update
            ServiceProvider
				.update(serviceProvider)
				.success(function(response) {
					if (response !== undefined && response.response !== undefined && response.response !== null) {
						$rootScope.$broadcast ('showSuccessMessage',response);
						$state.go('rl.service-providers');
					}
				});
        }
    }
    
    //Validate Service Center Details
    function hasErrors(serviceProvider) {
		if (serviceProvider === undefined) {
            $scope.errors.name = "Enter service provider name";
			$scope.errors.code = "Enter service provider code";
			return false;
        }
        if (isEmpty(serviceProvider.name,'name',"Enter service provider name",$scope.notification)) {
            $scope.errors.name = "Enter service provider name";
        }
        if (isEmpty(serviceProvider.code,'code',"Enter service provider code",$scope.notification)) {
            $scope.errors.code = "Enter service provider code";
        }
        
        if (Object.getOwnPropertyNames($scope.errors).length > 0) {
            return true;
        }
        return false;
    }
    
    //Get Service provider basic details
    function getServiceProviderBasicInfo() {
        if ($state.current.url == "/service-provider/get/:id") {
			$scope.breadcrumbs.push({title:"Update"});
			
            $scope.serviceProvider.id = $state.current.data.params.id;
            ServiceProvider.get($scope.serviceProvider).success(function(response) {
                $scope.serviceProvider.basicInfo = response;
            });
        }
		else {
			$scope.breadcrumbs.push({title:"Create"});
		}
    }
    
    //populate city dropdown
    function populateCity() {
        Utility.populate_city({},$scope);
    }
    
    $scope.populateServiceProviderLocationList = function(first, limit) {
        if ($state.current.url == "/service-provider/get/:id") {
            $scope.serviceProvider.id = $state.current.data.params.id;
			$scope.currentPage = first // initialize page no to 1
			
			first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
            ServiceProviderLocation
                .list({query:{serviceProviderId:$state.current.data.params.id},firstResult:first, maxResults: limit})
                .success(function(response) {
					if (response !== undefined && response.data !== undefined && response.data.length > 0){
						$scope.serviceProvider.locations = response.data;
						$scope.total_count = response.totalRecords;
					}
            });
        }
    }
    
    function getCityName(cityId) {
        for (var i = 0; i < $scope.cities.length; i++) {
            if ($scope.cities[i].id == cityId) {
                return $scope.cities[i].name;
            }
        }
        
        return "";
    }
	
	function populateServicesAuthorized() {
		ServiceProviderLocationService
			.by_serviceprovider_id({serviceProviderId:$state.current.data.params.id})
			.success(function(response){
			$scope.serviceProvider.services = response;
		});
    }
	
	function populateCategories() {
		Utility.populate_category({},$scope);
        
    }
	
	$scope.populateSubCategories = function(categoryId) {
		$scope.subCategories = [];
		Utility.populate_sub_category({productCategoryId:categoryId},$scope);
    }
	
	function populateBrands() {
		Utility.populate_brands({},$scope);
    }
	
	$scope.getServiceProviderLocationService = function(service) {
		service.id = service.serviceProviderLocationServiceId;
		$scope.populateSubCategories(service.categoryId)
		$scope.showServicesList();
		ServiceProviderLocationService.get(service).success(function(response){
			$scope.serviceProvider.serviceAuthorized = response;
		});
	}
    
    //Show Page
    $scope.showPage = function(stepId) {
		$scope.currentStep = stepId;
		$scope.currentStepName = $scope.steps[stepId-1];
		$(".onboarding-steps").hide();
		$(".onboarding-steps:nth-of-type("+stepId+")").show();
		$(".list-group-item").removeClass('active');
		$(".list-group-item:nth-of-type("+stepId+")").addClass('active');
		//Load Step Data
		$scope.loadStepData((stepId-1));
	}
    
    //Load Page data
	$scope.loadStepData = function(stepId) {
		switch (stepId) {
			case 0:
                getServiceProviderBasicInfo();
				break;
            
            case 1:
				$scope.showAddNewServicebtn = false;
                populateCity();
                $scope.populateServiceProviderLocationList(1, $scope.pageSize);
                $scope.showAddNewLocation();
                break;
			
			case 2:
				$scope.showAddNewbtn = false;
				populateServicesAuthorized();
				$scope.serviceProvider.serviceAuthorized = {
					isAuthorizedServiceProvider:"yes",
					isBizlogAuthorized:"yes"
				}
                $scope.showAddNewService();
				populateBrands();
				populateCategories();
                break;
        }
    }
	if ($state.current.name == 'rl.service-provider-onboarding' || $state.current.name == 'rl.service-provider-get') {
        $scope.showPage($scope.currentStep);
    }
    
    $scope.saveLocation = function(location) {
        $scope.errors = {};
        
        //Validate Location
        //if(hasErrorsLocation(location)) return false;
        
        location.serviceProviderId =  $state.current.data.params.id;
        location.serviceProviderCode =  $scope.serviceProvider.basicInfo.code;
        location.city = getCityName(location.cityId);
        
        if (location.id == undefined || location.id == "" || location.id == null) {
            //Continue with service Provider create
            ServiceProviderLocation.create(location).success(function(response) {
				if (response !== undefined && response.response !== undefined && response.response !== null) {
					$rootScope.$broadcast ('showSuccessMessage',response);
					$scope.populateServiceProviderLocationList(1, $scope.pageSize);
					$scope.showAddNewLocation();
                }
                return;
            });
        }
        else {
            //Continue with service Provider Update
            ServiceProviderLocation.update(location).success(function(response) {
                if (response !== undefined && response.response !== undefined && response.response !== null) {
                    $rootScope.$broadcast ('showSuccessMessage',response);
					$scope.populateServiceProviderLocationList(1, $scope.pageSize);
					$scope.showAddNewLocation();
                }
                return;
            });
        }
    }
    
    $scope.getServiceProviderLocation = function(location) {
		ServiceProviderLocation
			.get({id:location.id})
			.success(function(response){
				$scope.serviceProvider.location = response;
				$scope.serviceProvider.location.pincode = parseInt(response.pincode);
				$scope.serviceProvider.location.phone1 = parseInt(response.phone1);
				$scope.serviceProvider.location.phone2 = parseInt(response.phone2);
				
			});
        $scope.showList();
    }
    
   
    $scope.showAddNewLocation = function() {        
        $scope.showAddNewbtn = true;
        $scope.showListBtn = false;
		$scope.actionName = "";
        $("#addNewServiceProviderLocation").collapse('hide')
        $("#listServiceProviderLocations").collapse('show')
    }
    
    $scope.showList = function() {
		$scope.serviceProvider.location = {};
        $scope.showAddNewbtn = false;
        $scope.showListBtn = true;
		$scope.actionName = " - Create";
        $("#addNewServiceProviderLocation").collapse('show')
        $("#listServiceProviderLocations").collapse('hide')
    }
    
	$scope.showAddNewService = function() {        
        $scope.showAddNewServicebtn = true;
        $scope.showServiceListBtn = false;
        $("#addNewServiceProviderServices").collapse('hide')
        $("#listServiceProviderServices").collapse('show')	
    }
    
    $scope.showServicesList = function() {
		$scope.serviceProvider.serviceAuthorized = {
			isAuthorizedServiceProvider:"yes",
			isBizlogAuthorized:"yes"
		}
		$scope.populateServiceProviderLocationList(1, $scope.pageSize);
        $scope.showAddNewServicebtn = false;
        $scope.showServiceListBtn = true;
        $("#addNewServiceProviderServices").collapse('show')
        $("#listServiceProviderServices").collapse('hide')
		
    }
	
	$scope.saveServicesAuthorized = function(services) {
		$scope.errors = {};
        
        //Validate Location
        //if(hasErrorsServices(services)) return false;
        console.log(services);
        if (services.id == undefined || services.id == "" || services.id == null) {
			 console.log(services);
            //Continue with service Provider create
            ServiceProviderLocationService.create(services).success(function(response) {
                console.log(response);
                //$scope.populateServiceProviderLocationList();
				populateServicesAuthorized();
                $scope.showAddNewService();
            });
        }
        else {
            //Continue with service Provider Update
            ServiceProviderLocationService.update(services).success(function(response) {
                console.log(response);
                //$scope.populateServiceProviderLocationList();
				populateServicesAuthorized();
                $scope.showAddNewService();
            });
        }
	}
}]);