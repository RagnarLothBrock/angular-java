'use strict';

app.factory('ServiceProvider', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {
	return {
        list: function(params) {
			return $http.post(REST_API_URL + API.SERVICE_PROVIDER.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.SERVICE_PROVIDER.CREATE, {'serviceProvider':params});
		},
		get: function(params) {
			var getUrl = API.SERVICE_PROVIDER.GET.replace('<serviceproviderid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.SERVICE_PROVIDER.UPDATE, {'serviceProvider':params});
		},
		del: function(params) {
			var getUrl = API.SERVICE_PROVIDER.DELETE.replace('<serviceproviderid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		}
	}
}]);