'use strict'

app.config(['$stateProvider', '$urlRouterProvider','$httpProvider',
    function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $stateProvider
        .state('rl.ticket-bulk-upload', {
			url: '/ticket/bulk-upload',
			views: {
				'page-main-content@' : {
					templateUrl: 'partials/tpl/pages/tickets/upload.tpl.html'
				}
            },
			controller : 'TicketController',
			data : {
				page : "Ticket Upload",
				layout : "site",
				requireLogin : true
			}
		})

		//Hub Manager Ticket Re-Uplload Link
		.state('rl.hub-ticket-bulk-upload', {
			url: '/hub/ticket/bulk-upload',
			views: {
				'page-main-content@' : {
					templateUrl: 'partials/tpl/pages/tickets/upload.tpl.html'
				}
            },
			controller : 'TicketController',
			data : {
				page : "Ticket Upload",
				layout : "site",
				requireLogin : true
			}
		})


        .state('rl.ticket-retailers-bulk-upload', {
			url: '/ticket/retailer-ticket-upload',
			views: {
				'page-main-content@' : {
					templateUrl: 'partials/tpl/pages/tickets/upload.tpl.html'
				}
            },
			controller : 'TicketController',
			data : {
				page : "Retailer Ticket Upload",
				requireLogin : true
			}
		})
		.state('rl.retailer-services-availed', {
			url: '/retailer/services-availed',
			views: {
				'page-main-content@' : {
					templateUrl: 'partials/tpl/pages/tickets/retailerServicesAvailed.tpl.html'
				}
            },
			controller : 'TicketController',
			data : {
				page : "Sample ticket for services availed",
				requireLogin : true
			}
		})
		.state('rl.tickets', {
				url: '/tickets',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/tickets/list.tpl.html'
					}
				},
				controller: 'TicketController',
				data: {
					page: "Ticket Categories",
					requireLogin : true
				}
			})

			//Hub-Manager re-create Ticket
			.state('rl.retickets', {
				url: '/retickets',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/tickets/list.tpl.html'
					}
				},
				controller: 'TicketController',
				data: {
					page: "Ticket Categories",
					requireLogin : true
				}
			})

			.state('rl.tickets-view', {
				url: '/ticket/get/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/tickets/view.tpl.html'
					}
				},
				controller: 'TicketController',
				data: {
					page: "Create New Ticket",
					requireLogin : true
				}
			})
			;
    }]);

app.controller('TicketController', [
                    '$scope','$rootScope', '$state', 'ProcessService', 'notification', 'TokenFactory', 'ServiceMaster', '$location', 'Utils', 'Retailer', 'Utility',
                    function($scope,$rootScope, $state, ProcessService, notification, TokenFactory, ServiceMaster, $location, Utils, Retailer, Utility){
    
	var vm = this;
	vm.dtOptions =  {
		search :false
	};
	$scope.ticket = {data:{}, file:{}};
	$scope.ticketList = [];
	
	$scope.breadcrumbs = [
		{
			title: "Tickets",
			link:'/tickets'
		}
	];
	$scope.breadcrumbs.push({'title' : $state.current.data.page});
	
	
    
    if ($state.current.name == 'rl.tickets') {
		$scope.showUpload = true;
		$scope.uploadUrl = "/ticket/bulk-upload";
		$scope.uploadState = "rl.ticket-bulk-upload"
        ProcessService.list({}).success(function(response) {
			$scope.ticketList = response;
		});
    }

	//Hub Manager ReTicket
	if ($state.current.name == 'rl.retickets') {
		$scope.showUpload = true;
		$scope.uploadUrl = "/hub/ticket/bulk-upload";
		$scope.uploadState = "rl.hub-ticket-bulk-upload"
        ProcessService.list({}).success(function(response) {
			$scope.ticketList = response;
		});
    }
	
	if ($state.current.name == 'rl.ticket-retailers-bulk-upload') {
      //  Utility.populate_retailer({}, $scope);
			
			ServiceMaster.forRetailer($scope.loginInfo.retailerId).success(function(response) {
				$scope.retailerServiceList = response;
				console.log("Services " + $scope.retailerServiceList);
			});
		
    }
	
	if ($state.current.name == 'rl.retailer-services-availed') {
      //  Utility.populate_retailer({}, $scope);
			
			ServiceMaster.forRetailer($scope.loginInfo.retailerId).success(function(response) {
				$scope.retailerServiceList = response;
				console.log("Services " + $scope.retailerServiceList);
			});
		
    }
	$scope.servicesList = [];
	if ($state.current.name == 'rl.ticket-bulk-upload') {
        Utility.populate_retailer({}, $scope);
		ServiceMaster.list({query:{}}).success(function(response){
            $scope.servicesList = response.data;
		})
    }

	//HUb Manager Retailer List
	if ($state.current.name == 'rl.hub-ticket-bulk-upload') {
        Utility.populate_retailer({}, $scope);
		ServiceMaster.list({query:{}}).success(function(response){
            $scope.servicesList = response.data;
		})
    }
	
	if ($state.current.url == "/ticket/get/:id") {
		$scope.ticket.processName = TokenFactory.getToken('processName');
		$scope.ticket.processDataset = TokenFactory.getToken('processDataset');
		$scope.pageActionHeader = "Create Ticket For : " + $scope.processName + '(' + $scope.processDataset + ')';
		$scope.ticket.deploymentId = $state.current.data.params.id;
		ProcessService.firstgroup($state.current.data.params.id, {'fieldValues' : {}}).success(function(response) {
			$scope.ticket = angular.extend($scope.ticket,response);
		});
	}
	
	$scope.showCreateTicket = function(ticket) {
		var deploymentId = ticket.deploymentId;
		TokenFactory.setToken('processName', ticket.name);
		TokenFactory.setToken('processDataset', ticket.dataset);
		TokenFactory.setToken('deploymentId', ticket.deploymentId);
		$location.path("/ticket/get/"+deploymentId);
	}
	
	$scope.save = function(ticket) {
		var returnValues = Utils.fill_fieldValues(ticket);
		$scope.fieldValues = returnValues.fieldValues;
		$scope.errors = returnValues.errors;
		if (Object.getOwnPropertyNames($scope.errors).length > 1) {
			return false;
		}
		//If not lastgroup, go to next group; Otherwise show start ticket
		if (!ticket.lastGroup) {
			ProcessService
				.nextgroup(
					ticket.deploymentId,
					ticket.fillSessionId,
					{'fieldValues' : $scope.fieldValues})
				.success(function(response) {
					if (response != undefined && response.properties != undefined) {
						if (response.properties.length == 0 && response.lastGroup == true) {
						
							//Create new ticket
							$scope.createTicket(ticket.deploymentId,ticket.fillSessionId, $scope.fieldValues);
						}
						
						$scope.ticket = formatResponse(response);
						$scope.deploymentId = $state.current.data.params.id; 
						$scope.ticket.processName = TokenFactory.getToken('processName');
						$scope.ticket.processDataset = TokenFactory.getToken('processDataset');
						$scope.pageActionHeader = "Create New Ticket For : " + $scope.processName + '(' + $scope.processDataset + ')';
					   
					}
			});
		}
		else {
			$scope.createTicket(ticket.deploymentId,
					ticket.fillSessionId, $scope.fieldValues);
		}
	}
	
	$scope.createTicket = function(deploymentId, fillSessionId, fieldValues) {
		console.log(deploymentId, fillSessionId, fieldValues,$scope);
		ProcessService
			.start_process(deploymentId, fillSessionId,{'fieldValues' : fieldValues})
			.success(function(response) {
				
				if (response !== undefined && response.success !== undefined &&
					response.success === 'true' || response.success === true) {
					//code
					notification.success('Successfully created ticket for - ' + $scope.ticket.processName + ' (' +$scope.ticket.processDataset + ')');
					$state.go('rl.tickets');
					
				}
				else {
					
					$scope.startProcessError = 'Unable to create the ticket for '+ $scope.ticket.processName + ' (' +$scope.ticket.processDataset + ')';
					notification.error($scope.startProcessError);
				}
			});
	}
    
    $scope.upload = function(ticket) {
		if($scope.loginInfo.roleCode==="retailer") {
			console.log("Retailer id: " + $scope.loginInfo.retailerId);
			ticket.data.retailerId = $scope.loginInfo.retailerId;
		}
		
        ProcessService.upload(ticket)
            .progress(function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log(progressPercentage);
            })
            .success(function (data, status, headers, config) {
//                if (data !== undefined && data !== null && data !== "") {
//                    
//					if (status == 200 && data.noOfRecordsInserted > 0) {
//						if (data.noOfRecordsInserted == 1) {
//                            //code
//                        }
//						notification.success("Successfully uploaded " + data.noOfRecordsInserted + ( (data.noOfRecordsInserted == 1) ? " ticket!" : " tickets!") );
//					}
//					
//					if (data.errorMessages.length > 0) {
//						notification.error(data.errorMessages[0]);
//					}
//				}
				$rootScope.$broadcast('showUploadReturnMessage', {data:data, status:status, headers:headers, config:config});
                $scope.ticket = {data:{}, file:{}};
            })
            .error(function(error){
                var errorM = "";
                $(error.message).each(function(){
                    errorM += this +"<br/>";
                });
                notification.error(errorM)
            });
    }
    
    $scope.$watch('ticket.file', function () {
        $scope.ticket.selectedFileName = ($scope.ticket.file !== undefined) ? $scope.ticket.file.name : "";
    });
}]);