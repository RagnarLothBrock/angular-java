'use strict'

app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {

		$stateProvider
			.state('rl.role-management', {
				url: '/role-management',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/role_management/list.tpl.html'
					}
				},
				data: {
					page: "Role Management",
					layout : "site",
					requireLogin : true
				}
			});
    }]);
app.controller("RoleManagerController", ['$scope', function($scope){
	$scope.breadcrumbs = [
			{
				title: "Settings",
				link: "/role-management"
			}
		];
}]);