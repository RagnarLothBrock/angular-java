'use strict'

app.factory('FETRACK', ["$http", "REST_API_URL", "API", "Upload", function ($http, REST_API_URL, API, Upload) {
    return {

        list: function (params) {
            return $http.post(REST_API_URL + API.USER.LIST, params);
        },
        search_fe: function (params) {
            return $http.post(REST_API_URL + API.FE.TRACK_FE, params);
        },
        search_Each_fe: function (params) {
            return $http.post(REST_API_URL + API.FE.TRACK_EACH_FE, params);
        },
        start_stop_fe_tracking: function (params) {
            return $http.post(REST_API_URL + API.FE.START_FE_TRACK, params);

        },
        get_all_fe: function (params) {
            return $http.post(REST_API_URL + API.FE.GET_ALL_FE, params);

        },
        call_fe: function (params) {
            return $http.post(REST_API_URL + API.FE.CALL_FE, params);

        },
        fe_details: function (params) {
            return $http.post(REST_API_URL + API.FE.FE_DETAILS, params);
        },
        map_view: function (params) {
            return $http.post(REST_API_URL + API.FE.MAP_DETAILS, params);
        }
    }
}]);

app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
    function ($stateProvider, $urlRouterProvider, $httpProvider) {
        $stateProvider
            .state('rl.fe-track', {
                url: '/fe/track',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/fe_track/show_field_engg.tpl.html'
                    }
                },
                controller: 'FEController',
                data: {
                    page: "TRACK FE",
                    requireLogin: true,
                },
                params: {}
            })
            .state('rl.fe-track-details', {
                url: '/fe/track/details',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/fe_track/total_distance.tpl.html'
                    }
                },
                controller: 'FEController',
                data: {
                    page: "FE DETAILS",
                    requireLogin: true,
                },
                params: {}
            })
            .state('rl.fe-track-map', {
                url: '/fe/track/map',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/fe_track/map_view.tpl.html'
                    }
                },
                controller: 'FEController',
                data: {
                    page: "TRACK FE MAP",
                    requiredLogin: true,
                },
                params: {
                    feTrack: null,
                }


            });

    }
]);


app.controller('FEController',
    ['$scope', '$rootScope', '$state', 'notification', 'FETRACK', 'Utility', 'ngDialog', 'LIST_PAGE_SIZE', '$http', '$stateParams', '$cookieStore',
        function ($scope, $rootScope, $state, notification, FETRACK, Utility, ngDialog, LIST_PAGE_SIZE, $http, $stateParams, $interval, $cookieStore, TasksController) {
            console.log(TasksController);
            $scope.URL = host_base_url;
            //console.log($rootScope.loginInfo);
            //console.log($scope.loginInfo.number);
            $scope.pageSize = LIST_PAGE_SIZE;
            $scope.total_count = 0;
            $scope.currentPage = 1;
            $scope.feList = [];
            // $scope.feName = [];

            $scope.breadcrumbs = [
                {
                    title: "FE Track",
                    link: "/fe/track"
                }
            ];
            console.log("insdideous");
            if ($scope.loginInfo.roleCode === 'admin') {
                Utility.populate_location({}, $scope);
            }
            $scope.displayName = function (firstName, lastName) {
                var fullName = "";
                if (lastName !== null) {
                    fullName = firstName + " " + lastName;
                    return fullName;
                }
                return firstName;
            }

            $scope.getFEList = function (roleCode, location) {
                $scope.feList = [];
                $scope.total_count = 0;
                FETRACK.list({
                    'query': {roleCode: roleCode, locationId: location},
                    firstResult: 0,
                    maxResults: 100
                }).success(function (response) {
                    //$scope.users = response;
                    if (response !== undefined && response.data !== undefined && response.data.length > 0) {
                        $scope.feList = response.data;
                        $scope.total_count = response.totalRecords;
                    }
                });
            }
            $scope.fetrack = {};
            $scope.pagination = {};

            //PaginationLogic
            $scope.fepage = function (first, last) {
                $scope.currentPage = first;// initialize page no to 1

                first = $rootScope.calculateFirstRecord($scope.currentPage, last);
                if ($scope.loginInfo.roleCode === "coordinator") {
                    FETRACK.get_all_fe({
                        'Location': $scope.loginInfo.locationId,
                        firstResult: first,
                        maxResults: last
                    }).success(function (response) {
                        $scope.fetrack = response.data;
                        $scope.total_count = response.totalRecords;


                    });


                }


            }

            if ($state.current.url === "/fe/track") {
                console.log($scope.loginInfo);
                $scope.fepage(1, 10);
                // if ($scope.loginInfo.roleCode === "coordinator") {
                // FETRACK.get_all_fe({'Location' : $scope.loginInfo.locationId,firstResult : 0,maxResults : 100 }).success(function (response) {
                // $scope.fetrack = response.data;


                //});


                //}
                //if ($scope.loginInfo.roleCode === "admin") {
                // loc = "";
                //$scope.getFEList("field_engineer");
                //}

            }
            if ($state.current.url === "/fe/track/details") {
                if ($scope.loginInfo.roleCode === "coordinator") {
                    $scope.getFEList("field_engineer", $scope.loginInfo.locationId);
                }

            }

            $scope.locationData;

            $scope.clat;
            $scope.clong;
            $scope.testdata = [12.9613604, 77.59058569999999];
            $scope.positionbygps = [];
            if ($state.current.url === "/fe/track/map") {
                //TODO - fetch browsers geo location
                var options = {
                    enableHighAccuracy: true
                };
                $scope.pos1 = navigator.geolocation.getCurrentPosition(function (pos) {
                        $scope.position1 = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
                        $scope.clat = pos.coords.latitude;
                        $scope.clong = pos.coords.longitude;
                        $scope.positionbygps = [$scope.clat, $scope.clong];
                    },
                    function (error) {
                        console.log('Unable to get location: ' + error.message);
                    }, options);

                console.log($scope.clat);
                console.log($scope.clong);
                if($scope.clat !=null && $scope.clat != undefined){
                    $scope.testdata = $scope.positionbygps;
                }


                $scope.breadcrumbs = [{
                    title: 'MAP',
                    link: '/fe/track/map'

                }];
                $scope.theWaypoints = []
                $scope.origin = []
                $scope.dest = []
                //console.log($rootScope, $scope, $state);
                //getting the date from system
                var formatedDate = customformatDate(new Date());
                FETRACK.map_view({"location": $scope.loginInfo.locationName,"date":formatedDate}).success(function (response) {
                    if (response != undefined && response.success == true) {

                        $scope.stopLoc = [];
                        $scope.locationData = response.response;
                        if($scope.locationData.length > 0 && $scope.clat === undefined){
                            $scope.testdata = [$scope.locationData[0].latitude,$scope.locationData[0].longitude]
                        }
                        // var l = $scope.locationData.length-1;
                        // $scope.origin=[parseFloat($scope.locationData[0].latitude),parseFloat($scope.locationData[0].longitude)];
                        // $scope.dest =[$scope.locationData[l].latitude,$scope.locationData[l].longitude];
                        //
                        // if($scope.locationData.length > 0){
                        //     for (var i = 1; i < $scope.locationData.length - 1; i++ ){
                        //         var obj = {
                        //             location:{
                        //                 lat:parseFloat($scope.locationData[i].latitude),
                        //                 lng:parseFloat($scope.locationData[i].longitude)
                        //             },
                        //             stopover: true
                        //         };
                        //         $scope.theWaypoints.push(obj);
                        //     }
                        // }
                    }

                });


            }

            //connection to API


            $scope.feTracker = null;
            $scope.trackingFe = function (feTracker, first, limit) {
                console.log("inside Search feTracker", feTracker);
                $scope.feTracker = feTracker;
                //var feTracker = feTracker;
                var goForLocation = 0;
                var today = new Date();
                var today1 = today + "";
                var splitDate = today1.split(" ", 3);
                var spli = splitDate + "";
                var res = spli.replace(",", " ");
                var resdate = res.replace(",", " ");
                console.log("today : " + resdate);
                feTracker.appointmentDate = resdate;
                // Mon Feb 05 10:58:00 UTC 2018

                $scope.currentPage = first;// initialize page no to 1

                first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
                if (feTracker === undefined) {
                    console.log("inside if feTracker");
                    return;
                }
                if ((feTracker.feName !== null && feTracker.feName !== undefined)) {
                    console.log("inside your changes");
                    goForLocation = 1;
                    feTracker.processId = '121212';
                    feTracker.destination = 'new destination';
                    FETRACK.search_Each_fe({
                        processId: '12121245',
                        feName: feTracker.feName,
                        destination: 'new Desti',
                        firstResult: first,
                        maxResults: 10
                    }).success(function (response) {
                        if (response !== undefined && response.data !== undefined && response.data.length > 0) {
                            $scope.feTrackers = response.data;
                            console.log("$scope.feTracker : " + $scope.feTracker);
                            $scope.total_count = response.totalRecords;
                            feTracker = feTracker;
                        }
                    });
                    return;
                }
                if (isEmpty(feTracker.feCity, 'feCity') &&
                    isEmpty(feTracker.feName, 'feName')) {
                    return;
                }
                console.log(feTracker);

                //{ 'feCity':feTracker.feCity,'feName':feTrackerfeName,'appointmentDate':feTracker.appointmentDate}
                first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
                if (goForLocation === 0) {
                    FETRACK.search_fe({
                        feCity: feTracker.feCity,
                        feName: "abc",
                        appointmentDate: feTracker.appointmentDate,
                        firstResult: first,
                        maxResults: 10
                    }).success(function (response) {
                        if (response !== undefined && response.data !== undefined && response.data.length > 0) {
                            $scope.feTrackers = response.data;
                            console.log("$scope.feTracker : " + $scope.feTracker);
                            $scope.total_count = response.totalRecords;
                        }
                    });
                }
            }
            $scope.map = function (feTracker) {
                //console.log(feTracker);
                $state.go('rl.fe-track-map', {feTrack: feTracker});
                //console.log(feTracker);
            }

            $scope.changeAppoint = function (date) {
                console.log("date :" + date);
                $scope.newString = date.replace("UTC", "IST");
                // var str = date.substring(0,date.length-9);
                // var newDate =new Date(str);
                console.log("Inside StringDate :" + $scope.newString);
                return $scope.newString;
            }

            $scope.clearSearchFields = function () {
                $scope.feTrackers = [];
                $scope.fetrack = {};
                $scope.fedetails = {};
                $scope.fetrackresult = {};
            };


            $scope.start = 'start';
            $scope.stop = 'stop';
            $scope.button = {};
            $scope.startstatus = true;
            $scope.stopfalse = false;
            $scope.button.stop = false;
            $scope.startTrack = function (email, status, index) {
                if ($scope.fetrack[index].status == 0) {
                    $scope.fetrack[index].status = 1;
                } else if ($scope.fetrack[index].status == 1) {
                    $scope.fetrack[index].status = 0;
                }
                console.log($scope.fetrack);
                if (status === "start") {
                    $scope.button.start = false;
                    $scope.button.stop = true;
                }
                FETRACK.start_stop_fe_tracking({
                    'userId': email,
                    'status': status,
                    'location': '5078'
                }).success(function (response) {
                    if (response.success === true) {
                        notification.success(response.message);
                    }
                    if (response.success === false) {
                        notification.error("Something went wrong! Contact Tech-Support");
                    }

                })

            }


// CALL TO FE
            $scope.callFe = function (feNumber) {
                console.log($scope.loginInfo);
                FETRACK.call_fe({
                    'assigneeNumber': $scope.loginInfo.phone,
                    'feNumber': feNumber
                }).success(function (response) {
                    if (response.success === true) {
                        notification.success(response.message);
                    } else {
                        notification.error(response.message);
                    }
                })


            }
            $scope.fedata = {};

            $scope.viewmap = function (fetracker) {
                $state.go('rl.fe-track-map', {feTrack: fetracker});
                $scope.fedata = fetracker;
                console.log($state.current.data);
                console.log($scope.fedata);

            }
            $scope.fedetails = {};
            $scope.fetrackresult = [];
            $scope.datePicker = {};
            //$scope.datePicker.date = {startDate: null, endDate: null};
            $scope.datePicker.date = {}
            $scope.datePicker.date.startDate = null;
            $scope.datePicker.date.endDate = null;

            $scope.fetrackdetails = function (fedetails, first, last) {
                //var formatedDate = yyyyMMdd(new Date(fedetails.myDate));
                console.log($scope.datePicker);
                if ($scope.datePicker.date.startDate != null && $scope.datePicker.date.startDate != undefined) {
                    var formatedStartDate = customformatDate(new Date($scope.datePicker.date.startDate));
                }
                if ($scope.datePicker.date.endDate != null && $scope.datePicker.date.endDate != undefined) {
                    var formatedEndDate = customformatDate(new Date($scope.datePicker.date.endDate));
                }
                //var date = formatedDate.split("-").join("/");
                FETRACK.fe_details({"location": fedetails.feCity, "startDate": formatedStartDate,"endDate":formatedEndDate}).success(function (response) {
                    if (response != null && response != undefined) {
                        $scope.fetrackresult = response.response;
                        console.log($scope.fetrackresult);
                        // $scope.excel();
                    }
                    // else {
                    //     $scope.fetrackresult = null;
                    // }


                })
            };

            $scope.excel = function () {
                $scope.fileName = "report";
                $scope.exportData = [];
                // Headers:
                $scope.exportData.push(["Name", "Location", "Date", "Total Distance"]);
                // Data:
                angular.forEach($scope.fetrackresult, function (value, key) {
                    $scope.exportData.push([value.feName, value.locationName, value.date, value.distance]);
                })


                ;
            };


//

            $scope.generateExcell = function (jsonresult) {
                console.log("aaabbbccc");

                if (jsonresult != null && jsonresult.length != 0) {
                    $scope.fileName = "report";
                    $scope.exportData = [];
                    // Headers:
                    $scope.exportData.push(["Name", "Location", "Total Distance"]);
                    // Data:
                    angular.forEach(jsonresult, function (value, key) {
                        $scope.exportData.push([value.feId, value.location, value.distance]);
                    })


                    ;


                }


                //FETRACK.fe_details({'location':fedetails.feCity},'date': )

            };

            $scope.c = function (s) {
                console.log(s);
            }


        }
    ])
;


























