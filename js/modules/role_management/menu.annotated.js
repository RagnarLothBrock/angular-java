'use strict'

app.factory('MenuService', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.ROLE_MANAGEMENT.MENU.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.ROLE_MANAGEMENT.MENU.CREATE, {'menu':params});
		},
		get: function(params) {
			var getUrl = API.ROLE_MANAGEMENT.MENU.GET.replace('<menuId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.ROLE_MANAGEMENT.MENU.UPDATE, {'menu':params});
		},
		del: function(params) {
			var getUrl = API.ROLE_MANAGEMENT.MENU.DELETE.replace('<menuId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		}
	}
}]);


app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {
		$stateProvider
			.state('rl.role-management-menu', {
				url: '/role-management/menu',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/role_management/menu/list.tpl.html'
					}
				},
				controller: 'MenuController',
				data: {
					page: "Role Management - Menu",
					requireLogin : true
				}
			})
			.state('rl.role-management-menu-create', {
				url: '/role-management/menu/create',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/role_management/menu/save.tpl.html'
					}
				},
				controller: 'MenuController',
				data: {
					page: "Role Management - Menu : Create",
					requireLogin : true
				}
			})
			.state('rl.role-management-menu-get', {
				url: '/role-management/menu/get/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/role_management/menu/save.tpl.html'
					}
				},
				controller: 'MenuController',
				data: {
					page: "Role Management - Menu : Update",
					requireLogin : true
				}
			});
    }]);


app.controller('MenuController',
	['$scope', '$rootScope', '$state', 'notification',  'ngDialog', '$location', 'MenuService', 'LIST_PAGE_SIZE',
    function($scope, $rootScope, $state,  notification, ngDialog, $location, MenuService, LIST_PAGE_SIZE) {
		
		//$rootScope.checkAccess();
		
        $scope.notification = $rootScope.notification;        
        $scope.menus = [];
        $scope.pageActionHeader = "";
		$scope.errors = {};
		$scope.menu = {};
		
		$scope.pageSize = LIST_PAGE_SIZE;
		$scope.total_count = 0;
		$scope.currentPage = 1;
		
		$scope.breadcrumbs = [
			{
				title: "Admin"
			},
			{
				title: "Settings"
			},
			{
				title: "Menu",
				url: '/role-management/menu'
			}
		];
		
		$scope.menuList = function(first, limit) {
			$scope.currentPage = first // initialize page no to 1
			
			first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
			MenuService.list({'query':{}, firstResult: first, maxResults: limit}).success(function(response) {
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.menus = response.data;
					$scope.total_count = response.totalRecords;
				}
			});
		}
        
        if ($state.current.url == "/role-management/menu") {
			
			$scope.showAddNew = true;
			$scope.addNewUrl = "/role-management/menu/create";
			$scope.addNewState = "rl.role-management-menu-create";
			$scope.pageActionHeader = "Create new menu";
			
			$scope.menuList(1, $scope.pageSize)
		}
		
		if ($state.current.url == "/role-management/menu/create") {
			$scope.pageActionHeader = "Create new menu";
			$scope.breadcrumbs.push({title:"Create"});
		}
		
		if ($state.current.url == "/role-management/menu/get/:id") {
			$scope.pageActionHeader = "Update Menu Details";
			$scope.menu.id = $state.current.data.params.id;
			$scope.breadcrumbs.push({title:"Update"});
			MenuService.get($scope.menu).success(function(response) {
				$scope.menu = response;
			});
		}
		
		//Save Menu Details
		$scope.save = function(menu) {
			$scope.errors = {};
			//Validate Menu
			if(hasErrors(menu)) return false;
			
			if (menu.id == undefined || menu.id == "" || menu.id == null) {
                //Continue with menu create
				MenuService.create(menu).success(function(response) {
					console.log(response);
					$state.go('rl.role-management-menu');
				});
            }
			else {
				//Continue with menu Update
				MenuService.update(menu).success(function(response) {
					console.log(response);
					$state.go('rl.role-management-menu');
				});
			}
		}
		
		//Delete Existing Menu
		$scope.deleteMenu = function(menu, index) {
			ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete this menu?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				console.log(menu,index);
				MenuService.del(menu).success(function(response) {
					if (response) {
                        $state.go('rl.role-management-menu');
                    }
				});
			}, function (error) {
				// Error logic here
			});
			
		}
		
		//Validate Menu Details
		var hasErrors = function(menu) {
			if (isEmpty(menu.name,'name')) {
                $scope.errors.name = "Enter menu name";
            }
			if (isEmpty(menu.code,'code')) {
                $scope.errors.code = "Enter menu code";
            }
			if (isEmpty(menu.order,'order')) {
                $scope.errors.order = "Enter menu order";
            }
			if (isEmpty(menu.link,'link')) {
                $scope.errors.link = "Enter menu link";
            }
			
			if (Object.getOwnPropertyNames($scope.errors).length > 0) {
				return true;
			}
			return false;
		}
    }]);