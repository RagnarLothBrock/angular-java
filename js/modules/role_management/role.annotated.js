'use strict'

app.factory('RoleMasterService', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.ROLE_MANAGEMENT.ROLE.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.ROLE_MANAGEMENT.ROLE.CREATE, {'roleMaster':params});
		},
		get: function(params) {
			var getUrl = API.ROLE_MANAGEMENT.ROLE.GET.replace('<roleId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.ROLE_MANAGEMENT.ROLE.UPDATE, {'roleMaster':params});
		},
		del: function(params) {
			var getUrl = API.ROLE_MANAGEMENT.ROLE.DELETE.replace('<roleId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		}
	}
}]);


app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {
		$stateProvider
			.state('rl.roles', {
				url: '/roles',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/role_management/role/list.tpl.html'
					}
				},
				controller: 'RoleMasterController',
				data: {
					page: "Roles",
					requireLogin : true
				}
			})
			.state('rl.role-create', {
				url: '/role/create',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/role_management/role/save.tpl.html'
					}
				},
				controller: 'RoleController',
				data: {
					page: "Role : Create",
					requireLogin : true
				}
			})
			.state('rl.role-get', {
				url: '/role/get/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/role_management/role/save.tpl.html'
					}
				},
				controller: 'RoleController',
				data: {
					page: "Role : Update",
					requireLogin : true
				}
			});
    }]);


app.controller('RoleController',
	['$scope', '$rootScope', '$state', 'notification', 'AuthTokenFactory', 'ngDialog', '$location', 'logger', 'RoleMasterService',
    function($scope, $rootScope, $state,  notification, AuthTokenFactory, ngDialog, $location, logger, RoleMasterService) {
		
		//$rootScope.checkAccess();
		
        $scope.notification = $rootScope.notification;        
        $scope.roles = [];
        $scope.pageActionHeader = "";
		$scope.errors = {};
		$scope.role = {};
		
		$scope.breadcrumbs = [
			{
				title: "Admin"
			},
			{
				title: "Settings"
			},
			{
				title: "Roles",
				url: '/roles'
			}
		];
        
        if ($state.current.url == "/roles") {
			RoleMasterService.list({'query':{}, maxResults:1000, firstResult:0}).success(function(response) {
				$scope.roles = response.data;
			});
		}
		
		if ($state.current.url == "/role/create") {
			$scope.pageActionHeader = "Create new role";
			
			$scope.showAddNew = true;
			$scope.addNewUrl = "/role/create";
			$scope.addNewState = "rl.role-create";
			$scope.pageActionHeader = "Create new role";
		}
		
		if ($state.current.url == "/role/get/:id") {
			$scope.pageActionHeader = "Update Role Master Details";
			$scope.role.id = $state.current.data.params.id;
			RoleMasterService.get($scope.role).success(function(response) {
				$scope.role = response;
			});
		}
		
		//Save Role Master Details
		$scope.save = function(role) {
			$scope.errors = {};
			//Validate RoleMaster
			if(hasErrors(role)) return false;
			
			if (role.id == undefined || role.id == "" || role.id == null) {
                //Continue with role create
				RoleMasterService.create(role).success(function(response) {
					console.log(response);
					$state.go('rl.roles');
				});
            }
			else {
				//Continue with role Update
				RoleMasterService.update(role).success(function(response) {
					console.log(response);
					$state.go('rl.roles');
				});
			}
		}
		
		//Delete Existing RoleMaster
		$scope.deleteRole = function(role, index) {
			ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete this role?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				console.log(role,index);
				RoleMasterService.del(role).success(function(response) {
					if (response) {
                        $state.go('rl.roles');
                    }
				});
			}, function (error) {
				// Error logic here
			});
			
		}
		
		//Validate Role Master Details
		var hasErrors = function(role) {
			if (isEmpty(role.name,'name',"Enter role name",$scope.notification)) {
                $scope.errors.name = "Enter role name";
            }
			
			if (Object.getOwnPropertyNames($scope.errors).length > 0) {
				return true;
			}
			return false;
		}
    }]);