'use strict'

app.factory('FeatureService', function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			var getUrl = API.ROLE_MANAGEMENT.FEATURE.LIST.replace('<first>', params.firstResult).replace('<limit>', params.maxResults);
			return $http.post(REST_API_URL + getUrl, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.ROLE_MANAGEMENT.FEATURE.CREATE, {'feature':params});
		},
		get: function(params) {
			var getUrl = API.ROLE_MANAGEMENT.FEATURE.GET.replace('<featureId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.ROLE_MANAGEMENT.FEATURE.UPDATE, {'feature':params});
		},
		del: function(params) {
			var getUrl = API.ROLE_MANAGEMENT.FEATURE.DELETE.replace('<featureId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		}
	}
});


app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {
		$stateProvider
			.state('rl.role-management-features', {
				url: '/role-management/features',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/role_management/feature/list.tpl.html'
					}
				},
				controller: 'FeatureController',
				data: {
					page: "Role Management - Feature",
					requireLogin : true
				}
			})
			.state('rl.role-management-feature-create', {
				url: '/role-management/feature/create',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/role_management/feature/save.tpl.html'
					}
				},
				controller: 'FeatureController',
				data: {
					page: "Role Management - Feature : Create",
					requireLogin : true
				}
			})
			.state('rl.role-management-feature-get', {
				url: '/role-management/feature/get/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/role_management/feature/save.tpl.html'
					}
				},
				controller: 'FeatureController',
				data: {
					page: "Role Management - Feature : Update",
					requireLogin : true
				}
			});
    }]);


app.controller('FeatureController',
	['$scope', '$rootScope', '$state', 'notification', 'ngDialog', '$location', 'logger', 'FeatureService','Utility', 'LIST_PAGE_SIZE', 'MAX_DROPDOWN_SIZE',
    function($scope, $rootScope, $state,  notification,  ngDialog, $location, logger, FeatureService, Utility, LIST_PAGE_SIZE, MAX_DROPDOWN_SIZE) {
		
		$scope.breadcrumbs = [
			{
				title: "Settings",
				link: "/role-management"
			},
			{
				title: "Features",
				link : "/role-management/features"
			}
		];
		
        $scope.notification = $rootScope.notification;        
        $scope.features = [];
        $scope.pageActionHeader = "";
		$scope.errors = {};
		$scope.feature = {};
		$scope.menus = [];
		
		$scope.pageSize = 10000;
		$scope.total_count = 0;
		$scope.currentPage = 1;
		
		$scope.featureList = function(first, limit) {
			$scope.currentPage = first // initialize page no to 1
			
			first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
			FeatureService.list({'query':{}, maxResults:limit, firstResult: first}).success(function(response) {
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.features = response.data;
					$scope.total_count = response.totalRecords;
				}
			});
		}
		
		$scope.populateFeatureDDl = function() {
			FeatureService.list({'query':{}, maxResults:MAX_DROPDOWN_SIZE, firstResult: 0}).success(function(response) {
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.features = response.data;
				}
			});
		}
        
        if ($state.current.url == "/role-management/features") {
			$scope.showAddNew = true;
			$scope.addNewUrl = "/role-management/feature/create";
			$scope.addNewState = "rl.role-management-feature-create"
			$scope.featureList(1, $scope.pageSize);
		}
		
		if ($state.current.url == "/role-management/feature/create") {
			$scope.breadcrumbs.push({
				title: "Create New"
			});
			$scope.pageActionHeader = "Create new feature";
		}
		
		if ($state.current.url == "/role-management/feature/get/:id") {
			$scope.breadcrumbs.push({
				title: "Update"
			});
			$scope.pageActionHeader = "Update Feature Details";
			$scope.feature.id = $state.current.data.params.id;
			FeatureService.get($scope.feature).success(function(response) {
				$scope.feature = response;
			});
		}
		
		if ($state.current.url == "/role-management/feature/create" ||
			$state.current.url == "/role-management/feature/get/:id") {
			
			Utility.populate_menu({}, $scope);
			$scope.populateFeatureDDl();
		}
		
		//Save Feature Details
		$scope.save = function(feature) {
			$scope.errors = {};
			//Validate Feature
			if(hasErrors(feature)) return false;
			
			if (feature.id == undefined || feature.id == "" || feature.id == null) {
                //Continue with feature create
				FeatureService.create(feature).success(function(response) {
					console.log(response);
					$state.go('rl.role-management-features');
				});
            }
			else {
				//Continue with feature Update
				FeatureService.update(feature).success(function(response) {
					console.log(response);
					$state.go('rl.role-management-features');
				});
			}
		}
		
		//Delete Existing Feature
		$scope.deleteFeature = function(feature, index) {
			ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete this feature?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				console.log(feature,index);
				FeatureService.del(feature).success(function(response) {
					if (response) {
                        $state.go('rl.role-management-feature');
                    }
				});
			}, function (error) {
				// Error logic here
			});
			
		}
		
		//Validate Feature Details
		var hasErrors = function(feature) {
			if (isEmpty(feature.name,'name')) {
                $scope.errors.name = "Enter feature name";
            }
			if (isEmpty(feature.code,'code')) {
                $scope.errors.code = "Enter feature code";
            }
			if (isEmpty(feature.order,'order')) {
                $scope.errors.order = "Enter feature order";
            }
//			if (isEmpty(feature.menuId,'menuId')) {
//                $scope.errors.menuId = "Select menu";
//            }
//			if (isEmpty(feature.link,'link')) {
//                $scope.errors.link = "Enter feature link";
//            }
			
			if (Object.getOwnPropertyNames($scope.errors).length > 0) {
				return true;
			}
			return false;
		}
    }]);