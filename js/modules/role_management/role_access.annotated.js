'use strict'

app.factory('RoleAccessService', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			var getUrl = API.ROLE_MANAGEMENT.ROLE_ACCESS.LIST.replace('<roleCode>', params.roleCode);
			return $http.post(REST_API_URL + getUrl, {"query":{"orderBy":["name"]}});
		},
		bulkdelete: function(params) {
			var getUrl = API.ROLE_MANAGEMENT.ROLE_ACCESS.BULK_DELETE.replace('<roleCode>', params.roleCode);
			return $http.post(REST_API_URL + getUrl, {});
		},
        bulkcreate:function(params) {	
			return $http.post(REST_API_URL + API.ROLE_MANAGEMENT.ROLE_ACCESS.BULK_CREATE, {'roleAccessList':params});
		},
	}
}]);

app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {
		$stateProvider
			.state('rl.role-management-role-access', {
				url: '/role-management/role-access',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/role_management/role_access/list.tpl.html'
					}
				},
				controller: 'RoleAccessController',
				data: {
					page: "Role Management - Role Access",
					requireLogin : true
				}
			});
    }]);

app.controller('RoleAccessController',
	['$scope', '$rootScope', '$state', 'notification', 'AuthTokenFactory', 'ngDialog', '$location', 'logger', 'RoleAccessService','Utility', 'RoleMasterService',
    function($scope, $rootScope, $state,  notification, AuthTokenFactory, ngDialog, $location, logger, RoleAccessService, Utility, RoleMasterService) {
		
	$scope.breadcrumbs = [
		{
			title: "Settings",
			link: "/role-management"
		},
		{
			title: "Role Access Settings",
			link : "/role-management/role-access"
		}
	];
    
    $scope.roles = [];
    $scope.roleAccessList = [];
    $scope.roleAccess = {};
    $scope.saveRoleAccessList = [];
	
	Utility.populate_roles({}, $scope);
    
    $scope.populateAccess = function(roleAccess) {
		
        $scope.roleAccessList = [];
        $scope.saveRoleAccessList = [];
        RoleAccessService
            .list(roleAccess)
            .success(function(response) {
                $scope.roleAccessList = response;
                for(var i = 0; i < $scope.roleAccessList.length; i++ ) {
                    if ($scope.saveRoleAccessList[i] == undefined) {
                        $scope.saveRoleAccessList[i] = {
                            roleCode : $scope.roleAccess.roleCode,
                            featureId : $scope.roleAccessList[i].featureId
                        };
                    }
                    $scope.saveRoleAccessList[i].access = ($scope.roleAccessList[i].access==null) ? 'n' : $scope.roleAccessList[i].access;
                }
            });
    }
    
    $scope.save = function(roleAccess) {
        console.log(roleAccess);
        //Delete by role code
        RoleAccessService
            .bulkdelete($scope.roleAccess)
            .success(function(response){
				//Bulk Insert records
				RoleAccessService
					.bulkcreate($scope.saveRoleAccessList)
					.success(function(response){
					});
            });
    }
    
    $("input[type='radio'][name='selectAll']").click(function() {        
        for(var i = 0; i < $scope.roleAccessList.length; i++ ) {
            if ($scope.saveRoleAccessList[i] == undefined) {
                $scope.saveRoleAccessList[i] = {
                    roleCode : $scope.roleAccess.roleCode,
                    featureId : $scope.roleAccessList[i].featureId
                };
            }
            $scope.saveRoleAccessList[i].access = $(this).val();
        }
    });
}]);