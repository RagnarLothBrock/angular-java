'use strict'

app.factory('PackagingMaterialMaster', function ($http, REST_API_URL, API) {

	return {
		list: function (params, first, limit) {
			var getUrl = API.INVENTORY.PACKAGING_MATERIAL.MASTER.LIST
				.replace('<firstResult>', first)
				.replace('<maxResults>', limit);
			return $http.post(REST_API_URL + getUrl, params);
			//return $http.post(REST_API_URL + API.INVENTORY.PACKAGING_MATERIAL.MASTER.LIST, params);
		},
		create: function (params) {
			return $http.post(REST_API_URL + API.INVENTORY.PACKAGING_MATERIAL.MASTER.CREATE, { 'packagingMaterialMaster': params });
		},
		get: function (params) {
			var getUrl = API.INVENTORY.PACKAGING_MATERIAL.MASTER.GET.replace('<packagingMaterialMasterId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function (params) {
			return $http.post(REST_API_URL + API.INVENTORY.PACKAGING_MATERIAL.MASTER.UPDATE, { 'query': params });
		},
		del: function (params) {
			var getUrl = API.INVENTORY.PACKAGING_MATERIAL.MASTER.DELETE.replace('<packagingMaterialMasterId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		}
	}
});

app.factory('PackagingMaterialInventory', function ($http, REST_API_URL, API, Upload) {

	return {
		list: function (params, pageNo, maxResults) {
			var getUrl = API.INVENTORY.PACKAGING_MATERIAL.INVENTORY.LIST
				.replace('<maxResults>', maxResults)
				.replace('<pageNo>', pageNo);
			return $http.post(REST_API_URL + getUrl, params);
		},

		uploadIMEI: function (params) {
			console.log(params);
			return Upload.upload({
				url: REST_API_URL + API.INVENTORY.PACKAGING_MATERIAL.INVENTORY.UPLOAD_IMEI_EXCEL,
				data: params.data,
				file: params.file
			})
		},
		search_imei_by_variable: function (params) {
			var getUrl = API.INVENTORY.PACKAGING_MATERIAL.INVENTORY.SEARCH_IMEI_INVENTORY;
			return $http.post(REST_API_URL + getUrl, params);
		},
		imei_list: function (params) {
			var getUrl = API.INVENTORY.PACKAGING_MATERIAL.INVENTORY.IMEI_LIST;
			return $http.post(REST_API_URL + getUrl, params);
		},
		del_imei: function (params) {
			var getUrl = API.INVENTORY.PACKAGING_MATERIAL.INVENTORY.DELETE_IMEI.replace('<imeiInventoryId>', params);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update_imei: function (params) {
			var getUrl = API.INVENTORY.PACKAGING_MATERIAL.INVENTORY.UPDATE_IMEI;
			return $http.post(REST_API_URL + getUrl, params);
		},
		create: function (params) {
			return $http.post(REST_API_URL + API.INVENTORY.PACKAGING_MATERIAL.INVENTORY.CREATE, { 'packagingMaterialInventory': params });
		},
		get: function (params) {
			var getUrl = API.INVENTORY.PACKAGING_MATERIAL.INVENTORY.GET.replace('<packagingMaterialInventoryId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function (params) {
			return $http.post(REST_API_URL + API.INVENTORY.PACKAGING_MATERIAL.INVENTORY.UPDATE, { 'packagingMaterialInventory': params });
		},
		del: function (params) {
			var getUrl = API.INVENTORY.PACKAGING_MATERIAL.INVENTORY.DELETE.replace('<packagingMaterialInventoryId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		upload: function (params) {
			return Upload.upload({
				url: REST_API_URL + API.INVENTORY.PACKAGING_MATERIAL.INVENTORY.UPLOAD,
				data: params,
			});
		},
		status_update: function (params) {
			return $http.post(REST_API_URL + API.INVENTORY.PACKAGING_MATERIAL.INVENTORY.STATUS_UPDATE, { packagingMaterialInventory: params });
		},
		get_by_barcode: function (params) {
			var getUrl = API.INVENTORY.PACKAGING_MATERIAL.INVENTORY.GET_BY_BARCODE.replace('<barcode>', params.barcode);
			return $http.post(REST_API_URL + getUrl, {});
		},
		get_return_template: function () {
			return $http.get("partials/tpl/pages/inventory/packaging_material/return_item_dialog.tpl.html");
		}
	}
});

app.factory('BarcodeGeneration', function ($http, REST_API_URL, API) {
	return {
		generate_barcode: function (params) {
			return $http.post(REST_API_URL + API.INVENTORY.PACKAGING_MATERIAL.BARCODE_GENERATION.BULK_PRINT, { 'query': params });
		},
		list: function (params) {
			var getUrl = API.INVENTORY.PACKAGING_MATERIAL.BARCODE_GENERATION.LIST
				.replace('<firstResult>', params.firstResult)
				.replace('<maxResults>', params.maxResults);
			return $http.post(REST_API_URL + getUrl, params);
		}
	}
});

app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function ($stateProvider, $urlRouterProvider, $httpProvider) {
		$stateProvider
			.state('rl.inventory-packaging-material', {
				url: '/inventory/packaging-material',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/inventory/packaging_material/list.tpl.html'
					}
				},
				controller: 'PackagingMaterialController',
				data: {
					page: "Inventory - Packaging Material",
					requireLogin: true
				}
			})
			.state('rl.inventory-packaging-material-create', {
				url: '/inventory/packaging-material/create',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/inventory/packaging_material/save.tpl.html'
					}
				},
				controller: 'PackagingMaterialController',
				data: {
					page: "Inventory - Packaging Material : Create",
					requireLogin: true
				}
			})
			.state('rl.inventory-packaging-material-upload', {
				url: '/inventory/packaging-material/bulk-upload',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/inventory/packaging_material/upload.tpl.html'
					}
				},
				controller: 'PackagingMaterialController',
				data: {
					page: "Inventory - Packaging Material : Upload",
					requireLogin: true
				}
			})
			.state('rl.inventory-packaging-material-list', {
				url: '/inventory/packaging-material/list',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/inventory/packaging_material/barcode_list.tpl.html'
					}
				},
				controller: 'PackagingMaterialController',
				data: {
					page: "Inventory - Packaging Material : Upload",
					requireLogin: true
				}
			})
			.state('rl.inventory-packaging-material-item-view', {
				url: '/inventory/packaging-material/item-view/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/inventory/packaging_material/barcode_view.tpl.html'
					}
				},
				controller: 'PackagingMaterialController',
				data: {
					page: "Inventory - Packaging Material : View Item Details",
					requireLogin: true
				}
			})
			//Repair Stand-By Inventory Items
			.state('rl.inventory-repair-standby-item-view', {
				url: '/inventory/repair-standby/item-view',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/inventory/packaging_material/repair_standby_view.tpl.html'
					}
				},
				controller: 'PackagingMaterialController',
				data: {
					page: "Inventory - Repair StandBy : View Item Details",
					requireLogin: true
				}
			})

			//Repair Stand-By IMEI Excel Upload
			.state('rl.inventory-imei-upload', {
				url: '/inventory/repair-standby/upload',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/inventory/packaging_material/uploadImeiExcel.tpl.html'
					}
				},
				controller: 'PackagingMaterialController',
				data: {
					page: "Inventory - Repair StandBy : Upload Excel",
					requireLogin: true
				}
			})
			.state('rl.inventory-generate-barcode', {
				url: '/inventory/barcode/bulk-print',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/inventory/packaging_material/bulk_print.tpl.html'
					}
				},
				controller: 'PackagingMaterialController',
				data: {
					page: "Inventory - Bulk Print Barcodes",
					requireLogin: true
				}
			});
		//
	}]);

app.controller('PackagingMaterialController',
	['$scope', '$rootScope', '$state', 'notification', 'AuthTokenFactory', 'ngDialog', '$location', 'logger', '$document',
		'PackagingMaterialInventory', 'PackagingMaterialMaster', 'Utility', '$templateCache', 'BarcodeGeneration', '$window',
		'$http', 'LIST_PAGE_SIZE',
		function ($scope, $rootScope, $state, notification, AuthTokenFactory, ngDialog, $location, logger, $document,
			PackagingMaterialInventory, PackagingMaterialMaster, Utility, $templateCache, BarcodeGeneration, $window, $http,
			LIST_PAGE_SIZE) {

			$scope.pageSize = LIST_PAGE_SIZE;
			$scope.total_count = 0;
			$scope.currentPage = 1;
			$scope.showImeiResult = 0;
			$scope.breadcrumbs = [
				{
					title: "Packaging Material",
					link: "/inventory/packaging-material"
				}
			];
			$scope.options = {
				"width": 2,
				"height": 70,
				"displayValue": "RLASDTPC 13124235325345",
				"font": "monospace",
				"textAlign": "center",
				"fontSize": 14,
				"backgroundColor": "#fff",
				"lineColor": "#000"
			}
			$scope.showUpload = true;
			$scope.uploadState = 'rl.inventory-packaging-material-upload';
			$scope.uploadUrl = '/inventory/packaging-material/bulk-upload';

			$scope.notification = $rootScope.notification;
			$scope.packagingMaterials = [];
			$scope.pageActionHeader = "";
			$scope.errors = {};
			$scope.packagingMaterial = { locationId: "" };
			$scope.locations = [];
			$scope.packagingMaterialTypes = [];
			$scope.bulkPackagingMaterial = {};
			$scope.showControl = false;
			$scope.imeiInventory = {};

			$scope.print = function (image) {

				$http.get(host_base_url + '/partials/tpl/common/print_layout.tpl.html')
					.success(function (html) {
						var Pagelink = "about:blank";
						var pwa = window.open(Pagelink, "_new");
						pwa.document.open();
						pwa.document.write(html);
						pwa.document.close();
					});
			}

			$scope.getInventoryItemsList = function (packagingMaterial, pageNo, limit) {
				PackagingMaterialInventory
					.list({ query: packagingMaterial }, pageNo, limit)
					.success(function (response) {
						$scope.total_count = response.totalRecords;
						//angular.extend($scope.inventoryList, response.data);
						$scope.inventoryList = response.data;
					});
			}

			$scope.goToBarcodeNextPage = function (pageNo, pageSize) {
				$scope.currentPage = pageNo;
				$scope.pageSize = pageSize;
				$scope.searchBarcode($scope.packagingMaterial);
			}


			//Save Packaging Material Details
			$scope.create = function (packagingMaterial) {
				if ($scope.packagingMaterialForm.$invalid) {
					notification.error("There are some errors in data. Please correct it.");
					return;
				}
				PackagingMaterialInventory
					.create(packagingMaterial)
					.success(function (response) {
						//if (response === "") {
						//    notification.error("Error in saving data.");
						//    return;
						//}
						//notification.success("Successfully saved inventory details.");
						if (response.response === null) {
							return;
						}
						$state.go('rl.inventory-packaging-material');
					});
			}

			$scope.upload = function (bulkPackagingMaterial) {
				PackagingMaterialInventory.upload(bulkPackagingMaterial)
					.progress(function (evt) {
						var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
						$scope.uploadPercentage = progressPercentage;
					})
					.success(function (data, status, headers, config) {
						if (status == 200 && data.noOfRecordsInserted > 0) {
							notification.success("Successfully uploaded " + data.noOfRecordsInserted + " Packaging Material(s)!");
						}

						if (data.errorMessages.length > 0) {
							notification.error(data.errorMessages[0]);
						}
						$scope.bulkPackagingMaterial = { data: {}, file: {} };
					})
					.error(function (error) {
						var errorM = "";
						$(error.message).each(function () {
							errorM += this + "<br/>";
						});
						notification.error(errorM);
					});
			}

			$scope.$watch('bulkPackagingMaterial.file', function () {
				$scope.bulkPackagingMaterial.selectedFileName = ($scope.bulkPackagingMaterial.file !== undefined) ? $scope.bulkPackagingMaterial.file.name : "";
			});

			$scope.search = function (packagingMaterial, first, limit) {
				if($scope.loginInfo.roleCode==='hub_manager'){
					console.log("INside Search PAckagingn Material::::;");
					packagingMaterial.locationId= $scope.loginInfo.locationId;
					console.log("::::::::::::::"+$scope.loginInfo.locationId);
				}
				$scope.packagingMaterials = [];
				$scope.currentPage = first;// initialize page no to 1
				first = $scope.calculateFirstRecord($scope.currentPage, limit);
				PackagingMaterialMaster
					.list({ 'query': packagingMaterial }, first, limit)
					.success(function (response) {
						if (response !== undefined && response.data !== undefined && response.data.length > 0) {
							$scope.total_count = response.totalRecords;
							$scope.packagingMaterials = response.data;
						}
					});
			}

			$scope.clearSearchFields = function () {
				$scope.packagingMaterial = {};
				$scope.search($scope.packagingMaterial, 1, $scope.pageSize);
			}

			$scope.searchBarcode = function (packagingMaterial, pageNo, pageSize) {
				$scope.pageSize = 25;
				if (pageNo == undefined) {
					pageNo = 1;
				}
				if($scope.loginInfo.roleCode==='hub_manager'){
					console.log("INside Search Barcode Material::::;");
					packagingMaterial.locationId= $scope.loginInfo.locationId;
					console.log("::::::::::::::"+$scope.loginInfo.locationId);
				}

				$scope.currentPage = pageNo;
				$scope.inventoryList = [];
				if (packagingMaterial.locationId === "") {
					delete packagingMaterial.locationId;
				}
				if (packagingMaterial.barcode === "") {
					delete packagingMaterial.barcode;
				}
				$scope.getInventoryItemsList(packagingMaterial, pageNo, $scope.pageSize);
			}

			$scope.clearSearchFields = function () {
				$scope.inventoryList = [];
				$scope.packagingMaterial = {};
				$scope.searchBarcode($scope.packagingMaterial);
			}

			$scope.getBarcodeGenerationList = function (first, limit) {
				$scope.barcodeList = [];
				$scope.currentPage = first // initialize page no to 1			
				first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
				//limit = 2;
				BarcodeGeneration
					.list({ firstResult: first, maxResults: limit })
					.success(function (response) {
						if (response !== undefined && response.data !== undefined && response.data.length > 0) {
							$scope.barcodeList = response.data;
							$scope.total_count = response.totalRecords;
						}
					});
			}

			$scope.generateBarcodes = function (bardcodeGeneration) {
				BarcodeGeneration
					.generate_barcode(bardcodeGeneration)
					.success(function (response) {
						$scope.getBarcodeGenerationList(1, $scope.pageSize);
					});
			}

			$scope.printBarcodes = function (barcode) {
				//Call print status update API
			}

			if ($state.current.url == "/inventory/packaging-material" ||
				$state.current.url == "/inventory/packaging-material/list") {
				$scope.showAddNew = true;
				$scope.addNewState = 'rl.inventory-packaging-material-create';
				$scope.addNewUrl = '/inventory/packaging-material/create';
			}


			if ($state.current.url == "/inventory/packaging-material" ||
				$state.current.url == "/inventory/packaging-material/create" ||
				$state.current.url == "/inventory/packaging-material/get/:id" ||
				$state.current.url == "/inventory/packaging-material/list" ||
				$state.current.url == "/inventory/barcode/bulk-print"
			) {
				Utility.populate_location({}, $scope);
				Utility.populate_packging_material_type({}, $scope);
			}

			if ($state.current.url == "/inventory/repair-standby/item-view" && $scope.loginInfo.roleCode != 'retailer') {
				if($scope.loginInfo.roleCode === 'hub_manager'){
					Utility.populate_imei_inventory({location:$scope.loginInfo.locationName}, $scope);
				}
				if($scope.loginInfo.roleCode !== 'hub_manager'){
					Utility.populate_imei_inventory({}, $scope);
				}
				Utility.populate_retailer({}, $scope);
				$scope.showUpload = true;
				$scope.uploadUrl = "/inventory/repair-standby/upload";
				$scope.uploadState = "rl.inventory-imei-upload";
			}
			// Only For Particular Retailer
			if ($state.current.url == "/inventory/repair-standby/item-view" && $scope.loginInfo.roleCode == 'retailer') {
				var retailerName = $scope.loginInfo.retailerName;
				var retailerId = $scope.loginInfo.retailerId;
				console.log("retailerName : "+retailerName);
				console.log("retailerName : "+retailerId);

				Utility.populate_imei_inventory({retailerId:retailerId}, $scope);
				Utility.populate_retailer({}, $scope);
				// $scope.showUpload = true;
				// $scope.uploadUrl = "/inventory/repair-standby/upload";
				// $scope.uploadState = "rl.inventory-imei-upload";
			}

			if ($state.current.url == "/inventory/repair-standby/upload") {
				Utility.populate_retailer({}, $scope);
				
			}

			$scope.searchImei = function (imeiInventory, newPageNumber, pageSize) {
				console.log("tj");
				// if (imeiInventory === undefined) {
				// 	return;
				// }
				$scope.currentPage = newPageNumber // initialize page no to 1
				if($scope.loginInfo.retailerId != null){
					console.log("Inside Retailer Login :"+$scope.loginInfo.retailerId);
					imeiInventory.retailerId = $scope.loginInfo.retailerId;
				}

				newPageNumber = $rootScope.calculateFirstRecord($scope.currentPage, pageSize);
				if (isEmpty(imeiInventory.location, 'location') &&
					isEmpty(imeiInventory.brand, 'brand') &&
					isEmpty(imeiInventory.model, 'model') &&
					isEmpty(imeiInventory.year, 'year') &&
					isEmpty(imeiInventory.productCategory, 'productCategory') &&
					isEmpty(imeiInventory.productValue, 'productValue') &&
					isEmpty(imeiInventory.status, 'status') &&
					isEmpty(imeiInventory.retailerId, 'retailerId') &&
					isEmpty(imeiInventory.barcode, 'barcode') &&
					isEmpty(imeiInventory.imeiNo, 'imeiNo')) {
					console.log("inised if"+imeiInventory);
					newPageNumber = $rootScope.calculateFirstRecord($scope.currentPage, pageSize);
					PackagingMaterialInventory.imei_list({ query: {}, firstResult: newPageNumber, maxResults: 10 }).success(function (response) {
						if (response !== undefined && response.data !== undefined && response.data.length > 0) {
							$scope.imeiData = response.data;
							$scope.total_count = response.totalRecords;
						}


					});
					return;
				}


				var q = [];
				$scope.showImeiResult = 1;
					console.log("showImeiResult : "+$scope.showImeiResult);
				console.log("imeiInventory items :", imeiInventory, 'consumerName');
				// $scope.clearSearchFields();
				console.log("imeiInventory");
				// for (var key in imeiInventory) {
				// 	if (!isEmpty(imeiInventory[key], key) && imeiInventory[key].indexOf('%') === -1) {
				// 		var qItem = {
				// 			variable: key,
				// 			value: imeiInventory[key]
				// 		};
				// 		//					if (key == 'rlTicketNo') {
				// 		//                        qItem.value = task[key];
				// 		//                    }
				// 		q.push(qItem);
				// 	}
				// }
				PackagingMaterialInventory
					.search_imei_by_variable({
						'query': imeiInventory,
						'firstResult': newPageNumber,
						 'maxResults': 10
					})
					.success(function (response) {

						if (response !== undefined && response.data !== undefined && response.data.length > 0) {
							$scope.pages = 1;
							$scope.imeiData = response.data;
							$scope.total_count = response.totalRecords;
						}
					});
			}
			//This Method is called to delete the IMEI No.
			$scope.deleteImeiNo = function (imeiNo,index) {
				// console.log("inside 1st IMEI NO" + imeiNo+" : "+index);
				// if (imeiNo !== undefined && imeiNo !== null) {
				// 	$rootScope.$broadcast('deleteDialogAndConfirmEvent', "Are you sure want to delete this Record?", 'deleteSingleImeiNo', {imeiNo,index});
				// }

				//Instead Of Using BroadCast(it give some error) Using Template
				ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete Record?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				PackagingMaterialInventory.del_imei(imeiNo).success(function (response) {
					if (response !== undefined && response.response !== undefined &&
                        response.response !== null && response.response === true) {
                        $rootScope.$broadcast ('showSuccessMessage',response);
						$scope.imeiData.splice(index,1);
						}
				});
			}, function (error) {
				// Error logic here
			});
			}
			//Delete IMEI No. From Table.We r not using this one coz of some problem in $broadcast.
			// $scope.deleteSingleImeiNo = function (imeiNo) {
			// 	console.log("inisde 2nd IMEI Delete" + imeiNo.imeiNo+"index :"+imeiNo.index);

			// 	PackagingMaterialInventory.del_imei(imeiNo.imeiNo).success(function (response) {
			// 		if (response !== undefined && response.response !== undefined &&
            //             response.response !== null && response.response === true) {
            //             $rootScope.$broadcast ('showSuccessMessage',response);
			// 			$scope.imeiData.splice(imeiNo.imeiNo,(imeiNo.index+1));
			// 			}
			// 	});
			// }
			// $scope.imeiData = { data: {}, file: {} };

			//Making Damage From Available or Issued State
			$scope.makeDamage = function (imeiNo) {
				console.log("IMEINo : "+imeiNo);
				ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to change the Status to Damage?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				}]
			}).then(function (success) {
				PackagingMaterialInventory.update_imei({'query':{'imeiNo':imeiNo,'status':"2"}}).success(function (response) {
					if (response !== undefined && response.success === true) {
						console.log("Inside Damage");
                        $rootScope.$broadcast ('showSuccessMessage',response);
						}
				});
			}, function (error) {
				// Error logic here
			});
		}

		//Making Available from Damaged Status Of IMEI No.
		$scope.makeAvailable = function (imeiNo) {
				ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to change the Status to Available?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				}]
			}).then(function (success) {
				PackagingMaterialInventory.update_imei({'query':{'imeiNo':imeiNo,'status':"0"}}).success(function (response) {
					if (response !== undefined && response.success === true) {
						console.log("Inside Available");
                        $rootScope.$broadcast ('showSuccessMessage',response);
						}
				});
			}, function (error) {
				// Error logic here
			});
		}
		
			$scope.uploadIMEI = function (imeiData) {
				// if($scope.loginInfo.roleCode==="retailer") {
				// 	console.log("Retailer id: " + $scope.loginInfo.retailerId);
				// 	ticket.data.retailerId = $scope.loginInfo.retailerId;
				// }
				console.log("Imei File"+imeiData.file);
				console.log("Imei retailerId"+imeiData.data.retailerId);
				PackagingMaterialInventory.uploadIMEI(imeiData)
					.progress(function (evt) {
						var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
						console.log(progressPercentage);
					})
					.success(function (data, status, headers, config) {
						//                if (data !== undefined && data !== null && data !== "") {
						//                    
						//					if (status == 200 && data.noOfRecordsInserted > 0) {
						//						if (data.noOfRecordsInserted == 1) {
						//                            //code
						//                        }
						//						notification.success("Successfully uploaded " + data.noOfRecordsInserted + ( (data.noOfRecordsInserted == 1) ? " ticket!" : " tickets!") );
						//					}
						//					
						//					if (data.errorMessages.length > 0) {
						//						notification.error(data.errorMessages[0]);
						//					}
						//				}
						$rootScope.$broadcast('showUploadReturnMessage', { data: data, status: status, headers: headers, config: config });
						$scope.imeiData = { data: {}, file: {} };
					})
					.error(function (error) {
						var errorM = "";
						$(error.message).each(function () {
							errorM += this + "<br/>";
						});
						notification.error(errorM)
					});
			}

			if ($state.current.url == "/inventory/packaging-material/bulk-upload") {

				$scope.breadcrumbs.push(
					{
						title: "Bulk Upload",
						link: "/inventory/packaging-material/bulk-upload"
					});
				Utility.populate_location({}, $scope);
			}

			if ($state.current.url == "/inventory/barcode/bulk-print") {

				$scope.breadcrumbs.push(
					{
						title: "Bulk Print Barcodes",
						link: "/inventory/barcode/bulk-print"
					});

				$scope.getBarcodeGenerationList(1, $scope.pageSize);
			}

			if ($state.current.url == "/inventory/packaging-material") {
				$scope.search({}, 1, $scope.pageSize);
			}

			if ($state.current.url == "/inventory/packaging-material/create") {
				$scope.pageActionHeader = "Packaging Material - Create";
			}

			if ($state.current.url == "/inventory/packaging-material/get/:id") {
				$scope.pageActionHeader = "Packaging Material - Update";
			}

			if ($state.current.url == "/inventory/packaging-material/list") {
				$scope.breadcrumbs.push(
					{
						title: "Inventory Items List",
						link: "/inventory/packaging-material/list"
					});
				$scope.searchBarcode($scope.packagingMaterial, 1, $scope.pageSize);
			}

			if ($state.current.url == "/inventory/packaging-material/item-view/:id") {
				PackagingMaterialInventory
					.get({ id: $state.current.data.params.id })
					.success(function (response) {
						$scope.inventoryItem = response;
					});
			}

			$scope.savePackagingMaterial = function (packagingMaterial) {
				var p = {
					id: packagingMaterial.id,
					maximumQuantityAllowed: packagingMaterial.maximumQuantityAllowed,
					reorderLevel: packagingMaterial.reorderLevel,
					//locationId: packagingMaterial.locationId,
					//packagingTypeCode: packagingMaterial.packagingTypeCode,
					//availableQuantity: packagingMaterial.availableQuantity
				};
				PackagingMaterialMaster
					.update(p)
					.success(function (response) {
						console.log(response);
					});
			}

			$scope.openReturnItemDialog = function () {
				PackagingMaterialInventory
					.get_return_template()
					.success(function (templateStr) {
						console.log(templateStr);
						ngDialog.open({
							width: '40%',
							template: templateStr,
							controller: ['$scope', function ($scope) {
								$scope.isDisabled = false;
								$scope.inventoryItemStatusUpdate = function (barcode, status) {
									$scope.isDisabled = true;
									var params = { barcode: barcode, status: status };
									PackagingMaterialInventory
										.status_update(params)
										.success(function (response) {
											if (response !== undefined && response.response !== undefined &&
												response.response.success !== undefined && response.response.success) {
												notification.success("Successfully returned this item.");
												$scope.searchBarcode(barcode);
											}
										});
								}

								$scope.searchBarcode = function (returnBarcode) {
									var params = { barcode: returnBarcode };
									PackagingMaterialInventory
										.get_by_barcode(params)
										.success(function (response) {
											$scope.barcodeInfo = response;
										});
								}

							}]
						});
					});
			}
			
			$scope.$watch('imeiData.file', function () {
				$scope.imeiData.selectedFileName = ($scope.imeiData.file !== undefined) ? $scope.imeiData.file.name : "";
			});

			$scope.clearSearchFields = function () {
				$scope.imeiInventory = {};
				$scope.imeiData = [];
			}

			var evtDeleteSingleImeiNo = $rootScope.$on('deleteSingleImeiNo', function(e,imeiNo,index) {
        $scope.deleteSingleImeiNo(imeiNo);
    });
		}]);