'use strict'

app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {

		$stateProvider
			.state('rl.inventory', {
				url: '/inventory',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/inventory/list.tpl.html'
					}
				},
				data: {
					page: "Inventory List",
					layout : "site",
					requireLogin : true
				}
			});
    }]);