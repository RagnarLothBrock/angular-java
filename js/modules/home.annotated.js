'use strict'

app.config(['$stateProvider', '$urlRouterProvider','$httpProvider',
    function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $stateProvider
        .state('rl.home', {
			url: '/home',
			views: {
				'page-main-content@' : {
					templateUrl: 'partials/tpl/pages/home.tpl.html'
				}
            },
			controller : 'homeCntrl',
			data : {
				page : "Home",
				layout : "site",
				requireLogin : true,
				checkAccess : false
			}
		})
    }]);

app.controller('homeCntrl', ['$scope','$rootScope', function($scope,$rootScope){
	
	$scope.options = {
		"width": 5,
		"height": 100,
		"displayValue": "RL-ASD-TPC 13124235325345",
		"font": "monospace",
		"textAlign": "center",
		"fontSize": 15,
		"backgroundColor": "#fff",
		"lineColor": "#000"
	  }
	$scope.people = [
		{
			unwantedInfo : "abc",
			name:"abc",
			address:"abc",
			phone:"1231241241"
		}
					];
	
	$scope.PrintBarcode = function(){
		$http.get(host_base_url + "partials/tpl/printer_sample.prn")
			.success(function(response){
				
			});
	}
}]);