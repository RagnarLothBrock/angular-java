'use strict';

app.factory('ProductSubCategory', function($http, REST_API_URL, API) {

	return {
		ddl: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.PRODUCT_SUB_CATEGORY.LIST, params);
		},
		list: function(params) {
			var getUrl = API.MASTERS.PRODUCT_SUB_CATEGORY.GET_ALL
						.replace('<firstResult>', params.firstResult)
						.replace('<maxResults>', params.maxResults);
			return $http.post(REST_API_URL + getUrl, {});
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.PRODUCT_SUB_CATEGORY.CREATE, {'productSubCategory':params});
		},
		get: function(params) {
			var getUrl = API.MASTERS.PRODUCT_SUB_CATEGORY.GET.replace('<productsubcategoryid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.PRODUCT_SUB_CATEGORY.UPDATE, {'productSubCategory':params});
		},
		del: function(params) {
			var getUrl = API.MASTERS.PRODUCT_SUB_CATEGORY.DELETE.replace('<productsubcategoryid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		byServiceProviderLocation: function(params) {			
			var url = API.MASTERS.PRODUCT_SUB_CATEGORY.BY_SERVICE_PROVIDER_LOCATION
						.replace('<serviceProviderLocationId>', params.serviceProviderLocationId)
						.replace('<categoryId>', params.categoryId);
			return $http.post(REST_API_URL + url, params);
		}
	}
});

app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {
		$stateProvider
			.state('rl.masters-sub-category', {
				url: '/masters/sub-category',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/sub_category/list.tpl.html'
					}
				},
				controller: 'SubCategoryController',
				data: {
					page: "Masters - Sub Category",
					requireLogin : true
				}
			})
			.state('rl.masters-sub-category-create', {
				url: '/masters/sub-category/create',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/sub_category/save.tpl.html'
					}
				},
				controller: 'SubCategoryController',
				data: {
					page: "Masters - Sub Category : Create",
					requireLogin : true
				}
			})
			.state('rl.masters-sub-category-get', {
				url: '/masters/sub-category/get/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/sub_category/save.tpl.html'
					}
				},
				controller: 'SubCategoryController',
				data: {
					page: "Masters - Sub Category : Update",
					requireLogin : true
				}
			});
    }]);

app.controller('SubCategoryController',
	['$scope', '$rootScope', '$state', 'notification', 'AuthTokenFactory', 'ngDialog', '$location', 'logger', 'ProductSubCategory', 'Utility', 'LIST_PAGE_SIZE',
    function($scope, $rootScope, $state,  notification, AuthTokenFactory, ngDialog, $location, logger, ProductSubCategory, Utility, LIST_PAGE_SIZE) {
		
		$scope.breadcrumbs = [
			{
				title: "Master Tables"
			},
			{
				title: "Product Sub Category"
			}
		];
		
		$scope.pageSize = LIST_PAGE_SIZE;
		$scope.total_count = 0;
		$scope.currentPage = 1;
		
        $scope.notification = $rootScope.notification;        
        $scope.subCategories = [];
        $scope.pageActionHeader = "";
		$scope.errors = {};
		$scope.subCategory = {};
		
		$scope.items = [{"id":"1177576","name":"Backpacks","code":"Backpacks","masterCategoryId":"9994bc35-aa47-11e6-a659-0a6567e1b81d","description":"Backpacks","masterCategoryName":"Apparels"},{"id":"de8ae68d-c12e-11e6-9c4f-0a6567e1b81d","name":"Desktop","code":"DSK","masterCategoryId":"21d42c86-4f2f-11e6-a659-0a6567e1b81d","description":"Desktop","masterCategoryName":"IT Products"},{"id":"876651","name":"Fridge","code":"FDG","masterCategoryId":"7e4406bb-aa6c-11e6-a659-0a6567e1b81d","description":null,"masterCategoryName":"Electronics"},{"id":"880322","name":"Kids","code":"KDS","masterCategoryId":"1fac1e0d-aaf3-11e6-a659-0a6567e1b81d","description":null,"masterCategoryName":"Shoes"},{"id":"991502","name":"Kitchen Set","code":"KST","masterCategoryId":"36a7e1cc-aaf3-11e6-a659-0a6567e1b81d","description":null,"masterCategoryName":"Furniture"},{"id":"0d521af8-99cf-11e6-a659-0a6567e1b81d","name":"Laptop","code":"LAP","masterCategoryId":"21d42c86-4f2f-11e6-a659-0a6567e1b81d","description":"Laptop","masterCategoryName":"IT Products"},{"id":"995cbbdb-4f33-11e6-a659-0a6567e1b81d","name":"Mobile","code":"MOB","masterCategoryId":"21d42c86-4f2f-11e6-a659-0a6567e1b81d","description":null,"masterCategoryName":"IT Products"},{"id":"991501","name":"Oven","code":"OVN","masterCategoryId":"7e4406bb-aa6c-11e6-a659-0a6567e1b81d","description":null,"masterCategoryName":"Electronics"},{"id":"991503","name":"Silver Pearls","code":"SPL","masterCategoryId":"9994bc35-aa47-11e6-a659-0a6567e1b81d","description":null,"masterCategoryName":"Apparels"},{"id":"8f268d88-c2bc-4c76-9909-5ec727902ff6","name":"Smart Band","code":"SMB","masterCategoryId":"7e4406bb-aa6c-11e6-a659-0a6567e1b81d","description":"Smart Band","masterCategoryName":"Electronics"},{"id":"ec71e7b0-cc21-477b-a192-2ce550950b10","name":"Smart Watch","code":"SMW","masterCategoryId":"7e4406bb-aa6c-11e6-a659-0a6567e1b81d","description":"Smart Watch","masterCategoryName":"Electronics"},{"id":"158638","name":"Tablet","code":"TAB","masterCategoryId":"21d42c86-4f2f-11e6-a659-0a6567e1b81d","description":null,"masterCategoryName":"IT Products"},{"id":"854884","name":"Testing dasfsfsdf","code":"Testing2","masterCategoryId":"21d42c86-4f2f-11e6-a659-0a6567e1b81d","description":"asfasffsdfs","masterCategoryName":"IT Products"},{"id":"883844","name":"Washing Machine","code":"WSM","masterCategoryId":"7e4406bb-aa6c-11e6-a659-0a6567e1b81d","description":null,"masterCategoryName":"Electronics"},{"id":"df847ad6-aa47-11e6-a659-0a6567e1b81d","name":"Women","code":"WMN","masterCategoryId":"9994bc35-aa47-11e6-a659-0a6567e1b81d","description":"Women Clothing","masterCategoryName":"Apparels"}];
		
		$scope.getProductSubCategoryList = function(first, limit) {
			$scope.currentPage = first // initialize page no to 1
			
			first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
			ProductSubCategory.list({'query':{},firstResult: first, maxResults: limit}).success(function(response) {
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.subCategories = response.data;
					$scope.total_count = response.totalRecords;
				}
			});
		}
        
        if ($state.current.url == "/masters/sub-category") {
			$scope.showAddNew = true;
			$scope.addNewUrl = "/masters/sub-category/create";
			$scope.addNewState = "rl.masters-sub-category-create"
			$scope.getProductSubCategoryList(1, $scope.pageSize);
		}
		
		if ($state.current.url == "/masters/sub-category/create") {
			$scope.pageActionHeader = "Create new sub-category";
			$scope.breadcrumbs.push({title:"Create"});
		}
		
		if ($state.current.url == "/masters/sub-category/get/:id") {
			$scope.pageActionHeader = "Update Sub-Category Details";
			$scope.subCategory.id = $state.current.data.params.id;
			$scope.breadcrumbs.push({title:"Update"});
			ProductSubCategory.get($scope.subCategory).success(function(response) {
				$scope.subCategory = response;
			});
		}
		
		if ($state.current.url == "/masters/sub-category/create" ||
			$state.current.url == "/masters/sub-category/get/:id") {
			Utility.populate_category({},$scope);
		}
		
		//Save Brand Details
		$scope.save = function(subCategory) {
			$scope.errors = {};
			//Validate Brand
			if(hasErrors(subCategory)) return false;
			
			if (subCategory.id == undefined || subCategory.id == "" || subCategory.id == null) {
                //Continue with subCategory create
				ProductSubCategory.create(subCategory).success(function(response) {
					console.log(response);
					$state.go('rl.masters-sub-category');
				});
            }
			else {
				//Continue with subCategory Update
				ProductSubCategory.update(subCategory).success(function(response) {
					console.log(response);
					$state.go('rl.masters-sub-category');
				});
			}
		}
		
		//Delete Existing Sub Category
		$scope.deleteSubCategory = function(subCategory, index) {
			ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete this subCategory?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				console.log(subCategory,index);
				ProductSubCategory.del(subCategory).success(function(response) {
					if (response) {
                        $state.go('rl.masters-sub-category');
                    }
				});
			}, function (error) {
				// Error logic here
			});
			
		}
		
		//Validate Brand Details
		var hasErrors = function(subCategory) {
			if (isEmpty(subCategory.name,'name',"Enter sub category name",$scope.notification)) {
                $scope.errors.name = "Enter sub category name";
            }
			if (isEmpty(subCategory.code,'code',"Enter sub category code",$scope.notification)) {
                $scope.errors.code = "Enter sub category code";
            }
			
			if (Object.getOwnPropertyNames($scope.errors).length > 0) {
				return true;
			}
			return false;
		}
		
		$scope.calculateVolumetricSize = function() {
			hasErrors($scope.subCategory, $scope.subCategoryForm);
			var p = $scope.subCategory;
			$scope.subCategory.volumetricWeight = ((p.length * p.breadth * p.height)/5000);
		}
    }]);