'use strict'

app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {
		$stateProvider
			.state('rl.masters-costing-activity', {
				url: '/masters/costing-activity',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/costing_activity/list.tpl.html'
					}
				},
				controller: 'CostingActivityController',
				data: {
					page: "Costing Activity",
					requireLogin : true
				}
			})
			.state('rl.masters-costing-activity-create', {
				url: '/masters/costing-activity/create',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/costing_activity/save.tpl.html'
					}
				},
				controller: 'CostingActivityController',
				data: {
					page: "Create",
					requireLogin : true
				}
			})
			.state('rl.masters-costing-activity-get', {
				url: '/masters/costing-activity/get/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/costing_activity/save.tpl.html'
					}
				},
				controller: 'CostingActivityController',
				data: {
					page: "Update",
					requireLogin : true
				}
			});
    }]);

app.factory('CostingActivityService', ["$http", "REST_API_URL", "AuthTokenFactory", "API", function($http, REST_API_URL, AuthTokenFactory, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.COSTING_ACTIVITY.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.COSTING_ACTIVITY.CREATE, {'costingActivity':params});
		},
		get: function(params) {
			var getUrl = API.MASTERS.COSTING_ACTIVITY.GET.replace('<id>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.COSTING_ACTIVITY.UPDATE, {'costingActivity':params});
		},
		del: function(params) {
			var getUrl = API.MASTERS.COSTING_ACTIVITY.DELETE.replace('<id>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		}
	}
}]);

app.controller('CostingActivityController',
	['$scope', '$rootScope', '$state', 'notification', 'AuthTokenFactory', 'ngDialog', '$location', 'logger', 'CostingActivityService', 'LIST_PAGE_SIZE',
    function($scope, $rootScope, $state,  notification, AuthTokenFactory, ngDialog, $location, logger, CostingActivityService, LIST_PAGE_SIZE) {
		
		$scope.pageSize = LIST_PAGE_SIZE;
		$scope.total_count = 0;
		$scope.currentPage = 1;
		
		$scope.breadcrumbs = [
			{
				title: "Master Tables",
				link: "/masters"
			}
		];
		
        $scope.notification = $rootScope.notification;        
        $scope.costingActivities = [];
        $scope.pageActionHeader = "";
		$scope.errors = {};
		$scope.costingActivity = {};
		
		$scope.getCostingActivityList = function(first, limit) {
			$scope.currentPage = first // initialize page no to 1
			
			first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
			CostingActivityService.list({'query':{},firstResult: first, maxResults: limit}).success(function(response) {
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.costingActivities = response.data;
					$scope.total_count = response.totalRecords;
				}
			});
		}
        
        if ($state.current.url == "/masters/costing-activity") {
			$scope.showAddNew = true;
			$scope.addNewUrl = "/masters/costing-activity/create";
			$scope.addNewState = "rl.masters-costing-activity-create"
			$scope.breadcrumbs.push({title:$state.current.data.page, url:$state.current.url});
			$scope.getCostingActivityList(1, $scope.pageSize);
		}
		
		if ($state.current.url == "/masters/costing-activity/create") {
			$scope.breadcrumbs.push({title:"Costing Activities", link:"/masters/costing-activity"});
			$scope.breadcrumbs.push({title:$state.current.data.page, link:$state.current.url});
			$scope.pageActionHeader = "Create new costing activity";
		}
		
		if ($state.current.url == "/masters/costing-activity/get/:id") {
			$scope.breadcrumbs.push({title:"Costing Activities", link:"/masters/costing-activity"});
			$scope.breadcrumbs.push({title:$state.current.data.page});
			$scope.pageActionHeader = "Update Costing Activity Details";
			
			$scope.costingActivity.id = $state.current.data.params.id;
			CostingActivityService.get($scope.costingActivity).success(function(response) {
				$scope.costingActivity = response;
			});
		}
		
		
		//Save Costing Activity Details
		$scope.save = function(costingActivity) {
			$scope.errors = {};
			//Validate Costing Activity
			if(hasErrors(costingActivity)) return false;
			
			if (costingActivity.id == undefined || costingActivity.id == "" || costingActivity.id == null) {
                //Continue with costingActivity create
				CostingActivityService.create(costingActivity).success(function(response) {
					console.log(response);
					$state.go('rl.masters-costing-activity');
				});
            }
			else {
				//Continue with costingActivity Update
				CostingActivityService.update(costingActivity).success(function(response) {
					console.log(response);
					$state.go('rl.masters-costing-activity');
				});
			}
		}
		
		//Delete Existing Costing Activity
		$scope.deleteBrand = function(costingActivity, index) {
			ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete this costing activity?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				console.log(costingActivity,index);
				CostingActivityService.del(costingActivity).success(function(response) {
					if (response) {
                        $state.go('rl.masters-costing-activity');
                    }
				});
			}, function (error) {
				// Error logic here
			});
			
		}
		
		//Validate Costing Activity Details
		var hasErrors = function(costingActivity) {
			if (isEmpty(costingActivity.name,'name',"Enter costing activity name",$scope.notification)) {
                $scope.errors.name = "Enter costing activity name";
            }
			if (isEmpty(costingActivity.code,'code',"Enter costing activity code",$scope.notification)) {
                $scope.errors.code = "Enter costing activity code";
            }
			
			if (Object.getOwnPropertyNames($scope.errors).length > 0) {
				return true;
			}
			return false;
		}
    }]);