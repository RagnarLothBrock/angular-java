'use strict'

app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {
		$stateProvider
			.state('rl.masters-location', {
				url: '/masters-location',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/location/list.tpl.html'
					}
				},
				controller: 'LocationController',
				data: {
					page: "Masters - Location",
					requireLogin : true
				}
			})
			.state('rl.masters-location-create', {
				url: '/masters-location/create',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/location/view.tpl.html'
					}
				},
				controller: 'LocationController',
				data: {
					page: "Masters - Location : Create",
					requireLogin : true
				}
			})
			.state('rl.masters-location-get', {
				url: '/masters-location/get/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/location/view.tpl.html'
					}
				},
				controller: 'LocationController',
				data: {
					page: "Masters - Location : Update",
					requireLogin : true
				}
			});
    }]);

app.factory('LocationService', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.LOCATION.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.LOCATION.CREATE, {'processLocation':params});
		},
		get: function(params) {
			var getUrl = API.MASTERS.LOCATION.GET.replace('<locationId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.LOCATION.UPDATE, {'processLocation':params});
		},
		del: function(params) {
			var getUrl = API.MASTERS.LOCATION.DELETE.replace('<locationId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
	}
}]);

app.factory('LocationPincodeService', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.LOCATION.PINCODES.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.LOCATION.PINCODES.CREATE, {'locationPincode':params});
		},
		get: function(params) {
			var getUrl = API.MASTERS.LOCATION.PINCODES.GET.replace('<locationPincodeId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.LOCATION.PINCODES.UPDATE, {'locationPincode':params});
		},
		del: function(params) {
			var getUrl = API.MASTERS.LOCATION.PINCODES.DELETE.replace('<locationPincodeId>', params);
			return $http.post(REST_API_URL + getUrl, {});
		},
        bulkcreate:function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.LOCATION.PINCODES.BULK_CREATE, {'locationPincodesList':params});
		},
		bulkdelete:function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.LOCATION.PINCODES.BULK_DELETE, {'locationPincodeIdList':params});
		}
	}
}]);

app.controller('LocationController',
	['$scope', '$rootScope', '$state', 'notification', 'AuthTokenFactory', 'ngDialog', '$location', 'logger', 'LocationService', 'Utility', 'LocationPincodeService', 'Utils', 'LIST_PAGE_SIZE',
    function($scope, $rootScope, $state,  notification, AuthTokenFactory, ngDialog, $location, logger, LocationService, Utility, LocationPincodeService, Utils, LIST_PAGE_SIZE) {
		
		$scope.pageSize = LIST_PAGE_SIZE;
		$scope.total_count = 0;
		$scope.currentPage = 1;
		
		$scope.breadcrumbs = [
			{title: "Master Tables"},
			{title: "BizLog Locations/Branches",link: '/masters-location'},
		];
        
        $scope.steps = ["Basic Info", "Pincodes"];
		$scope.currentStep = 1;
        $scope.currentStepName = $scope.steps[$scope.currentStep-1];
        $scope.notification = $rootScope.notification;        
        $scope.locations = [];
        $scope.pageActionHeader = "";
		$scope.errors = {};
		$scope.location = {};
        $scope.pincodeConfig = {
            delimiter: ',',
            create: true,
            labelField: 'pincode',
            onInitialize: function(selectize){
                // receives the selectize object as an argument 
            }
        }
        $scope.pincodeOptions = [];
        $scope.pincodeList = [];
		
		$scope.getLocationList = function(first, limit) {
			$scope.currentPage = first // initialize page no to 1
			
			first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
			LocationService.list({'query':{},firstResult: first, maxResults: limit}).success(function(response) {
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.locations = response.data;
					$scope.total_count = response.totalRecords;
				}
			});
		}
        
        if ($state.current.url == "/masters-location") {
			$scope.showAddNew = true;
			$scope.addNewUrl = "/masters-location/create";
			$scope.addNewState = "rl.masters-location-create"
			$scope.getLocationList(1, $scope.pageSize);
		}
		
		if ($state.current.url == "/masters-location/create") {
			$scope.pageActionHeader = "Create new location";
            $scope.location = {};
			$scope.breadcrumbs.push({title:"Create"});
		}
		
		if ($state.current.url == "/masters-location/get/:id") {
			$scope.pageActionHeader = "Update Location Details";
			$scope.breadcrumbs.push({title:"Update"});
		}
        
        $scope.getLocationDetails = function() {
            if ($state.current.url == "/masters-location/create" ||
                $state.current.url == "/masters-location/get/:id") {
                Utility.populate_city({},$scope);
                Utility.populate_location({},$scope);
                
                $scope.$watch('locations',$scope.populateParentLocation);
            }
            
            if ($state.current.url == "/masters-location/get/:id") {
                $scope.location.id = $state.current.data.params.id;
                LocationService.get($scope.location).success(function(response) {
					var code = parseInt(response.code);
                    $scope.location = response;
					$scope.location.code = code;
                });
            }
        }
        
        $scope.populateParentLocation = function() {
            if ($scope.locations != undefined) {
                if ($scope.locations.length > 0) {
                    for(var i = 0; i < $scope.locations.length; i++) {
                        if ($scope.locations[i].id == $scope.location.id) {
                            $scope.locations.splice(i,1);
                        }
                    }
                }
            }
        }
        
        $scope.populatePincodeList = function(first, limit) {
			$scope.currentPage = first // initialize page no to 1
			
			first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
            LocationPincodeService
				.list({'query':{'locationId':$state.current.data.params.id},firstResult: first, maxResults: limit})
				.success(function(response) {
					if (response !== undefined && response.data !== undefined && response.data.length > 0){
						$scope.pincodes = response.data;
						$scope.total_count = response.totalRecords;
					}
			});
        }
		
		//Save Location Details
		$scope.save = function(location) {
			$scope.errors = {};
			//Validate Location
			if(hasErrors(location)) return false;
			
			var code = location.code.toString();
			if (code.length < 3) {
				while (code.length <= 2) {
                    code = '0' + code;
                }
			}
			location.code = code;
			
			if (location.id == undefined || location.id == "" || location.id == null) {
                //Continue with location create
				LocationService.create(location).success(function(response) {
					console.log(response);
					$state.go('rl.masters-location');
				});
            }
			else {
				//Continue with location Update
				LocationService.update(location).success(function(response) {
					console.log(response);
					$state.go('rl.masters-location');
				});
			}
		}
		
		//Delete Existing Location
		$scope.deleteBrand = function(location, index) {
			ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete this location?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				console.log(location,index);
				LocationService.del(location).success(function(response) {
					if (response) {
                        $state.go('rl.masters-location');
                    }
				});
			}, function (error) {
				// Error logic here
			});
			
		}
		
		//Validate Location Details
		var hasErrors = function(location) {
            if (location === undefined) {
                $scope.errors.name = "Enter location name";
				$scope.errors.code = "Enter location code";
				$scope.errors.address1 = "Enter location address";
				$scope.errors.cityId = "Select location city";
				$scope.errors.pincode = "Enter location pincode";
				$scope.errors.email = "Enter valid email";
				$scope.errors.phoneNumber = "Enter location phone number";
            }
			if (isEmpty(location.name,'name')) {
                $scope.errors.name = "Enter location name";
            }
			if (isEmpty(location.code,'code')) {
                $scope.errors.code = "Enter location code";
            }
            if (isEmpty(location.address1,'address1')) {
                $scope.errors.address1 = "Enter location address";
            }
            if (isEmpty(location.cityId,'cityId')) {
                $scope.errors.cityId = "Select location city";
            }
            if (isEmpty(location.pincode,'pincode')) {
                $scope.errors.pincode = "Enter location pincode";
            }
			if (isEmpty(location.email,'email')) {
                $scope.errors.email = "Enter valid email";
            }
			else if (!validEmail(location.email)) {
                $scope.errors.email = "Enter valid email";
            }
			if (isEmpty(location.phoneNumber,'phoneNumber')) {
                $scope.errors.phoneNumber = "Enter location phone number";
            }
			
			if (isEmpty(location.contactPerson,'contactPerson')) {
                $scope.errors.contactPerson = "Enter location contact person name";
            }
			
			
			if (Object.getOwnPropertyNames($scope.errors).length > 0) {
				return true;
			}
            location.city = Utils.get_city_name($scope.cities, location.cityId);
			return false;
		}
        
        //Show Page
        $scope.showPage = function(stepId) {
            $scope.currentStep = stepId;
            $scope.currentStepName = $scope.steps[stepId-1];
            $(".onboarding-steps").hide();
            $(".onboarding-steps:nth-of-type("+stepId+")").show();
            $(".list-group-item").removeClass('active');
            $(".list-group-item:nth-of-type("+stepId+")").addClass('active');
            //Load Step Data
            $scope.loadStepData((stepId-1));
        }
        
        //Load Page data
        $scope.loadStepData = function(stepId) {
            switch (stepId) {
                case 0:
                    $scope.getLocationDetails();
                    break;
                
                case 1:
                    $scope.populatePincodeList(1, $scope.pageSize);
                    break;
            }
        }
        $scope.showPage($scope.currentStep);
        
        $scope.savePincodes = function(pincodes) {
            console.log(pincodes);
            
            var pincodeList = $scope.hasErrorsPincodes(pincodes); 
            
            if (pincodeList.length > 0) {
                //Save data in DB
                LocationPincodeService
                    .bulkcreate(pincodeList)
                    .success(function(response){
                        $scope.pincodeList = "";
                        $scope.showPage(2);
                    });
            }
        }
        
        $scope.hasErrorsPincodes = function(pincodes) {
            var locationPincodeList = [];
            if (pincodes.length <= 0) {
                $scope.errors.pincode = "Enter Pincode!";
            }
            var template = {
                locationId : "",
                pincode: ""
            }
            for(var i in pincodes) {
                console.log(pincodes[i]);
                if (pincodes[i] != "" && pincodes[i].length !== 6) {
                    $scope.errors.pincode = "Data contains invalid pincodes!. Pincode should be a number & 6 characters long."
                    
                    setTimeout(function() {
                        $scope.errors.pincode = "";
                    },3000);
                    continue;
                }
                if (isNaN(pincodes[i])) {
                    $scope.errors.pincode = "Data contains invalid pincodes!. Pincode should be a number & 6 characters long."
                    
                    setTimeout(function() {
                        $scope.errors.pincode = "";
                    },3000);
                    continue;
                }
                
                var p = angular.copy(template);
                p.pincode = pincodes[i];
                p.locationId = $state.current.data.params.id;
                
                locationPincodeList.push(p);
            }
            
            if (locationPincodeList.length <= 0) {
                $scope.pincodeList = [];
                $scope.pincodeOptions = [];
            }
            
            return locationPincodeList;
        }

		// DELETING PINCODE FROM DIFFRENT LOCATION
		$scope.deleteLocationPincode = function (index, id) {
				ngDialog.openConfirm({
					template: '<div class="dialog-contents">' +
					'Do you want to delete this pincode?<br/><br/>' +
					'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
					'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
					'</div>',
					controller: ['$scope', function ($scope) {
						// Controller logic here
					}]
				}).then(function (success) {
					// Success logic here
					console.log(id, index);
					LocationPincodeService.del(id).success(function (response) {
						if (response) {
							// $state.go('rl.user-pincode-update');
							console.log("Deleted Successfully");
						}
					});
				}, function (error) {
					// Error logic here
				});
			}
    }]);