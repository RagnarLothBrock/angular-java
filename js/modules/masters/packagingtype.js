'use strict';

app.factory('PackagingType', function($http, REST_API_URL, AuthTokenFactory, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.PACKAGING_TYPE.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.PACKAGING_TYPE.CREATE, {'packagingType':params});
		},
		get: function(params) {
			var getUrl = API.MASTERS.PACKAGING_TYPE.GET.replace('<packagingtypeid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.PACKAGING_TYPE.UPDATE, {'packagingType':params});
		},
		del: function(params) {
			var getUrl = API.MASTERS.PACKAGING_TYPE.DELETE.replace('<packagingtypeid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		}
	}
});

app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {
		$stateProvider
			.state('rl.masters-packaging-type', {
				url: '/masters-packaging-type',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/packaging_type/list.tpl.html'
					}
				},
				controller: 'PackagingTypeController',
				data: {
					page: "Masters - Packaging Type",
					requireLogin : true
				}
			})
			.state('rl.masters-packaging-type-create', {
				url: '/masters-packaging-type/create',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/packaging_type/save.tpl.html'
					}
				},
				controller: 'PackagingTypeController',
				data: {
					page: "Masters - Packaging Type : Create",
					requireLogin : true
				}
			})
			.state('rl.masters-packaging-type-get', {
				url: '/masters-packaging-type/get/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/packaging_type/save.tpl.html'
					}
				},
				controller: 'PackagingTypeController',
				data: {
					page: "Masters - Packaging Type : Update",
					requireLogin : true
				}
			});
    }]);


app.controller('PackagingTypeController',
	['$scope', '$rootScope', '$state', 'notification', 'AuthTokenFactory', 'ngDialog', '$location', 'logger', 'PackagingType', 'LIST_PAGE_SIZE',
    function($scope, $rootScope, $state,  notification, AuthTokenFactory, ngDialog, $location, logger, PackagingType, LIST_PAGE_SIZE) {
		
		$scope.pageSize = LIST_PAGE_SIZE;
		$scope.total_count = 0;
		$scope.currentPage = 1;
		
		$scope.breadcrumbs = [
			{title: "Master Tables"},
			{title: "Packaging Type", link: "/masters-packaging-type"}
		];
		
        $scope.notification = $rootScope.notification;        
        $scope.packagingTypes = [];
		$scope.packagingType = {
			volumetricSizeMin:"",
			volumetricSizeMax:"",
			lengthMin : "",
			lengthMax: "",
			breadthMin: "",
			breadthMax: "",
			heightMin: "",
			heightMax: "",
			type: 0
		};
		
		$scope.getPackagingTypeList = function(first, limit) {
			$scope.currentPage = first // initialize page no to 1
			
			first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
			PackagingType.list({'query':{},firstResult: first, maxResults: limit}).success(function(response) {
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.packagingTypes = response.data;
					$scope.total_count = response.totalRecords;
				}
			});
		}
		
        if ($state.current.url == "/masters-packaging-type") {
			$scope.getPackagingTypeList(1, $scope.pageSize);
			
			$scope.showAddNew = true;
			$scope.addNewUrl = "/masters-packaging-type/create";
			$scope.addNewState = "rl.masters-packaging-type-create"
		}
		
		if ($state.current.url == "/masters-packaging-type/create") {
			$scope.pageActionHeader = "Create New Packaging Type";
			$scope.breadcrumbs.push({title:"Create"});
		}
		
		if ($state.current.url == "/masters-packaging-type/get/:id") {
			$scope.pageActionHeader = "Update Packaging Type Details";
			$scope.packagingType.id = $state.current.data.params.id;
			$scope.breadcrumbs.push({title:"Update"});
			PackagingType.get($scope.packagingType).success(function(response) {
				var c = parseInt(response.code);
				$scope.packagingType = response;
				$scope.packagingType.code = c;
			});
		}
		
		if ($state.current.url == "/masters-packaging-type/create" ||
			$state.current.url == "/masters-packaging-type/get/:id") {
			PackagingType.list({'query':{}}).success(function(response) {
				if (response != undefined) {
					if (response.length > 0) {
                        for(var i = 0; i < response.length; i++) {
							if (response[i].id == $scope.packagingType.id) {
                                response.splice(i,1);
                            }
						}
                    }
					$scope.packagingTypes = response;
                }
			});
		}
		
		//Save Packaging Type Details
		$scope.save = function(packagingType, packagingTypeForm) {
			
			//Validate Packaging Type
			if(hasErrors(packagingType, packagingTypeForm)) return false;
			
			if (packagingTypeForm.$invalid) {
                 return false;
            }
			
			var code = packagingType.code.toString();
			if (code.length < 2) {
				while (code.length <= 1) {
                    code = '0' + code;
                }
			}
			packagingType.code = code;
			
			if (packagingType.id == undefined || packagingType.id == "" || packagingType.id == null) {
                //Continue with packagingType create
				PackagingType.create(packagingType).success(function(response) {
					console.log(response);
					$state.go('rl.masters-packaging-type');
				});
            }
			else {
				//Continue with packagingType Update
				PackagingType.update(packagingType).success(function(response) {
					console.log(response);
					$state.go('rl.masters-packaging-type');
				});
			}
		}
		
		//Delete Existing Packaging Type
		$scope.deletePackagingType = function(packagingType, index) {
			ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete this Packaging Type?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				console.log(packagingType,index);
				PackagingType.del(packagingType).success(function(response) {
					if (response) {
                        $state.go('rl.masters-packaging-type');
                    }
				});
			}, function (error) {
				// Error logic here
			});
			
		}
		
		$scope.calculateVolumetricSize = function() {
			hasErrors($scope.packagingType, $scope.packagingTypeForm);
			var p = $scope.packagingType;
			$scope.packagingType.volumetricSizeMin = ((p.lengthMin * p.breadthMin * p.heightMin)/5000);
			$scope.packagingType.volumetricSizeMax = ((p.lengthMax * p.breadthMax * p.heightMax)/5000);
		}
		
		//Validate Packaging Type Details
		var hasErrors = function(packagingType, packagingTypeForm) {
			if($scope.enableLBHCheck) {
				packagingTypeForm.breadthMin.$error.min = false;
				if (packagingTypeForm.breadthMin.$modelValue <= 0) {
					packagingTypeForm.breadthMin.$error.min = true;
					$scope.packagingTypeForm.$invalid = true;
				}
				
				packagingTypeForm.breadthMax.$error.min = true;
				if ($scope.packagingType.breadthMax <= 0) {
					packagingTypeForm.breadthMax.$error.min = true;
					$scope.packagingTypeForm.$invalid = true;
				}
				
				packagingTypeForm.heightMin.$error.min = false;
				if (packagingTypeForm.heightMin.$modelValue <= 0) {
					packagingTypeForm.heightMin.$error.min = true;
					$scope.packagingTypeForm.$invalid = true;
				}
				
				packagingTypeForm.heightMax.$error.min = true;
				if ($scope.packagingType.heightMax <= 0) {
					packagingTypeForm.heightMax.$error.min = true;
					$scope.packagingTypeForm.$invalid = true;
				}
				
				packagingTypeForm.lengthMin.$error.min = false;
				if (packagingTypeForm.lengthMin.$modelValue <= 0) {
					packagingTypeForm.lengthMin.$error.min = true;
					$scope.packagingTypeForm.$invalid = true;
				}
				
				packagingTypeForm.lengthMax.$error.min = true;
				if ($scope.packagingType.lengthMax <= 0) {
					packagingTypeForm.lengthMax.$error.min = true;
					$scope.packagingTypeForm.$invalid = true;
				}
			}
			else if($scope.packagingType.type==0){
				packagingTypeForm.volumetricSizeMin.$error.min = false;
				if (packagingTypeForm.volumetricSizeMin.$modelValue <= 0) {
					packagingTypeForm.volumetricSizeMin.$error.min = true;
					$scope.packagingTypeForm.$invalid = true;
				}
				
				packagingTypeForm.volumetricSizeMax.$error.min = true;
				if ($scope.packagingType.volumetricSizeMax <= 0) {
					packagingTypeForm.volumetricSizeMax.$error.min = true;
					$scope.packagingTypeForm.$invalid = true;
				}
			}
			
			if (Object.getOwnPropertyNames(packagingTypeForm.$error).length > 0 || $scope.packagingTypeForm.$invalid) {
				$scope.packagingTypeForm.$invalid = true;
				return true;
			}
			return false;
		}
    }]);