'use strict';

app.factory('SMSEmailTrigger', function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.SMS_EMAIL_TRIGGER.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.SMS_EMAIL_TRIGGER.CREATE, {'smsEmailTrigger':params});
		},
		get: function(params) {
			var getUrl = API.MASTERS.SMS_EMAIL_TRIGGER.GET.replace('<smsEmailTriggerId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.SMS_EMAIL_TRIGGER.UPDATE, {'smsEmailTrigger':params});
		},
		del: function(params) {
			var getUrl = API.MASTERS.SMS_EMAIL_TRIGGER.DELETE.replace('<smsEmailTriggerId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		}
	}
});

app.config(['$stateProvider', function($stateProvider) {
	$stateProvider
		.state('rl.masters-sms-email-trigger', {
			url: '/masters/sms-email-trigger',
			views: {
				'page-main-content@': {
					templateUrl: 'partials/tpl/pages/masters/sms_email_trigger/list.tpl.html'
				}
			},
			controller: 'SMSEmialTriggerController',
			data: {
				page: "Masters - SMS/Emial Trigger",
				requireLogin : true
			}
		})
		.state('rl.masters-sms-email-trigger-create', {
			url: '/masters/sms-email-trigger/create',
			views: {
				'page-main-content@': {
					templateUrl: 'partials/tpl/pages/masters/sms_email_trigger/save.tpl.html'
				}
			},
			controller: 'SMSEmialTriggerController',
			data: {
				page: "Masters - SMS/Emial Trigger : Create",
				requireLogin : true
			}
		})
		.state('rl.masters-sms-email-trigger-get', {
			url: '/masters/sms-email-trigger/get/:id',
			views: {
				'page-main-content@': {
					templateUrl: 'partials/tpl/pages/masters/sms_email_trigger/save.tpl.html'
				}
			},
			controller: 'SMSEmialTriggerController',
			data: {
				page: "Masters - SMS/Emial Trigger : Update",
				requireLogin : true
			}
		});
    }]);


app.controller('SMSEmialTriggerController',
	['$scope', '$state', 'ngDialog', 'SMSEmailTrigger', 'LIST_PAGE_SIZE', '$rootScope',
    function($scope, $state, ngDialog, SMSEmailTrigger, LIST_PAGE_SIZE, $rootScope) {
				
		$scope.pageSize = LIST_PAGE_SIZE;
		$scope.total_count = 0;
		$scope.currentPage = 1;
		
		$scope.breadcrumbs = [
			{
				title: "Master Tables"
			},
			{
				title: "SMS/Email Trigger",
				link: "/masters/sms-email-trigger"
			}
		];
		
        $scope.smsEmailTriggerList = [];
        $scope.pageActionHeader = "";
		$scope.errors = {};
		$scope.smsEmailTrigger = {};
		
		$scope.getSMSEmailTriggerList = function(first, limit) {
			$scope.currentPage = first // initialize page no to 1
			
			first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
			SMSEmailTrigger.list({'query':{},firstResult: first, maxResults: limit}).success(function(response) {
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.smsEmailTriggerList = response.data;
					$scope.total_count = response.totalRecords;
				}
			});
		}
        
        if ($state.current.url == "/masters/sms-email-trigger") {
			$scope.getSMSEmailTriggerList(1, $scope.pageSize);
			$scope.showAddNew = true;
			$scope.addNewUrl = "/masters/sms-email-trigger/create";
			$scope.addNewState = "rl.masters-sms-email-trigger-create"
			
		}
		
		if ($state.current.url == "/masters/sms-email-trigger/create") {
			$scope.pageActionHeader = "Create New SMS/Email Trigger";
			$scope.breadcrumbs.push({title:"Create"});
		}
		
		if ($state.current.url == "/masters/sms-email-trigger/get/:id") {
			$scope.pageActionHeader = "Update SMS/Emial Trigger Details";
			$scope.smsEmailTrigger.id = $state.current.data.params.id;
			$scope.breadcrumbs.push({title:"Update"});
			SMSEmailTrigger.get($scope.smsEmailTrigger).success(function(response) {
				$scope.smsEmailTrigger = response;
			});
		}
		
		if ($state.current.url == "/masters/sms-email-trigger/create" ||
			$state.current.url == "/masters/sms-email-trigger/get/:id") {
			SMSEmailTrigger.list({'query':{}}).success(function(response) {
				if (response != undefined) {
					if (response.length > 0) {
                        for(var i = 0; i < response.length; i++) {
							if (response[i].id == $scope.smsEmailTrigger.id) {
                                response.splice(i,1);
                            }
						}
                    }
					$scope.smsEmailTriggerList = response;
                }
			});
		}
		
		//Save SMSEmialTrigger Details
		$scope.save = function(smsEmailTrigger) {
			$scope.errors = {};
			//Validate SMSEmialTrigger
			if($scope.smsEmailTriggerForm.$invalid) return false;
			
			if (smsEmailTrigger.id == undefined || smsEmailTrigger.id == "" || smsEmailTrigger.id == null) {
                //Continue with smsEmailTrigger create
				SMSEmailTrigger.create(smsEmailTrigger).success(function(response) {
					console.log(response);
					$state.go('rl.masters-sms-email-trigger');
				});
            }
			else {
				//Continue with smsEmailTrigger Update
				SMSEmailTrigger.update(smsEmailTrigger).success(function(response) {
					console.log(response);
					$state.go('rl.masters-sms-email-trigger');
				});
			}
		}
		
		//Delete Existing SMSEmialTrigger
		$scope.deleteSMSEmialTrigger = function(smsEmailTrigger, index) {
			ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete this SMS/Email Trigger?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				console.log(smsEmailTrigger,index);
				SMSEmailTrigger.del(smsEmailTrigger).success(function(response) {
					if (response) {
                        $state.go('rl.masters-sms-email-trigger');
                    }
				});
			}, function (error) {
				// Error logic here
			});
		}
		
    }]);