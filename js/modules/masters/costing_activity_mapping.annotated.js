'use strict'

app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {
		$stateProvider
			.state('rl.masters-costing-activity-mapping', {
				url: '/masters/costing-activity/mapping',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/costing_activity_mapping/list.tpl.html'
					}
				},
				controller: 'CostingActivityMappingController',
				data: {
					page: "Costing Activity Mapping",
					requireLogin : true					
				},
				params : {
					costingActivityId : null
				}
			})
			.state('rl.masters-costing-activity-upload', {
				url: '/masters/costing-activity/bulk-upload',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/costing_activity_mapping/bulk_upload.tpl.html'
					}
				},
				controller: 'CostingActivityMappingController',
				data: {
					page: "Masters - Costing Activity Mapping : Bulk Upload",
					requireLogin : true
				}
			});;
    }]);

app.factory('CostingActivityMappingService', ["$http", "REST_API_URL", "AuthTokenFactory", "API", "Upload", function($http, REST_API_URL, AuthTokenFactory, API, Upload) {

	return {
		list: function(params) {
			var getUrl = API.MASTERS.COSTING_ACTIVITY_MAPPING.LIST
						.replace('<first>', params.firstResult)
						.replace('<limit>', params.maxResults);
			return $http.post(REST_API_URL + getUrl, params);
			//return $http.post(REST_API_URL + API.MASTERS.COSTING_ACTIVITY_MAPPING.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.COSTING_ACTIVITY_MAPPING.CREATE, {'costingActivityMapping':params});
		},
		get: function(params) {
			var getUrl = API.MASTERS.COSTING_ACTIVITY_MAPPING.GET.replace('<id>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.COSTING_ACTIVITY_MAPPING.UPDATE, {'costingActivityMapping':params});
		},
		del: function(params) {
			var getUrl = API.MASTERS.COSTING_ACTIVITY_MAPPING.DELETE.replace('<id>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		upload: function(params) {
			return Upload.upload({
                url: REST_API_URL + API.MASTERS.COSTING_ACTIVITY_MAPPING.UPLOAD,
                data: params.data,
                file: params.file
            })
		}
	}
}]);

app.controller('CostingActivityMappingController',
	['$scope', '$rootScope', '$state', 'notification', 'AuthTokenFactory', 'ngDialog', '$location', 'logger', 'CostingActivityMappingService', 'ServiceMaster',
		'Utility', 'CostingActivityService', 'LIST_PAGE_SIZE', 'MAX_DROPDOWN_SIZE', '$window',
    function($scope, $rootScope, $state,  notification, AuthTokenFactory, ngDialog, $location, logger, CostingActivityMappingService, ServiceMaster,
		Utility, CostingActivityService, LIST_PAGE_SIZE, MAX_DROPDOWN_SIZE, $window) {
		
		$scope.breadcrumbs = [
			{
				title: "Master Tables",
				link: "/masters"
			},
			{
				title: "Costing Activity",
				link: "/masters/costing-activity"
			},
			{title:$state.current.data.page, url:$state.current.url}
		];
		
        $scope.notification = $rootScope.notification;        
        $scope.costingActivities = [];
        $scope.pageActionHeader = "";
		$scope.errors = {};
		$scope.costingActivityMapping = {};
		$scope.pageSize = 50;
		$scope.costingActivity = {};
		$scope.costingActivityMappingUpload = {};
		
		$scope.showUpload = true;
		$scope.uploadUrl = "/masters/costing-activity/bulk-upload";
		$scope.uploadState = "rl.masters-costing-activity-upload"
		
		$scope.masterServices = [];		
		Utility.populate_master_services({},$scope);
		$scope.categories = [];
		Utility.populate_category({},$scope);
		$scope.costingActivities = [];
		Utility.populate_costing_activity({},$scope);
		
		$scope.populateList = function(first, limit) {
			$scope.currentPage = first // initialize page no to 1
			
			first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
			CostingActivityMappingService.list({'query':$scope.costingActivity,firstResult:first, maxResults: limit})
			.success(function(response) {
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.costingActivityMappingList = response.data;
					$scope.total_count = response.totalRecords;
				}
				else {
					$scope.costingActivityMappingList  = [];
					$scope.total_count = 0;
				}
				//}
				
			});
		}
		$scope.populateList(1, $scope.pageSize);
		
		$scope.filterCostingActivity = function(costingActivity) {
			$scope.populateList(1, $scope.pageSize);
		}
		
		$scope.clearSearchFields = function() {
			$scope.form = {};
			$scope.costingActivity = {};
			$scope.populateList(1, $scope.pageSize);
		}
		
		$scope.formReset = function() {
			$scope.costingActivityMapping = {};
			$scope.costingActivityMappingForm.$setPristine();
			$scope.costingActivityMappingForm.$setUntouched();
		}
		
		//Save Costing Activity Details
		$scope.save = function(costingActivityMapping) {
			$scope.errors = {};
			
			delete(costingActivityMapping.costingActivityCode);
			delete(costingActivityMapping.costingActivityName);
			delete(costingActivityMapping.productCategoryName);
			delete(costingActivityMapping.serviceName);
			
			if($scope.costingActivityMappingForm.$invalid) return false;
			
			//Validate Costing Activity
			if(hasErrors(costingActivityMapping)) return false;
			
			if (costingActivityMapping.id == undefined || costingActivityMapping.id == "" || costingActivityMapping.id == null) {
                //Continue with costingActivityMapping create
				CostingActivityMappingService.create(costingActivityMapping).success(function(response) {
					$scope.formReset();
					$scope.populateList(1, LIST_PAGE_SIZE);
				});
            }
			else {
				//Continue with costingActivityMapping Update
				CostingActivityMappingService.update(costingActivityMapping).success(function(response) {
					$scope.formReset();
					$scope.populateList(1, LIST_PAGE_SIZE);
					
				});
			}
		}
		
		$scope.edit = function(mapping) {
			$scope.costingActivityMapping = mapping;
			$window.scrollTo(0, 0);
		}
		
		//Validate Costing Activity Details
		var hasErrors = function(costingActivityMapping) {
			if (isEmpty(costingActivityMapping.costingActivityId,'costingActivityId')) {
                $scope.errors.costingActivityId = "Costing activity is required";
            }
			if (isEmpty(costingActivityMapping.categoryId,'categoryId')) {
                $scope.errors.categoryId = "Category is required";
            }
			if (isEmpty(costingActivityMapping.serviceId,'serviceId')) {
                $scope.errors.serviceId = "Service is required";
            }
			
			if (Object.getOwnPropertyNames($scope.errors).length > 0) {
				return true;
			}
			return false;
		}
		
		if ($state.current.data.params.costingActivityId !== null) {
            $scope.costingActivityMapping.costingActivityId = $state.current.data.params.costingActivityId;
        }
		
		$scope.upload = function(costingActivityMappingUpload) {
			CostingActivityMappingService.upload(costingActivityMappingUpload)
				.progress(function (evt) {
					var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
					$scope.uploadPercentage = progressPercentage;
					console.log(progressPercentage);
				})
				.success(function (data, status, headers, config) {
					console.log(data);
					$rootScope.$broadcast('showUploadReturnMessage', {data:data, status:status, headers:headers, config:config});
					$scope.costingActivityMappingUpload = {data:{}, file:{}};
				})
				.error(function(error){
					var errorM = "";
					$(error.message).each(function(){
						errorM += this +"<br/>";
					});
					notification.error(errorM)
				});
		}
		
		$scope.$watch('costingActivityMappingUpload.file', function () {
			$scope.costingActivityMappingUpload.selectedFileName = ($scope.costingActivityMappingUpload.file !== undefined) ? $scope.costingActivityMappingUpload.file.name : "";
		});
    }]);