'use strict'

app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {
		$stateProvider
			.state('rl.masters-brand', {
				url: '/masters-brand',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/brand/list.tpl.html'
					}
				},
				controller: 'BrandController',
				data: {
					page: "Brand",
					requireLogin : true
				}
			})
			.state('rl.masters-brand-create', {
				url: '/masters-brand/create',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/brand/save.tpl.html'
					}
				},
				controller: 'BrandController',
				data: {
					page: "Brand Create",
					requireLogin : true
				}
			})
			.state('rl.masters-brand-get', {
				url: '/masters-brand/get/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/brand/save.tpl.html'
					}
				},
				controller: 'BrandController',
				data: {
					page: "Brand Update",
					requireLogin : true
				}
			})
			.state('rl.masters-brand-upload', {
				url: '/masters/brand/bulk-upload',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/brand/bulk_upload.tpl.html'
					}
				},
				controller: 'BrandController',
				data: {
					page: "Masters - Brand : Bulk Upload",
					requireLogin : true
				}
			});
    }]);

app.factory('BrandService', function($http, REST_API_URL, AuthTokenFactory, API, Upload) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.BRAND.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.BRAND.CREATE, {'brand':params});
		},
		get: function(params) {
			var getUrl = API.MASTERS.BRAND.GET.replace('<brandid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.BRAND.UPDATE, {'brand':params});
		},
		del: function(params) {
			var getUrl = API.MASTERS.BRAND.DELETE.replace('<brandid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		byServiceProviderLocation: function(params) {			
			var url = API.MASTERS.BRAND.BY_SERVICE_PROVIDER_LOCATION.replace('<serviceProviderLocationId>', params.serviceProviderLocationId);
			return $http.post(REST_API_URL + url, params);
		},
		upload: function(params) {
			return Upload.upload({
                url: REST_API_URL + API.MASTERS.BRAND.UPLOAD,
                data: params.data,
                file: params.file
            })
		}
	}
});

app.controller('BrandController',
	['$scope', '$rootScope', '$state', 'notification', 'AuthTokenFactory', 'ngDialog', '$location', 'logger', 'BrandService', 'LIST_PAGE_SIZE', 'Utility',
    function($scope, $rootScope, $state,  notification, AuthTokenFactory, ngDialog, $location, logger, BrandService, LIST_PAGE_SIZE, Utility) {
		$scope.pageSize = LIST_PAGE_SIZE;
		$scope.total_count = 0;
		$scope.currentPage = 1;
		
		$scope.breadcrumbs = [
			{
				title: "Master Tables"
			}
		];
		
        $scope.notification = $rootScope.notification;        
        $scope.brands = [];
        $scope.pageActionHeader = "";
		$scope.errors = {};
		$scope.brand = {};
		
		$scope.getBrandList = function(first, limit) {
			$scope.currentPage = first // initialize page no to 1
			
			first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
			BrandService.list({'query':{},firstResult: first, maxResults: limit}).success(function(response) {
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.brands = response.data;
					$scope.total_count = response.totalRecords;
				}
			});
		}
        
        if ($state.current.url == "/masters-brand") {
			$scope.showAddNew = true;
			$scope.addNewUrl = "/masters-brand/create";
			$scope.addNewState = "rl.masters-brand-create"
			
			$scope.showUpload = true;
			$scope.uploadUrl = "/masters/brand/bulk-upload";
			$scope.uploadState = "rl.masters-brand-upload"
			
			$scope.breadcrumbs.push({title:$state.current.data.page, url:$state.current.url});
			$scope.getBrandList(1, $scope.pageSize);
		}
		
		if ($state.current.url == "/masters-brand/create") {
			$scope.breadcrumbs.push({title:"Brands", link:"/masters-brand"});
			$scope.breadcrumbs.push({title:$state.current.data.page, link:$state.current.url});
			$scope.pageActionHeader = "Create new brand";
		}
		
		if ($state.current.url == "/masters-brand/get/:id") {
			$scope.breadcrumbs.push({title:"Brands", link:"/masters-brand"});
			$scope.breadcrumbs.push({title:$state.current.data.page});
			$scope.pageActionHeader = "Update Brand Details";
			$scope.brand.id = $state.current.data.params.id;
			BrandService.get($scope.brand).success(function(response) {
				$scope.brand = response;
			});
		}
		
		if ($state.current.url == "/masters-brand/create" ||
			$state.current.url == "/masters-brand/get/:id") {
			Utility.populate_brands({},$scope);
		}
		
		//Save Brand Details
		$scope.save = function(brand) {
			$scope.errors = {};
			//Validate Brand
			if(hasErrors(brand)) return false;
			
			delete($scope.brand.selectedFileName);
			
			if (brand.id == undefined || brand.id == "" || brand.id == null) {
                //Continue with brand create
				BrandService.create(brand).success(function(response) {
					console.log(response);
					$state.go('rl.masters-brand');
				});
            }
			else {
				//Continue with brand Update
				BrandService.update(brand).success(function(response) {
					console.log(response);
					$state.go('rl.masters-brand');
				});
			}
		}
		
		//Delete Existing Brand
		$scope.deleteBrand = function(brand, index) {
			ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete this brand?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				console.log(brand,index);
				BrandService.del(brand).success(function(response) {
					if (response) {
                        $state.go('rl.masters-brand');
                    }
				});
			}, function (error) {
				// Error logic here
			});
			
		}
		
		//Validate Brand Details
		var hasErrors = function(brand) {
			if (isEmpty(brand.name,'name',"Enter brand name",$scope.notification)) {
                $scope.errors.name = "Enter brand name";
            }
			if (isEmpty(brand.code,'code',"Enter brand code",$scope.notification)) {
                $scope.errors.code = "Enter brand code";
            }
			
			if (Object.getOwnPropertyNames($scope.errors).length > 0) {
				return true;
			}
			return false;
		}
		
		$scope.upload = function(brand) {
			BrandService.upload(brand)
				.progress(function (evt) {
					var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
					$scope.uploadPercentage = progressPercentage;
					console.log(progressPercentage);
				})
				.success(function (data, status, headers, config) {
					console.log(data);
					$rootScope.$broadcast('showUploadReturnMessage', {data:data, status:status, headers:headers, config:config});
					$scope.brand = {data:{}, file:{}};
				})
				.error(function(error){
					var errorM = "";
					$(error.message).each(function(){
						errorM += this +"<br/>";
					});
					notification.error(errorM)
				});
		}
		
		$scope.$watch('brand.file', function () {
			$scope.brand.selectedFileName = ($scope.brand.file !== undefined) ? $scope.brand.file.name : "";
		});
    }]);