'use strict'

app.factory('CityService', function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.CITY.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.CITY.CREATE, {'city':params});
		},
		get: function(params) {
			var getUrl = API.MASTERS.CITY.GET.replace('<cityId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.CITY.UPDATE, {'city':params});
		},
		del: function(params) {
			var getUrl = API.MASTERS.CITY.DELETE.replace('<cityId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		}
	}
});

app.factory('LocalPackingService', function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.LOCAL_PACKING.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.LOCAL_PACKING.CREATE, {'localPacking':params});
		},
		get: function(params) {
			var getUrl = API.MASTERS.LOCAL_PACKING.GET.replace('<cityId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.LOCAL_PACKING.UPDATE, {'localPacking':params});
		},
		del: function(params) {
			var getUrl = API.MASTERS.LOCAL_PACKING.DELETE.replace('<cityId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		}
	}
});

app.factory('OutstationPackingService', function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.OUTSTATION_PACKING.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.OUTSTATION_PACKING.CREATE, {'outstationPacking':params});
		},
		get: function(params) {
			var getUrl = API.MASTERS.OUTSTATION_PACKING.GET.replace('<cityId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.OUTSTATION_PACKING.UPDATE, {'outstationPacking':params});
		},
		del: function(params) {
			var getUrl = API.MASTERS.OUTSTATION_PACKING.DELETE.replace('<cityId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		}
	}
});


app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {
		$stateProvider
			.state('rl.masters-city', {
				url: '/masters-city',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/city/list.tpl.html'
					}
				},
				controller: 'CityController',
				data: {
					page: "Masters - City",
					requireLogin : true
				}
			})
			.state('rl.masters-city-create', {
				url: '/masters-city/create',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/city/save.tpl.html'
					}
				},
				controller: 'CityController',
				data: {
					page: "Masters - City : Create",
					requireLogin : true
				}
			})
			.state('rl.masters-city-get', {
				url: '/masters-city/get/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/city/save.tpl.html'
					}
				},
				controller: 'CityController',
				data: {
					page: "Masters - City : Update",
					requireLogin : true
				}
			})
			.state('rl.masters-local-packing', {
				url: '/masters-localPacking',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/local_packing/list.tpl.html'
					}
				},
				controller: 'CityController',
				data: {
					page: "Masters - Local Packing",
					requireLogin : true
				}
			})
			.state('rl.masters-local-packing-create', {
				url: '/masters/local-packing/create',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/local_packing/save.tpl.html'
					}
				},
				controller: 'CityController',
				data: {
					page: "Masters - Local Packing : Create",
					requireLogin : true
				}
			})
			.state('rl.masters-local-packing-get', {
				url: '/masters/local-packing/get/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/local_packing/save.tpl.html'
					}
				},
				controller: 'CityController',
				data: {
					page: "Masters - Local Packing : Update",
					requireLogin : true
				}
			})
			.state('rl.masters-outstation-packing', {
				url: '/masters-outstationPacking',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/outstation_packing/list.tpl.html'
					}
				},
				controller: 'CityController',
				data: {
					page: "Masters - Outstation Packing",
					requireLogin : true
				}
			})
			.state('rl.masters-outstation-packing-create', {
				url: '/masters/outstation-packing/create',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/outstation_packing/save.tpl.html'
					}
				},
				controller: 'CityController',
				data: {
					page: "Masters - Outstation Packing : Create",
					requireLogin : true
				}
			})
			.state('rl.masters-outstation-packing-get', {
				url: '/masters/outstation-packing/get/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/outstation_packing/save.tpl.html'
					}
				},
				controller: 'CityController',
				data: {
					page: "Masters - Outstation Packing : Update",
					requireLogin : true
				}
			});
    }]);


app.controller('CityController',
	['$scope', '$rootScope', '$state', 'notification', 'AuthTokenFactory', 'ngDialog', '$location', 'logger', 'CityService', 'LocalPackingService', 'OutstationPackingService', 'Utility', 'LIST_PAGE_SIZE',
    function($scope, $rootScope, $state,  notification, AuthTokenFactory, ngDialog, $location, logger, CityService, LocalPackingService, OutstationPackingService, Utility, LIST_PAGE_SIZE) {
		
		$scope.breadcrumbs = [
			{
				title: "Master Tables",
				link: "/masters"
			}
		];
		$scope.breadcrumbs.push({title:$state.current.data.page, link:$state.current.url});
		
        $scope.notification = $rootScope.notification;        
        $scope.cities = [];
		$scope.localPackings = [];
		$scope.outstationPackings = [];
        $scope.pageActionHeader = "";
		$scope.errors = {};
		$scope.city = {};
		$scope.localPacking = {};
		$scope.outstationPacking = {};
		$scope.pageSize = LIST_PAGE_SIZE;
		$scope.total_count = 0;
		$scope.currentPage = 1;
		
		$scope.getCityList = function(first, limit) {
			$scope.currentPage = first // initialize page no to 1
			
			first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
			CityService.list({'query':{},firstResult: first, maxResults: limit}).success(function(response) {
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.cities = response.data;
					$scope.total_count = response.totalRecords;
				}
			});
		}
		
		$scope.getLocalPackingList = function(first, limit) {
			$scope.currentPage = first // initialize page no to 1
			
			first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
			LocalPackingService.list({'query':{},firstResult: first, maxResults: limit}).success(function(response) {
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.localPackings = response.data;
					$scope.total_count = response.totalRecords;
				}
			});
		}
		
		$scope.getOutstationPackingList = function(first, limit) {
			$scope.currentPage = first // initialize page no to 1
			
			first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
			OutstationPackingService.list({'query':{},firstResult: first, maxResults: limit}).success(function(response) {
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.outstationPackings = response.data;
					$scope.total_count = response.totalRecords;
				}
			});
		}
        
        if ($state.current.url == "/masters-city") {
			$scope.showAddNew = true;
			$scope.addNewUrl = "/masters-city/create";
			$scope.addNewState = "rl.masters-city-create"
			$scope.getCityList(1, $scope.pageSize);
		}
		
		if ($state.current.url == "/masters-localPacking") {
			$scope.showAddNew = true;
			$scope.addNewUrl = "/masters/local-packing/create";
			$scope.addNewState = "rl.masters-local-packing-create"
			$scope.getLocalPackingList(1, $scope.pageSize);
		}
		
		if ($state.current.url == "/masters-outstationPacking") {
			$scope.showAddNew = true;
			$scope.addNewUrl = "/masters/outstation-packing/create";
			$scope.addNewState = "rl.masters-outstation-packing-create"
			$scope.getOutstationPackingList(1, $scope.pageSize);
		}
		
		if ($state.current.url == "/masters-city/create") {
			$scope.pageActionHeader = "Create new city";
		}
		
		if ($state.current.url == "/masters/local-packing/create") {
			$scope.categories = [];
			Utility.populate_category({},$scope);
			$scope.pageActionHeader = "Create new local packing";
			
		}
		
		if ($state.current.url == "/masters/outstation-packing/create") {
			$scope.categories = [];
			Utility.populate_category({},$scope);
			$scope.pageActionHeader = "Create new outstation packing";
			
		}
		
		
		if ($state.current.url == "/masters-city/get/:id") {
			$scope.pageActionHeader = "Update City Details";
			$scope.city.id = $state.current.data.params.id;
			CityService.get($scope.city).success(function(response) {
				$scope.city = response;
				console.log(response + $scope.city.id);
			});
		}
		
		if ($state.current.url == "/masters/local-packing/get/:id") {
			$scope.pageActionHeader = "Update Local Packing";
			$scope.localPacking.id = $state.current.data.params.id;
			$scope.categories = [];
			Utility.populate_category({},$scope);
			LocalPackingService.get($scope.localPacking).success(function(response) {
				$scope.localPacking = response;
			});
		}
		
		if ($state.current.url == "/masters/outstation-packing/get/:id") {
			$scope.pageActionHeader = "Update Outstation Packing";
			$scope.outstationPacking.id = $state.current.data.params.id;
			$scope.categories = [];
			Utility.populate_category({},$scope);
			OutstationPackingService.get($scope.outstationPacking).success(function(response) {
				$scope.outstationPacking = response;
			});
		}
		
		if ($state.current.url == "/masters-city/create") {
			//Changes are here < || url =
			$scope.breadcrumbs.push({title:$state.current.data.page, link:$state.current.url});
		}
		
		//Save City Details
		console.log($state.current);
		$scope.save = function(city) {
			console.log($scope.state);
			$scope.errors = {};
			//Validate City
			if(hasErrors(city)) return false;
			console.log(city.id);
			
			if (city.id == undefined || city.id == "" || city.id == null) {
                //Continue with city create
				CityService.create(city).success(function(response) {
					console.log(response);
					$state.go('rl.masters-city');
				});
            }
			else {
				//Continue with city Update
				CityService.update(city).success(function(response) {
					console.log(response);
					$state.go('rl.masters-city');
				});
			}
		}
		
		//Save Local Packing
		$scope.savelocalPacking = function(localPacking) {
			
			if (localPacking.id == undefined || localPacking.id == "" || localPacking.id == null) {
                //Continue with city create
				LocalPackingService.create(localPacking).success(function(response) {
					console.log(response);
					$state.go('rl.masters-local-packing');
				});
            }
			else {
				//Continue with city Update
				LocalPackingService.update(localPacking).success(function(response) {
					console.log(response);
					$state.go('rl.masters-local-packing');
				});
			}
		}
		
		//Save Outstation Packing
		$scope.saveOutstationPacking = function(outstationPacking) {
			
			if (outstationPacking.id == undefined || outstationPacking.id == "" || outstationPacking.id == null) {
                //Continue with city create
				OutstationPackingService.create(outstationPacking).success(function(response) {
					console.log(response);
					$state.go('rl.masters-outstation-packing');
				});
            }
			else {
				//Continue with city Update
				OutstationPackingService.update(outstationPacking).success(function(response) {
					console.log(response);
					$state.go('rl.masters-outstation-packing');
				});
			}
		}
		
		//Delete Existing City
		$scope.deleteCity = function(city, index) {
			ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete this city?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				console.log(city,index);
				CityService.del(city).success(function(response) {
					if (response) {
                        $state.go('rl.masters-city');
                    }
				});
			}, function (error) {
				// Error logic here
			});
			
		}
		
		
		//Delete Local Packing
		$scope.deleteLocalPacking = function(localPacking, index) {
			ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete this record?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				console.log(localPacking,index);
				LocalPackingService.del(localPacking).success(function(response) {
					if (response) {
                        $state.go('rl.masters-local-packing');
                    }
				});
			}, function (error) {
				// Error logic here
			});
			
		}
		
		//Delete Outstation Packing
		$scope.deleteOutstationPacking = function(outstationPacking, index) {
			ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete this record?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				console.log(outstationPacking,index);
				OutstationPackingService.del(outstationPacking).success(function(response) {
					if (response) {
                        $state.go('rl.masters-outstation-packing');
                    }
				});
			}, function (error) {
				// Error logic here
			});
			
		}
		
		//Validate City Details
		var hasErrors = function(city) {
			if (isEmpty(city.name,'name',"Enter city name",$scope.notification)) {
                $scope.errors.name = "Enter city name";
            }
			
			if (Object.getOwnPropertyNames($scope.errors).length > 0) {
				return true;
			}
			return false;
		}
    }]);