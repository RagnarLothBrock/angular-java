'use strict';

app.factory('SMSEmailTemplate', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.SMS_EMAIL_TEMPLATE.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.SMS_EMAIL_TEMPLATE.CREATE, {'smsEmailTemplate':params});
		},
		get: function(params) {
			var getUrl = API.MASTERS.SMS_EMAIL_TEMPLATE.GET.replace('<smsEmailTemplateId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.SMS_EMAIL_TEMPLATE.UPDATE, {'smsEmailTemplate':params});
		},
		del: function(params) {
			var getUrl = API.MASTERS.SMS_EMAIL_TEMPLATE.DELETE.replace('<smsEmailTemplateId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		}
	}
}]);

app.config(['$stateProvider', function($stateProvider) {
	$stateProvider
		.state('rl.masters-sms-email-template', {
			url: '/masters/sms-email-template',
			views: {
				'page-main-content@': {
					templateUrl: 'partials/tpl/pages/masters/sms_email_template/list.tpl.html'
				}
			},
			controller: 'SMSEmialTemplateController',
			data: {
				page: "Masters - SMS/Emial Template",
				requireLogin : true
			}
		})
		.state('rl.masters-sms-email-template-create', {
			url: '/masters/sms-email-template/create',
			views: {
				'page-main-content@': {
					templateUrl: 'partials/tpl/pages/masters/sms_email_template/save.tpl.html'
				}
			},
			controller: 'SMSEmialTemplateController',
			data: {
				page: "Masters - SMS/Emial Template : Create",
				requireLogin : true
			}
		})
		.state('rl.masters-sms-email-template-get', {
			url: '/masters/sms-email-template/get/:id',
			views: {
				'page-main-content@': {
					templateUrl: 'partials/tpl/pages/masters/sms_email_template/save.tpl.html'
				}
			},
			controller: 'SMSEmialTemplateController',
			data: {
				page: "Masters - SMS/Emial Template : Update",
				requireLogin : true
			}
		});
    }]);


app.controller('SMSEmialTemplateController',
	['$scope', '$state', 'ngDialog', 'SMSEmailTemplate', 'Utility', 'LIST_PAGE_SIZE', '$rootScope',
	 function($scope, $state, ngDialog, SMSEmailTemplate, Utility, LIST_PAGE_SIZE, $rootScope) {
				
		$scope.pageSize = LIST_PAGE_SIZE;
		$scope.total_count = 0;
		$scope.currentPage = 1;
		
		$scope.breadcrumbs = [
			{
				title: "Master Tables"
			},
			{
				title: "SMS/Email Template",
				link: "/masters/sms-email-template"
			}
		];
		
        $scope.smsEmailTemplateList = [];
        $scope.pageActionHeader = "";
		$scope.errors = {};
		$scope.smsEmailTemplate = {};
		$scope.smsCKEditorOptions = {
			height: 100,
			toolbarCanCollapse:true,
			placeholder: "Enter Email Template",
			toolbarGroups : [
				{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
				{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
				{ name: 'links', groups: [ 'links' ] },
				{ name: 'insert', groups: [ 'insert' ] },
				{ name: 'forms', groups: [ 'forms' ] },
				{ name: 'tools', groups: [ 'tools' ] },
				{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
				{ name: 'others', groups: [ 'others' ] },
				'/',
				{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
				{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
				{ name: 'styles', groups: [ 'styles' ] },
				{ name: 'colors', groups: [ 'colors' ] },
				{ name: 'about', groups: [ 'about' ] }
			],
			removeButtons : 'Underline,Subscript,Superscript,Link,Unlink,Anchor,Image,Table,HorizontalRule,SpecialChar,Maximize,Strike,RemoveFormat,NumberedList,BulletedList,Blockquote,Outdent,Indent,About'
		};
		
		$scope.getSmsEmailTemplateList = function(first, limit) {
			$scope.currentPage = first // initialize page no to 1
			
			first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
			SMSEmailTemplate.list({'query':{},firstResult: first, maxResults: limit}).success(function(response) {
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.smsEmailTemplateList = response.data;
					$scope.total_count = response.totalRecords;
				}
			});
		}
        
        if ($state.current.url == "/masters/sms-email-template") {
			$scope.getSmsEmailTemplateList(1, $scope.pageSize);
			
			$scope.showAddNew = true;
			$scope.addNewUrl = "/masters/sms-email-template/create";
			$scope.addNewState = "rl.masters-sms-email-template-create"
		}
		
		if ($state.current.url == "/masters/sms-email-template/create") {
			$scope.pageActionHeader = "Create New SMS/Email Template";
			$scope.breadcrumbs.push({title:"Create"});
		}
		
		if ($state.current.url == "/masters/sms-email-template/get/:id") {
			$scope.pageActionHeader = "Update SMS/Emial Template Details";
			
			$scope.breadcrumbs.push({title:"Update"});
			$scope.smsEmailTemplate.id = $state.current.data.params.id;
			SMSEmailTemplate.get($scope.smsEmailTemplate).success(function(response) {
				$scope.smsEmailTemplate = response;
			});
		}
		
		if ($state.current.url == "/masters/sms-email-template/create" ||
			$state.current.url == "/masters/sms-email-template/get/:id") {
			Utility.populate_sms_email_trigger_code({}, $scope);
		}
		
		//Save SMSEmialTrigger Details
		$scope.save = function(smsEmailTemplate) {
			$scope.errors = {};
			//Validate SMSEmialTrigger
			if($scope.SmsEmailTemplateForm.$invalid) return false;
			
			if (smsEmailTemplate.id == undefined || smsEmailTemplate.id == "" || smsEmailTemplate.id == null) {
                //Continue with smsEmailTemplate create
				SMSEmailTemplate.create(smsEmailTemplate).success(function(response) {
					console.log(response);
					$state.go('rl.masters-sms-email-template');
				});
            }
			else {
				//Continue with smsEmailTemplate Update
				SMSEmailTemplate.update(smsEmailTemplate).success(function(response) {
					console.log(response);
					$state.go('rl.masters-sms-email-template');
				});
			}
		}
		
		//Delete Existing SMSEmialTrigger
		$scope.deleteSMSEmialTrigger = function(smsEmailTemplate, index) {
			ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete this SMS/Email Template?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				console.log(smsEmailTemplate,index);
				SMSEmailTemplate.del(smsEmailTemplate).success(function(response) {
					if (response) {
                        $state.go('rl.masters-sms-email-template');
                    }
				});
			}, function (error) {
				// Error logic here
			});
		}
		
		$scope.populateSMSEmailTemplate = function(triggerCode) {
			$scope.smsEmailTemplate.params = "";
			$scope.smsEmailTemplate.emailTemplate = "";
			$scope.smsEmailTemplate.smsTemplate = "";
			SMSEmailTemplate
            .list({query:{'smsEmailTriggerCode':triggerCode}})
            .success(function(response) {
                if (response !== undefined && response.data !== undefined && response.data.length > 0){
                    $scope.smsEmailTemplate = response.data[0];
					if ($scope.smsEmailTemplate.params.indexOf(",") !== -1) {
                        $scope.paramsList = $scope.smsEmailTemplate.params.split(",");
                    }
                }
            });
		}
		
		$scope.$on('populateSMSEmailTemplate', function(e, args){
			$scope.populateSMSEmailTemplate(args.triggerCode);
		});
    }]);