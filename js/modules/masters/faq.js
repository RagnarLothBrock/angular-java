'use strict'

app.factory('FAQService', function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			var getUrl = API.MASTERS.FAQ.LISTING
						.replace('<firstResult>', params.firstResult)
						.replace('<maxResults>', params.maxResults);
			return $http.post(REST_API_URL + getUrl, {});
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.FAQ.CREATE, {'fAQ':params});
		},
		get: function(params) {
			var getUrl = API.MASTERS.FAQ.GET.replace('<faqId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.FAQ.UPDATE, {'fAQ':params});
		},
		del: function(params) {
			var getUrl = API.MASTERS.FAQ.DELETE.replace('<faqId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		}
	}
});


app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {
		$stateProvider
			.state('rl.masters-faqs', {
				url: '/masters-faqs',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/faq/list.tpl.html'
					}
				},
				controller: 'FAQController',
				data: {
					page: "Masters - FAQ",
					requireLogin : true
				}
			})
			.state('rl.masters-faq-create', {
				url: '/masters-faq/create',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/faq/save.tpl.html'
					}
				},
				controller: 'FAQController',
				data: {
					page: "Masters - FAQ : Create",
					requireLogin : true
				}
			})
			.state('rl.masters-faq-get', {
				url: '/masters-faq/get/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/faq/save.tpl.html'
					}
				},
                controller: 'FAQController',
				data: {
					page: "Masters - FAQ : Update",
					requireLogin : true
				}
			});
    }]);


app.controller('FAQController',
	['$scope', '$rootScope', '$state', 'notification', 'AuthTokenFactory', 'ngDialog', '$location', 'logger', 'FAQService', 'Utility', 'LIST_PAGE_SIZE',
    function($scope, $rootScope, $state,  notification, AuthTokenFactory, ngDialog, $location, logger, FAQService, Utility, LIST_PAGE_SIZE) {
		
		$scope.pageSize = LIST_PAGE_SIZE;
		$scope.total_count = 0;
		$scope.currentPage = 1;
		
		$scope.breadcrumbs = [
			{
				title: "Master Tables"
			},
			{
				title: "FAQ",
				link: "/masters-faqs"
			}
		];
		
        $scope.notification = $rootScope.notification;        
        $scope.faqs = [];
        $scope.pageActionHeader = "";
		$scope.errors = {};
		$scope.faq = {};
        $scope.products = [];
        $scope.categories = [];
        $scope.subCategories = [];
        $scope.brands = [];
        $scope.selectionType = 'category';
		
		$scope.getFAQList = function(first, limit) {
			$scope.currentPage = first // initialize page no to 1
			
			first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
			FAQService.list({'query':{},firstResult: first, maxResults: limit}).success(function(response) {
				
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.faqs = response.data;
					$scope.total_count = response.totalRecords;
				}
			});
		}
        
        if ($state.current.url == "/masters-faqs") {
			$scope.showAddNew = true;
			$scope.addNewUrl = "/masters-faq/create";
			$scope.addNewState = "rl.masters-faq-create"
			
			$scope.getFAQList(1, $scope.pageSize);
		}
		
		if ($state.current.url == "/masters-faq/create") {
			$scope.pageActionHeader = "Create new faq";
			$scope.breadcrumbs.push({title: "Create"});
		}
		
		if ($state.current.url == "/masters-faq/get/:id") {
			$scope.pageActionHeader = "Update FAQ Details";
			$scope.breadcrumbs.push({title: "Update"});
			$scope.faq.id = $state.current.data.params.id;
			FAQService.get($scope.faq).success(function(response) {
				$scope.faq = response;
                
                if ($scope.faq.categoryId != null && $scope.faq.categoryId != "") {
                    $scope.selectionType = 'category';
                }
                if ($scope.faq.productId != null && $scope.faq.productId != "") {
                    $scope.selectionType = 'product';
                }
                if ($scope.faq.categoryId != null && $scope.faq.brandId && $scope.faq.categoryId != "" && $scope.faq.brandId != "") {
                    $scope.selectionType = 'categoryBrand';
                }
                if ($scope.faq.subCategoryId != null && $scope.faq.brandId && $scope.faq.subCategoryId != "" && $scope.faq.brandId != "") {
                    $scope.selectionType = 'subCategoryBrand';
                }
			});
		}
		
		if ($state.current.url == "/masters-faq/create" ||
			$state.current.url == "/masters-faq/get/:id") {
			//Utility.populate_products({'retailerId':$state.current.data.params.id},$scope);
            Utility.populate_category({},$scope);
            Utility.populate_sub_category({},$scope);
            Utility.populate_brands({},$scope);
            
             // Editor options.
            $scope.options = {
                language: 'en',
                allowedContent: true,
                entities: false
            };
		}
        
        // Called when the editor is completely ready.
        $scope.onReady = function () {
            // ...
        };
		
		//Save FAQ Details
		$scope.save = function(faq) {
			$scope.errors = {};
			//Validate FAQ
			if(hasErrors(faq)) return false;
            
            var faqObj = {id: faq.id, faq : faq.faq};
            switch ($scope.selectionType) {
                case 'category' :
                    faqObj.categoryId = faq.categoryId;
                    break;
                
                case 'product' :
                    faqObj.productId = faq.productId;
                    break;
                
                case 'categoryBrand' :
                    faqObj.categoryId = faq.categoryId;
                    faqObj.brandId = faq.brandId;
                    break;
                
                case 'subCategoryBrand' :
                    faqObj.subCategoryId = faq.subCategoryId;
                    faqObj.brandId = faq.brandId;
                    break;
            }
			
			if (faqObj.id == undefined || faqObj.id == "" || faqObj.id == null) {
                //Continue with faqObj create
				FAQService.create(faqObj).success(function(response) {
					console.log(response);
					$state.go('rl.masters-faqs');
				});
            }
			else {
				//Continue with faqObj Update
				FAQService.update(faqObj).success(function(response) {
					console.log(response);
					$state.go('rl.masters-faqs');
				});
			}
		}
		
		//Delete Existing FAQ
		$scope.deleteFAQ = function(faq, index) {
			ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete this faq?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				console.log(faq,index);
				FAQService.del(faq).success(function(response) {
					if (response) {
                        $state.go('rl.masters-faqs');
                    }
				});
			}, function (error) {
				// Error logic here
			});
			
		}
		
        $scope.isAcceptable = false;
		//Validate FAQ Details
		var hasErrors = function(faq) {
			if (isEmpty(faq.faq,'faq')) {
                $scope.errors.faq = "Enter faq";
            }
            
            if (isEmpty(faq.categoryId,'categoryId') &&
                isEmpty(faq.subCategoryId,'subCategoryId') &&
                isEmpty(faq.brandId,'brandId') &&
                isEmpty(faq.productId,'productId')) {
                $scope.errors.required = "Required";
            }
			
			if (Object.getOwnPropertyNames($scope.errors).length > 0) {
				return true;
			}
			return false;
		}
    }]);