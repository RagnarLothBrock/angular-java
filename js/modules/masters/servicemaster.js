'use strict';

app.factory('ServiceMaster', function($http, REST_API_URL, AuthTokenFactory, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.SERVICES.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.SERVICES.CREATE, {'servicemaster':params});
		},
		get: function(params) {
			var getUrl = API.MASTERS.SERVICES.GET.replace('<servicemasterid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.SERVICES.UPDATE, {'servicemaster':params});
		},
		del: function(params) {
			var getUrl = API.MASTERS.SERVICES.DELETE.replace('<servicemasterid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		byRetailer: function(params) {
			var url = API.MASTERS.SERVICES.BY_RETAILER.replace('<retailerid>', params.id);
			return $http.post(REST_API_URL + url, params);
		},
		forRetailer: function(retailerId) {
			var url = API.MASTERS.SERVICES.FOR_RETAILER.replace('<retailerid>', retailerId);
			return $http.post(REST_API_URL + url, {});
		}
	}
});