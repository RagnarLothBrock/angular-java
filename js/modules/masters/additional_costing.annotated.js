'use strict'

app.factory('AdditionalCostingService', ["$http", "REST_API_URL", "API", function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.ADDITIONAL_COSTING.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.ADDITIONAL_COSTING.CREATE, {'additionalCosting':params});
		},
		get: function(params) {
			var getUrl = API.MASTERS.ADDITIONAL_COSTING.GET.replace('<id>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.ADDITIONAL_COSTING.UPDATE, {'additionalCosting':params});
		},
		del: function(params) {
			var getUrl = API.MASTERS.ADDITIONAL_COSTING.DELETE.replace('<id>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		}
	}
}]);


app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {
		$stateProvider
			.state('rl.masters-additional-costing', {
				url: '/masters/additional-costing',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/additional_costing/list.tpl.html'
					}
				},
				controller: 'AdditionalCostingController',
				data: {
					page: "Masters - Additional Costing",
					requireLogin : true
				}
			})
			.state('rl.masters-additional-costing-create', {
				url: '/masters/additional-costing/create',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/additional_costing/save.tpl.html'
					}
				},
				controller: 'AdditionalCostingController',
				data: {
					page: "Masters - Additional Costing : Create",
					requireLogin : true
				}
			})
			.state('rl.masters-additional-costing-get', {
				url: '/masters/additional-costing/get/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/additional_costing/save.tpl.html'
					}
				},
				controller: 'AdditionalCostingController',
				data: {
					page: "Masters - Additional Costing : Update",
					requireLogin : true
				}
			});
    }]);


app.controller('AdditionalCostingController',
	['$scope', '$rootScope', '$state', 'notification', 'AuthTokenFactory', 'ngDialog', '$location', 'logger', 'AdditionalCostingService', 'LIST_PAGE_SIZE',
    function($scope, $rootScope, $state,  notification, AuthTokenFactory, ngDialog, $location, logger, AdditionalCostingService, LIST_PAGE_SIZE) {
		
		$scope.breadcrumbs = [
			{
				title: "Master Tables",
				link: "/masters"
			}
		];
		$scope.breadcrumbs.push({title:"Additional Costing", link:"/masters/additional-costing"});
		
        $scope.notification = $rootScope.notification;        
        $scope.additionalCostingList = [];
        $scope.pageActionHeader = "";
		$scope.errors = {};
		$scope.additionalCosting = {};
		$scope.pageSize = LIST_PAGE_SIZE;
		$scope.total_count = 0;
		$scope.currentPage = 1;
		
		$scope.getAdditionalCostingList = function(first, limit) {
			$scope.currentPage = first // initialize page no to 1
			
			first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
			AdditionalCostingService.list({'query':{},firstResult: first, maxResults: limit}).success(function(response) {
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.additionalCostingList = response.data;
					$scope.total_count = response.totalRecords;
				}
			});
		}
        
        if ($state.current.url == "/masters/additional-costing") {
			$scope.showAddNew = true;
			$scope.addNewUrl = "/masters/additional-costing/create";
			$scope.addNewState = "rl.masters-additional-costing-create"
			$scope.getAdditionalCostingList(1, $scope.pageSize);
		}
		
		if ($state.current.url == "/masters/additional-costing/create") {
			$scope.pageActionHeader = "Create new Additional Costing";
		}
		
		if ($state.current.url == "/masters/additional-costing/get/:id") {
			$scope.pageActionHeader = "Update Additional Costing Details";
			$scope.additionalCosting.id = $state.current.data.params.id;
			AdditionalCostingService.get($scope.additionalCosting).success(function(response) {
				$scope.additionalCosting = response;
			});
		}
		
		if ($state.current.url == "/masters/additional-costing/create" ||
			$state.current.url == "/masters/additional-costing/get/:id") {
			
			$scope.breadcrumbs.push({title:$state.current.data.page, link:$state.current.url});
		}
		
		//Save AdditionalCosting Details
		$scope.save = function(additionalCosting) {
			$scope.errors = {};
			//Validate AdditionalCosting
			if($scope.additionalCostingForm.$invalid) return false;
			
			if (additionalCosting.id == undefined || additionalCosting.id == "" || additionalCosting.id == null) {
                //Continue with additionalCosting create
				AdditionalCostingService.create(additionalCosting).success(function(response) {
					console.log(response);
					$state.go('rl.masters-additional-costing');
				});
            }
			else {
				//Continue with additionalCosting Update
				AdditionalCostingService.update(additionalCosting).success(function(response) {
					console.log(response);
					$state.go('rl.masters-additional-costing');
				});
			}
		}
		
		//Delete Existing AdditionalCosting
		$scope.deleteCity = function(additionalCosting, index) {
			ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete this additional costing?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				console.log(additionalCosting,index);
				AdditionalCostingService.del(additionalCosting).success(function(response) {
					if (response) {
                        $state.go('rl.masters-additional-costing');
                    }
				});
			}, function (error) {
				// Error logic here
			});
			
		}
    }]);