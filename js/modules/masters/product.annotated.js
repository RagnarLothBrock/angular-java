'use strict';

app.factory('Product', ["$http", "REST_API_URL", "API", "Upload", function($http, REST_API_URL, API, Upload) {

	return {
		list: function(params) {
			var getUrl = API.MASTERS.PRODUCT.LIST.replace('<first>', params.firstResult).replace('<limit>', params.maxResults);
			return $http.post(REST_API_URL + getUrl, params);
			//return $http.post(REST_API_URL + API.MASTERS.PRODUCT.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.PRODUCT.CREATE, {'product':params});
		},
		get: function(params) {
			var getUrl = API.MASTERS.PRODUCT.GET.replace('<productid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.PRODUCT.UPDATE, {'product':params});
		},
		del: function(params) {
			var getUrl = API.MASTERS.PRODUCT.DELETE.replace('<productid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		upload: function(params) {
			return Upload.upload({
                url: REST_API_URL + API.MASTERS.PRODUCT.UPLOAD,
                data: params.data,
                file: params.file
            })
		}
	}
}]);

app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {
		$stateProvider
			
			.state('rl.product-bulk-upload', {
				url: '/product/upload',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/product/bulk_upload.tpl.html'
					}
				},
				controller: 'ProductController',
				data: {
					page: "Product : Bulk Upload",
					requireLogin : true
				}
			});
    }]);

app.controller('ProductController',
	['$scope', '$rootScope', '$state', 'notification', 'AuthTokenFactory', 'ngDialog', '$location', 'logger',
		'Product', 'PackagingType', 'ProductSubCategory', 'BrandService', 'ProductCategory', 'LIST_PAGE_SIZE', 'Utility',
    function($scope, $rootScope, $state,  notification, AuthTokenFactory, ngDialog, $location, logger,
			 Product, PackagingType, ProductSubCategory, BrandService, ProductCategory, LIST_PAGE_SIZE, Utility) {
		
		$scope.pageSize = LIST_PAGE_SIZE;
		$scope.total_count = 0;
		$scope.currentPage = 1;
		
		$scope.breadcrumbs = [
			{title: "Admin"},
			{title: "Masters"},
			{title: "Product Upload", link: "/product/upload"}
		];
		
		$scope.uploadPercentage = 0;
        $scope.notification = $rootScope.notification;        
        $scope.products = [];
        $scope.pageActionHeader = "";
		$scope.errors = {};
		$scope.product = {};
		
		Utility.populate_retailer({}, $scope);
		
		$scope.upload = function(product) {
			Product.upload(product)
				.progress(function (evt) {
					var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
					$scope.uploadPercentage = progressPercentage;
					console.log(progressPercentage);
				})
				.success(function (data, status, headers, config) {
					$rootScope.$broadcast('showUploadReturnMessage', {data:data, status:status, headers:headers, config:config});
					$scope.product = {data:{}, file:{}};
				})
				.error(function(error){
					var errorM = "";
					$(error.message).each(function(){
						errorM += this +"<br/>";
					});
					notification.error(errorM)
				});
		}
		
		$scope.$watch('product.file', function () {
			$scope.product.selectedFileName = ($scope.product.file !== undefined) ? $scope.product.file.name : "";
		});
    }]);