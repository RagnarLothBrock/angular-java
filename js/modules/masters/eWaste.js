'use strict';

app.factory('EwasteProductCategory', function($http, REST_API_URL, AuthTokenFactory, API, Upload) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.EWASTE_PRODUCT_CATEGORY.LIST,params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.EWASTE_PRODUCT_CATEGORY.CREATE, {'ewasteProductCategory':params});
		},
		get: function(params) {
			var getUrl = API.MASTERS.EWASTE_PRODUCT_CATEGORY.GET.replace('<productCategoryid>',params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.EWASTE_PRODUCT_CATEGORY.UPDATE, {'ewasteProductCategory':params});
		},
		del: function(params) {
			var getUrl = API.MASTERS.EWASTE_PRODUCT_CATEGORY.DELETE.replace('<productCategoryid>',params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
	}
});


app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {
		$stateProvider
			.state('rl.masters-ewaste-product-category', {
				url: '/masters/ewaste-product-category',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/eWaste/list.tpl.html'
					}
				},
				controller: 'EwasteProductCategoryController',
				data: {
					page: "Masters - Ewaste Product Category",
					requireLogin : true
				}
			})
			.state('rl.masters-ewaste-product-category-create', {
				url: '/masters/ewaste-product-category/create',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/eWaste/save.tpl.html'
					}
				},
				controller: 'EwasteProductCategoryController',
				data: {
					page: "Masters - Ewaste Product Category : Create",
					requireLogin : true
				}
			})
			.state('rl.masters-ewaste-product-category-get', {
				url: '/masters/ewaste-product-category/get/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/eWaste/save.tpl.html'
					}
				},
				controller: 'EwasteProductCategoryController',
				data: {
					page: "Masters - Ewaste Product Category : Update",
					requireLogin : true
				}
			})
    }]);

app.controller('EwasteProductCategoryController',
	['$scope', '$rootScope', '$state', 'notification', 'AuthTokenFactory', 'ngDialog', '$location', 'logger', 'Utility', 'MasterCategory', 'LIST_PAGE_SIZE','EwasteProductCategory',
    function($scope, $rootScope, $state,  notification, AuthTokenFactory, ngDialog, $location, logger, Utility, MasterCategory, LIST_PAGE_SIZE,EwasteProductCategory) {
		
		$scope.breadcrumbs = [
			{
				title: "Master Tables"
			},
			{
				title: "Ewaste Product Category",
				link: '/masters/ewaste-product-category'
			}
		];
		
		$scope.pageSize = LIST_PAGE_SIZE;
		$scope.total_count = 0;
		$scope.currentPage = 1;
		
        $scope.notification = $rootScope.notification;        
        $scope.productCategories = [];
        $scope.pageActionHeader = "";
		$scope.errors = {};
		$scope.productCategory = {};
		
		$scope.getEwasteCategoryList = function(first, limit) {
			$scope.currentPage = first // initialize page no to 1
			
			first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
			EwasteProductCategory.list({'query':{},firstResult: first, maxResults: limit}).success(function(response) {
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.productCategories = response.data;
					$scope.total_count = response.totalRecords;
				}
			});
		}

		// $scope.getProductCategoryList = function(first, limit) {
		// 	$scope.currentPage = first // initialize page no to 1
			
		// 	// newPageNumber = $rootScope.calculateFirstRecord($scope.currentPage, pageSize);
		// 	// console.log("category :"+newPageNumber);
		// 	first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
		// 	EwasteProductCategory.list({'query':{},firstResult: first, maxResults: limit}).success(function(response) {
		// 		if (response !== undefined && response.data !== undefined && response.data.length > 0){
		// 			$scope.productCategories = response.data;
		// 			$scope.total_count = response.totalRecords;
		// 		}
		// 	});
		// }
        
        if ($state.current.url == "/masters/ewaste-product-category") {
			$scope.showAddNew = true;
			$scope.addNewUrl = "/masters/ewaste-product-category/create";
			$scope.addNewState = "rl.masters-ewaste-product-category-create"
			
			$scope.getEwasteCategoryList(1, $scope.pageSize);
		}
		
		if ($state.current.url == "/masters/ewaste-product-category/create") {
			$scope.pageActionHeader = "Create new product category";
			$scope.breadcrumbs.push({title:"Create"});
		}
		
		if ($state.current.url == "/masters/ewaste-product-category/get/:id") {
			$scope.pageActionHeader = "Update Ewast product category details";
			$scope.breadcrumbs.push({title:"Update"});
			$scope.productCategory.id = $state.current.data.params.id;
			console.log("$scope.productCategory",$scope.productCategory);
			EwasteProductCategory.get($scope.productCategory).success(function(response) {
				$scope.productCategory = response;
			});
		}
		
		if ($state.current.url == "/masters/ewaste-product-category/create" ||
			$state.current.url == "/masters/ewaste-product-category/get/:id") {
			
			Utility.populate_ewaste_category({},$scope);
		}
		
		//Save Product Category Details
		$scope.save = function(productCategory) {
			$scope.errors = {};
			delete(productCategory.selectedFileName);
			if (productCategory.id == undefined || productCategory.id == "" || productCategory.id == null) {
				console.log("inside save",productCategory);
                //Continue with productCategory create
				EwasteProductCategory.create(productCategory).success(function(response) {
					console.log("qwety",response);
					$state.go('rl.masters-ewaste-product-category');
				});
            }
			else {
				//Continue with productCategory Update
				EwasteProductCategory.update(productCategory).success(function(response) {
					console.log(response);
					$state.go('rl.masters-ewaste-product-category');
				});
			}
		}
		
		//Delete Existing Sub Category
		$scope.deleteProductCategory = function(productCategory, index) {
			ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete this product category?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				console.log("qwertjgf",productCategory,index);
				EwasteProductCategory.del(productCategory).success(function(response) {
					if (response) {
						console.log("dele",response);
                        $state.go('rl.masters-ewaste-product-category');
                    }
				});
			}, function (error) {
				// Error logic here
			});
			
		}
		
		$scope.$watch('productCategory.file', function () {
			$scope.productCategory.selectedFileName = ($scope.productCategory.file !== undefined) ? $scope.productCategory.file.name : "";
		});
    }]);