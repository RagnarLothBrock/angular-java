'use strict';

app.factory('SMSEmailTriggerConfig', function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.SMS_EMAIL_TRIGGER_CONFIG.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.SMS_EMAIL_TRIGGER_CONFIG.CREATE, {'smsemailtriggerconfig':params});
		},
		get: function(params) {
			var getUrl = API.MASTERS.SMS_EMAIL_TRIGGER_CONFIG.GET.replace('<smsEmailTriggerConfigId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.SMS_EMAIL_TRIGGER_CONFIG.UPDATE, {'smsemailtriggerconfig':params});
		},
		del: function(params) {
			var getUrl = API.MASTERS.SMS_EMAIL_TRIGGER_CONFIG.DELETE.replace('<smsEmailTriggerConfigId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		config_list_by_service: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.SMS_EMAIL_TRIGGER_CONFIG.CONFIG_LIST_BY_SERVICE, params);
		},
		bulkcreate:function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.SMS_EMAIL_TRIGGER_CONFIG.BULK_CREATE, {'smsEmailTriggerConfigList':params});
		},
		bulkupdate:function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.SMS_EMAIL_TRIGGER_CONFIG.BULK_UPDATE, {'smsEmailTriggerConfigList':params});
		},
	}
});

app.config(['$stateProvider', function($stateProvider) {
	$stateProvider
		.state('rl.masters-sms-email-trigger-config', {
			url: '/masters/sms-email-trigger-config',
			views: {
				'page-main-content@': {
					templateUrl: 'partials/tpl/pages/masters/sms_email_trigger_config/save.tpl.html'
				}
			},
			controller: 'SMSEmialTriggerConfigController',
			data: {
				page: "Masters - SMS/Emial Trigger Config",
				requireLogin : true
			}
		})
		.state('rl.masters-sms-email-trigger-config-create', {
			url: '/masters/sms-email-trigger-config/create',
			views: {
				'page-main-content@': {
					templateUrl: 'partials/tpl/pages/masters/sms_email_trigger_config/save.tpl.html'
				}
			},
			controller: 'SMSEmialTriggerConfigController',
			data: {
				page: "Masters - SMS/Emial Trigger Config : Create",
				requireLogin : true
			}
		})
		.state('rl.masters-sms-email-trigger-config-get', {
			url: '/masters/sms-email-trigger-config/get/:id',
			views: {
				'page-main-content@': {
					templateUrl: 'partials/tpl/pages/masters/sms_email_trigger_config/save.tpl.html'
				}
			},
			controller: 'SMSEmialTriggerConfigController',
			data: {
				page: "Masters - SMS/Emial Trigger Config : Update",
				requireLogin : true
			}
		});
    }]);


app.controller('SMSEmialTriggerConfigController',
	['$scope', '$state', 'ngDialog', 'SMSEmailTriggerConfig', 'notification',
    function($scope, $state, ngDialog, SMSEmailTriggerConfig, notification) {
		
		$scope.breadcrumbs = [
			{
				title: "Master Tables"
			},
			{
				title: "SMS/Email Trigger Configuration",
				link: "/masters/sms-email-template"
			}
		];
		
        $scope.smsEmailTriggerConfigList = [];
        $scope.pageActionHeader = "";
		$scope.errors = {};
		$scope.smsEmailTriggerConfig = {};
        
        if ($state.current.url == "/masters/sms-email-trigger-config") {
			SMSEmailTriggerConfig
				.config_list_by_service({'query':{}})
				.success(function(response) {
					$scope.smsEmailTriggerConfigList = response;
				});
		}
		
		$scope.SMSEmailTriggerConfigTemplate = {
			id : "",
			smsEmailTriggerCode: "",
			serviceId: "",
			isEmailRequired: 0,
			isSmsRequired: 0
		}
		
		//Save SMSEmialTriggerConfig Details
		$scope.save = function(smsEmailTriggerConfigList) {
			 var InsertArr = [], UpdateArr = [];
			for(var i in $scope.smsEmailTriggerConfigList) {
				var item = $scope.smsEmailTriggerConfigList[i];
				
				var templateItem = angular.copy($scope.SMSEmailTriggerConfigTemplate);
				templateItem.smsEmailTriggerCode = item.smsEmailTriggerCode;
				templateItem.serviceId = item.serviceId;
				templateItem.isEmailRequired = item.isEmailRequired;
				templateItem.isSmsRequired = item.isSmsRequired;
				templateItem.id = item.smsEmailTriggerConfigId;
				if (templateItem.id == null) {
					delete templateItem.id;
					InsertArr.push(templateItem);
				}
				else
					UpdateArr.push(templateItem);
			}
			
			if (InsertArr.length > 0) {
				SMSEmailTriggerConfig
					.bulkcreate(InsertArr)
					.success(function(response) {
						if (response == undefined) {
							notification.error("Error while saving data!.");
							return;
						}
						notification.success("Successfully saved data!");
					});
			}
			
			if (UpdateArr.length > 0) {
				SMSEmailTriggerConfig
					.bulkupdate(UpdateArr)
					.success(function(response) {
						if (response == undefined) {
							notification.error("Error while saving data!.");
							return;
						}
						notification.success("Successfully saved data!");
					});
			}
		}
		
		$scope.checkAll = function(type) {
			for(var i in $scope.smsEmailTriggerConfigList) {
				if (type == 'selectAllEmailRequired') {
                    $scope.smsEmailTriggerConfigList[i].isEmailRequired	 = $scope.selectAllEmailRequired;
                }
				if (type == 'selectAllSMSRequired') {
                    $scope.smsEmailTriggerConfigList[i].isSmsRequired = $scope.selectAllSMSRequired;
                }
			}
		}
		
    }]);