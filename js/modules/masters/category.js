'use strict';

app.factory('MasterCategory', function($http, REST_API_URL, AuthTokenFactory, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.CATEGORY.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.CATEGORY.CREATE, {'masterCategory':params});
		},
		get: function(params) {
			var getUrl = API.MASTERS.CATEGORY.GET.replace('<mastercategoryid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.CATEGORY.UPDATE, {'masterCategory':params});
		},
		del: function(params) {
			var getUrl = API.MASTERS.CATEGORY.DELETE.replace('<mastercategoryid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		}
	}
});
