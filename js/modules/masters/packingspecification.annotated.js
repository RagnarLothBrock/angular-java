'use strict'

app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
    function ($stateProvider, $urlRouterProvider, $httpProvider) {
        $stateProvider
            .state('rl.masters-packingspecification', {
                url: '/masters-packingspecification',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/masters/PackingSpecification/list.tpl.html'
                    }
                },
                controller: 'NewSubmenuController',
                data: {
                    page: "PackingSpecification",
                    requireLogin: true
                }
            })
            .state('rl.masters-packingspecification-create', {
                url: '/masters-packingspecification/create',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/masters/PackingSpecification/save.tpl.html'
                    }
                },
                controller: 'NewSubmenuController',
                data: {
                    page: "PackingSpecification Create",
                    requireLogin: true
                }
            })
            .state('rl.masters-packingspecification-get', {
				url: '/masters-packingspecification/get/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/PackingSpecification/save.tpl.html'
					}
				},
				controller: 'NewSubmenuController',
				data: {
					page: "PageSpecification Update",
					requireLogin : true
				}
			})
    }]);

app.factory('PackingSpecificationService', ["$http", "REST_API_URL", "API", function ($http, REST_API_URL, API) {

    return {
        list: function (params) {
           var getUrl = API.MASTERS.PACKING_SPECIFICATION.LIST
							.replace('<firstResult>', params.firstResult)
							.replace('<maxResult>', params.maxResult);
			return $http.post(REST_API_URL + getUrl, params);
            // return $http.post(REST_API_URL + API.MASTERS.PACKING_SPECIFICATION.LIST, params);
        },
        get: function(params) {
			var getUrl = API.MASTERS.PACKING_SPECIFICATION.GET.replace('<packingspecificationid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
        create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.PACKING_SPECIFICATION.CREATE, {'packingSpecification':params});
		},
        update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.PACKING_SPECIFICATION.UPDATE, {'packingSpecification':params});
		},
        del: function(params) {
			var getUrl = API.MASTERS.PACKING_SPECIFICATION.DELETE.replace('<packingspecificationid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		}
    }
}]);
app.controller('NewSubmenuController',
    ['$scope', '$rootScope', '$state', 'AuthTokenFactory', 'ngDialog', 'LIST_PAGE_SIZE', 'RetailerPackingSpecification', 'PackingSpecificationService','Utility',
        function ($scope, $rootScope, $state, AuthTokenFactory, ngDialog, LIST_PAGE_SIZE, RetailerPackingSpecification, PackingSpecificationService,Utility) {

            $scope.breadcrumbs = [
                {
                    title: "Master Tables",
                    link: "/masters"
                }
            ];
            $scope.pageActionHeader = "";
            $scope.pageSize = LIST_PAGE_SIZE;
            $scope.total_count = 0;
            $scope.currentPage = 1;
            $scope.notification = $rootScope.notification;
            $scope.packingSpecs = [];
            $scope.errors = {};
            $scope.packing = {};
            $scope.submenudata = "";

            $scope.getPackingList = function (first, limit) {
                $scope.currentPage = first // initialize page no to 1

                first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
                console.log("first : limit",limit);
                PackingSpecificationService.list({ 'query': {}, firstResult: first, maxResult: limit }).success(function (response) {
                    if (response !== undefined && response !== undefined && response.data.length > 0) {
                        $scope.packingSpecs = response.data;
                        $scope.total_count = response.totalRecords;
                        console.log("$scope.packingSpecs",$scope.packingSpecs);
                        console.log("$scope.total_count",$scope.total_count);
                        // $scope.total_count = response.totalRecords;
                    }
                });
            }

            if ($state.current.url == "/masters-packingspecification") {
                $scope.showAddNew = true;
                $scope.addNewUrl = "/masters-packingspecification/create";
                $scope.addNewState = "rl.masters-packingspecification-create";
                $scope.breadcrumbs.push({ title: $state.current.data.page, url: $state.current.url });
                $scope.getPackingList(1, $scope.pageSize);
            }

            if ($state.current.url == "/masters-packingspecification/create") {
                $scope.breadcrumbs.push({title:"PageSpecification", link:"/masters-packingspecification"});
			    $scope.breadcrumbs.push({title:$state.current.data.page, link:$state.current.url});
			    $scope.pageActionHeader = "Create new PackingSpecification";
            }
            if ($state.current.url == "/masters-packingspecification/get/:id") {
                
                $scope.breadcrumbs.push({ title: "PackingSpecification", link: "/masters-packingspecification" });
                $scope.breadcrumbs.push({ title: $state.current.data.page });
                $scope.pageActionHeader = "Update PackingSpecification Details";
                $scope.packing.id = $state.current.data.params.id;
                console.log("$scope.packing.id",$scope.packing.id);
                PackingSpecificationService.get($scope.packing).success(function (response) {
                    $scope.packing = response;
                    console.log("$scope.packing with Id:",$scope.packing);
                });
            }

            if ($state.current.url == "/masters-packingspecification/create" ||
                $state.current.url == "/masters-packingspecification/get/:id") {
                Utility.populate_category({}, $scope);
                Utility.populate_master_services({}, $scope);
            }

            $scope.save = function(packing) {
			$scope.errors = {};
			//Validate Brand
            console.log("Packing",packing);
			if(hasErrors(packing)) return false;
			delete($scope.packing.selectedFileName);
			
			if (packing.id == undefined || packing.id == "" || packing.id == null) {
                //Continue with packing create
				PackingSpecificationService.create(packing).success(function(response) {
					$state.go('rl.masters-packingspecification');
				});
            }
			else {
				//Continue with packing Update
				PackingSpecificationService.update(packing).success(function(response) {
					$state.go('rl.masters-packingspecification');
				});
			}
		}
        	//Delete Existing Packing Specification
		$scope.deletepackingSpecs = function(packing, index) {
			ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete this Packing Specification?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				console.log(packing,index);
				PackingSpecificationService.del(packing).success(function(response) {
					if (response) {
                        $state.go('rl.masters-packingspecification');
                    }
				});
			}, function (error) {
				// Error logic here
			});
			
		}
        var hasErrors = function(packing) {
			if (isEmpty(packing.serviceId,'serviceId',"Enter Service Name",$scope.notification)) {
                $scope.errors.serviceId = "Enter Service Name";
            }
			if (isEmpty(packing.categoryId,'categoryId',"Enter Category Name",$scope.notification)) {
                $scope.errors.categoryId = "Enter Category Name";
            }
            if (isEmpty(packing.newPickupLocal,'newPickupLocal',"Enter Category Name",$scope.notification)) {
                $scope.errors.newPickupLocal = "Enter New PickUp local";
            }
            if (isEmpty(packing.newPickupOutstation,'newPickupOutstation',"Enter Category Name",$scope.notification)) {
                $scope.errors.newPickupOutstation = "Enter New PickUp OutStation";
            }
            if (isEmpty(packing.oldPickupLocal,'oldPickupLocal',"Enter Category Name",$scope.notification)) {
                $scope.errors.oldPickupLocal = "Enter Old PickUp Local";
            }
            if (isEmpty(packing.oldPickupOutstation,'oldPickupOutstation',"Enter Category Name",$scope.notification)) {
                $scope.errors.oldPickupOutstation = "Enter Old PickUp OutStation";
            }
			
			if (Object.getOwnPropertyNames($scope.errors).length > 0) {
				return true;
			}
			return false;
		}

        }]);