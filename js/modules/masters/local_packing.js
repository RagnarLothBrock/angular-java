'use strict'

app.factory('LocalPackingService', function($http, REST_API_URL, API) {

	return {
		list: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.CITY.LIST, params);
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.CITY.CREATE, {'city':params});
		},
		get: function(params) {
			var getUrl = API.MASTERS.CITY.GET.replace('<cityId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.CITY.UPDATE, {'city':params});
		},
		del: function(params) {
			var getUrl = API.MASTERS.CITY.DELETE.replace('<cityId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		}
	}
});


app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {
		$stateProvider
			
			.state('rl.masters-local-packing', {
				url: '/masters-localPacking',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/city/list.tpl.html'
					}
				},
				controller: 'CityController',
				data: {
					page: "Masters - Local Packing",
					requireLogin : true
				}
			})
			.state('rl.masters-local-packing-create', {
				url: '/masters/local-packing/create',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/local_packing/save.tpl.html'
					}
				},
				controller: 'CityController',
				data: {
					page: "Masters - Local Packing : Create",
					requireLogin : true
				}
			})
			.state('rl.masters-local-packing-get', {
				url: '/masters/local-packing/get/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/local_packing/save.tpl.html'
					}
				},
				controller: 'CityController',
				data: {
					page: "Masters - Local Packing : Update",
					requireLogin : true
				}
			});
    }]);


app.controller('LocalPackingController',
	['$scope', '$rootScope', '$state', 'notification', 'AuthTokenFactory', 'ngDialog', '$location', 'logger', 'LocalPackingService', 'LIST_PAGE_SIZE',
    function($scope, $rootScope, $state,  notification, AuthTokenFactory, ngDialog, $location, logger, LocalPackingService, LIST_PAGE_SIZE) {
		
		$scope.breadcrumbs = [
			{
				title: "Master Tables",
				link: "/masters"
			}
		];
		$scope.breadcrumbs.push({title:"Local Packing", link:"/masters-localPacking"});
		
        $scope.notification = $rootScope.notification;        
        $scope.cities = [];
        $scope.pageActionHeader = "";
		$scope.errors = {};
		$scope.city = {};
		$scope.pageSize = LIST_PAGE_SIZE;
		$scope.total_count = 0;
		$scope.currentPage = 1;
		
		$scope.getCityList = function(first, limit) {
			$scope.currentPage = first // initialize page no to 1
			
			first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
			CityService.list({'query':{},firstResult: first, maxResults: limit}).success(function(response) {
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.cities = response.data;
					$scope.total_count = response.totalRecords;
				}
			});
		}
        
        if ($state.current.url == "/masters-city") {
			$scope.showAddNew = true;
			$scope.addNewUrl = "/masters-city/create";
			$scope.addNewState = "rl.masters-city-create"
			$scope.getCityList(1, $scope.pageSize);
		}
		
		if ($state.current.url == "/masters-city/create") {
			$scope.pageActionHeader = "Create new city";
		}
		
		if ($state.current.url == "/masters-city/get/:id") {
			$scope.pageActionHeader = "Update City Details";
			$scope.city.id = $state.current.data.params.id;
			CityService.get($scope.city).success(function(response) {
				$scope.city = response;
			});
		}
		
		if ($state.current.url == "/masters-city/create" ||
			$state.current.url == "/masters-city/get/:id") {
			
			$scope.breadcrumbs.push({title:$state.current.data.page, link:$state.current.url});
		}
		
		//Save City Details
		$scope.save = function(city) {
			$scope.errors = {};
			//Validate City
			if(hasErrors(city)) return false;
			
			if (city.id == undefined || city.id == "" || city.id == null) {
                //Continue with city create
				CityService.create(city).success(function(response) {
					console.log(response);
					$state.go('rl.masters-city');
				});
            }
			else {
				//Continue with city Update
				CityService.update(city).success(function(response) {
					console.log(response);
					$state.go('rl.masters-city');
				});
			}
		}
		
		//Delete Existing City
		$scope.deleteCity = function(city, index) {
			ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete this city?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				console.log(city,index);
				CityService.del(city).success(function(response) {
					if (response) {
                        $state.go('rl.masters-city');
                    }
				});
			}, function (error) {
				// Error logic here
			});
			
		}
		
		//Validate City Details
		var hasErrors = function(city) {
			if (isEmpty(city.name,'name',"Enter city name",$scope.notification)) {
                $scope.errors.name = "Enter city name";
            }
			
			if (Object.getOwnPropertyNames($scope.errors).length > 0) {
				return true;
			}
			return false;
		}
    }]);