'use strict';

app.factory('ProductCategory', ["$http", "REST_API_URL", "AuthTokenFactory", "API", "Upload", function($http, REST_API_URL, AuthTokenFactory, API, Upload) {

	return {
		list: function(params) {
			var getUrl = API.MASTERS.PRODUCT_CATEGORY.GET_ALL
						.replace('<firstResult>', params.firstResult)
						.replace('<maxResults>', params.maxResults);
			return $http.post(REST_API_URL + getUrl, {});
		},
		create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.PRODUCT_CATEGORY.CREATE, {'productCategory':params});
		},
		get: function(params) {
			var getUrl = API.MASTERS.PRODUCT_CATEGORY.GET.replace('<productcategoryid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.PRODUCT_CATEGORY.UPDATE, {'productCategory':params});
		},
		del: function(params) {
			var getUrl = API.MASTERS.PRODUCT_CATEGORY.DELETE.replace('<productcategoryid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		byRetailer: function(params) {
			var url = API.MASTERS.PRODUCT_CATEGORY.BY_RETAILER.replace('<retailerid>', params.id);
			return $http.post(REST_API_URL + url, params);
		},
		byServiceProviderLocation: function(params) {			
			var url = API.MASTERS.PRODUCT_CATEGORY.BY_SERVICE_PROVIDER_LOCATION.replace('<serviceProviderLocationId>', params.serviceProviderLocationId);
			return $http.post(REST_API_URL + url, params);
		},
		upload: function(params) {
			return Upload.upload({
                url: REST_API_URL + API.MASTERS.PRODUCT_CATEGORY.UPLOAD,
                data: params.data,
                file: params.file
            })
		}
	}
}]);


app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {
		$stateProvider
			.state('rl.masters-product-category', {
				url: '/masters/product-category',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/product_category/list.tpl.html'
					}
				},
				controller: 'ProductCategoryController',
				data: {
					page: "Masters - Product Category",
					requireLogin : true
				}
			})
			.state('rl.masters-product-category-create', {
				url: '/masters/product-category/create',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/product_category/save.tpl.html'
					}
				},
				controller: 'ProductCategoryController',
				data: {
					page: "Masters - Product Category : Create",
					requireLogin : true
				}
			})
			.state('rl.masters-product-category-get', {
				url: '/masters/product-category/get/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/product_category/save.tpl.html'
					}
				},
				controller: 'ProductCategoryController',
				data: {
					page: "Masters - Product Category : Update",
					requireLogin : true
				}
			})
			.state('rl.masters-product-category-upload', {
				url: '/masters/product-category/bulk-upload',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/product_category/bulk_upload.tpl.html'
					}
				},
				controller: 'ProductCategoryController',
				data: {
					page: "Masters - Product Category : Bulk Upload",
					requireLogin : true
				}
			});
    }]);

app.controller('ProductCategoryController',
	['$scope', '$rootScope', '$state', 'notification', 'AuthTokenFactory', 'ngDialog', '$location', 'logger', 'ProductCategory', 'Utility', 'MasterCategory', 'LIST_PAGE_SIZE',
    function($scope, $rootScope, $state,  notification, AuthTokenFactory, ngDialog, $location, logger, ProductCategory, Utility, MasterCategory, LIST_PAGE_SIZE) {
		
		$scope.breadcrumbs = [
			{
				title: "Master Tables"
			},
			{
				title: "Product Category",
				link: '/masters/product-category'
			}
		];
		
		$scope.pageSize = LIST_PAGE_SIZE;
		$scope.total_count = 0;
		$scope.currentPage = 1;
		
        $scope.notification = $rootScope.notification;        
        $scope.productCategories = [];
        $scope.pageActionHeader = "";
		$scope.errors = {};
		$scope.productCategory = {};
		
		$scope.getProductCategoryList = function(first, limit) {
			$scope.currentPage = first // initialize page no to 1
			
			first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
			ProductCategory.list({'query':{},firstResult: first, maxResults: limit}).success(function(response) {
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.productCategories = response.data;
					$scope.total_count = response.totalRecords;
				}
			});
		}
        
        if ($state.current.url == "/masters/product-category") {
			$scope.showAddNew = true;
			$scope.addNewUrl = "/masters/product-category/create";
			$scope.addNewState = "rl.masters-product-category-create"
			
			$scope.showUpload = true;
			$scope.uploadUrl = "/masters/product-category/bulk-upload";
			$scope.uploadState = "rl.masters-product-category-upload"
			
			$scope.getProductCategoryList(1, $scope.pageSize);
		}
		
		if ($state.current.url == "/masters/product-category/create") {
			$scope.pageActionHeader = "Create new product category";
			$scope.breadcrumbs.push({title:"Create"});
		}
		
		if ($state.current.url == "/masters/product-category/get/:id") {
			$scope.pageActionHeader = "Update product category details";
			$scope.breadcrumbs.push({title:"Update"});
			$scope.productCategory.id = $state.current.data.params.id;
			ProductCategory.get($scope.productCategory).success(function(response) {
				$scope.productCategory = response;
			});
		}
		
		if ($state.current.url == "/masters/product-category/create" ||
			$state.current.url == "/masters/product-category/get/:id") {
			
			Utility.populate_master_category({},$scope);
		}
		
		//Save Product Category Details
		$scope.save = function(productCategory) {
			$scope.errors = {};
			
			delete(productCategory.selectedFileName);
			if (productCategory.id == undefined || productCategory.id == "" || productCategory.id == null) {
                //Continue with productCategory create
				ProductCategory.create(productCategory).success(function(response) {
					console.log(response);
					$state.go('rl.masters-product-category');
				});
            }
			else {
				//Continue with productCategory Update
				ProductCategory.update(productCategory).success(function(response) {
					console.log(response);
					$state.go('rl.masters-product-category');
				});
			}
		}
		
		//Delete Existing Sub Category
		$scope.deleteProductCategory = function(productCategory, index) {
			ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete this product category?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				console.log(productCategory,index);
				ProductCategory.del(productCategory).success(function(response) {
					if (response) {
                        $state.go('rl.masters-product-category');
                    }
				});
			}, function (error) {
				// Error logic here
			});
			
		}
		
		$scope.upload = function(productCategory) {
			ProductCategory.upload(productCategory)
				.progress(function (evt) {
					var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
					$scope.uploadPercentage = progressPercentage;
					console.log(progressPercentage);
				})
				.success(function (data, status, headers, config) {
					console.log(data);
					$rootScope.$broadcast('showUploadReturnMessage', {data:data, status:status, headers:headers, config:config});
					$scope.productCategory = {data:{}, file:{}};
				})
				.error(function(error){
					var errorM = "";
					$(error.message).each(function(){
						errorM += this +"<br/>";
					});
					notification.error(errorM)
				});
		}
		
		$scope.$watch('productCategory.file', function () {
			$scope.productCategory.selectedFileName = ($scope.productCategory.file !== undefined) ? $scope.productCategory.file.name : "";
		});
    }]);