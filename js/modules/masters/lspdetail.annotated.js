'use strict'

app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
    function ($stateProvider, $urlRouterProvider, $httpProvider) {
        $stateProvider
            .state('rl.masters-lspDetail', {
                url: '/masters-lspdetail',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/masters/LspDetail/list.tpl.html'
                    }
                },
                controller: 'LspDetailController',
                data: {
                    page: "LspDetail",
                    requireLogin: true
                }
            })
            .state('rl.masters-lspdetail-create', {
                url: '/masters-lspdetail/create',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/masters/LspDetail/save.tpl.html'
                    }
                },
                controller: 'LspDetailController',
                data: {
                    page: "LspDetail Create",
                    requireLogin: true
                }
            })
            .state('rl.masters-lspdetail-get', {
				url: '/masters-lspdetail/get/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/masters/LspDetail/save.tpl.html'
					}
				},
				controller: 'LspDetailController',
				data: {
					page: "LspDetail Update",
					requireLogin : true
				}
			})
    }]);

app.factory('LspDetailService', ["$http", "REST_API_URL", "API", function ($http, REST_API_URL, API) {

    return {
        list: function (params) {
           var getUrl = API.MASTERS.LSP_DETAIL.LIST
							.replace('<firstResult>', params.firstResult)
							.replace('<maxResult>', params.maxResult);
			return $http.post(REST_API_URL + getUrl, params);
        // list:function (params){
        //     return $http.post(REST_API_URL + API.MASTERS.LSP_DETAIL.LIST, params);
        },
        get: function(params) {
			var getUrl = API.MASTERS.LSP_DETAIL.GET.replace('<lspdetailid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
        create: function(params) {	
			return $http.post(REST_API_URL + API.MASTERS.LSP_DETAIL.CREATE, {'lspDetail':params});
		},
        update: function(params) {
			return $http.post(REST_API_URL + API.MASTERS.LSP_DETAIL.UPDATE, {'lspDetail':params});
		},
        del: function(params) {
			var getUrl = API.MASTERS.LSP_DETAIL.DELETE.replace('<lspdetailid>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		}
    }
}]);
app.controller('LspDetailController',
    ['$scope', '$rootScope', '$state', 'AuthTokenFactory', 'ngDialog', 'LIST_PAGE_SIZE', 'LspDetailService','Utility',
        function ($scope, $rootScope, $state, AuthTokenFactory, ngDialog, LIST_PAGE_SIZE, LspDetailService,Utility) {

            $scope.breadcrumbs = [
                {
                    title: "Master Tables",
                    link: "/masters"
                }
            ];
            $scope.pageActionHeader = "";
            $scope.pageSize = LIST_PAGE_SIZE;
            $scope.total_count = 0;
            $scope.currentPage = 1;
            $scope.notification = $rootScope.notification;
            $scope.lspDetails = [];
            $scope.errors = {};
            $scope.lspDetail = {};
            // $scope.submenudata = "";

            $scope.getLspDetailList = function (first, limit) {
                $scope.currentPage = first // initialize page no to 1

                first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
                console.log("First: ",first);
                console.log("limit: ",limit);
                LspDetailService.list({ 'query': {}, firstResult: first, maxResult: limit }).success(function (response) {
                    // console.log("response.length:",response.length);
                    if (response !== undefined && response !== undefined && response.data.length > 0) {
                        // console.log("response from Api",response);
                        $scope.lspDetails = response.data;
                        $scope.total_count = response.totalRecords;
                        console.log(".lspDetails",$scope.lspDetails);
                    }
                });
            }

            if ($state.current.url == "/masters-lspdetail") {
                $scope.showAddNew = true;
                $scope.addNewUrl = "/masters-lspdetail/create";
                $scope.addNewState = "rl.masters-lspdetail-create";
                $scope.breadcrumbs.push({ title: $state.current.data.page, url: $state.current.url });
                $scope.getLspDetailList(1, $scope.pageSize);
            }

            if ($state.current.url == "/masters-lspdetail/create") {
                $scope.breadcrumbs.push({title:"LspDetail", link:"/masters-lspdetail"});
			    $scope.breadcrumbs.push({title:$state.current.data.page, link:$state.current.url});
			    $scope.pageActionHeader = "Create new LspDetail";
            }
            if ($state.current.url == "/masters-lspdetail/get/:id") {
                
                $scope.breadcrumbs.push({ title: "LspDetail", link: "/masters-lspdetail" });
                $scope.breadcrumbs.push({ title: $state.current.data.page });
                $scope.pageActionHeader = "Update LspDetail Details";
                $scope.lspDetail.id = $state.current.data.params.id;
                console.log("Scope.lsdfbcbvpDetail",$scope.lspDetail);
                LspDetailService.get($scope.lspDetail).success(function (response) {
                    $scope.lspDetail = response;
                    console.log("lspDetail value:",$scope.lspDetail);
                    // console.log("lspDetails value:",$scope.lspDetails);
                });
            }

            if ($state.current.url == "/masters-lspdetail/create" ||
                $state.current.url == "/masters-lspdetail/get/:id") {
                Utility.populate_city({}, $scope);
                Utility.populate_lsp_detail({}, $scope);
            }

            $scope.save = function(lspDetail) {
			$scope.errors = {};
			//Validate lspDetail
			if(hasErrors(lspDetail)) return false;
			delete($scope.lspDetail.selectedFileName);
			
			if (lspDetail.id == undefined || lspDetail.id == "" || lspDetail.id == null) {
                //Continue with lspDetail create
                console.log("creste lsp",lspDetail);
				LspDetailService.create(lspDetail).success(function(response) {
					$state.go('rl.masters-lspDetail');
				});
            }
			else {
				//Continue with lspDetail Update
                console.log("Lsp Update",lspDetail);
				LspDetailService.update(lspDetail).success(function(response) {
                    console.log("response of Update",response);
					$state.go('rl.masters-lspDetail');
				});
			}
		}
        	//Delete Existing lspDetail 
		$scope.deleteLspdetail = function(lspDetail, index) {
			ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete this Lsp Detail?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				// console.log(lsp,index);
				LspDetailService.del(lspDetail).success(function(response) {
					if (response) {
                        $state.go('rl.masters-lspDetail');
                    }
				});
			}, function (error) {
				// Error logic here
			});
			
		}
        var hasErrors = function(lspDetail) {
			if (isEmpty(lspDetail.name,'name',"Enter lspDetail Name",$scope.notification)) {
                $scope.errors.name = "Enter lsp Detail Name";
            }
			if (isEmpty(lspDetail.code,'code',"Enter Code Name",$scope.notification)) {
                $scope.errors.code = "Enter Lsp Code ";
            }
            if (isEmpty(lspDetail.cityId,'city',"Enter City Name",$scope.notification)) {
                $scope.errors.cityId = "Enter Lsp City Name";
            }
            if (isEmpty(lspDetail.address,'address',"Enter Address",$scope.notification)) {
                $scope.errors.address = "Enter Lsp Address";
            }
            if (isEmpty(lspDetail.contactNumber,'contactNumber',"Enter Contact Number ",$scope.notification)) {
                $scope.errors.contactNumber = "Enter Lsp Contact Number";
            }
			
			if (Object.getOwnPropertyNames($scope.errors).length > 0) {
				return true;
			}
			return false;
		}

        }]);