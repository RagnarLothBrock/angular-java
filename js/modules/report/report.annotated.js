app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
    function ($stateProvider, $urlRouterProvider, $httpProvider) {
        $stateProvider
            .state('rl.report.tickets', {
                url: '/report/tickets',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/report/report.tpl.html'
                    }
                },
                controller: 'ReportController',
                data: {
                    page: "Reports",
                    requireLogin: true,
                    checkAccess: false
                }
            })
            .state('rl.report-call-details', {
                url: '/report/call/details',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/report/exotel_report.tpl.html'
                    }
                },
                controller: 'ReportController',
                data: {
                    page: "Call Details",
                    requireLogin: true,
                    checkAccess: false
                }


            })
            .state('rl.report', {
                url: '/get/report',
                views: {
                    'page-main-content@': {
                        templateUrl: 'partials/tpl/pages/report/report.tpl.html'
                    }
                },
                controller: 'ReportController',
                data: {
                    page: 'Report',
                    requireLogin: true,
                    checkAccess: false

                }
            })
    }
]);

app.factory('ReportService', ["$http", "REST_API_URL", "AuthTokenFactory", "API", "Upload", function ($http, REST_API_URL, AuthTokenFactory, API, Upload) {
    return {
        getReport: function (params) {
            var Url = API.REPORT.GET_REPORT;
            return $http.post(REST_API_URL + Url, params);
            //return $http.post(REST_API_URL + API.TASKS.FIND_BY_VARIABLE_VALUES, params);
        },

        getcalllog: function (params) {
            return $http.post(REST_API_URL + API.REPORT.GET_CALL_LOG, params)
        }
    }
}]);

app.controller('ReportController', ['$scope', '$rootScope', '$state', 'TasksService', 'notification', 'AuthTokenFactory', 'ngDialog', '$location', 'logger',
    'TokenFactory', 'ProcessService', 'Utility', '$window', 'UserService', 'LIST_PAGE_SIZE', 'RestHTTPService', 'API', 'RetailerReturnOrPickupLocations', '$cookies', 'ReportService',
    function ($scope, $rootScope, $state, TasksService, notification, AuthTokenFactory, ngDialog, $location, logger, TokenFactory,
              ProcessService, Utility, $window, UserService, LIST_PAGE_SIZE, RestHTTPService, API, RetailerReturnOrPickupLocations, $cookies, ReportService) {
        $scope.errors = {};
        $scope.calllog = [];
        $scope.pageSize = LIST_PAGE_SIZE;
        $scope.total_count = 0;
        $scope.currentPage = 0;
        $scope.pagination = {};


        if ($state.current.url == "/reports/tickets") {
            Utility.populate_city({}, $scope);
            Utility.populate_retailer({}, $scope);
            Utility.populate_roles({}, $scope);
        }

        if ($state.current.url == "/report/call/details") {
            Utility.populate_city({}, $scope);
            $scope.exotelReport = {};
        }
        if ($state.current.url == "/get/report") {
            Utility.populate_city({}, $scope);
            Utility.populate_retailer({}, $scope);
        }

        //
        function convertDate(d) {
            var dd = d.getDate();
            var mm = d.getMonth() + 1; //January is 0!

            var yyyy = d.getFullYear();
            if (dd < 10) {
                dd = '0' + dd
            }
            if (mm < 10) {
                mm = '0' + mm
            }
            var date = yyyy + '-' + mm + '-' + dd;
            return date;
        }

        $scope.datePicker = {};
        $scope.datePicker.date = {startDate: null, endDate: null};
        $scope.report = {};


        $scope.getreport = function (data, first, last) {
            console.log(data);
            $scope.currentPage = first;// initialize page no to 1

            first = $rootScope.calculateFirstRecord($scope.currentPage, first);
            if ($scope.datePicker.date.startDate != null && $scope.datePicker.date.endDate != null) {
                data.startDate = convertDate(new Date($scope.datePicker.date.startDate));
                data.endDate =convertDate(new Date($scope.datePicker.date.endDate));
            }
            ReportService.getReport({
                "query": data,
                "first": first,
                "last": last
            }).success(function (response) {
                if (response !== undefined && response.data !== undefined && response.data.length > 0) {
                    console.log(response);
                    $scope.reportresult = response.data;
                    $scope.reportcount = response.totalCount;
                }
            })
        }


        $scope.searchlog = function (data, first, last) {
            $scope.currentPage = first;// initialize page no to 1

            first = $rootScope.calculateFirstRecord($scope.currentPage, first);
            $scope.callResult = {};
            console.log(data);
            console.log($scope.datePicker);
            if ($scope.datePicker.date.startDate != null && $scope.datePicker.date.endDate != null) {
                data.date = $scope.datePicker.date;
                // data.date.startDate=formatDate(new Date($scope.datePicker.date.startDate));
                // data.date.endDate=formatDate(new Date($scope.datePicker.date.endDate));
            }
            console.log($scope.datePicker.date);

            ReportService.getcalllog({
                "query": data,
                "firstResult": first,
                "maxResults": last
            }).success(function (response) {
                if (response !== undefined && response.data !== undefined && response.data.length > 0) {
                    console.log(response.data);
                    $scope.callResult = response.data;
                    $scope.total_count = response.totalRecords;

                }
            })
        }

        //Probable status,needs to be added if found more
        $scope.callstatus = ["completed", "busy", "failed", "no-answer", "canceled", "in-progress"];

        $scope.clearSearchFields = function () {
            $scope.exotelReport = {};
            $scope.callResult = {};
            $scope.datePicker = {};
            $scope.datePicker = {};
            $scope.datePicker.date = {}
            $scope.datePicker.date.endDate = null;
            $scope.datePicker.date.endDate = null;
            $scope.report = {};
            $scope.reportresult={};
        }
    }
])