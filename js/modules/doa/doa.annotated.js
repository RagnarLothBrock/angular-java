'use strict'

app.factory('DOA', ["$http", "REST_API_URL", "API", "Upload", function($http, REST_API_URL, API, Upload) {
	return {
		upload:function(params) {
            console.log(params);
			return Upload.upload({
                url: REST_API_URL + API.DOA.UPLOAD,
                data: params.data,
                file: params.file
            })
		},
		download: function(params) {
            var getUrl = API.DOA.DOWNLOAD.replace('<categoryId>', params.categoryId).replace('<brandId>', params.brandId);
			return $http.post(REST_API_URL +getUrl, {});
		},
        list: function(params, first, limit) {
			var getUrl = API.DOA.LIST
						.replace('<firstResult>', first)
						.replace('<maxResults>', limit);
			return $http.post(REST_API_URL + getUrl, {query:params});
        },
		search: function(params) {
			return $http.post(REST_API_URL + API.DOA.SEARCH, {query:params});
		},
		get: function(params) {
			var getUrl = API.DOA.GET.replace('<id>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		del: function(params) {
			var getUrl = API.DOA.DELETE.replace('<id>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		}
	}
}]);

app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function($stateProvider, $urlRouterProvider, $httpProvider) {
		$stateProvider
			.state('rl.doa', {
				url: '/doa-forms',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/doa/list.tpl.html'
					}
				},
				controller: 'DOAController',
				data: {
					page: "DOA Forms",
					requireLogin : true
				}
			})
            .state('rl.doa-form-upload', {
				url: '/doa-form/upload',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/doa/upload.tpl.html'
					}
				},
				controller: 'DOAController',
				data: {
					page: "Upload DOA Form",
					requireLogin : true
				}
			})
			.state('rl.doa-form-upload-update', {
				url: '/doa-form/upload/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/doa/upload.tpl.html'
					}
				},
				controller: 'DOAController',
				data: {
					page: "Upload DOA Form",
					requireLogin : true
				}
			});
    }
]);

app.controller('DOAController',
            ['$scope', '$rootScope', '$state', 'notification', 'DOA', 'Utility', 'ngDialog', 'LIST_PAGE_SIZE',
    function($scope, $rootScope, $state,  notification, DOA, Utility, ngDialog,LIST_PAGE_SIZE) {
        $scope.doaForms = [];
		$scope.categories = [];
		$scope.brands = [];
		$scope.URL = host_base_url;
		
		$scope.pageSize = LIST_PAGE_SIZE;
		$scope.total_count = 0;
		$scope.currentPage = 1;
		
		$scope.breadcrumbs = [
			{
				title: "Admin"
			},
			{
				title: "DOA Forms",
				link: "/doa-forms"
			}
		];
		
		$scope.getDOAList = function(first, limit) {
			$scope.currentPage = first // initialize page no to 1
			
			first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
			DOA.list({},first, limit).success(function(response){
				if (response !== undefined && response.data !== undefined && response.data.length > 0){
					$scope.doaForms = response.data;
					$scope.total_count = response.totalRecords;
				}
            });
		}
		
		//$scope.doaForm = {categoryId:"",brandId:""};
        //List all DOA forms
        if ($state.current.url == "/doa-forms") {
			$scope.showUpload =  true;
			$scope.uploadState = "rl.doa-form-upload-update";
			$scope.uploadUrl = "/doa-form/upload";
			
			$scope.getDOAList(1, $scope.pageSize);
		}
		
		if ($state.current.url == "/doa-form/upload" || $state.current.url == "/doa-form/upload/:id") {
			Utility.populate_category({},$scope);
			Utility.populate_brands({},$scope);
			
			$scope.breadcrumbs.push({title:"Upload"});
			
			try {
				$scope.$watch('doaForm.file', function () {
					$scope.doaForm.selectedFileName = ($scope.doaForm.file !== undefined) ? $scope.doaForm.file.name : "";
				});
			}
			catch(e) {
				
			}
			
			if ($state.current.url == "/doa-form/upload/:id") {
               DOA.get({id:$state.current.data.params.id}).success(function(response){
					$scope.doaForm = response;
			   });
            }
		}
		
		$scope.getDoaForm = function() {
			if ($scope.doaForm.categoryId !== undefined && $scope.doaForm.categoryId !== null && $scope.doaForm.categoryId !== "" &&
				$scope.doaForm.brandId !== undefined && $scope.doaForm.brandId !== null && $scope.doaForm.brandId !== "") {
                DOA.search($scope.doaForm).success(function(response){
					if (response !== undefined && response.length > 0) {
						ngDialog.openConfirm({
							template: '<div class="dialog-contents"><br/>' +
										'A DOA form is already exists for this category and brand.<br/>' +
										'Do you want to upload new form?<br/><br/>' +
										'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
										'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
									'</div>',
							controller: ['$scope', function($scope) { 
							  // Controller logic here
							}]
						}).then(function (success) {
							$scope.doaForm.id = response[0].id;
							$scope.doaForm.attachmentId = response[0].attachmentId;
						}, function (error) {
							// Error logic here
							$scope.doaForm = null;
						});
                    }
				});
            }
		}
		
		$scope.upload = function(doaForm) {
			if ($scope.doaFormElement.$invalid) {
                return;
            }
			
			var param = {data: {categoryId: doaForm.categoryId, brandId: doaForm.brandId}, file: doaForm.file};
			
			if ($scope.doaForm.attachmentId !== undefined && $scope.doaForm.attachmentId !== null && $scope.doaForm.attachmentId !== "") {
                param.data.attachmentId = $scope.doaForm.attachmentId;
            }
			
			if ($scope.doaForm.id !== undefined && $scope.doaForm.id !== null && $scope.doaForm.id !== "") {
                param.data.id = $scope.doaForm.id;
            }
			
			DOA.upload(param)
				.progress(function (evt) {
					var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
					$scope.uploadPercentage = progressPercentage;
				})
				.success(function (data, status, headers, config) {
					if (status == 200 && data.success === true) {
						notification.success("Successfully uploaded DOA Form!");
					}
					
					if (data.errorMessages !== undefined && data.errorMessages.length > 0) {
						notification.error(data.errorMessages[0]);
					}
					$scope.doaForm = {};
					$scope.uploadPercentage = 0;
					$state.go("rl.doa");
				})
				.error(function(error){
					var errorM = "";
					$(error.message).each(function(){
						errorM += this +"<br/>";
					});
					notification.error(errorM);
				});
		}
		
		$scope.download = function(doa) {
			DOA.download(doa)
				.success(function(response){
					console.log(response);
				})
		}
		
		$scope.deleteDOAForm = function(doa, index) {
			ngDialog.openConfirm({
				template: '<div class="dialog-contents">' +
							'Do you want to delete this DOA form?<br/><br/>' +
							'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
							'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
						'</div>',
				controller: ['$scope', function($scope) { 
				  // Controller logic here
				}]
			}).then(function (success) {
				// Success logic here
				DOA.del(doa).success(function(response) {
					if (response) {
                        $state.go('rl.doa');
						$scope.doaForms.splice(index,1);
                    }
				});
			}, function (error) {
				// Error logic here
			});
		}
    }
]);