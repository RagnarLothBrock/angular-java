'use strict'

app.factory('UserService', function ($http, REST_API_URL, API) {

	return {
		list: function (params) {
			return $http.post(REST_API_URL + API.USER.LIST, params);
		},
		create: function (params) {
			return $http.post(REST_API_URL + API.USER.CREATE, { 'user': params });
		},
		get: function (params) {
			var getUrl = encodeURI(API.USER.GET.replace('<userId>', params.id));
			return $http.post(REST_API_URL + getUrl, {});
		},
		getPincode: function (params, firstResult, maxResults) {
			var getUrl = API.USER.GET_PINCODES.replace('<firstResult>', firstResult).replace('<maxResults>', maxResults);
			return $http.post(REST_API_URL + getUrl, params);
		},
		getSingleLocationPincode: function (params) {
			var getUrl = API.USER.GET_LOCATION_PINCODE;
			return $http.post(REST_API_URL + getUrl, params);
		},
		coordinator_list: function (params) {
			var getUrl = API.USER.GET_COORDINATORS;
			return $http.post(REST_API_URL + getUrl, params);
		},
		saveSinglePincode: function (params) {
			var getUrl = API.USER.SAVE_SINGLE_PINCODE;
			return $http.post(REST_API_URL + getUrl, params);
		},
		savePincodes: function (params) {
			var getUrl = API.USER.SAVE_PINCODES;
			return $http.post(REST_API_URL + getUrl, { 'coordinatorServicablePincodesList': params });
		},
		delPincode: function (params) {
			var getUrl = API.USER.DELETE_PINCODE.replace('<id>', params);
			return $http.post(REST_API_URL + getUrl, {});
		},
		update: function (params) {
			return $http.post(REST_API_URL + API.USER.UPDATE, { 'user': params });
		},
		del: function (params) {
			var getUrl = API.USER.DELETE.replace('<userId>', params.id);
			return $http.post(REST_API_URL + getUrl, {});
		},
		save: function (params) {
			return $http.post(REST_API_URL + API.USER.SAVE, { 'processUser': params });
		},
		generate_reset_password_token: function (params) {
			var getUrl = API.USER.GENERATE_RESET_PASSWORD_TOKEN.replace('<email>', params.email);
			return $http.post(REST_API_URL + getUrl, {});
		},
		reset_password: function (params) {
			return $http.post(REST_API_URL + API.USER.RESET_PASSWORD, { 'query': params });
		},
		change_my_password: function (params) {
			return $http.post(REST_API_URL + API.USER.CHANGE_MY_PASSWORD, { 'query': params });
		}
	}
});


app.config(['$stateProvider', '$urlRouterProvider', '$httpProvider',
	function ($stateProvider, $urlRouterProvider, $httpProvider) {
		$stateProvider
			.state('rl.users', {
				url: '/users',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/user_management/search.tpl.html'
					}
				},
				controller: 'UserController',
				data: {
					page: "Users",
					requireLogin: true,
					checkAccess: false
				}
			})
			.state('rl.user-create', {
				url: '/user/create',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/user_management/save.tpl.html'
					}
				},
				controller: 'UserController',
				data: {
					page: "User : Create",
					requireLogin: true,
					checkAccess: true
				}
			})
			.state('rl.user-pincode-update', {
				url: '/pincode/update/:id/:locId',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/user_management/save_pincode.tpl.html'
					}
				},
				controller: 'UserController',
				data: {
					page: "Pincode : Update",
					requireLogin: true,
					checkAccess: true
				}
			})
			.state('rl.coordinator-pincode-search', {
				url: '/coordinator/pincode',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/user_management/search_pincode.tpl.html'
					}
				},
				controller: 'UserController',
				data: {
					page: "Pincode : Search",
					requireLogin: true,
					checkAccess: true
				}
			})
			.state('rl.user-get', {
				url: '/user/get/:id',
				views: {
					'page-main-content@': {
						templateUrl: 'partials/tpl/pages/user_management/save.tpl.html'
					}
				},
				controller: 'UserController',
				data: {
					page: "User : Update",
					requireLogin: true
				}
			});
	}]);


app.controller('UserController',
	['$scope', '$rootScope', '$state', 'notification', 'AuthTokenFactory', 'ngDialog', '$location', '$timeout', 'logger', 'UserService', 'Utility', 'Utils', 'LIST_PAGE_SIZE', 'TokenFactory',
		function ($scope, $rootScope, $state, notification, AuthTokenFactory, ngDialog, $location, $timeout, logger, UserService, Utility, Utils, LIST_PAGE_SIZE, TokenFactory) {

			$scope.breadcrumbs = [
				{
					title: "Admin"
				},
				{
					title: "Users",
					link: "/users"
				}
			];

			$scope.roles = [];
			$scope.locations = [];
			$scope.notification = $rootScope.notification;
			$scope.users = [];
			$scope.pageActionHeader = "";
			$scope.errors = {};
			$scope.user = {};
			$scope.disableEmail = false;

			$scope.pageSize = LIST_PAGE_SIZE;
			$scope.total_count = 0;
			$scope.currentPage = 1;
			var loginInfo = JSON.parse(TokenFactory.getToken('loginInfo'));
			$scope.userList = function (first, limit) {
				$scope.currentPage = first // initialize page no to 1
				var filter = {};
				if (loginInfo.roleCode !== 'superadmin')
					filter.roleCodeNotEquals = 'superadmin';
				first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
				UserService.list({ 'query': filter, firstResult: first, maxResults: limit }).success(function (response) {
					//$scope.users = response;
					if (response !== undefined && response.data !== undefined && response.data.length > 0) {
						$scope.users = response.data;
						$scope.total_count = response.totalRecords;
					}
				});
			}

			if ($state.current.url == "/users") {
				$scope.showAddNew = true;
				$scope.addNewState = "rl.user-create"
				$scope.addNewUrl = "/user/create";
				$scope.userList(1, $scope.pageSize)
				Utility.populate_roles({}, $scope);
				console.log("$scope.showAddNew" + $scope.roles[1]);
			}

			if ($state.current.url == "/user/create") {
				$scope.pageActionHeader = "Create new user";
				$scope.user.status = 1;
			}
			$scope.showSave = true;
			if ($state.current.url == "/user/get/:id") {
				$scope.pageActionHeader = "Update User Details";

				//console.log($state.current.data.params.id,"--",Utils.decodeURI($state.current.data.params.id))
				$scope.user.id = ($state.current.data.params.id);
				UserService.get($scope.user).success(function (response) {
					$scope.user = response;
					if (response.roleCode === 'superadmin' && loginInfo.roleCode !== 'superadmin') {
						$scope.showSave = false;
					}
				});
			}

			$scope.getAllPincodes = function (emailId, firstResult, maxResults) {
				$scope.currentPage = firstResult;
				firstResult = $rootScope.calculateFirstRecord($scope.currentPage, maxResults);
				$scope.user.email = emailId;
				console.log("Email : " + emailId + " firstResult : " + firstResult + " maxResults : " + maxResults);
				UserService.getPincode({ query: { 'coordinatorEmailId': $scope.user.email }, firstResult: firstResult, maxResults: maxResults }).success(function (response) {
					$scope.pincodes = response.data;
					$scope.total_count = response.totalRecords;
				});
			}

			//GETTING ALL COORDINATOR LIST FROM USER TABLE
			// if(true){
			// 	UserService.coordinator_list({ query: { 'roleCode': 'coordinator' }}).success(function (response) {
			// 		$scope.coordinators = response.data;
			// 		$scope.total_count = response.totalRecords;
			// 	});
			// }
			$scope.locationData = [];
			$scope.initialPincode = "";
			if ($state.current.url === "/coordinator/pincode") {
				Utility.populate_location({}, $scope);

			}
			// retrive all the respective coordinator pincodes list
			if ($state.current.url === "/pincode/update/:id/:locId") {
				var userLocId = $state.current.data.params.locId;
				$scope.pageActionHeader = "Update Pincode Details";
				$scope.user.email = ($state.current.data.params.id);
				console.log("$scope.user.email :" + $state.current.data.params.id);
				$scope.getAllPincodes($scope.user.email, 1, 10);
				UserService.getSingleLocationPincode({ query: { 'id': userLocId } }).success(function (response) {
					$scope.locationData = response.data;
					$scope.total_count = response.totalRecords;
					var existingPincode = $scope.locationData[0].pincode;
					$scope.initialPincode = existingPincode.substring(0, 2);
				});
			}

			//PrePopulate Of Pincodes For Particular Coordinator
			var pincodeListDB = [];
			$scope.allPincodeList = function () {
				pincodeListDB = [];
				var email = ($state.current.data.params.id)
				UserService.getPincode({ query: { 'coordinatorEmailId': email }, firstResult: 0, maxResults: 100 }).success(function (response) {
					$scope.response = response.data;
					$scope.total_count = response.totalRecords;
					for (var i = 0; i < $scope.total_count; i++) {
						pincodeListDB.push($scope.response[i].pincode);
					}
				});
			}
			if ($state.current.url === "/pincode/update/:id/:locId") {
				$scope.allPincodeList();
			}

			if ($state.current.url == "/user/create" ||
				$state.current.url == "/user/get/:id") {
				$scope.breadcrumbs.push({ title: $state.current.data.page, link: $state.current.url });
				Utility.populate_roles({}, $scope);
				Utility.populate_location({}, $scope);
				Utility.populate_retailer({}, $scope);
			}

			//Save User Details
			$scope.save = function (user) {

				UserService.save(user).success(function (response) {
					console.log(response);
					$state.go('rl.users');
				});
			}

			//Delete Existing City
			$scope.deleteUser = function (user, index) {
				ngDialog.openConfirm({
					template: '<div class="dialog-contents">' +
					'Do you want to delete this user?<br/><br/>' +
					'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
					'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
					'</div>',
					controller: ['$scope', function ($scope) {
						// Controller logic here
					}]
				}).then(function (success) {
					// Success logic here
					console.log(user, index);
					UserService.del(user).success(function (response) {
						if (response) {
							$state.go('rl.user');
						}
					});
				}, function (error) {
					// Error logic here
				});

			}

			//Validate User Details
			var hasErrors = function (user) {
				console.log(user);
				if (isEmpty(user.firstName, 'firstName')) {
					$scope.errors.firstName = "Enter first name";
				}

				//if (isEmpty(user.lastName,'lastName')) {
				//    $scope.errors.lastName = "Enter last name";
				//}

				if (isEmpty(user.email, 'email')) {
					$scope.errors.email = "Enter email";
				}
				else if (!validEmail(user.email)) {
					$scope.errors.email = "Enter valid email";
				}

				if (isEmpty(user.password, 'password')) {
					$scope.errors.password = "Enter password";
				}
				else if (!isValidPassword(user.password)) {
					$scope.errors.invalidPassword = "Password should be between 6-20 characters long and should contain at-least one numeric digit, one uppercase and one lowercase letter!";
				}

				if (isEmpty(user.confirmPassword, 'confirmPassword')) {
					$scope.errors.confirmPassword = "Re-type password";
				}
				else if (user.password !== user.confirmPassword) {
					$scope.errors.confirmPassword = "Password and Confirm password should be same";
				}

				if (isEmpty(user.phone, 'phone')) {
					$scope.errors.phone = "Enter phone number";
				}
				else if (user.phone.length < 10) {
					$scope.errors.phone = "Phone number should have 10 digits!";
				}

				if (isEmpty(user.roleCode, 'roleCode')) {
					$scope.errors.roleCode = "Select Role";
				}

				if (isEmpty(user.locationId, 'locationId')) {
					$scope.errors.locationId = "Select location";
				}


				if (Object.getOwnPropertyNames($scope.errors).length > 0) {
					return true;
				}
				return false;
			}

			$scope.searchUsers = function (user, first, limit) {
				// $scope.users = [];
				console.log("inside Search Users", user);
				$scope.currentPage = first // initialize page no to 1

				first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
				if (user === undefined) {
					console.log("inside if");
					return;
				}
				if (isEmpty(user.firstName, 'firstName') &&
					isEmpty(user.roleCode, 'role') &&
					isEmpty(user.email, 'email') &&
					isEmpty(user.status, 'status')) {
					var filter = {};
					if (loginInfo.roleCode !== 'superadmin')
						filter.roleCodeNotEquals = 'superadmin';
					first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
					UserService.list({ 'query': filter, firstResult: first, maxResults: limit }).success(function (response) {
						//$scope.users = response;
						if (response !== undefined && response.data !== undefined && response.data.length > 0) {
							$scope.users = response.data;
							$scope.total_count = response.totalRecords;
						}
					});
				}
				//save For Pincodes Detail

				var filter = [];
				console.log("users", user, 'consumerName');
				//Search Users
				UserService.list({ 'query': user, firstResult: first, maxResults: limit }).success(function (response) {
					//$scope.users = response;
					if (response !== undefined && response.data !== undefined && response.data.length > 0) {
						$scope.users = response.data;
						$scope.total_count = response.totalRecords;
					}
				});
			}

			$scope.searchCOPincode = function (coordinator, first, limit) {
				// $scope.users = [];
				console.log("inside Search coordinators", coordinator);
				$scope.currentPage = first // initialize page no to 1

				first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
				if (coordinator === undefined) {
					console.log("inside if");
					return;
				}
				if (isEmpty(coordinator.pincode, 'pincode') &&
					isEmpty(coordinator.locationId, 'locationId') &&
					isEmpty(coordinator.coordinatorEmailId, 'coordinatorEmailId')) { return; }
				first = $rootScope.calculateFirstRecord($scope.currentPage, limit);
				UserService.coordinator_list({ 'query': coordinator, firstResult: first, maxResults: limit }).success(function (response) {
					//$scope.users = response;
					if (response !== undefined && response.data !== undefined && response.data.length > 0) {
						$scope.coordinators = response.data;
						console.log("$scope.coordinators : " + $scope.coordinators);
						$scope.total_count = response.totalRecords;
					}
				});
			}

			$scope.clearSearchFields = function () {
				$scope.user = {};
				$scope.users = [];
			}

			$scope.clearCoordinatorFields = function () {
				$scope.coordinator = {};
				$scope.coordinators = [];
			}

			$scope.savePincodes = function (pincode) {
				console.log("initialPincode initialPincode : " + $scope.initialPincode);
				var userEmailId = $state.current.data.params.id;
				var userLocId = $state.current.data.params.locId;
				var pinList = pincode.list;
				var isExist = "false";
				var initialPincode = "";
				if (pinList.indexOf(",") !== -1) {
					var findComma = pinList.indexOf(",");
					var pincodeList = [];
					var coordinatorServicablePincodesList = [];
					if (findComma !== -1) {
						var pincodes = pinList.split(",");
						for (var i in pincodes) {
							if (pincodes[i] != "" && pincodes[i].length !== 6) {
								$scope.errors.pincode = "Data contains invalid pincodes!. Pincode should be a number & 6 characters long.";

								setTimeout(function () {
									$scope.errors.pincode = "";
								}, 4000);
								return;
							}
							if (isNaN(pincodes[i])) {
								$scope.errors.pincode = "Data contains invalid pincodes!. Pincode should be a number & 6 characters long.";

								setTimeout(function () {
									$scope.errors.pincode = "";
								}, 4000);
								return;
							}
						}
						console.log("initialPincode : " + $scope.initialPincode);
						for (i in pincodes) {
							if (pincodeListDB.indexOf(pincodes[i]) === -1) {
								if (pincodes[i].substring(0, 2) === $scope.initialPincode) {
									pincodeList.push(pincodes[i]);
									isExist = true;
								}
							}
						}
						for (var i in pincodeList) {
							console.log("pincodeList" + pincodeList[i]);
							var item = {
								"locationId": userLocId,
								"coordinatorEmailId": userEmailId,
								"pincode": pincodeList[i]
							}
							coordinatorServicablePincodesList.push(item);
						}
						// $timeout(callAtTimeout, 1000);
						console.log("$scope.isExist : +" + typeof coordinatorServicablePincodesList[0] !== 'undefined' && coordinatorServicablePincodesList[0] !== null);
						if (typeof coordinatorServicablePincodesList[0] !== 'undefined' && coordinatorServicablePincodesList[0] !== null) {
							UserService.savePincodes(coordinatorServicablePincodesList).success(function (response) {
								if (response !== undefined) {
									var firstResult = 0;
									var maxResults = 10;
									UserService.getPincode({ query: { 'coordinatorEmailId': userEmailId }, firstResult: firstResult, maxResults: maxResults }).success(function (response) {
										$scope.pincodes = response.data;
										$scope.total_count = response.totalRecords;
										$scope.allPincodeList();
									});
								}
							});
						}
						else {
							$scope.errors.pincode = "Record(s) Already Exist.";

							setTimeout(function () {
								$scope.errors.pincode = "";
							}, 4000);
							return;
						}
					}
				}
				else {
					if (pincode.list.length === 6 && (!isNaN(pincode.list)) && ((pincode.list).substring(0, 2) === $scope.initialPincode)) {
						if ((pincodeListDB.indexOf(pinList) === -1)) {
							UserService.saveSinglePincode({ 'coordinatorServicablePincodes': { "locationId": userLocId, "coordinatorEmailId": userEmailId, "pincode": pinList } }).success(function (response) {
								if (response !== undefined) {
									var firstResult = 0;
									var maxResults = 10;
									UserService.getPincode({ query: { 'coordinatorEmailId': userEmailId }, firstResult: firstResult, maxResults: 100 }).success(function (response) {
										$scope.pincodes = response.data;
										$scope.total_count = response.totalRecords;
										$scope.allPincodeList();
									});
								}
							});
						} else {
							$scope.errors.pincode = "Record is Already Exist.";

							setTimeout(function () {
								$scope.errors.pincode = "";
							}, 4000);
						}
					}
					else {
						$scope.errors.pincode = "Data contains invalid pincodes!. Pincode should be a number & 6 characters long & belong to your location only.";

						setTimeout(function () {
							$scope.errors.pincode = "";
						}, 4000);

					}
				}
			}

			// function callAtTimeout (){
			// 	console.log("Indide callAtTimeout ");
			// }

			$scope.deletecoordinatorPincode = function (index, id) {
				// $scope.user.email = $state.current.data.params.id;
				ngDialog.openConfirm({
					template: '<div class="dialog-contents">' +
					'Do you want to delete this pincode?<br/><br/>' +
					'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
					'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
					'</div>',
					controller: ['$scope', function ($scope) {
						// Controller logic here
					}]
				}).then(function (success) {
					// Success logic here
					console.log(id, index);
					UserService.delPincode(id).success(function (response) {
						if (response) {
							$scope.user.email = $state.current.data.params.id;
							// $state.go('rl.user-pincode-update');
							var firstResult = 0;
							var maxResults = 10;
							UserService.getPincode({ query: { 'coordinatorEmailId': $scope.user.email }, firstResult: firstResult, maxResults: maxResults }).success(function (response) {
								$scope.pincodes = response.data;
								$scope.total_count = response.totalRecords;
								$scope.allPincodeList();
							});
						}
					});
				}, function (error) {
					// Error logic here
				});
			}

			//Delete the Pincode From Search Results
			$scope.deleteSearchPincode = function (index, coordinator) {
				// $scope.user.email = $state.current.data.params.id;
				var id = coordinator.id;
				ngDialog.openConfirm({
					template: '<div class="dialog-contents">' +
					'Do you want to delete this pincode?<br/><br/>' +
					'<button ng-click="closeThisDialog()" class="btn btn-default">Cancel</button>&nbsp;&nbsp;' +
					'<button ng-click="confirm()" class="btn btn-primary">Confirm</button>' +
					'</div>',
					controller: ['$scope', function ($scope) {
						// Controller logic here
					}]
				}).then(function (success) {
					// Success logic here
					console.log(id, index);
					UserService.delPincode(id).success(function (response) {
						console.log("before response");
						if (response) {
							$scope.coordinators.splice(index, 1);
						}
					});
				}, function (error) {
					// Error logic here
				});
			}

			$scope.clearPincodeFields = function () {
				$scope.pincode = "";
			}
			$scope.disableEmail = $scope.user.id != undefined && $scope.user.id != null && $scope.user.id != ''
		}]);