'use strict'

app.factory('ProcessService', function($http, REST_API_URL, AuthTokenFactory, API, Upload) {

	return {
        list: function(params) {
			return $http.post(REST_API_URL + API.PROCESS.LIST, params);
		},
        firstgroup: function(deploymentId, fieldValues) {			
			var firstgroupUrl = API.PROCESS.FIRSTGROUP.replace('<deploymentId>', deploymentId);
			return $http.post(REST_API_URL + firstgroupUrl, fieldValues);
		},
		nextgroup: function(deploymentId, fillSessionId, fieldValues) {			
			var nextgroupUrl = API.PROCESS.NEXTGROUP.replace('<deploymentId>', deploymentId).replace('<fillSessionId>',fillSessionId);
			return $http.post(REST_API_URL + nextgroupUrl, fieldValues);
		},
		start_process: function(deploymentId, fillSessionId, fieldValues) {
			var completeUrl = API.PROCESS.START_PROCESS.replace('<deploymentId>', deploymentId).replace('<fillSessionId>',fillSessionId);
			return $http.post(REST_API_URL + completeUrl, fieldValues);
		},
		nexttask: function(processId) {
			var url = API.PROCESS.NEXT_TASK.replace('<processId>', processId);
			return $http.post(REST_API_URL + url, {});
		},
		upload: function(params) {
            console.log(params);
			return Upload.upload({
                url: REST_API_URL + API.PROCESS.UPLOAD_EXCEL,
                data: params.data,
                file: params.file
            })
		}
	}
});
