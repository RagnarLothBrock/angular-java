
app.directive('datepicker', function(){
    return {
      require: '?ngModel',
      restrict: 'A',
      link: function($scope, element, attrs, controller) {
        var updateModel;
        updateModel = function(ev) {
          element.datepicker('hide');
          element.blur();
          return $scope.$apply(function() {
			    return controller.$setViewValue(formatDate(ev.date));
          });
        };
        if (controller != null) {
          controller.$render = function() {
            element.datepicker({format: 'dd-mm-yyyy'}).data().datepicker.date = controller.$viewValue;
            element.datepicker('setValue');
            element.datepicker('update');
            return controller.$viewValue;
          };
        }
        return attrs.$observe('datepicker', function(value) {
          var options;
          options = {format: 'dd-mm-yyyy'};
          if (angular.isObject(value)) {
            options = value;
          }
          if (typeof(value) === "string" && value.length > 0) {
            options = angular.fromJson(value);
          }
          return element.datepicker(options).on('changeDate', updateModel);
        });
      }
    };
  });
app.directive('timestampToDatestring',  function(){
	return {
		restrict: 'A',
		require: '?ngModel',
		link: function(scope, el, attr, ctrl) {
			var d = new Date(parseInt(attr.timestampToDatestring));
            if (attr.format == "dd-MM-yyyy hh:mm") {
                
                // Hours part from the timestamp
                var hours = d.getHours();
                // Minutes part from the timestamp
                var minutes = "0" + d.getMinutes();
                // Seconds part from the timestamp
                var seconds = "0" + d.getSeconds();
                
                // Will display time in 10:30:23 format
                var formattedTime = hours + ':' + minutes.substr(-2);// + ':' + seconds.substr(-2);
                
                var newval = d.getDate() + '-' + (d.getMonth()+1) + '-' + d.getFullYear() + " " + formattedTime;
                ctrl.$setViewValue(newval);
            }
            else {
                var newval = d.getDate() + '-' + (d.getMonth()+1) + '-' + d.getFullYear();
                ctrl.$setViewValue(newval);
            }			
		}
	}
});
		

app.filter('to_timestamp', function() {
	return dateString(dateStr);
});

app.directive("menuToggle", function() {
	return function() {

		$("#menu-toggle").click(function(e) {
			e.preventDefault();
			$("#wrapper").toggleClass("toggled");
		});
		$("#menu-toggle-2").click(function(e) {
			e.preventDefault();
			$("#wrapper").toggleClass("toggled-2");
			$('#menu ul').hide();
		});
	}
});
app.directive('pre', function() {
	 return {
        restrict: 'C',
        priority: 450,
        link: function(scope, el, attr, ctrl)  {
            scope.$watch(
                el[0].innerHTML,
                function(newVal) {
                    var lineBreakIndex = newVal.indexOf('\n');
                    if (lineBreakIndex > -1 && lineBreakIndex !== newVal.length - 1 && newVal.substr(lineBreakIndex + 1, 4) != '</p>') {
                        var newHtml = "<p>${replaceAll(el[0].innerHTML, '\n\n', '\n').split('\n').join('</p><p>')}</p>";
                        el[0].innerHTML = newHtml;
                    }
                }
            )
        }
    };

    function replaceAll(str, find, replace) {
        return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
    }

    function escapeRegExp(str) {
        return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
    }
});

app
    .directive('excelExport',
        function () {
            return {
                restrict: 'A',
                scope: {
                    fileName: "@",
                    data: "&exportData"
                },
                replace: true,
                template: '<div class="col-sm-12 text-right"><md-button class="md-raised md-primary" ng-click = "download()">Excel</md-button></div>',
                link: function (scope, element) {

                    scope.download = function() {

                        function datenum(v, date1904) {
                            if(date1904) v+=1462;
                            var epoch = Date.parse(v);
                            return (epoch - new Date(Date.UTC(1899, 11, 30))) / (24 * 60 * 60 * 1000);
                        };

                        function getSheet(data, opts) {
                            var ws = {};
                            var range = {s: {c:10000000, r:10000000}, e: {c:0, r:0 }};
                            for(var R = 0; R != data.length; ++R) {
                                for(var C = 0; C != data[R].length; ++C) {
                                    if(range.s.r > R) range.s.r = R;
                                    if(range.s.c > C) range.s.c = C;
                                    if(range.e.r < R) range.e.r = R;
                                    if(range.e.c < C) range.e.c = C;
                                    var cell = {v: data[R][C] };
                                    if(cell.v == null) continue;
                                    var cell_ref = XLSX.utils.encode_cell({c:C,r:R});

                                    if(typeof cell.v === 'number') cell.t = 'n';
                                    else if(typeof cell.v === 'boolean') cell.t = 'b';
                                    else if(cell.v instanceof Date) {
                                        cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                                        cell.v = datenum(cell.v);
                                    }
                                    else cell.t = 's';

                                    ws[cell_ref] = cell;
                                }
                            }
                            if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);
                            return ws;
                        };

                        function Workbook() {
                            if(!(this instanceof Workbook)) return new Workbook();
                            this.SheetNames = [];
                            this.Sheets = {};
                        }

                        var wb = new Workbook(), ws = getSheet(scope.data());
                        /* add worksheet to workbook */
                        wb.SheetNames.push(scope.fileName);
                        wb.Sheets[scope.fileName] = ws;
                        var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:true, type: 'binary'});

                        function s2ab(s) {
                            var buf = new ArrayBuffer(s.length);
                            var view = new Uint8Array(buf);
                            for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
                            return buf;
                        }

                        saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), scope.fileName+'.xlsx');

                    };

                }
            };
        }
    );



app.directive('ngConfirmClick', [
    function(){
        return {
            link: function (scope, element, attr) {
                var msg = attr.ngConfirmClick || "Are you sure?";
                var clickAction = attr.confirmedClick;
                element.bind('click',function (event) {
                    if ( window.confirm(msg) ) {
                        scope.$eval(clickAction)
                    }
                });
            }
        };
}])

function dateString(dateStr) {
	// Split timestamp into [ Y, M, D, h, m, s ]
	var t = dateStr.split(/[- :]/);

	// Apply each element to the Date function
	var d = new Date(t[2], t[1] - 1, t[0]);

	return d.getTime();
}

app.directive('loadTemplate', function($compile){
	return {
        templateUrl: function(scope, element, attrs){
            //console.log(scope,attrs,element);
			return host_base_url + "/partials/tpl/"+element.url+".tpl.html";
		}
	}
});

app.directive("loadTemplateVar", ["$compile", '$http', '$templateCache', '$parse',
                                  function ($compile, $http, $templateCache, $parse) {
    return {
        scope: {
            template: '@template',
            ngModel: '@retailer'
        },
        link: function (scope, element, attrs) {
            
            var template = scope.template;            
            scope.$watch('dir',function(){
                var dir = attrs.dir;
                //console.log(dir,scope.template);
                var templateUrl = host_base_url + "/partials/tpl/"+dir+".tpl.html";
                $http.get(templateUrl, { cache: $templateCache }).success(function (tplContent) {
                    element.replaceWith($compile(tplContent)(scope));
                });
            });
            
        }
    };
}]);
//app.directive('loadTemplateVar', function($compile){
//	return {
//        scope:{
//            url : '='
//        },
////        templateUrl: function(scope, element, attrs){
////            console.log(scope,attrs,element);
////			return host_base_url + "/partials/tpl/"+element.url+".tpl.html";
////		},
//        link: function(scope, element, attrs){
//            console.log(scope,attrs,element);
//			return host_base_url + "/partials/tpl/"+element.url+".tpl.html";
//		},
//	}
//});

function formatDate( d) {
    var dd = d.getDate();
	var mm = d.getMonth()+1; //January is 0!
	
	var yyyy = d.getFullYear();
	if(dd<10){
		dd='0'+dd
	} 
	if(mm<10){
		mm='0'+mm
	} 
	var date = dd+'-'+mm+'-'+yyyy;
	
	return date;
}

function customformatDate(d) {
    var dd = d.getDate();
    var mm = d.getMonth()+1; //January is 0!

    var yyyy = d.getFullYear();
    if(dd<10){
        dd='0'+dd
    }
    if(mm<10){
        mm='0'+mm
    }
    var date = dd+'/'+mm+'/'+yyyy;

    return date;
}
function yyyyMMdd(d) {
    var dd = d.getDate();
    var mm = d.getMonth()+1; //January is 0!

    var yyyy = d.getFullYear();
    if(dd<10){
        dd='0'+dd
    }
    if(mm<10){
        mm='0'+mm
    }
    var date = yyyy+'/'+mm+'/'+dd;
    return date;
}

function highlightTab(tabId) {
    $('#menu li').removeClass('active');
	$('#'+tabId).addClass('active');
}

var clickIndex = 0;
app.directive('sideMenu', ['$rootScope', '$timeout',function($rootScope, $timeout) {
    return {
        restrict: 'CA',
        link:function(scope, el, attr) {
            
            //$timeout(function(){
                $('ul.dropdown-menu [data-toggle=dropdown]').bind('click', function(event) {
                    
                    console.log("Menu Click" + clickIndex++);
                    event.preventDefault(); 
                    event.stopPropagation(); 
                    $(this).parent().siblings().removeClass('open');
                    $(this).parent().toggleClass('open');
                    
                    // Re-add .open to parent sub-menu item
                    $(this).parent().addClass('open');
                    $(this).parent().find("ul").parent().find("li.dropdown").addClass('open');
                    
                });
                
                //$("li.main-menu [data-toggle=dropdown]").click(function(){
                //    $("li.main-menu").removeClass("active");
                //    $(this).addClass("active");
                //});
            //});
        }
    }
}]);

app.directive('alphaNumericOnly', function() {
    return {
        restrict: 'C',
        require: '?ngModel',
        link: function(scope, el, attr, ctrl) {
            ctrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) return '';
                var transformedInput = inputValue.replace(/[^0-9a-zA-Z\s+]/g, '');
                if (transformedInput !== inputValue) {
                    ctrl.$setViewValue(transformedInput);
                    ctrl.$render();
                }
                return transformedInput;
            });
        }
    }
});

app.directive('alphaOnly', function() {
    return {
        restrict: 'C',
        require: '?ngModel',
        link: function(scope, el, attr, ctrl) {
            ctrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) return '';
                var transformedInput = inputValue.replace(/[^a-zA-Z]/g, '');
                if (transformedInput !== inputValue) {
                    ctrl.$setViewValue(transformedInput);
                    ctrl.$render();
                }
                return transformedInput;
            });
        }
    }
});

app.directive('numberOnly', function() {
    return {
        restrict: 'C',
        require: '?ngModel',
        link: function(scope, el, attr, ctrl) {
            ctrl.$parsers.push(function (inputValue) {
                var transformedInput = inputValue.replace(/[^0-9]/g, '');
                ctrl.$setViewValue(transformedInput);
                ctrl.$render();
                 return transformedInput;
            });
        }
    }
});

app.directive('pincodesWithComma', function() {
    return {
        restrict: 'C',
        require: '?ngModel',
        link: function(scope, el, attr, ctrl) {
            ctrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) return '';
                var transformedInput = inputValue.replace(/[^0-9\,]/g, '');
                
                if (transformedInput !== inputValue) {
                    ctrl.$setViewValue(transformedInput);
                    ctrl.$render();
                }
                return transformedInput;
            });
        }
    }
});

app.directive('decimalOnly', function() {
    return {
        restrict: 'C',
        require: '?ngModel',
        link: function(scope, el, attr, ctrl) {
            ctrl.$parsers.push(function (inputValue) {
                if (inputValue == null) {
                    return null;
                }
                var inp = inputValue.toString();
                var regEx = /[^0-9\.]/g;
                var transformedInput;
                if (regEx.test(inp)) {
                    transformedInput = parseFloat(inp);                    
                }
                else {
                    transformedInput = parseFloat(inp.replace(regEx, ''));
                }
                
                if (transformedInput !== inputValue) {
                    ctrl.$setViewValue(transformedInput);
                    ctrl.$render();
                }
                
                return transformedInput;
            });
        }
    }
});

app.directive('validateLong', function() {
    return {
        restrict: 'C',
        require: '?ngModel',
        link: function(scope, el, attr, ctrl) {
            ctrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) return '';
                var transformedInput = inputValue.replace(/[^0-9]/g, '');
                if (transformedInput !== inputValue) {
                    ctrl.$setViewValue(transformedInput);
                    ctrl.$render();
                }
                return transformedInput;
            });
        }
    }
});

app.directive('validateDouble', function() {
    return {
        restrict: 'C',
        require: '?ngModel',
        link: function(scope, el, attr, ctrl) {
            ctrl.$parsers.push(function (inputValue) {
                if (inputValue == undefined) return '';
                var transformedInput = inputValue.replace(/[^0-9\.]/g, '');
                if (transformedInput !== inputValue) {
                    ctrl.$setViewValue(transformedInput);
                    ctrl.$render();
                }
                return transformedInput;
            });
        }
    }
});

app.directive('validatePassword', function() {
    return {
        restrict: 'CA',
        require: '?ngModel',
        link: function(scope, el, attr, ctrl) {
            $(el).on('keyup', function () {
                var passw = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
                var inputValue = $(this).val();
                var v;
                if (inputValue.match(passw)) {
                    //alert('Correct, try another...')
                    v = true;
                } else {
                    //alert('Wrong...!')
                    v = false;
                }
                
                ctrl.$setValidity('password-strength', v);
            });
        }
    }
});

app.directive('checkAll', function(){
    return {
        require: '?ngModel',
        restrict: 'A',
        scope: {
            selectedIds: '=',
            ngModel: '=',
            objList: '=',
        },
        link: function(scope, el, attr, ngModel) {
            $(el).click(function(val) {
                scope.selectedIds = [];
                if ($(el).is(':checked')) {
                    for(var i in scope.objList) {
                        scope.selectedIds.push(scope.objList[i].id);
                    }
                }
            });
        }
    }
});

app.directive('tooltip', function(){
        return {
            restrict: 'A',
            link: function(scope, element, attrs){
                $(element).hover(function(){
                    // on mouseenter
                    $(element).tooltip('show');
                }, function(){
                    // on mouseleave
                    $(element).tooltip('hide');
                });
            }
        };
    })

app.directive('checkAndAddToList', function(){
    return {
        require: '?ngModel',
        restrict: 'A',
        scope: {
            ngModel: '=',
            selectedIds: '=',
            index: '='
        },
        link: function(scope, el, attr, ngModel) {
            $(el).click(function(val) {
                
                if ($(el).is(':checked')) {
                    scope.selectedIds.push($(el).val());
                }
                else {
                    var index = scope.selectedIds.indexOf($(el).val());
                    if (index !== -1) {
                        scope.selectedIds.splice(index,1);
                    }
                }
            });
        }
    }
});
//app.directive("input", ['$rootScope', '$timeout', function($rootScope, $timeout){
//    return {
//        restrict : 'E',
//        //template : '<span >{{ model }}</span>',
//        scope : {
//            model : '=ngModel',
//            type : '@'
//        },
//        link: function(scope, elm, attr) {
//            $timeout(function(){
//                if($rootScope.access_perm === 'r') {
//                    var itemValue = angular.copy(scope.model);
//                    if( scope.type == "hidden" ) {
//                        return;
//                    }
//                    $(elm).replaceWith('<div class="readonly-label">' + itemValue + '</div>');
//               }
//            }, 800);
//        }
//    }
//}]);
//app.directive('input', inputDirective)
app.directive('select', selectDirective)
app.directive('textarea', inputDirective);


function inputDirective($rootScope) {
    return {
        scope : {
            model : '=ngModel',
            type : '@',
            //options : '=ngOptions'
        },
        link: updateUI
    };
    
    function updateUI(scope, iElement, iAttr) {
        //scope.$watch(function(){return scope.model;}, function(model) {
        //    //console.log(scope.options);
        //    if($rootScope.access_perm === 'r' && model !== undefined) {
        //        if( scope.type == "hidden" ) {
        //            return;
        //        }
        //        model = model == null ? "" : model;
        //        $(iElement).replaceWith('<div class="readonly-label">' + model + '</div>');
        //    }
        //}, true);
    }
}
function selectDirective($rootScope) {
    return {
        scope : {
            model : '=ngModel'
        },
        link: updateSelectUI
    };
    
    function updateSelectUI(scope, iElement, iAttr) {
        $(iElement).addClass('read-only');
        
        if($rootScope.access_perm === 'r')
            $(iElement).attr("disabled", "disabled");
        //console.log(iAttr.selectedText);
        //var text123 = iAttr.selectedText;
        //console.log(text123);
        //scope.$watch(function(){return iAttr.selectedText;}, function(text) {
        //    //var html = '<div class="readonly-label">' + angular.copy(text) + '</div>';
        //    //
        //    //if($rootScope.access_perm === 'r' && text !== undefined && text !== null) {                
        //    //    $(iElement).after(html).hide();
        //    //}
        //}, true);
    }
}
inputDirective.$inject = ['$rootScope'];
selectDirective.$inject = ['$rootScope'];

app.directive('checkAccessPerm', ['LoginService', function(LoginService) {
    return {
        restrict: 'AC',
        scope: {
            access_perm: '=checkAccessPerm',
            url: '=url'
        },
        link: function(scope, el, attr, ngModel) {
            if (scope.url == null || scope.url == undefined || scope.url == "") {
                if (scope.access_perm == 'n' || scope.access_perm == 'r') {
                    $(el).remove();
                }
            }
            else {
                 checkAccess(scope.url);
            }
            
            scope.$watch('url',function(oldValue,newValue){
                    checkAccess(newValue);
            });
            
            function checkAccess(url) {
                if (url != null && url != undefined && url != "") {
                    LoginService
                        .has_access(url)
                        .success(function(response){
                            if (response == 'n') {
                                $(el).remove();
                            }
                        });
                }
            }
        }
    }
}]);

app.filter('encodeURIComponent', function() {
    return function(val){
        return window.encodeURIComponent(val.replace(/\//g, "--$$--"));
    };
});

app.filter('decodeURIComponent', function() {
    return function(val){
        return window.decodeURIComponent(val.replace(/--$$--//g, "/"));
    };
});

app.directive('passwordMatchCheck', [function () {
    return {
      require: 'ngModel',
      link: function (scope, elem, attrs, ctrl) {
        var firstPassword = '#' + attrs.passwordMatchCheck;
        
        elem.add(firstPassword).on('keyup', function () {
          scope.$apply(function () {
            var v = elem.val()===$(firstPassword).val();
            ctrl.$setValidity('passwordmatch', v);
          });
        });
      }
    }
  }]);

app.filter('trim', function () {
    return function(value) {
        if(!angular.isString(value)) {
            return value;
        }  
        return value.replace(/^\s+|\s+$/g, ''); // you could use .trim, but it's not going to work in IE<9
    };
});
app.directive('capitalize', function() {
    return {
      require: 'ngModel',
      link: function(scope, element, attrs, modelCtrl) {
        var capitalize = function(inputValue) {
          if (inputValue == undefined) inputValue = '';
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }
          return capitalized;
        }
        modelCtrl.$parsers.push(capitalize);
        capitalize(scope[attrs.ngModel]); // capitalize initial value
      }
    };
  });
app.filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
});

app.filter('decodeHtmlEntity', function() {
    return function(str) {
        return str.replace(/&#(\d+);/g, function(match, dec) {
            return String.fromCharCode(dec);
        });
    }
});

app.filter('encodeHtmlEntity', function() {
    return function(str) {
        var buf = [];
        for (var i=str.length-1;i>=0;i--) {
            buf.unshift(['&#', str[i].charCodeAt(), ';'].join(''));
        }
        return buf.join('');
    }
});

function decodeHtmlEntity(str) {
    return str.replace(/&#(\d+);/g, function(match, dec) {
        return String.fromCharCode(dec);
    });
}

app.filter('orderObjectBy', function() {
  return function(items, field, reverse) {
    var filtered = [];
    angular.forEach(items, function(item) {
      filtered.push(item);
    });
    filtered.sort(function (a, b) {
      return (a[field] > b[field] ? 1 : -1);
    });
    if(reverse) filtered.reverse();
    return filtered;
  };
});

app.directive('preventEnterSubmit', function () {
    return function (scope, el, attrs) {
      el.bind('keydown', function (event) {
        if (13 == event.which) {
            event.preventDefault(); // Doesn't work at all
            window.stop(); // Works in all browsers but IE...
            document.execCommand('Stop'); // Works in IE
            return false; // Don't even know why it's here. Does nothing.
        }
      });
    };
});

app.directive('ddTextCollapse', ['$compile', function($compile) {

  return {
      restrict: 'A',
      scope: true,
      link: function(scope, element, attrs) {

          /* start collapsed */
          scope.collapsed = false;

          /* create the function to toggle the collapse */
          scope.toggle = function() {
              scope.collapsed = !scope.collapsed;
          };

          /* wait for changes on the text */
          attrs.$observe('ddTextCollapseText', function(text) {

              /* get the length from the attributes */
              var maxLength = scope.$eval(attrs.ddTextCollapseMaxLength);

              if (text.length > maxLength) {
                  /* split the text in two parts, the first always showing */
                  var firstPart = String(text).substring(0, maxLength);
                  var secondPart = String(text).substring(maxLength, text.length);

                  /* create some new html elements to hold the separate info */
                  var firstSpan = $compile('<span>' + firstPart + '</span>')(scope);
                  var secondSpan = $compile('<span ng-if="collapsed">' + secondPart + '</span>')(scope);
                  var moreIndicatorSpan = $compile('<span ng-if="!collapsed">... </span>')(scope);
                  var lineBreak = $compile('<br ng-if="collapsed">')(scope);
                  var toggleButton = $compile('<span class="collapse-text-toggle hand read-more" ng-click="toggle()">{{collapsed ? "(less)" : "(more)"}}</span>')(scope);

                  /* remove the current contents of the element
                   and add the new ones we created */
                  element.empty();
                  element.append(firstSpan);
                  element.append(secondSpan);
                  element.append(moreIndicatorSpan);
                  element.append(lineBreak);
                  element.append(toggleButton);
              }
              else {
                  element.empty();
                  element.append(text);
              }
          });
      }
  };
}]);

app.directive('trackProduct', function() {
    return {
        require: '?docketno',
        restrict: 'E',
        //scope: {
        //  docketno: '=docketno'
        //},
        templateUrl:'partials/tpl/common/track-product-link.tpl.html',
        link: function($scope, element, attrs, controller) {
            $scope.trackProduct = JSON.parse(attrs.docketNo);
            console.log(JSON.parse(attrs.docketNo));
        }
       // template: '<div><span>' + docketNo + '</span></div>'
    }
});