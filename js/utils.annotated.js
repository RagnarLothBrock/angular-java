'use strict'

app.factory('Utils', function () {
    return {
        fill_fieldValues: function (Obj) {
            var savedFieldValues = {}, errors = new Array();
            for (var i = 0; i < Obj.properties.length; i++) {
                var prop = Obj.properties[i];

                //Validate Mandatory fileds
                if (!prop.readOnly) {
                    if (isEmpty(prop.value, prop.id)) {
                        if (prop.mandatory)
                            errors[prop.id] = prop.name + " is required!";
                    }
                    else {
                        savedFieldValues[prop.id] = prop.value;
                    }
                }

            }

            //delete(errors["length"]);
            return { 'fieldValues': savedFieldValues, 'errors': errors };
        },
        get_city_name: function (cities, cityId) {
            for (var i = 0; i < cities.length; i++) {
                if (cities[i].id == cityId) {
                    return cities[i].name;
                }
            }

            return "";
        },
        get_selected_name: function (arr, selectedValue, valueFieldName, textFieldName) {
            for (var i = 0; i < arr.length; i++) {
                if (arr[i][valueFieldName] == selectedValue) {
                    return arr[i][textFieldName];
                }
            }

            return "";
        },
        decodeURI: function (val) {
            return val.replace("--$--", "/");
        }
    }
});

app.service('Utility', ['MAX_DROPDOWN_SIZE',
    'CityService', 'LocationService', 'ProductCategory', 'Retailer', 'MenuService', 'LoginService', 'TasksService',
    'RoleMasterService', 'Product', 'ProductSubCategory', 'BrandService', 'PackagingType', 'SMSEmailTrigger', 'PackagingMaterialInventory',
    'CostingActivityService', 'MasterCategory', 'TokenFactory', 'ServiceMaster', 'LspDetailService', 'EwasteProductCategory','UserService',
    function (MAX_DROPDOWN_SIZE, CityService, LocationService, ProductCategory, Retailer, MenuService, LoginService, TasksService,
        RoleMasterService, Product, ProductSubCategory, BrandService, PackagingType, SMSEmailTrigger, PackagingMaterialInventory,
        CostingActivityService, MasterCategory, TokenFactory, ServiceMaster, LspDetailService, EwasteProductCategory, UserService,F) {
        return {
            populate_city: function (query, $scope) {
                $scope.cities = [];
                CityService.list({ query: query, firstResult: 0, maxResults: MAX_DROPDOWN_SIZE }).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0)
                        $scope.cities = response.data;
                    console.log("$scope.cities : ", $scope.cities);
                });
            },
            populate_location: function (query, $scope) {
                $scope.locations = [];
                LocationService.list({ query: query, firstResult: 0, maxResults: MAX_DROPDOWN_SIZE }).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0)
                        $scope.locations = response.data;
                        console.log("$scope.locations : "+$scope.locations);
                });
            },
            populate_master_services: function (query, $scope) {
                $scope.masterServices = [];
                ServiceMaster.list({ query: {}, firstResult: 0, maxResults: MAX_DROPDOWN_SIZE }).success(function (response) {
                    $scope.masterServices = response.data;
                });
            },

            // // retriving the rlProcessStatus from rlProcessStatus Table
            // populate_rlprocess_status: function(query, $scope) {
            //     $scope.processStatus = [];
            //     TasksService.get_rl_process_status({query: {},firstResult:0, maxResults: 1000}).success(function(response) {
            //         $scope.processStatus =  response.data;
            //         console.log("sdfgjkloiygthjkiuyigjklogbjkiguopu"+$scope.processStatus);
            //     });
            // },
            populate_master_category: function (query, $scope) {
                $scope.categories = [];
                MasterCategory.list({ query: query, firstResult: 0, maxResults: MAX_DROPDOWN_SIZE }).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0)
                        $scope.categories = response.data;
                });
            },

            //Populate Coordinator Pincodes
            // populate_coordinator_pincode: function (query, $scope) {
            //     $scope.coordinators = [];
            //     UserService.coordinator_list({ query: query, firstResult: 0, maxResults: MAX_DROPDOWN_SIZE }).success(function (response) {
            //         if (response !== undefined && response.data !== undefined && response.data.length > 0)
            //             $scope.coordinators = response.data;
            //     });
            // },

            //Populating Imei Inventory
            populate_imei_inventory: function (query, $scope) {
                $scope.imeiData = [];
                $scope.Warelocations = [];
                PackagingMaterialInventory.imei_list({ query: query, firstResult: 0, maxResults: MAX_DROPDOWN_SIZE }).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0)
                        $scope.imeiData = response.data;
                        $scope.total_count = response.totalRecords;
                    var input = [];
                    for (var i = 0; i < $scope.imeiData.length; i++) {
                        input.push($scope.imeiData[i].location);
                    }
                    var b = input.reduce(function (w, x, y, input) {
                        if (w.indexOf(x) == -1) w.push(x);
                        return w;
                    }, [])
                    console.log("$scope.imeiData : " + b);
                    $scope.location = b;
                });
            },
            populate_ewaste_category: function (query, $scope) {
                console.log("inside Utility");
                $scope.categories = [];
                EwasteProductCategory.list({ 'query': {}, firstResult: 0, maxResults: MAX_DROPDOWN_SIZE }).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0)
                        console.log("responseew", response);
                    $scope.categories = response.data;
                });
            },
            populate_category: function (query, $scope) {
                $scope.categories = [];
                ProductCategory.list({ query: query, firstResult: 0, maxResults: MAX_DROPDOWN_SIZE }).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0)
                        $scope.categories = response.data;
                    console.log("$scope.categories : ", $scope.categories);
                });
            },
            populate_sub_category: function (query, $scope) {
                $scope.subCategories = [];
                ProductSubCategory.ddl({ query: query, firstResult: 0, maxResults: MAX_DROPDOWN_SIZE }).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0)
                        $scope.subCategories = response.data;
                });
            },
            populate_retailer: function (query, $scope) {
                $scope.retailers = [];
                Retailer.list({ query: query, firstResult: 0, maxResults: MAX_DROPDOWN_SIZE }).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0)
                        $scope.retailers = response.data;
                    console.log("$scope.retailers : ", $scope.retailers);
                });
            },
            populate_menu: function (query, $scope) {
                $scope.menus = [];
                MenuService.list({ query: query, firstResult: 0, maxResults: MAX_DROPDOWN_SIZE }).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0)
                        $scope.menus = response.data;
                });
            },
            populate_menubar: function ($rootScope) {

            },
            has_access: function (url) {
                LoginService
                    .has_access(url)
                    .success(function (response) {
                        ///if (response !== undefined && response.data !== undefined && response.data.length > 0)
                        $scope.menubar = response;
                    });
            },
            populate_roles: function (query, $scope) {
                $scope.roles = [];
                var loginInfo = JSON.parse(TokenFactory.getToken('loginInfo'));//,JSON.stringify( $scope.loginInfo ));
                //    RoleMasterService.get({query:query}).success(function(response){
                RoleMasterService.list({ query: query, firstResult: 0, maxResults: MAX_DROPDOWN_SIZE }).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0)

                        if (loginInfo.roleCode !== 'superadmin') {
                            for (var i = 0; i < response.data.length; i++) {
                                if (response.data[i].code == 'superadmin') {
                                    response.data.splice(i, 1);
                                }
                            }
                        }
                    $scope.roles = response.data;
                });
            },
            populate_products: function (query, $scope) {
                $scope.products = [];
                Product.list({ 'query': query, firstResult: 0, maxResults: MAX_DROPDOWN_SIZE }).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0)
                        $scope.products = response.data;
                });
            },
            populate_brands: function (query, $scope) {
                $scope.brands = [];
                BrandService.list({ 'query': {}, firstResult: 0, maxResults: MAX_DROPDOWN_SIZE }).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0)
                        $scope.brands = response.data;
                });
            },
            populate_lsp_detail: function (query, $scope) {
                $scope.lspDetails = [];
                LspDetailService.list({ 'query': {}, firstResult: 0, maxResults: MAX_DROPDOWN_SIZE }).success(function (response) {
                    console.log("response.data before if", response.data);
                    if (response !== undefined && response !== undefined && response.data.length > 0)
                        $scope.lspDetails = response.data;
                    console.log("response after populated().data", $scope.lspDetails);
                });
            },
            populate_packging_material_type: function (query, $scope) {
                $scope.packagingMaterialTypes = [];
                PackagingType.list({ 'query': {}, firstResult: 0, maxResults: MAX_DROPDOWN_SIZE }).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0) {
                        $scope.packagingMaterialTypes = response.data;
                    }
                });
            },
            populate_sms_email_trigger_code: function (query, $scope) {
                $scope.smsEmailTriggerCodeList = [];
                SMSEmailTrigger.list({ 'query': query, firstResult: 0, maxResults: MAX_DROPDOWN_SIZE }).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0) {
                        $scope.smsEmailTriggerCodeList = response.data;
                    }
                });
            },
            populate_costing_activity: function (query, $scope) {
                $scope.costingActivities = [];
                CostingActivityService.list({ 'query': {}, firstResult: 0, maxResults: MAX_DROPDOWN_SIZE }).success(function (response) {
                    if (response !== undefined && response.data !== undefined && response.data.length > 0)
                        $scope.costingActivities = response.data;
                });
            }

            
        }
    }]);