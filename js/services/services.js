'use strict'

app.service('logger', function() {
	return {
		log: function() {			
			console.log(arguments);
		}
	}
});

app.service('openDeleteDialogAndConfirm', function() {
	return {
	}
});

app.service('notification', ['ngNotify',function(ngNotify){
	ngNotify.addTheme('newTheme', 'my-new-theme');
	ngNotify.config({
		theme: 'newTheme',
		position: 'top',
		duration: 1000,
		type: 'info',
		sticky: false,
		button: true,
		html: false
	});
	return {
		error: function(message) {
			ngNotify.set(message, 'error');
		},
		success: function(message) {
			ngNotify.set(message, 'success');
		},
		warning: function(message) {
			ngNotify.set(message, 'warning');
		},
		info: function(message) {
			ngNotify.set(message, 'info');
		},
		show: function(message) {
			ngNotify.set(message);
		},
		taskMessage: function(message) {
			ngNotify.set(message, {type: 'success', duration: 2000});
		},
		taskErrorMessage: function(message) {
			ngNotify.set(message, {type: 'error', duration: 2000});
		}
	}
}]);

var loaderIndex = 0;
//Spinner object

app.factory('AuthInterceptor', ['AuthTokenFactory', '$rootScope', '$cookies', '$q', '$location', '$window',
	function AuthInterceptor(AuthTokenFactory, $rootScope, $cookies, $q, $location, $window) {



		function handleHttpRequest(config) {
			try {
				$window.scrollTo(0, 0);
				//console.log("Showing loader!!!" + loaderIndex, new Date());
				$('#mydiv').show();
				loaderIndex++;
			}
			catch(e) {
				console.log(e);
			}
			return config;
		}
		
		function handleRequestError(args) {
            //code
        }
		
		function success(response) {
			//console.log("Hiding loader!!!" + loaderIndex, new Date());
			loaderIndex--;
			if (loaderIndex == 0) {
                $('#mydiv').hide();
            }
            
			
			if (response !== undefined && response.data.response !== undefined && response.data.response !== null) {
                $rootScope.$broadcast ('showSuccessMessage',response.data);
			}
			
		   return response;
        }
		
		function error(error) {
            console.log("ERROR", error);
			
			var e = {};
			
			//console.log("Hiding loader!!!" + loaderIndex, new Date());
			loaderIndex--;
			if (loaderIndex == 0) {
                $('#mydiv').hide();
                //loading_screen.hide();
            }
			
			if (error === undefined || error.data.error === undefined || error.data.response === null) {
                $rootScope.$broadcast ('showErrorMessage',error.data);
			}
			 
			if (error !== undefined && error.data !== undefined && error.data.message !== undefined) {
                $rootScope.$broadcast ('showErrorMessage',error.data);
            }
			
			if (error.status == 401 && $rootScope.stateName != "login") {
				$location.path('/login');
			}
			
			if (error.status == 404 && AuthTokenFactory.isLoggedIn()) {
				
				if ($rootScope.stateName == "rl.task-view-assigned") {
					$location.path('/tasks/assigned');
				}
			}
			return e;
        }
		
		
		return {
			request: handleHttpRequest,
			response: success,
			responseError: error
		};
	}
]);

app.factory('noCacheInterceptor', function() {
	return {
		request: function(config) {
			if (config.method == 'GET') {
				var separator = config.url.indexOf('?') === -1 ? '?' : '&';
				config.url = config.url + separator + 'noCache=' + new Date().getTime();
			}
			return config;
		}
	};
});

app.factory('AccessCheck', ['$interval', '$cookies', 'AuthTokenFactory','notification', 'LoginService', '$location', '$rootScope',
	function AccessCheck($interval, $cookies, AuthTokenFactory, notification, LoginService, $location, $rootScope) {
	var intervalId = null;
	//var isLoggedIn = 0;
	return {
		
		sesion_check: function($rootScope) {
			//console.log("session check");
			var token = AuthTokenFactory.getToken();
			if (!AuthTokenFactory.isLoggedIn() && $rootScope.requireLogin) {
				notification.error("Session Expired");
				$rootScope.logout();
				return;
			}
			
			//Check PHP cookie
			//var preval = $cookies.get('PHPSESSID');        
			//if(preval === null || preval === undefined || preval === "") {
			//	notification.error("Session Expired");
			//	$rootScope.logout();
			//	return;
			//}
		},
		start : function($rootScope, $scope) {
			
			if (intervalId != null && intervalId.$$state.status == 0) {				
                return;
            }
			
			var a = this;
			intervalId = $interval(function() {
				a.sesion_check($rootScope);
			}, 5000);
			
		},
		cancel: function() {			
			if (angular.isDefined(intervalId)) {
				$interval.cancel(intervalId);
			}
		},
		logout: function() {
			$rootScope.menubar = null;
			$rootScope.access_perm = null;
			this.cancel();
			AuthTokenFactory.logout();
			LoginService
				.logout()
				.success(function(response){
				});
			$cookies.remove('RLOGISTICS');
			//isLoggedIn = 0;
		},
		has_access: function(toState, $rootScope) {
			var a = this;
			LoginService
				.has_access(toState.url)
				.success(function(response){
					$rootScope.access_perm = toState.data.access_perm = response;
					if (response == 'n' || response == null) {                    
						//a.logout();
						var path = a.checkAccessAndReturnPath();
						notification.error("Access Denied!");
						$location.path(path);
					}
				});
		},
		checkAccessAndReturnPath: function() {
			var a = this;
			try {
				var loginInfo = JSON.parse(localStorage.getItem('loginInfo'));
				
				if (loginInfo === undefined || loginInfo === null ) {
					a.logout();
					return '/';
				}
				
				if (loginInfo.roleCode === 'admin' || loginInfo.roleCode === 'superadmin') {
					return '/tasks/manager/unclaimed/0/10'
				}
				else if (loginInfo.roleCode === 'retailer') {
					return ('/tasks/retailer/0/10');
				}else if (loginInfo.roleCode === 'hub_manager') {
					return ('/tasks/hub_manager/0/10');
				}
				else {
					return ('/tasks/assigned/0/10');
				}
			}
			catch(e) {
				console.log(e);
				a.logout();
				return '/';
			}
		}
		
	}
}]);

app.factory('AuthTokenFactory', ['$window', '$cookies', function AuthTokenFactory($window, $cookies) {
	'use strict';
	var store = $window.localStorage;
	var key = 'rltoken';
	return {
		getToken: getToken,
		setToken: setToken,
		removeToken: removeToken,
		isLoggedIn: isLoggedIn,
		logout : logout
	};

	function getToken() {
		return store.getItem(key);
	}

	function setToken(token) {
		if (token) {
			store.setItem(key, token);
		} else {
			store.removeItem(key);
		}
	}

	function removeToken() {		
		return store.removeItem(key)
	}
	
	function logout() {
        for(var i in store)
			store.removeItem(i);
		
		var cookies = $cookies.getAll();
		for(var i in cookies)
			$cookies.remove(i);
    }

	function isLoggedIn() {
        var token = this.getToken();
		if (token == null || typeof token == undefined || token.trim() == "") {
			return false;
		}
		return true;
	}
}]);

app.factory('TokenFactory', function TokenFactory($window) {
	'use strict';
	var store = $window.localStorage;
	return {
		getToken: getToken,
		setToken: setToken,
		removeToken: removeToken
	};

	function getToken(key) {
		return store.getItem(key);
	}

	function setToken(key,token) {
		if (token) {
			store.setItem(key, token);
		} else {
			store.removeItem(key);
		}
	}

	function removeToken() {
		return store.removeItem(key);
	}
});

app.factory('beforeUnload', function ($rootScope, $window) {
    // Events are broadcast outside the Scope Lifecycle
    
    $window.onbeforeunload = function (e) {
		//e.preventDefault();
        var confirmation = {};
        var event = $rootScope.$broadcast('onBeforeUnload', confirmation);
		
        if (event.defaultPrevented) {
			console.log("Inside");
            return confirmation.message;
        }
		console.log("Outside");
    };
    
    $window.onunload = function () {
        $rootScope.$broadcast('onUnload');
    };
    return {};
})


app.run(['$rootScope', '$state', 'AuthTokenFactory', '$http', 'logger', 'beforeUnload',  'Utility', '$urlRouter', 'LoginService', 'notification', 'AccessCheck',
		 function($rootScope, $state, AuthTokenFactory, $http, logger, beforeUnload,  Utility, $urlRouter, LoginService, notification, AccessCheck) {
		
	var evtStateChangeStart = $rootScope.$on('$stateChangeStart', function(event, toState, toParams,fromState, fromParams, options) {
		
		$rootScope.layout = toState.data.layout;
		//console.log(toParams);

		toState.data.params = toParams;
		
		var requireLogin = toState.data.requireLogin;
		var token = AuthTokenFactory.getToken();

		$rootScope.token = token;
		$rootScope.page = toState.data.page;
		$rootScope.requireLogin = requireLogin;
		$rootScope.stateName = toState.name;
		$rootScope.stateUrl = toState.url;
		$rootScope.redirectTo = $state.current.url;
		
		$rootScope.excelUploadErrors = null;
		
		if ($state.current.data !== undefined) {
            toState.redirectParams = $state.current.data.params;
        }
		if ($state.current.url !== undefined) {
			toState.redirectTo = $state.current.url;
		}
		if ($state.current.name !== undefined) {
			toState.redirectToState = $state.current.name;
		}
		
		console.log("To State:", toState,"To Params:", toParams,"Current:", $state.current,"TOKEN",token);
		//console.log("From State:", toState,"From Params:", toParams,"options:", options);
		
		if (toState.name == 'login' || $state.current.name == "rlna.forgot-password") {
			console.log("Cancel");
            AccessCheck.cancel();
        }
        else {
			console.log("Start");
            AccessCheck.start($rootScope);
        }
		
		//Check Access
		//$rootScope.checkAccess();		
		if (toState.data.requireLogin &&
			(toState.data.checkAccess == undefined || toState.data.checkAccess)) {
            AccessCheck
				.has_access(toState, $rootScope);
        }

		if (requireLogin && (typeof token === 'undefined' || token == null || token == "")) {
			event.preventDefault();
			$rootScope.logout();
			$rootScope.layout = "login";
			$state.go('login');
		}
		else if (toState.name === "login" && token) {
			$rootScope.logout();
		}
		
		console.log("DONE" + toState.name);
	});
	
	$rootScope.$on('$stateChangeError', 
		function(event, toState, toParams, fromState, fromParams, error){
			console.log("State Chage Error", toState, toParams, fromState, fromParams, error);
	});
	
	// somewhere else
	$rootScope.$on('$stateNotFound', 
		function(event, unfoundState, fromState, fromParams){
			console.log("stateNotFound" , fromState,fromParams );
			console.log(unfoundState.to); // "lazy.state"
			console.log(unfoundState.toParams); // {a:1, b:2}
			console.log(unfoundState.options); // {inherit:false} + default options
	});
	
	var evtViewContentLoading = $rootScope.$on('$viewContentLoading', 
		function(event, viewConfig){ 
			// Access to all the view config properties.
			// and one special property 'targetView'
			// viewConfig.targetView
			//console.log(event, viewConfig);
			//console.log( viewConfig,$state.current.name, $rootScope.menubar);
			if ($state.current.name !== 'rlna.forgot-password') {
                if ((viewConfig === "top-header@" ) &&
					($rootScope.menubar === undefined || $rootScope.menubar === null || $rootScope.menubar === "")) {
					console.log("Loading menubar");
					$rootScope.$broadcast ('populateMenubar');
				}
            }
		});
	
	$rootScope.$on('$destroy', function() {
		evtStateChangeStart();
		evtViewContentLoading();
	});
}]);

app.service('RestHTTPService', ['$http', 'REST_API_URL', 'API', function ($http, REST_API_URL, API) {
    return {
        get: function(apiUrl, params, paramFieldName) {
			var getUrl = apiUrl.replace('<'+ paramFieldName +'>', params[paramFieldName]);
			return $http.post(REST_API_URL + getUrl, params);
        },
        del: function(apiUrl, params, paramFieldName) {
			var getUrl = apiUrl.replace('<'+ paramFieldName +'>', params[paramFieldName]);
			return $http.post(REST_API_URL + getUrl, {});
		},
		post: function(apiUrl, params) {
            return $http.post(REST_API_URL + apiUrl, params);
        },
    }
}]);