<?php

session_start();

require_once('./vendor/autoload.php');



$uri = $_GET['uri'];
$postUrl = REST_API . $uri;

//Post Data
$data_string = json_decode(file_get_contents("php://input"),true);
if(isset($_POST)) {
    foreach($_POST as $key => $data) {
        $data_string[$key] = $data;
    }
}
if($uri != 'auth/login')
    $data_string['rltoken'] = UserService::get_token();

if(($uri != 'auth/login' && $uri != 'userservice/logout' && $uri != 'forgot-password') && !UserService::is_active_session()) {
    $response = array('success' => false, 'response' => array());
    UserService::send_response($response, 401);
}

//error_log($uri);
if(strpos($uri, 'userservice') !== false) {
   $action = str_ireplace("userservice/", "", $uri);
   switch($action) {
        case "logout":
            UserService::logout();
            break;
        
        case "has_access":
            UserService::has_access($data_string['menubar']);
            break;
        
        case "menu":
            UserService::populate_menu();
            break;
   }
   
   //error_log($uri."---".$action);
   return; 
}
else if(stripos($uri, "attachment") !== false) {
    $postUrl .= "?rltoken=".UserService::get_token();
    
    $curl = new Curl();
    if(isset($_SESSION['cookie'])) {
        $cookie =  $_SESSION['cookie'];
        if(is_array($cookie))
            $curl->setCookie($cookie[0],$cookie[1]);
    }
    
    $response = $curl->get($postUrl);
    
    if(isset($curl->response_headers['Set-Cookie'])) {
        $cookie = explode('=',$curl->response_headers['Set-Cookie']);
        $_SESSION['cookie'] = $cookie;
    }
    foreach($curl->response_headers as $key => $header) {
        header($key.": ".$header);
    }
    http_response_code($curl->http_status_code);
        
    // Turn on output buffering with the gzhandler
    ob_start('ob_gzhandler');
    print_r($response);
    
    //UserService::send_response($response,$http_code);
    exit;
}
else {
    //error_log("Failed--0".$uri);
}


$response;
if(isset($_FILES) && sizeof($_FILES) > 0) {
    
    $curlFile = new CURLFile($_FILES['file']['tmp_name'],$_FILES['file']['type'],$_FILES['file']['name']);
    
    $data_string = array_merge(array('file' => $curlFile),$data_string);
}
else {
   if($uri != 'auth/login')
      $data_string = UserService::build_post_data($uri,$data_string);
   else
      $data_string = http_build_query($data_string);
}
//error_log("Your Post Data");
//error_log(print_r($postUrl,true));
//error_log(print_r($data_string,true));
$result = UserService::send_request($postUrl, $data_string);
//error_log("Your Response Data");
//error_log(print_r($result,true));
$response = $result['response'];
$http_code = $result['http_code'];

//Get Role Access details
if($uri == 'auth/login' && $http_code == 200 && $response->response != null) {    
    $_SESSION['session_user'] = $response->response;
    $_SESSION['time_out'] = time();
    if(!isset($response->response->roleCode)) {
        throw new ErrorException("Unable to retrieve account details. Please try later.", 500 );
        exit;
    }
    // UserService::get_role_based_access($response->response->roleCode);
       UserService::get_role_based_access($response->response->id);
}

UserService::send_response($response,$http_code);
exit;