<?php
function build_post_data($uri, $data_string) {
    $postData = array();
    $postData['rltoken'] = $data_string['rltoken'];
    $r_data_string = http_build_query($postData);
    $string = urldecode(http_build_query($postData));
    switch($uri) {
        default:
            if(strpos($uri, 'firstgroup') !== false ||
               strpos($uri, 'nextgroup') !== false ||
               strpos($uri, '/command/complete') !== false ||
               strpos($uri, '/command/start-process') !== false)
            {
                if(empty($data_string['fieldValues']))
                    $r_data_string = $string."&fieldValues={}";
                else {
                    $q = $data_string['fieldValues'];
                    $postData['fieldValues'] = json_encode($q);
                    $r_data_string = http_build_query($postData);
                }
            }
            else if(strpos($uri, 'create') !== false ||
                    strpos($uri, 'update') !== false ||
                    strpos($uri, 'get') !== false ||
                    strpos($uri, 'query') !== false ||
                    strpos($uri, 'bulkcreate') !== false ||
                    strpos($uri, 'bulkupdate') !== false ||
                    strpos($uri, 'list') !== false ||
                    strpos($uri, 'bulkdelete') !== false) {

                $parts = explode("/",$uri);
                foreach($data_string as $key => $v) {
                    if(is_array($data_string[$key])) {
                        $parts = $data_string[$key];
                        if(empty($data_string[$key]))
                            $postData[$key] = "{}";
                        else
                            $postData[$key] = json_encode($parts);
                    }
                }
                $r_data_string = http_build_query($postData);
            }
            else if(strpos($uri, 'find-by-variable-values') !== false) {
                $parts = explode("/",$uri);
                foreach($data_string as $key => $v) {
                    if(is_array($data_string[$key])) {
                        $parts = $data_string[$key];
                        if(empty($data_string[$key]))
                            $postData[$key] = "{}";
                        else
                            $postData[$key] = json_encode($parts);
                    }
                }
                error_log(print_r($postData,true));
                $r_data_string = http_build_query($postData);
                //$r_data_string = urldecode($r_data_string);
            }
            else
                $r_data_string = $string;
            break;
        case 'brand/query':
        case 'retailerservices/query' :
        case 'retailercategories/query' :
        case 'retailerserviceablelocations/query':
        case 'servicemaster/query':
        case 'serviceprovider/query':
        case 'serviceproviderlocation/query':
        case 'productcategory/query':
        case 'productsubcategory/query':
        case 'packagingtype/query':
        case 'product/query':
        case 'retailer/query':
        //case 'city/query':
            if(empty($data_string['query']))
                $r_data_string = $string."&query={}";
            else {
                $q = $data_string['query'];
                $postData['query'] = json_encode($q);
                $r_data_string = http_build_query($postData);
            }
            break;
    }
    error_log(print_r($r_data_string,true));
    return $r_data_string;
}