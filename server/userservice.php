<?php
class UserService {
    public static function logout() {
        //global $session_config;
        foreach($_SESSION as $key => $value) {
            unset($_SESSION[$key]);
        }
        @session_destroy();
        //setcookie($session_config['session.name'], "", time() - $session_config['session.cookie_lifetime']);
        self::send_response(array('success' => true), 200);
    }
    
    public static function has_access($featureUrl) {
        if(!isset($_SESSION['roleAccessByFeatureUrl']) || !isset($_SESSION['roleAccessByFeatureUrl'][$featureUrl])){
            self::send_response('n',200);
        }
       
        $access =  $_SESSION['roleAccessByFeatureUrl'][$featureUrl];
        
        self::send_response($access,200);
    }
    
    public static function role_access_by_feature_url($roleAccess) {
        $response = [];
        
        foreach($roleAccess as $k => $v) {
            $response[$v->featureLink] = $v->access;
        }
        error_log(print_r($response,true));
        return $response;
    }
    
    public static function get_role_based_access($roleCode) {
        $role_access_api = str_ireplace("<roleCode>", $roleCode, ROLE_BASED_ACCESS_API);
       
        $role_access_url = REST_API . $role_access_api;
        
        $data = array('query' => json_encode(array('orderBy'=>array())));
        
        $response = UserService::send_request($role_access_url, $data);
        
        $access_response = $response['response'];
        //print_r($access_response);
        $role_access = self::menu_wise_access_list($access_response);
        //print_r($role_access);
        $_SESSION['featureBasedRoleAccess'] = $role_access;
        $_SESSION['roleAccess'] = $access_response;
        $_SESSION['roleAccessByFeatureUrl'] = self::role_access_by_feature_url($access_response);
        //print_r($_SESSION);
    }
    
    public static function send_response($response, $http_code) {
        $r = json_encode($response);
        
        $date = new DateTime(date('Y-m-d H:i:s'));
        $date->add(new DateInterval('P1D'));
        
        header("Content-Length: ". strlen($r));
        header('Content-Type: application/json; charset=utf-8');
        header("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");//Dont cache
        header("Pragma: no-cache");//Dont cache
        header("Expires: " . $date->format('D, d M Y H:i:s'));
        http_response_code($http_code);
        
        // Turn on output buffering with the gzhandler
        ob_start('ob_gzhandler');
        
        // Output as normal
        echo $r;
        exit;
    }
    
    public static function send_request($postUrl, $postData) {
        $curl = new Curl();
        if(isset($_SESSION['cookie'])) {
            $cookie =  $_SESSION['cookie'];
            if(is_array($cookie))
                $curl->setCookie($cookie[0],$cookie[1]);
        }
        
       // error_log(print_r($postData,true));
        $response = $curl->post($postUrl,$postData);
        error_log(print_r($curl,true));
        if(isset($curl->response_headers['Set-Cookie'])) {
            $cookie = explode('=',$curl->response_headers['Set-Cookie']);
            $_SESSION['cookie'] = $cookie;
        }
        
        return array('response' => $response, 'http_code' => $curl->http_status_code);
    }
    
    public static function build_post_data($uri, $data_string) {
        $postData = array();
        
        $postData['rltoken'] = self::get_token();
        
        $r_data_string = http_build_query($postData);
        $string = urldecode(http_build_query($postData));
        switch($uri) {
            default:
                if(strpos($uri, 'firstgroup') !== false ||
                   strpos($uri, 'nextgroup') !== false ||
                   strpos($uri, '/command/complete') !== false ||
                   strpos($uri, '/command/start-process') !== false)
                {
                    if(empty($data_string['fieldValues']))
                        $r_data_string = $string."&fieldValues={}";
                    else {
                        $q = $data_string['fieldValues'];
                        $postData['fieldValues'] = json_encode($q);
                        $r_data_string = http_build_query($postData);
                    }
                }
                //else if(strpos($uri, 'create') !== false ||
                //        strpos($uri, 'add') !== false ||
                //        strpos($uri, 'update') !== false ||
                //        strpos($uri, 'get') !== false ||
                //        strpos($uri, 'query') !== false ||
                //        strpos($uri, 'bulkcreate') !== false ||
                //        strpos($uri, 'bulkupdate') !== false ||
                //        strpos($uri, 'list') !== false ||
                //        strpos($uri, 'bulkdelete') !== false ||
                //        strpos($uri, 'register') !== false ||
                //        strpos($uri, 'find-by-variable-values') !== false ||
                //        strpos($uri, 'save') !== false ||
                //        strpos($uri, 'change-password') !== false ||
                //        strpos($uri, 'reset-password') !== false ||
                //        strpos($uri, 'barcode') !== false ||//packaging-material/barcode/generate
                //        strpos($uri, 'generate-reset-token') !== false)
                else
                {
    
                    $parts = explode("/",$uri);
                    foreach($data_string as $key => $v) {
                        
                        if(is_array($data_string[$key])) {
                            $parts = $data_string[$key];
                            if(empty($data_string[$key]))
                                $postData[$key] = "{}";
                            else
                                $postData[$key] = json_encode($v);
                        }
                        else if(is_object($data_string[$key])) {
                            $parts = $data_string[$key];
                            if(empty($data_string[$key]))
                                $postData[$key] = "{}";
                            else
                                $postData[$key] = json_encode($v);
                        }
                        else
                            $postData[$key] = $v;
                    }
                    error_log(print_r($postData, true));
                    $r_data_string = http_build_query($postData);
                }
                //else
                //    $r_data_string = $string;
                break;
        }
        error_log(print_r($r_data_string,true));
        return $r_data_string;
    }
    
    public static function populate_menu() {
        $roleAccess = isset($_SESSION['featureBasedRoleAccess']) ? $_SESSION['featureBasedRoleAccess'] : [];
        self::send_response($roleAccess, 200);
    }
    
    public static function menu_wise_access_list($roleAccess) {
        $response = [];
        foreach($roleAccess as $k => $v) {
            if($v->access == 'n') continue;
            
            if(!isset($response[$v->menuOrder.'_'.$v->menuCode]) ||
               !is_array($response[$v->menuOrder.'_'.$v->menuCode])) {
                $response[$v->menuOrder.'_'.$v->menuCode] = [];
                $response[$v->menuOrder.'_'.$v->menuCode]['features'] = [];
                $response[$v->menuOrder.'_'.$v->menuCode]['menuId'] = $v->menuId;
                $response[$v->menuOrder.'_'.$v->menuCode]['menuOrder'] = $v->menuOrder;
                $response[$v->menuOrder.'_'.$v->menuCode]['menuName'] = $v->menuName;
                $response[$v->menuOrder.'_'.$v->menuCode]['menuLink'] = $v->menuLink;
                $response[$v->menuOrder.'_'.$v->menuCode]['menuCode'] = $v->menuCode;
                $response[$v->menuOrder.'_'.$v->menuCode]['retailerId'] = $v->retailerId;
                $response[$v->menuOrder.'_'.$v->menuCode]['menuAccess'] = 'n';
                $response[$v->menuOrder.'_'.$v->menuCode]['hasFeatures'] = 0;
            }
            
            if($response[$v->menuOrder.'_'.$v->menuCode]['menuAccess'] != $v->access && $v->access != 'n' && $v->access != null) {
                $response[$v->menuOrder.'_'.$v->menuCode]['menuAccess'] = $v->access;
            }
            
                $f = array(
                        "featureId" => $v->featureId,
                        "featureName" => $v->featureName,
                        "menuId" => $v->menuId,
                        "featureOrder" => $v->featureOrder,
                        "featureCode" => $v->featureCode,
                        "access" => $v->access,
                        "featureLink" => $v->featureLink,
                        "retailerId" => $v->retailerId,
                        "parent" => $v->parent,
                        "level" => $v->level,
                        "hasSubMenu" =>$v->hasSubMenu
                    );
                
                if($v->parent) {
                    if(!isset($response[$v->menuOrder.'_'.$v->menuCode]['features'][$v->parent])) {
                        $response[$v->menuOrder.'_'.$v->menuCode]['features'][$v->parent]['subMenus'] = array();
                    }
                    if(is_array($response[$v->menuOrder.'_'.$v->menuCode]['features'][$v->parent]) && $f['level'] == 2) {
                        $response[$v->menuOrder.'_'.$v->menuCode]['features'][$v->parent]['subMenus'][] = $f;
                    }
                }
                else {
                    if(!isset($response[$v->menuOrder.'_'.$v->menuCode]['features'][$v->featureId])) {
                        $response[$v->menuOrder.'_'.$v->menuCode]['features'][$v->featureId] = $f;
                    }
                    else {
                        if($f['level'] == 1)
                            $response[$v->menuOrder.'_'.$v->menuCode]['features'][$v->featureId] = array_merge($response[$v->menuOrder.'_'.$v->menuCode]['features'][$v->featureId], $f);
                    }
                    
                    if($v->level > 0)
                        $response[$v->menuOrder.'_'.$v->menuCode]['hasFeatures'] = 1;
                }
        }
        
        return $response;
    }
    
    public static function get_sub_features($features, $featureId) {
        
        $subFeatures = array();
        foreach($features as $k => $f) {
            if( $f['featureId'] == $featureId ) {
                $subFeatures[] = $f;
            }
        }
        
        return $subFeatures;
    }
    
    public static function get_token() {
        if(isset($_SESSION) && isset($_SESSION['session_user'])) {
            $session_user = $_SESSION['session_user'];
            
            if(isset($session_user->id) && isset($session_user->token)) {
                return $session_user->token;
            }
        }
        
        return false;
    }
    
    public static  function is_active_session() {
        if(isset($_SESSION) && isset($_SESSION['session_user'])) {
            $session_user = $_SESSION['session_user'];
            
            if(isset($session_user->id) && isset($session_user->token)) {
                return true;
            }
        }
        
        return false;
    }
}